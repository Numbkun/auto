# Инструкция по разворачиванию виртуальной машины проекта

<blockquote>
    Внимание!!! Для работы рецептов Chef необходимо, чтобы для вашего аккаунта на сайте git.cloud-team.ru были доступны на чтение репозитории `cookbooks/cloudteam.git` и `cookbooks/php-app.git`.
    Также необходимо чтобы для вашего аккаунта был установлен ssh-ключ.
</blockquote>

* Установить VirtualBox и Vagrant.
* Установить плагины для Vagrant:

```
    $ vagrant plugin install vagrant-librarian-chef-nochef
    $ vagrant plugin install vagrant-omnibus
```

<blockquote>
    Команды выше выполняются глобально, поэтому их можно выполнить в любом месте файловой системы.
</blockquote>

* Клонировать проект с сервера git:

```
    git clone git@bitbucket.org:Numbkun/auto.git
```

* Создать `VagrantFile` из шаблона `.chef/VagrantFile.dist`:

```
    $ cp .chef/Vagrantfile.dist VagrantFile
```

* При необходимости скорректировать `VagrantFile`.

* Создать `.chef/nodes/auto.json` из шаблона `.chef/nodes/auto.json.dist`:

```
    $ cp .chef/nodes/auto.json.dist .chef/nodes/auto.json
```

* При необходимости скорректировать `.chef/nodes/auto.json`.

* Получить токен на сайте https://github.com/. Для этого необходимо авторизоваться на сайте github.com. Перейти на страницу 
настроек приложений профиля https://github.com/settings/applications. Создать токен.

* В файле .chef/nodes/auto.json заменить `{your_github_token}` на токен, полученный на предыдущем шаге.

* Развернуть виртуальный сервер:

```
    $ vagrant up
```

<blockquote>
    В процессе установки может потребоваться ввести пароль вашего приватного ключа, для доступа к репозиториям `cookbooks/cloudteam.git` и `cookbooks/php-app.git`.
</blockquote>

* Прописать в хостах:

```
    10.2.2.9 auto.local www.auto.local
```

* После разворачивания виртуальной машины, для нормальной работы IDE (автодополнения и прочее), необходимо скопировать с виртуальной машины папку `vendor`.

* Способы входа на сервер по SSH:
    * `vagrant ssh`
    * `ssh super-admin@10.2.2.2 -i "./chef/files/private_keys/id_rsa"`
    
* Параметры подключения к PostreSQL с правами администратора:
    * host: `10.2.2.2`;
    * username: `postgres`;
    * password: `1111`;
    
* Параметры подключения к PostreSQL под пользователем приложения:
    * host: `10.2.2.2`;
    * username: `auto`;
    * password: `1111`;
    * dbname: `auto`;
    