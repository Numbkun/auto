<?php

namespace console\components;

use common\components\rbac\DbManager;
use yii\base\InvalidConfigException;
use Yii;

class Migration extends \yii\db\Migration {

    /**
     * Возвращает менеджер авторизации
     * @throws InvalidConfigException
     * @return DbManager
     */
    protected function getAuthManager()
    {
        $authManager = Yii::$app->getAuthManager();
        if (!$authManager instanceof DbManager) {
            throw new InvalidConfigException(
                'You should configure "authManager" component to use database before executing this migration.'
            );
        }
        return $authManager;
    }

    /**
     * Возвращает опции для таблиц по умолчанию
     * @return null|string
     */
    protected function getDefaultTableOptions() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        return $tableOptions;
    }

    /**
     * Обновление кеша схемы таблицы
     *
     * @param string $table название таблицы
     */
    protected function refreshTableSchema($table) {
        $this->db->getTableSchema($table, true);
    }
}