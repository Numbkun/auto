<?php
/**
 * Файл установки глобальных настроек Dependency Injection
 */

use yii\console\controllers\MigrateController;

Yii::$container->set(MigrateController::className(), ['templateFile' => '@app/views/migrate/migration.php']);