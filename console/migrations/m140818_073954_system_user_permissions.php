<?php

use console\components\Migration;

class m140818_073954_system_user_permissions extends Migration
{
    public function safeUp()
    {
        $auth = $this->getAuthManager();

        $permision = $auth->createPermission('backend.system-user.read');
        $permision->description = 'Пользователи (все): Просмотр';
        $permision->is_system = true;
        $auth->add($permision);
        $permision = $auth->createPermission('backend.system-user.edit');
        $permision->description = 'Пользователи (все): Изменение';
        $permision->is_system = true;
        $auth->add($permision);
        $permision = $auth->createPermission('backend.system-user.set-password');
        $permision->description = 'Пользователи (все): Установка пароля';
        $permision->is_system = true;
        $auth->add($permision);
    }

    public function safeDown()
    {
        $auth = $this->getAuthManager();
        $auth->remove($auth->getPermission('backend.system-user.set-password'));
        $auth->remove($auth->getPermission('backend.system-user.edit'));
        $auth->remove($auth->getPermission('backend.system-user.read'));
    }
}
