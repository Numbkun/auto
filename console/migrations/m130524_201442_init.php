<?php

use common\components\DateTime;
use console\components\Migration;
use yii\db\Schema;

class m130524_201442_init extends Migration
{
    public function safeUp()
    {
        $tableOptions = $this->getDefaultTableOptions();

        // Тема
        $this->createTable(
            '{{%theme}}',
            [
                'id'         => Schema::TYPE_PK,
                'name'       => Schema::TYPE_STRING . '(64) NOT NULL',
                'created_at' => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
                'updated_at' => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
            ],
            $tableOptions
        );
        $this->createIndex('ux_theme_name', '{{%theme}}', 'name', true);

        // Магазин
        $this->createTable(
            '{{%shop}}',
            [
                'id'            => Schema::TYPE_PK,
                'name'          => Schema::TYPE_STRING . ' NOT NULL',
                'is_predefined' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT FALSE',
                'theme_id'      => Schema::TYPE_INTEGER,
                'created_at'    => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
                'updated_at'    => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
            ],
            $tableOptions
        );
        $this->addForeignKey('fk_shop_theme_id', '{{%shop}}', 'theme_id', '{{%theme}}', 'id', 'RESTRICT', 'RESTRICT');

        $this->insert(
            '{{%shop}}',
            [
                'id'            => 1,
                'name'          => 'Leather',
                'is_predefined' => true,
                'created_at'    => new DateTime(),
                'updated_at'    => new DateTime()
            ]
        );
        $this->db->createCommand()->resetSequence('{{%shop}}', 2)->execute();

        // Домен
        $this->createTable(
            '{{%domain}}',
            [
                'id'         => Schema::TYPE_PK,
                'shop_id'    => Schema::TYPE_INTEGER . ' NOT NULL',
                'name'       => Schema::TYPE_STRING . '(253) NOT NULL',
                'created_at' => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
                'updated_at' => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
            ],
            $tableOptions
        );
        $this->createIndex('ux_domain_name', '{{%domain}}', 'name', true);
        $this->addForeignKey('fk_domain_shop_id', '{{%domain}}', 'shop_id', '{{%shop}}', 'id', 'RESTRICT', 'RESTRICT');

        // Настройки
        $this->createTable(
            '{{%setting}}',
            [
                'id'         => Schema::TYPE_PK,
                'shop_id'    => Schema::TYPE_INTEGER . ' NOT NULL',
                'name'       => Schema::TYPE_STRING . '(64) NOT NULL',
                'value'      => Schema::TYPE_STRING . ' NOT NULL',
                'created_at' => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
                'updated_at' => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
            ],
            $tableOptions
        );
        $this->addForeignKey('fk_setting_shop_id', '{{%setting}}', 'shop_id', '{{%shop}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->createIndex('ux_setting_name', '{{%setting}}', 'name', true);

        // Пользователь
        $this->createTable(
            '{{%user}}',
            [
                'id'            => Schema::TYPE_PK,
                'shop_id'       => Schema::TYPE_INTEGER . ' NOT NULL',
                'username'      => Schema::TYPE_STRING . ' NOT NULL',
                'auth_key'      => Schema::TYPE_STRING . '(32) NOT NULL',
                'password_hash' => Schema::TYPE_STRING . ' NOT NULL',
                'email'         => Schema::TYPE_STRING . ' NOT NULL',
                'is_active'     => Schema::TYPE_BOOLEAN . ' NOT NULL',
                'created_at'    => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
                'updated_at'    => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
            ],
            $tableOptions
        );
        $this->addForeignKey('fk_user_shop_id', '{{%user}}', 'shop_id', '{{%shop}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->createIndex('ux_user_shop_id_username', '{{%user}}', ['shop_id', 'username'], true);

        $this->insert(
            '{{%user}}',
            [
                'id'            => 1,
                'shop_id'       => 1,
                'username'      => 'admin',
                'auth_key'      => Yii::$app->getSecurity()->generateRandomString(32),
                'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('admin'),
                'email'         => 'admin@leather.local',
                'is_active'     => true,
                'created_at'    => new DateTime(),
                'updated_at'    => new DateTime(),
            ]
        );
        $this->db->createCommand()->resetSequence('{{%user}}', 2)->execute();

        $this->refreshTableSchema('{{%user}}');
        $this->refreshTableSchema('{{%setting}}');
        $this->refreshTableSchema('{{%domain}}');
        $this->refreshTableSchema('{{%shop}}');
        $this->refreshTableSchema('{{%theme}}');

    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_user_shop_id', '{{%user}}');
        $this->dropTable('{{%user}}');

        $this->dropForeignKey('fk_setting_shop_id', '{{%setting}}');
        $this->dropTable('{{%setting}}');

        $this->dropForeignKey('fk_domain_shop_id', '{{%domain}}');
        $this->dropTable('{{%domain}}');

        $this->dropForeignKey('fk_shop_theme_id', '{{%shop}}');
        $this->dropTable('{{%shop}}');

        $this->dropTable('{{%theme}}');
    }
}
