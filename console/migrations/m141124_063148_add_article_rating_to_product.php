<?php

use console\components\Migration;
use yii\db\Schema;

class m141124_063148_add_article_rating_to_product extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%product}}', 'article', Schema::TYPE_STRING . ' NULL' );
        $this->addColumn('{{%product}}', 'rating', Schema::TYPE_DECIMAL . '(2, 1) NULL' );
        $this->addColumn('{{%product}}', 'rating_count', Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0' );

        $this->refreshTableSchema('{{%product}}');

    }

    public function safeDown()
    {
        $this->dropColumn('{{%product}}','article');
        $this->dropColumn('{{%product}}','rating');
        $this->dropColumn('{{%product}}','rating_count');
    }
}