<?php

use console\components\Migration;
use yii\db\Schema;

class m140506_102106_rbac_init extends Migration
{
    public function safeUp()
    {
        $authManager = $this->getAuthManager();

        $tableOptions = $this->getDefaultTableOptions();

        $this->createTable($authManager->ruleTable, [
            'name' => Schema::TYPE_STRING . '(64) NOT NULL',
            'data' => Schema::TYPE_TEXT,
            'created_at' => Schema::TYPE_DATETIME . ' with time zone',
            'updated_at' => Schema::TYPE_DATETIME . ' with time zone',
            'PRIMARY KEY (name)',
        ], $tableOptions);

        $this->createTable($authManager->itemTable, [
            'id' => Schema::TYPE_PK,
            'shop_id' => Schema::TYPE_INTEGER,
            'name' => Schema::TYPE_STRING . '(64) NOT NULL',
            'type' => Schema::TYPE_INTEGER . ' NOT NULL',
            'description' => Schema::TYPE_TEXT . ' NOT NULL',
            'rule_name' => Schema::TYPE_STRING . '(64)',
            'data' => Schema::TYPE_TEXT,
            'is_system' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT FALSE',
            'created_at' => Schema::TYPE_DATETIME . ' with time zone',
            'updated_at' => Schema::TYPE_DATETIME . ' with time zone',
            'FOREIGN KEY (rule_name) REFERENCES ' . $authManager->ruleTable . ' (name) ON DELETE SET NULL ON UPDATE CASCADE',
        ], $tableOptions);
        $this->createIndex('ux_auth_item_name', $authManager->itemTable, 'name', true);
        $this->createIndex('idx-auth_item-type', $authManager->itemTable, 'type');
        $this->addForeignKey('fk_auth_item_shop_id', $authManager->itemTable, 'shop_id', '{{%shop}}', 'id', 'CASCADE', 'CASCADE');

        $this->createTable($authManager->itemChildTable, [
            'parent' => Schema::TYPE_STRING . '(64) NOT NULL',
            'child' => Schema::TYPE_STRING . '(64) NOT NULL',
            'PRIMARY KEY (parent, child)',
            'FOREIGN KEY (parent) REFERENCES ' . $authManager->itemTable . ' (name) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY (child) REFERENCES ' . $authManager->itemTable . ' (name) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);

        $this->createTable($authManager->assignmentTable, [
            'item_name' => Schema::TYPE_STRING . '(64) NOT NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'created_at' => Schema::TYPE_DATETIME . ' with time zone',
            'PRIMARY KEY (item_name, user_id)',
            'FOREIGN KEY (item_name) REFERENCES ' . $authManager->itemTable . ' (name) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);

        $this->addForeignKey('fk_auth_assigment_user_id', $authManager->assignmentTable, 'user_id', '{{%user}}', 'id', 'RESTRICT', 'RESTRICT');
        $role = $authManager->createRole($authManager->superRoleName);
        $role->shop_id = 1;
        $role->description = $authManager->superRoleDescription;
        $role->is_system = true;
        $authManager->add($role);
        $authManager->assign($role, 1);

        $permision = $authManager->createPermission('backend.role.edit');
        $permision->description = 'Роли: Изменение';
        $permision->is_system = false;
        $authManager->add($permision);
        $permision = $authManager->createPermission('backend.role.read');
        $permision->description = 'Роли: Просмотр';
        $permision->is_system = false;
        $authManager->add($permision);

        $permision = $authManager->createPermission('backend.user.edit');
        $permision->description = 'Пользователи: Изменение';
        $permision->is_system = false;
        $authManager->add($permision);
        $permision = $authManager->createPermission('backend.user.read');
        $permision->description = 'Пользователи: Просмотр';
        $permision->is_system = false;
        $authManager->add($permision);
        $permision = $authManager->createPermission('backend.user.set-password');
        $permision->description = 'Пользователи: Установка пароля';
        $permision->is_system = false;
        $authManager->add($permision);

        $permision = $authManager->createPermission('backend.setting.edit');
        $permision->description = 'Настройки: Изменение';
        $permision->is_system = false;
        $authManager->add($permision);
        $permision = $authManager->createPermission('backend.setting.read');
        $permision->description = 'Настройки: Просмотр';
        $permision->is_system = false;
        $authManager->add($permision);

        $permision = $authManager->createPermission('backend.shop.edit');
        $permision->description = 'Сайты: Изменение';
        $permision->is_system = true;
        $authManager->add($permision);
        $permision = $authManager->createPermission('backend.shop.read');
        $permision->description = 'Сайты: Просмотр';
        $permision->is_system = true;
        $authManager->add($permision);

        $permision = $authManager->createPermission('backend.theme.edit');
        $permision->description = 'Темы: Изменение';
        $permision->is_system = true;
        $authManager->add($permision);
        $permision = $authManager->createPermission('backend.theme.read');
        $permision->description = 'Темы: Просмотр';
        $permision->is_system = true;
        $authManager->add($permision);

        $this->refreshTableSchema($authManager->assignmentTable);
        $this->refreshTableSchema($authManager->itemChildTable);
        $this->refreshTableSchema($authManager->itemTable);
        $this->refreshTableSchema($authManager->ruleTable);
    }

    public function safeDown()
    {
        $authManager = $this->getAuthManager();

        $this->dropForeignKey('fk_auth_assigment_user_id', $authManager->assignmentTable);
        $this->dropForeignKey('fk_auth_item_shop_id', $authManager->itemTable);

        $this->dropTable($authManager->assignmentTable);
        $this->dropTable($authManager->itemChildTable);
        $this->dropTable($authManager->itemTable);
        $this->dropTable($authManager->ruleTable);
    }
}
