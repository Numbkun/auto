<?php

use yii\db\Schema;
use console\components\Migration;

class m140912_070830_create_product_tables extends Migration
{
    public function safeUp()
    {
        $tableOptions = $this->getDefaultTableOptions();

        $this->createTable(
            '{{%product}}',
            [
                'id'              => Schema::TYPE_PK,
                'manufacture_id'  => Schema::TYPE_INTEGER  . ' NOT NULL',
                'product_type_id' => Schema::TYPE_INTEGER  . ' NOT NULL',
                'name'            => Schema::TYPE_STRING   . ' NOT NULL',
                'description'     => Schema::TYPE_TEXT     . ' NOT NULL',
                'created_at'      => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
                'updated_at'      => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
            ],
            $tableOptions
        );
        $this->addForeignKey('fk_product_manufacture_id', '{{%product}}', 'manufacture_id', '{{%manufacture}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_product_product_type_id', '{{%product}}', 'product_type_id', '{{%product_type}}', 'id', 'RESTRICT', 'RESTRICT');

        $this->createTable(
            '{{%shop_product}}',
            [
                'id'          => Schema::TYPE_PK,
                'product_id'  => Schema::TYPE_INTEGER  . ' NOT NULL',
                'category_id' => Schema::TYPE_INTEGER  . ' NOT NULL',
                'shop_id'     => Schema::TYPE_INTEGER  . ' NOT NULL',
                'name'        => Schema::TYPE_STRING   . ' NOT NULL',
                'description' => Schema::TYPE_TEXT     . ' NOT NULL',
                'created_at'  => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
                'updated_at'  => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
            ],
            $tableOptions
        );
        $this->addForeignKey('fk_shop_product_product_id', '{{%shop_product}}', 'product_id', '{{%product}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_shop_product_shop_id', '{{%shop_product}}', 'shop_id', '{{%shop}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_shop_product_category_id', '{{%shop_product}}', 'category_id', '{{%category}}', 'id', 'RESTRICT', 'RESTRICT');


        $this->createTable(
            '{{%shop_product_category}}',
            [
                'shop_product_id' => Schema::TYPE_INTEGER . ' NOT NULL',
                'category_id'     => Schema::TYPE_INTEGER . ' NOT NULL',
                'sort'            => Schema::TYPE_INTEGER . ' NOT NULL',
                'PRIMARY KEY (shop_product_id, category_id)'
            ],
            $tableOptions
        );
        $this->addForeignKey('fk_shop_product_category_shop_product_id', '{{%shop_product_category}}', 'shop_product_id', '{{%shop_product}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_shop_product_category_category_id', '{{%shop_product_category}}', 'category_id', '{{%category}}', 'id', 'RESTRICT', 'RESTRICT');

        $this->refreshTableSchema('{{%product}}');
        $this->refreshTableSchema('{{%shop_product}}');
        $this->refreshTableSchema('{{%shop_product_category}}');

        $auth = $this->getAuthManager();

        $permision = $auth->createPermission('backend.product.read');
        $permision->description = 'Товары: Просмотр';
        $permision->is_system = true;
        $auth->add($permision);
        $permision = $auth->createPermission('backend.product.edit');
        $permision->description = 'Товары: Изменение';
        $permision->is_system = true;
        $auth->add($permision);

        $permision = $auth->createPermission('backend.shop-product.manage');
        $permision->description = 'Товары магазина: Управление';
        $permision->is_system = true;
        $auth->add($permision);

        $permision = $auth->createPermission('backend.shop-product.read');
        $permision->description = 'Товары магазина: Просмотр';
        $permision->is_system = false;
        $auth->add($permision);
        $permision = $auth->createPermission('backend.shop-product.edit');
        $permision->description = 'Товары магазина: Изменение';
        $permision->is_system = false;
        $auth->add($permision);
    }

    public function safeDown()
    {
        $auth = $this->getAuthManager();
        $auth->remove($auth->getPermission('backend.product.read'));
        $auth->remove($auth->getPermission('backend.product.edit'));

        $auth->remove($auth->getPermission('backend.shop-product.manage'));

        $auth->remove($auth->getPermission('backend.shop-product.read'));
        $auth->remove($auth->getPermission('backend.shop-product.edit'));

        $this->dropForeignKey('fk_shop_product_category_shop_product_id', '{{%shop_product_category}}');
        $this->dropForeignKey('fk_shop_product_category_category_id', '{{%shop_product_category}}');
        $this->dropTable('{{%shop_product_category}}');

        $this->dropForeignKey('fk_shop_product_product_id', '{{%shop_product}}');
        $this->dropForeignKey('fk_shop_product_shop_id', '{{%shop_product}}');
        $this->dropForeignKey('fk_shop_product_category_id', '{{%shop_product}}');
        $this->dropTable('{{%shop_product}}');

        $this->dropForeignKey('fk_product_manufacture_id', '{{%product}}');
        $this->dropForeignKey('fk_product_product_type_id', '{{%product}}');
        $this->dropTable('{{%product}}');
    }
}
