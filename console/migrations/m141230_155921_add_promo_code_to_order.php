<?php

use yii\db\Schema;
use console\components\Migration;

class m141230_155921_add_promo_code_to_order extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%order}}', 'promo', Schema::TYPE_STRING . ' DEFAULT 0 NULL');
        $this->refreshTableSchema('{{%order}}');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%order}}', 'promo');
        $this->refreshTableSchema('{{%order}}');
    }
}
