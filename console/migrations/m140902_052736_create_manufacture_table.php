<?php

use yii\db\Schema;
use console\components\Migration;

class m140902_052736_create_manufacture_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = $this->getDefaultTableOptions();

        $this->createTable(
            '{{%manufacture}}',
            [
                'id'          => Schema::TYPE_PK,
                'name'        => Schema::TYPE_STRING . ' NOT NULL',
                'description' => Schema::TYPE_TEXT . ' NOT NULL',
                'created_at'  => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
                'updated_at'  => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
            ],
            $tableOptions
        );
        $this->refreshTableSchema('{{%manufacture}}');

        $auth = $this->getAuthManager();

        $permision = $auth->createPermission('backend.manufacture.read');
        $permision->description = 'Производитель: Просмотр';
        $permision->is_system = true;
        $auth->add($permision);
        $permision = $auth->createPermission('backend.manufacture.edit');
        $permision->description = 'Производитель: Изменение';
        $permision->is_system = true;
        $auth->add($permision);
    }

    public function safeDown()
    {
        $auth = $this->getAuthManager();
        $auth->remove($auth->getPermission('backend.manufacture.read'));
        $auth->remove($auth->getPermission('backend.manufacture.edit'));

        $this->dropTable('{{%manufacture}}');
    }
}
