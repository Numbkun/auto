<?php

use yii\db\Schema;
use console\components\Migration;

class m141215_084656_create_col_for_order extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%customer}}', 'shipping_address', Schema::TYPE_STRING . ' NULL');
        $this->refreshTableSchema('{{%customer}}');
        $this->addColumn('{{%order}}', 'comment', Schema::TYPE_STRING . ' NULL');
        $this->addColumn('{{%order}}', 'created_at', Schema::TYPE_DATETIME . ' with time zone NOT NULL');
        $this->addColumn('{{%order}}', 'updated_at', Schema::TYPE_DATETIME . ' with time zone NOT NULL');
        $this->refreshTableSchema('{{%order}}');
        $this->addColumn('{{%order_product}}', 'amount', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->refreshTableSchema('{{%order_product}}');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%customer}}','shipping_address');
        $this->refreshTableSchema('{{%customer}}');
        $this->dropColumn('{{%order}}','comment');
        $this->dropColumn('{{%order}}','created_at');
        $this->dropColumn('{{%order}}','updated_at');
        $this->refreshTableSchema('{{%order}}');
        $this->dropColumn('{{%order_product}}','amount');
        $this->refreshTableSchema('{{%order_product}}');
    }
}
