<?php

use yii\db\Schema;
use console\components\Migration;

class m141216_131953_create_order_status_tabel extends Migration
{
    public function safeUp()
    {
        $tableOptions = $this->getDefaultTableOptions();

        $this->createTable(
            '{{%order_status}}',
            [
                'id'            => Schema::TYPE_PK,
                'name'          => Schema::TYPE_STRING . ' NOT NULL',
            ],
            $tableOptions
        );
        $this->insert('{{%order_status}}', [
            'id'         => '1',
            'name'       => 'В обработке',
        ]);
        $this->insert('{{%order_status}}', [
            'id'         => '2',
            'name'       => 'Доставляется',
        ]);
        $this->insert('{{%order_status}}', [
            'id'         => '3',
            'name'       => 'Отгружен',
        ]);
        $this->db->createCommand()->resetSequence('{{%order_status}}', 4)->execute();
        $this->refreshTableSchema('{{%order_status}}');

        $auth = $this->getAuthManager();

        $permision = $auth->createPermission('backend.order_status.read');
        $permision->description = 'Статус заказа: Просмотр';
        $permision->is_system = false;
        $auth->add($permision);
        $permision = $auth->createPermission('backend.order_status.edit');
        $permision->description = 'Статус заказа: Изменение';
        $permision->is_system = false;
        $auth->add($permision);

        $this->addColumn('{{%order}}', 'status_id', Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 1');
        $this->addForeignKey('fk_order_status_id', '{{%order}}', 'status_id', '{{%order_status}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->refreshTableSchema('{{%order}}');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_order_status_id', '{{%order}}');
        $this->dropColumn('{{%order}}', 'status_id');
        $this->refreshTableSchema('{{%order}}');

        $auth = $this->getAuthManager();
        $auth->remove($auth->getPermission('backend.order_status.read'));
        $auth->remove($auth->getPermission('backend.order_status.edit'));
        $this->dropTable('{{%order_status}}');
    }
}
