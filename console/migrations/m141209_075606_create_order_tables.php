<?php

use yii\db\Schema;
use console\components\Migration;

class m141209_075606_create_order_tables extends Migration
{
    public function safeUp()
    {
        $tableOptions = $this->getDefaultTableOptions();

        $this->createTable(
            '{{%sku}}',
            [
                'id'            => Schema::TYPE_PK,
                'product_id'    => Schema::TYPE_INTEGER . ' NOT NULL',
                'name'          => Schema::TYPE_STRING . ' NOT NULL',
                'created_at'    => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
                'updated_at'    => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
            ],
            $tableOptions
        );
        $this->addForeignKey('fk_sku_product_id', '{{%sku}}', 'product_id', '{{%product}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->refreshTableSchema('{{%sku}}');

        $this->createTable(
            '{{%sku_parameter_value}}',
            [
                'id'                      => Schema::TYPE_PK,
                'sku_id'                  => Schema::TYPE_INTEGER . ' NOT NULL',
                'parameter_value_id'      => Schema::TYPE_INTEGER . ' NOT NULL',
            ],
            $tableOptions
        );
        $this->addForeignKey('fk_sky_sku_id', '{{%sku_parameter_value}}', 'sku_id', '{{%sku}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_sku_parameter_value_id', '{{%sku_parameter_value}}', 'parameter_value_id', '{{%parameter_value}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->refreshTableSchema('{{%sku_parameter_value}}');

        $this->createTable(
            '{{%sku_parameter_price}}',
            [
                'id'                      => Schema::TYPE_PK,
                'sku_id'                  => Schema::TYPE_INTEGER . ' NOT NULL',
                'shop_id'                 => Schema::TYPE_INTEGER . ' NOT NULL',
                'currency_id'             => Schema::TYPE_INTEGER . ' NOT NULL',
                'value'                   => Schema::TYPE_STRING  . ' NOT NULL',
            ],
            $tableOptions
        );
        $this->addForeignKey('fk_sku_parameter_price_sku_id', '{{%sku_parameter_price}}', 'sku_id', '{{%sku}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_sku_parameter_price_shop_id', '{{%sku_parameter_price}}', 'shop_id', '{{%shop}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_sku_parameter_price_currency_id', '{{%sku_parameter_price}}', 'currency_id', '{{%currency}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->refreshTableSchema('{{%sku_parameter_price}}');

        //Таблица Заказа
        $this->createTable(
            '{{%order}}',
            [
                'id'                => Schema::TYPE_PK,
                'customer_id'       => Schema::TYPE_INTEGER . ' NOT NULL',
                'shop_id'           => Schema::TYPE_INTEGER . ' NOT NULL',
                'currency_id'       => Schema::TYPE_INTEGER . ' NOT NULL',
                'name'              => Schema::TYPE_STRING  . ' NOT NULL',
                'price'             => Schema::TYPE_DECIMAL . '(12, 2) NOT NULL',
            ],
            $tableOptions
        );
        $this->addForeignKey('fk_order_shop_customer_id', '{{%order}}', 'customer_id', '{{%customer}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_order_shop_id', '{{%order}}', 'shop_id', '{{%shop}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_order_currency_id', '{{%order}}', 'currency_id', '{{%currency}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->refreshTableSchema('{{%order}}');

        $auth = $this->getAuthManager();

        $permision = $auth->createPermission('backend.order.read');
        $permision->description = 'Заказ: Просмотр';
        $permision->is_system = false;
        $auth->add($permision);
        $permision = $auth->createPermission('backend.order.edit');
        $permision->description = 'Заказ: Изменение';
        $permision->is_system = false;
        $auth->add($permision);

        //Таблица Заказа
        $this->createTable(
            '{{%order_product}}',
            [
                'id'                => Schema::TYPE_PK,
                'sku_id'            => Schema::TYPE_INTEGER . ' NOT NULL',
                'order_id'          => Schema::TYPE_INTEGER . ' NOT NULL',
                'price'             => Schema::TYPE_DECIMAL . '(12, 2) NOT NULL',
            ],
            $tableOptions
        );
        $this->addForeignKey('fk_order_product_sku_id', '{{%order_product}}', 'sku_id', '{{%sku}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_order_product_order_id', '{{%order_product}}', 'order_id', '{{%order}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->refreshTableSchema('{{%order_product}}');

        $this->createTable(
            '{{%cart}}',
            [
                'id'            => Schema::TYPE_PK,
                'sku_id'        => Schema::TYPE_INTEGER . ' NOT NULL',
                'customer_id'   => Schema::TYPE_INTEGER . ' NOT NULL',
                'count'         => Schema::TYPE_INTEGER . ' NOT NULL',
            ],
            $tableOptions
        );
        $this->createIndex('ux_cart_sku_id_customer_id', '{{%cart}}', ['sku_id', 'customer_id'], true);
        $this->addForeignKey('fk_cart_product_sku_id', '{{%cart}}', 'sku_id', '{{%sku}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_cart_product_customer_id', '{{%cart}}', 'customer_id', '{{%customer}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->refreshTableSchema('{{%cart}}');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_cart_product_sku_id', '{{%cart}}');
        $this->dropForeignKey('fk_cart_product_customer_id', '{{%cart}}');
        $this->dropTable('{{%cart}}');

        $this->dropForeignKey('fk_sku_parameter_price_sku_id', '{{%sku_parameter_price}}');
        $this->dropForeignKey('fk_sku_parameter_price_shop_id', '{{%sku_parameter_price}}');
        $this->dropForeignKey('fk_sku_parameter_price_currency_id', '{{%sku_parameter_price}}');
        $this->dropTable('{{%sku_parameter_price}}');

        $this->dropForeignKey('fk_sky_sku_id', '{{%sku_parameter_value}}');
        $this->dropForeignKey('fk_sku_parameter_value_id', '{{%sku_parameter_value}}');
        $this->dropTable('{{%sku_parameter_value}}');

        $this->dropForeignKey('fk_order_product_sku_id', '{{%order_product}}');
        $this->dropForeignKey('fk_order_product_order_id', '{{%order_product}}');
        $this->dropTable('{{%order_product}}');

        $this->dropForeignKey('fk_order_shop_customer_id', '{{%order}}');
        $this->dropForeignKey('fk_order_shop_id', '{{%order}}');
        $this->dropForeignKey('fk_order_currency_id', '{{%order}}');
        $auth = $this->getAuthManager();
        $auth->remove($auth->getPermission('backend.order.read'));
        $auth->remove($auth->getPermission('backend.order.edit'));
        $this->dropTable('{{%order}}');

        $this->dropForeignKey('fk_sku_product_id', '{{%sku}}');
        $this->dropTable('{{%sku}}');
    }
}
