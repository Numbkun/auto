<?php

use yii\caching\TagDependency;
use yii\db\Schema;
use console\components\Migration;
use common\components\DateTime;

class m141127_070336_create_shop_product_price_tables extends Migration
{
    public function safeUp()
    {
        $tableOptions = $this->getDefaultTableOptions();

        // Таблица валют
        $this->createTable(
            '{{%currency}}',
            [
                'id'            => Schema::TYPE_PK,
                'name'          => Schema::TYPE_STRING . ' NOT NULL',
                'short_name'    => Schema::TYPE_STRING . ' NOT NULL',
                'created_at'    => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
                'updated_at'    => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
            ],
            $tableOptions
        );
        $this->insert('{{%currency}}', [
            'id'         => '1',
            'name'       => 'Рубль',
            'short_name' => 'RUB',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        $this->db->createCommand()->resetSequence('{{%currency}}', 2)->execute();
        $this->refreshTableSchema('{{%currency}}');

        // Таблица магазинов
        $this->addColumn('{{%shop}}', 'currency_id', Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 1');
        $this->addForeignKey('fk_shop_currency_id', '{{%shop}}', 'currency_id', '{{%currency}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->refreshTableSchema('{{%shop}}');
        TagDependency::invalidate(Yii::$app->cache, '#table_cache#{{%shop}}');

        //Таблица Регионы
        $this->createTable(
            '{{%region}}',
            [
                'id'                => Schema::TYPE_PK,
                'name'              => Schema::TYPE_STRING . ' NOT NULL',
                'created_at'        => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
                'updated_at'        => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
            ],
            $tableOptions
        );
        $this->insert('{{%region}}', [
            'id'         => '1',
            'name'       => 'Владимир',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        $this->db->createCommand()->resetSequence('{{%region}}', 2)->execute();
        $this->refreshTableSchema('{{%region}}');

        $auth = $this->getAuthManager();

        $permision = $auth->createPermission('backend.region.read');
        $permision->description = 'Регион: Просмотр';
        $permision->is_system = false;
        $auth->add($permision);
        $permision = $auth->createPermission('backend.region.edit');
        $permision->description = 'Регион: Изменение';
        $permision->is_system = false;
        $auth->add($permision);

        //Таблица Цены
        $this->createTable(
            '{{%shop_product_price}}',
            [
                'id'                => Schema::TYPE_PK,
                'shop_product_id'   => Schema::TYPE_INTEGER . ' NOT NULL',
                'currency_id'       => Schema::TYPE_INTEGER . ' NOT NULL',
                'region_id'         => Schema::TYPE_INTEGER . ' NOT NULL',
                'price'             => Schema::TYPE_DECIMAL . '(12, 2) NOT NULL',
            ],
            $tableOptions
        );
        $this->addForeignKey('fk_shop_product_price_shop_product_id', '{{%shop_product_price}}', 'shop_product_id', '{{%shop_product}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_shop_product_price_currency_id', '{{%shop_product_price}}', 'currency_id', '{{%currency}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_shop_product_price_region_id', '{{%shop_product_price}}', 'region_id', '{{%region}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->refreshTableSchema('{{%shop_product_price}}');

        $auth = $this->getAuthManager();

        $permision = $auth->createPermission('backend.price.read');
        $permision->description = 'Цена: Просмотр';
        $permision->is_system = false;
        $auth->add($permision);
        $permision = $auth->createPermission('backend.price.edit');
        $permision->description = 'Цена: Изменение';
        $permision->is_system = false;
        $auth->add($permision);

        //Таблица разделения магазинов по регионам
        $this->createTable(
            '{{%shop_region}}',
            [
                'region_id'                     => Schema::TYPE_INTEGER . ' NOT NULL',
                'shop_id'                       => Schema::TYPE_INTEGER . ' NOT NULL',
                'PRIMARY KEY (region_id, shop_id)',
            ],
            $tableOptions
        );
        $this->addForeignKey('fk_shop_region_region_id', '{{%shop_region}}', 'region_id', '{{%region}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_shop_region_shop_id', '{{%shop_region}}', 'shop_id', '{{%shop}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->refreshTableSchema('{{%shop_region}}');

        // Связь Таблица клиентов с регионом
        $this->addColumn('{{%customer}}', 'region_id', Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 1');
        $this->addForeignKey('fk_customer_region_id', '{{%customer}}', 'region_id', '{{%region}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->refreshTableSchema('{{%customer}}');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_shop_product_price_shop_product_id', '{{%shop_product_price}}');
        $this->dropForeignKey('fk_shop_product_price_currency_id', '{{%shop_product_price}}');
        $this->dropForeignKey('fk_shop_product_price_region_id', '{{%shop_product_price}}');
        $auth = $this->getAuthManager();
        $auth->remove($auth->getPermission('backend.price.read'));
        $auth->remove($auth->getPermission('backend.price.edit'));
        $this->dropTable('{{%shop_product_price}}');

        $this->dropForeignKey('fk_shop_currency_id', '{{%shop}}');
        $this->dropColumn('{{%shop}}', 'currency_id');
        $this->refreshTableSchema('{{%shop}}');
        TagDependency::invalidate(Yii::$app->cache, '#table_cache#{{%shop}}');
        $this->dropTable('{{%currency}}');

        $this->dropForeignKey('fk_shop_region_region_id', '{{%shop_region}}');
        $this->dropForeignKey('fk_shop_region_shop_id', '{{%shop_region}}');
        $this->dropTable('{{%shop_region}}');
        $this->dropColumn('{{%customer}}', 'region_id');
        $this->refreshTableSchema('{{%customer}}');

        $auth = $this->getAuthManager();
        $auth->remove($auth->getPermission('backend.region.read'));
        $auth->remove($auth->getPermission('backend.region.edit'));
        $this->dropTable('{{%region}}');
    }
}
