<?php

use common\models\ParameterType;
use yii\db\Schema;
use console\components\Migration;

class m140916_060924_create_parameter_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = $this->getDefaultTableOptions();

        $this->createTable(
            '{{%parameter_type}}',
            [
                'id'   => Schema::TYPE_PK,
                'name' => Schema::TYPE_STRING . ' NOT NULL',
            ],
            $tableOptions
        );
        $this->insert('{{%parameter_type}}', ['id' => ParameterType::INTEGER, 'name' => 'Целое число']);
        $this->insert('{{%parameter_type}}', ['id' => ParameterType::FLOAT,   'name' => 'Дробное число с плавающей запятой']);
        $this->insert('{{%parameter_type}}', ['id' => ParameterType::STRING,  'name' => 'Строка']);
        $this->insert('{{%parameter_type}}', ['id' => ParameterType::BOOL,    'name' => 'Логическое']);
        $this->insert('{{%parameter_type}}', ['id' => ParameterType::SINGLE,  'name' => 'Перечисление с единственным выбором']);
        $this->insert('{{%parameter_type}}', ['id' => ParameterType::MULTIPE, 'name' => 'Перечисление с множественным выбором']);
        $this->insert('{{%parameter_type}}', ['id' => ParameterType::RANGE,   'name' => 'Диапазон чисел']);
        $this->insert('{{%parameter_type}}', ['id' => ParameterType::COLOR,   'name' => 'Цвет']);
        $this->insert('{{%parameter_type}}', ['id' => ParameterType::PICTURE, 'name' => 'Картинка']);
        $this->insert('{{%parameter_type}}', ['id' => ParameterType::FABRIC,  'name' => 'Ткань']);
        $this->db->createCommand()->resetSequence('{{%parameter_type}}', 11)->execute();

        $this->createTable(
            '{{%parameter_group}}',
            [
                'id'         => Schema::TYPE_PK,
                'name'       => Schema::TYPE_STRING . ' NOT NULL',
                'created_at' => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
                'updated_at' => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
            ],
            $tableOptions
        );

        $this->createTable(
            '{{%parameter}}',
            [
                'id'                 => Schema::TYPE_PK,
                'parameter_type_id'  => Schema::TYPE_INTEGER  . ' NOT NULL',
                'parameter_group_id' => Schema::TYPE_INTEGER  . ' NOT NULL',
                'name'               => Schema::TYPE_STRING   . ' NOT NULL',
                'description'        => Schema::TYPE_TEXT     . ' NOT NULL',
                'created_at'         => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
                'updated_at'         => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
            ],
            $tableOptions
        );
        $this->addForeignKey('fk_parameter_parameter_type_id', '{{%parameter}}', 'parameter_type_id', '{{%parameter_type}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_parameter_parameter_group_id', '{{%parameter}}', 'parameter_group_id', '{{%parameter_group}}', 'id', 'RESTRICT', 'RESTRICT');

        $this->createTable(
            '{{%parameter_available_value}}',
            [
                'id'           => Schema::TYPE_PK,
                'parameter_id' => Schema::TYPE_INTEGER . ' NOT NULL',
                'name'         => Schema::TYPE_STRING . ' NOT NULL',
                'value'        => Schema::TYPE_STRING . ' NOT NULL',
                'created_at'   => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
                'updated_at'   => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
            ],
            $tableOptions
        );
        $this->addForeignKey('fk_parameter_available_value_parameter_id', '{{%parameter_available_value}}', 'parameter_id', '{{%parameter}}', 'id', 'RESTRICT', 'RESTRICT');

        $this->createTable(
            '{{%category_parameter}}',
            [
                'parameter_id' => Schema::TYPE_INTEGER . ' NOT NULL',
                'category_id'  => Schema::TYPE_INTEGER . ' NOT NULL',
                'PRIMARY KEY (parameter_id, category_id)'
            ],
            $tableOptions
        );
        $this->addForeignKey('fk_category_parameter_parameter_id', '{{%category_parameter}}', 'parameter_id', '{{%parameter}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_category_parameter_category_id', '{{%category_parameter}}', 'category_id', '{{%category}}', 'id', 'RESTRICT', 'RESTRICT');

        $this->refreshTableSchema('{{%category_parameter}}');
        $this->refreshTableSchema('{{%parameter_available_value}}');
        $this->refreshTableSchema('{{%parameter}}');
        $this->refreshTableSchema('{{%parameter_group}}');
        $this->refreshTableSchema('{{%parameter_type}}');

        $auth = $this->getAuthManager();

        $permision = $auth->createPermission('backend.parameter-group.read');
        $permision->description = 'Группы характеристик: Просмотр';
        $permision->is_system = true;
        $auth->add($permision);
        $permision = $auth->createPermission('backend.parameter-group.edit');
        $permision->description = 'Группы характеристик: Изменение';
        $permision->is_system = true;
        $auth->add($permision);

        $permision = $auth->createPermission('backend.parameter.read');
        $permision->description = 'Характеристики: Просмотр';
        $permision->is_system = true;
        $auth->add($permision);
        $permision = $auth->createPermission('backend.parameter.edit');
        $permision->description = 'Характеристики: Изменение';
        $permision->is_system = true;
        $auth->add($permision);
    }

    public function safeDown()
    {
        $auth = $this->getAuthManager();
        $auth->remove($auth->getPermission('backend.parameter.read'));
        $auth->remove($auth->getPermission('backend.parameter.edit'));
        $auth->remove($auth->getPermission('backend.parameter-group.read'));
        $auth->remove($auth->getPermission('backend.parameter-group.edit'));
        $this->dropForeignKey('fk_category_parameter_parameter_id', '{{%category_parameter}}');
        $this->dropForeignKey('fk_category_parameter_category_id', '{{%category_parameter}}');
        $this->dropTable('{{%category_parameter}}');
        $this->dropForeignKey('fk_parameter_available_value_parameter_id', '{{%parameter_available_value}}');
        $this->dropTable('{{%parameter_available_value}}');
        $this->dropForeignKey('fk_parameter_parameter_type_id', '{{%parameter}}');
        $this->dropForeignKey('fk_parameter_parameter_group_id', '{{%parameter}}');
        $this->dropTable('{{%parameter}}');
        $this->dropTable('{{%parameter_group}}');
        $this->dropTable('{{%parameter_type}}');
    }
}
