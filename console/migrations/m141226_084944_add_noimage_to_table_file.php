<?php

use console\components\Migration;
use common\components\DateTime;

class m141226_084944_add_noimage_to_table_file extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%file}}',
            [
                'id'         => 1,
                'name'       => 'no_image.png',
                'extension'  => 'png',
                'path'       => 'product',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ]
        );
        $this->db->createCommand()->resetSequence('{{%file}}', 2)->execute();
        $this->refreshTableSchema('{{%file}}');
    }

    public function safeDown()
    {
        $this->delete('{{%file}}', ['id' => 1, 'name' => 'no_image.png']);
    }
}
