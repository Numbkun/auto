<?php

use yii\db\Schema;
use console\components\Migration;

class m141225_151527_change_sku extends Migration
{
    public function safeUp()
    {
        $this->db->createCommand('ALTER TABLE public.sku_parameter_price ALTER COLUMN value TYPE DECIMAL(12,4) USING value::DECIMAL(12,4);')->execute();

        $this->addColumn('{{%sku_parameter_price}}', 'region_id', Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 1');
        $this->addForeignKey('fk_sku_parameter_price_region_id', '{{%sku_parameter_price}}', 'region_id', '{{%region}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->refreshTableSchema('{{%sku_parameter_price}}');
    }
    public function safeDown()
    {
        $this->dropForeignKey('fk_sku_parameter_price_region_id', '{{%sku_parameter_price}}');
        $this->dropColumn('{{%sku_parameter_price}}', 'region_id');
        $this->refreshTableSchema('{{%sku_parameter_price}}');
    }
}
