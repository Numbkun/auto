<?php

use common\models\PriceType;
use yii\db\Schema;
use console\components\Migration;

class m141023_055352_create_parameter_price_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = $this->getDefaultTableOptions();

        $this->createTable(
            '{{%price_type}}',
            [
                'id'   => Schema::TYPE_PK,
                'name' => Schema::TYPE_STRING . ' NOT NULL',
            ],
            $tableOptions
        );
        $this->insert('{{%price_type}}', ['id' => PriceType::PERCENT, 'name' => 'Процент']);
        $this->insert('{{%price_type}}', ['id' => PriceType::FIXED, 'name' => 'Фиксированная']);
        $this->insert('{{%price_type}}', ['id' => PriceType::REPLACE, 'name' => 'Заменяющая']);
        $this->db->createCommand()->resetSequence('{{%parameter_type}}', 4)->execute();

        $this->createTable(
            '{{%parameter_value_price}}',
            [
                'id'                 => Schema::TYPE_PK,
                'parameter_value_id' => Schema::TYPE_INTEGER  . ' NOT NULL',
                'price_type_id'      => Schema::TYPE_INTEGER  . ' NOT NULL',
                'shop_id'            => Schema::TYPE_INTEGER  . ' NOT NULL',
                'value'              => Schema::TYPE_DECIMAL  . '(10, 4) NOT NULL',
            ],
            $tableOptions
        );
        $this->addForeignKey('fk_parameter_value_price_parameter_value_id', '{{%parameter_value_price}}', 'parameter_value_id', '{{%parameter_value}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_parameter_value_price_price_type_id', '{{%parameter_value_price}}', 'price_type_id', '{{%price_type}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_parameter_value_price_shop_id', '{{%parameter_value_price}}', 'shop_id', '{{%shop}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->createIndex('ux_parameter_value_price_parameter_value_id_price_type_id_shop_id', '{{%parameter_value_price}}', ['parameter_value_id', 'price_type_id', 'shop_id'], true);
        $this->refreshTableSchema('{{%price_type}}');
        $this->refreshTableSchema('{{%parameter_value_price}}');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_parameter_value_price_parameter_value_id', '{{%parameter_value_price}}');
        $this->dropForeignKey('fk_parameter_value_price_price_type_id', '{{%parameter_value_price}}');
        $this->dropForeignKey('fk_parameter_value_price_shop_id', '{{%parameter_value_price}}');
        $this->dropTable('{{%parameter_value_price}}');
        $this->dropTable('{{%price_type}}');
    }
}
