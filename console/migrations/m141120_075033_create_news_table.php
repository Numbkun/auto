<?php

use yii\db\Schema;
use console\components\Migration;

class m141120_075033_create_news_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = $this->getDefaultTableOptions();

        $this->createTable(
            '{{%news}}',
            [
                'id'            => Schema::TYPE_PK,
                'shop_id'       => Schema::TYPE_INTEGER . ' NOT NULL',
                'name'          => Schema::TYPE_STRING . ' NOT NULL',
                'translite'     => Schema::TYPE_STRING . ' NOT NULL',
                'description'    => Schema::TYPE_TEXT . ' NOT NULL',
                'text'          => Schema::TYPE_TEXT . ' NOT NULL',
                'page_description'   => Schema::TYPE_TEXT . ' NULL',
                'page_keywords'      => Schema::TYPE_TEXT . ' NULL',
                'page_title'         => Schema::TYPE_TEXT . ' NULL',
                'created_at'    => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
                'updated_at'    => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
            ],
            $tableOptions
        );
        $this->addForeignKey('fk_news_shop_id', '{{%news}}', 'shop_id', '{{%shop}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->refreshTableSchema('{{%news}}');

        $auth = $this->getAuthManager();

        $permision = $auth->createPermission('backend.news.read');
        $permision->description = 'Новости: Просмотр';
        $permision->is_system = false;
        $auth->add($permision);
        $permision = $auth->createPermission('backend.news.edit');
        $permision->description = 'Новости: Изменение';
        $permision->is_system = false;
        $auth->add($permision);
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_news_shop_id', '{{%news}}');
        $this->dropTable('{{%news}}');
        $auth = $this->getAuthManager();
        $auth->remove($auth->getPermission('backend.news.read'));
        $auth->remove($auth->getPermission('backend.news.edit'));
    }
}
