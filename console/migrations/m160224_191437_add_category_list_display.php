<?php

use yii\db\Schema;
use console\components\Migration;

class m160224_191437_add_category_list_display extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%category}}', 'is_list', Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT FALSE');
        $this->addColumn('{{%category}}', 'in_catalog', Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT FALSE');
        $this->refreshTableSchema('{{%category}}');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%category}}','in_catalog');
        $this->dropColumn('{{%category}}','is_list');
    }
}
