<?php

use yii\db\Schema;
use console\components\Migration;

class m141119_102633_create_dignity_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = $this->getDefaultTableOptions();

        $this->createTable(
            '{{%dignity}}',
            [
                'id'            => Schema::TYPE_PK,
                'shop_id'       => Schema::TYPE_INTEGER . ' NOT NULL',
                'name'          => Schema::TYPE_STRING . ' NOT NULL',
                'translite'     => Schema::TYPE_STRING . ' NOT NULL',
                'text'          => Schema::TYPE_TEXT . ' NOT NULL',
                'created_at'    => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
                'updated_at'    => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
            ],
            $tableOptions
        );
        $this->refreshTableSchema('{{%dignity}}');

        $auth = $this->getAuthManager();

        $permision = $auth->createPermission('backend.dignity.read');
        $permision->description = 'Достоинства: Просмотр';
        $permision->is_system = false;
        $auth->add($permision);
        $permision = $auth->createPermission('backend.dignity.edit');
        $permision->description = 'Достоинства: Изменение';
        $permision->is_system = false;
        $auth->add($permision);
    }

    public function safeDown()
    {
        $this->dropTable('{{%dignity}}');
        $auth = $this->getAuthManager();
        $auth->remove($auth->getPermission('backend.dignity.read'));
        $auth->remove($auth->getPermission('backend.dignity.edit'));
    }
}
