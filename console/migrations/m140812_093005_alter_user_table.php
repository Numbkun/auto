<?php

use console\components\Migration;

class m140812_093005_alter_user_table extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('{{%user}}', 'username', 'name');
        $this->refreshTableSchema('{{%user}}');
    }

    public function safeDown()
    {
        $this->renameColumn('{{%user}}', 'name', 'username');
        $this->refreshTableSchema('{{%user}}');
    }
}
