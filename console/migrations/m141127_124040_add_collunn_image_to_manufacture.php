<?php

use yii\db\Schema;
use console\components\Migration;

class m141127_124040_add_collunn_image_to_manufacture extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%manufacture}}', 'image_id', Schema::TYPE_INTEGER . ' NULL');

        $this->refreshTableSchema('{{%manufacture}}');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%manufacture}}','image_id');
    }
}
