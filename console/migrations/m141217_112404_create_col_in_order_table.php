<?php

use yii\db\Schema;
use console\components\Migration;

class m141217_112404_create_col_in_order_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%order}}', 'delivery_date', Schema::TYPE_DATE . ' NULL');
        $this->addColumn('{{%order}}', 'shipping_address', Schema::TYPE_STRING . ' NULL');
        $this->addColumn('{{%order}}', 'created_by', Schema::TYPE_STRING . ' NULL');
        $this->refreshTableSchema('{{%order}}');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%order}}','delivery_date');
        $this->dropColumn('{{%order}}','shipping_address');
        $this->dropColumn('{{%order}}','created_by');
        $this->refreshTableSchema('{{%order}}');
    }
}
