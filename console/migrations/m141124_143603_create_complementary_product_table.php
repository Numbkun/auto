<?php

use yii\db\Schema;
use console\components\Migration;

class m141124_143603_create_complementary_product_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = $this->getDefaultTableOptions();

        $this->createTable(
            '{{%complementary_product}}',
            [
                'product_id'                     => Schema::TYPE_INTEGER . ' NOT NULL',
                'complementary_product_id'       => Schema::TYPE_INTEGER . ' NOT NULL',
                'PRIMARY KEY (product_id, complementary_product_id)',
            ],
            $tableOptions
        );
        $this->addForeignKey('fk_complementary_product_product_id', '{{%complementary_product}}', 'product_id', '{{%product}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_complementary_product_complementary_product_id', '{{%complementary_product}}', 'complementary_product_id', '{{%product}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->refreshTableSchema('{{%complementary_product}}');

    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_complementary_product_product_id', '{{%complementary_product}}');
        $this->dropForeignKey('fk_complementary_product_complementary_product_id', '{{%complementary_product}}');
        $this->dropTable('{{%complementary_product}}');
    }
}
