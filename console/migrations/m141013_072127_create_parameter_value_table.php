<?php

use yii\db\Schema;
use console\components\Migration;

class m141013_072127_create_parameter_value_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = $this->getDefaultTableOptions();

        $this->createTable(
            '{{%parameter_value}}',
            [
                'id'            => Schema::TYPE_PK,
                'product_id'    => Schema::TYPE_INTEGER . ' NOT NULL',
                'parameter_id'  => Schema::TYPE_INTEGER . ' NOT NULL',
                'list_value_id' => Schema::TYPE_INTEGER,
                'value_string'  => Schema::TYPE_STRING,
                'value_int'     => Schema::TYPE_INTEGER,
                'value_bool'    => Schema::TYPE_BOOLEAN,
                'begin_range'   => Schema::TYPE_INTEGER,
                'end_range'     => Schema::TYPE_INTEGER,
            ],
            $tableOptions
        );
        $this->addForeignKey('fk_parameter_value_product_id', '{{%parameter_value}}', 'product_id', '{{%product}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_parameter_value_parameter_id', '{{%parameter_value}}', 'parameter_id', '{{%parameter}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_parameter_value_list_value_id', '{{%parameter_value}}', 'list_value_id', '{{%parameter_available_value}}', 'id', 'RESTRICT', 'RESTRICT');

        $this->createTable(
            '{{%parameter_value_list_value}}',
            [
                'parameter_value_id' => Schema::TYPE_INTEGER . ' NOT NULL',
                'available_value_id' => Schema::TYPE_INTEGER . ' NOT NULL',
                'PRIMARY KEY (parameter_value_id, available_value_id)'
            ],
            $tableOptions
        );
        $this->addForeignKey('fk_parameter_value_list_value_parameter_value_id', '{{%parameter_value_list_value}}', 'parameter_value_id', '{{%parameter_value}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_parameter_value_list_value_available_value_id', '{{%parameter_value_list_value}}', 'available_value_id', '{{%parameter_available_value}}', 'id', 'RESTRICT', 'RESTRICT');

        $this->refreshTableSchema('{{%parameter_value}}');
        $this->refreshTableSchema('{{%parameter_value_list}}');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_parameter_value_list_value_available_value_id', '{{%parameter_value_list_value}}');
        $this->dropForeignKey('fk_parameter_value_list_value_parameter_value_id', '{{%parameter_value_list_value}}');
        $this->dropTable('{{%parameter_value_list_value}}');
        $this->dropForeignKey('fk_parameter_value_list_value_id', '{{%parameter_value}}');
        $this->dropForeignKey('fk_parameter_value_parameter_id', '{{%parameter_value}}');
        $this->dropForeignKey('fk_parameter_value_product_id', '{{%parameter_value}}');
        $this->dropTable('{{%parameter_value}}');
    }
}
