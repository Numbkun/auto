<?php

use console\components\Migration;
use common\components\DateTime;

class m140815_220204_add_settings extends Migration
{
    public function safeUp()
    {
        $this->dropIndex('ux_setting_name', '{{%setting}}');
        $this->createIndex('ux_setting_shop_id_name', '{{%setting}}', ['shop_id', 'name'], true);

        $this->insert('{{%setting}}',
            [
                'shop_id'    => 1,
                'name'       => 'shop_default_user_name',
                'value'      => 'admin',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]
        );
        $this->insert('{{%setting}}',
            [
                'shop_id'    => 1,
                'name'       => 'shop_default_user_password',
                'value'      => 'admin',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]
        );
        $this->insert('{{%setting}}',
            [
                'shop_id'    => 1,
                'name'       => 'shop_default_user_email',
                'value'      => 'admin@example.com',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]
        );
        $this->insert('{{%setting}}',
            [
                'shop_id'    => 1,
                'name'       => 'shop_default_role_name',
                'value'      => '{shop}.administrator',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]
        );
        $this->insert('{{%setting}}',
            [
                'shop_id'    => 1,
                'name'       => 'shop_default_role_description',
                'value'      => '{shop}: Администратор',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]
        );
    }

    public function safeDown()
    {
        $this->delete('{{%setting}}', ['shop_id' => 1, 'name' => 'shop_default_user_name']);
        $this->delete('{{%setting}}', ['shop_id' => 1, 'name' => 'shop_default_user_password']);
        $this->delete('{{%setting}}', ['shop_id' => 1, 'name' => 'shop_default_user_email']);
        $this->delete('{{%setting}}', ['shop_id' => 1, 'name' => 'shop_default_role_name']);
        $this->delete('{{%setting}}', ['shop_id' => 1, 'name' => 'shop_default_role_description']);

        $this->dropIndex('ux_setting_shop_id_name', '{{%setting}}');
        $this->createIndex('ux_setting_name', '{{%setting}}', 'name', true);
    }
}
