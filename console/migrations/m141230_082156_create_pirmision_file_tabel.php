<?php

use console\components\Migration;

class m141230_082156_create_pirmision_file_tabel extends Migration
{
    public function safeUp()
    {
        $auth = $this->getAuthManager();

        $permision = $auth->createPermission('backend.file.read');
        $permision->description = 'Загрузка файлов: Просмотр';
        $permision->is_system = false;
        $auth->add($permision);
        $permision = $auth->createPermission('backend.file.edit');
        $permision->description = 'Загрузка файлов: Изменение';
        $permision->is_system = false;
        $auth->add($permision);
    }

    public function safeDown()
    {
        $auth = $this->getAuthManager();
        $auth->remove($auth->getPermission('backend.file.read'));
        $auth->remove($auth->getPermission('backend.file.edit'));
    }
}
