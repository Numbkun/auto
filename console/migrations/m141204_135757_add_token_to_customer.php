<?php

use yii\db\Schema;
use console\components\Migration;

class m141204_135757_add_token_to_customer extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%customer}}', 'password_reset_token', Schema::TYPE_STRING);
        $this->refreshTableSchema('{{%customer}}');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%customer}}', 'password_reset_token');
        $this->refreshTableSchema('{{%customer}}');
    }
}
