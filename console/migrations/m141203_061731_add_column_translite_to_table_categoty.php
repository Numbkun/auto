<?php

use yii\db\Schema;
use console\components\Migration;

class m141203_061731_add_column_translite_to_table_categoty extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%category}}', 'translite', Schema::TYPE_STRING . ' NOT NULL');
        $this->addColumn('{{%category}}', 'text', Schema::TYPE_STRING . ' NULL');
        $this->refreshTableSchema('{{%category}}');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%category}}', 'text');
        $this->dropColumn('{{%category}}', 'translite');
        $this->refreshTableSchema('{{%category}}');
    }
}

