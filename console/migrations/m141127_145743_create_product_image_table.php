<?php

use yii\db\Schema;
use console\components\Migration;

class m141127_145743_create_product_image_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = $this->getDefaultTableOptions();

        $this->createTable(
            '{{%product_image}}',
            [
                'id'               => Schema::TYPE_PK,
                'product_id'       => Schema::TYPE_INTEGER . ' NOT NULL',
                'file_id'          => Schema::TYPE_INTEGER . ' NOT NULL',
            ],
            $tableOptions
        );
        $this->addForeignKey('fk_product_image_file', '{{%product_image}}', 'file_id', '{{%file}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_product_product_image', '{{%product_image}}', 'product_id', '{{%product}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->refreshTableSchema('{{%product_image}}');

        $this->addColumn('{{%product}}', 'product_image_id', Schema::TYPE_INTEGER . ' NULL' );
        $this->addForeignKey('fk_product_main_image_product_image', '{{%product}}', 'product_image_id', '{{%product_image}}', 'id', 'RESTRICT', 'RESTRICT');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_product_main_image_product_image', '{{%product}}');
        $this->dropColumn('{{%product}}','product_image_id');

        $this->dropForeignKey('fk_product_image_file', '{{%product_image}}');
        $this->dropTable('{{%product_image}}');

    }
}
