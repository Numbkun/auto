<?php

use yii\db\Schema;
use console\components\Migration;

class m141202_111042_create_main_page_section extends Migration
{
    public function safeUp()
    {
        $tableOptions = $this->getDefaultTableOptions();

        $this->createTable(
            '{{%banner}}',
            [
                'id'            => Schema::TYPE_PK,
                'shop_id'       => Schema::TYPE_INTEGER  . ' NOT NULL',
                'name'          => Schema::TYPE_STRING   . ' NOT NULL',
                'url'           => Schema::TYPE_STRING   . ' NULL',
                'image_id'      => Schema::TYPE_INTEGER  . ' NOT NULL',
                'created_at'    => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
                'updated_at'    => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
            ],
            $tableOptions
        );
        $this->refreshTableSchema('{{%banner}}');

        $auth = $this->getAuthManager();

        $permision = $auth->createPermission('backend.banner.read');
        $permision->description = 'Баннер: Просмотр';
        $permision->is_system = false;
        $auth->add($permision);
        $permision = $auth->createPermission('backend.banner.edit');
        $permision->description = 'Баннер: Изменение';
        $permision->is_system = false;
        $auth->add($permision);

        // Таблица магазинов
        $this->addColumn('{{%product}}', 'special', Schema::TYPE_BOOLEAN . ' NULL');
        $this->addColumn('{{%product}}', 'new', Schema::TYPE_BOOLEAN . ' NULL');
        $this->refreshTableSchema('{{%product}}');
    }

    public function safeDown()
    {
        $this->dropTable('{{%banner}}');
        $auth = $this->getAuthManager();
        $auth->remove($auth->getPermission('backend.banner.read'));
        $auth->remove($auth->getPermission('backend.banner.edit'));
        $this->dropColumn('{{%product}}', 'special');
        $this->dropColumn('{{%product}}', 'new');
        $this->refreshTableSchema('{{%product}}');
    }
}
