<?php

use yii\db\Schema;
use console\components\Migration;

class m141117_101743_create_static_page_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = $this->getDefaultTableOptions();

        $this->createTable(
            '{{%static_page}}',
            [
                'id'            => Schema::TYPE_PK,
                'shop_id'       => Schema::TYPE_INTEGER . ' NOT NULL',
                'name'          => Schema::TYPE_STRING . ' NOT NULL',
                'translite'      => Schema::TYPE_STRING . ' NOT NULL',
                'description'   => Schema::TYPE_TEXT . ' NOT NULL',
                'text'          => Schema::TYPE_TEXT . ' NOT NULL',
                'created_at'    => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
                'updated_at'    => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
            ],
            $tableOptions
        );
        $this->refreshTableSchema('{{%static_page}}');

        $auth = $this->getAuthManager();

        $permision = $auth->createPermission('backend.static-page.read');
        $permision->description = 'Статические страницы: Просмотр';
        $permision->is_system = false;
        $auth->add($permision);
        $permision = $auth->createPermission('backend.static-page.edit');
        $permision->description = 'Статические страницы: Изменение';
        $permision->is_system = false;
        $auth->add($permision);
    }

    public function safeDown()
    {
        $this->dropTable('{{%static_page}}');
        $auth = $this->getAuthManager();
        $auth->remove($auth->getPermission('backend.static-page.read'));
        $auth->remove($auth->getPermission('backend.static-page.edit'));
    }
}
