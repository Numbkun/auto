<?php

use yii\db\Schema;
use console\components\Migration;

class m141224_075043_change_col_email_on_null_order extends Migration
{
    public function safeUp()
    {
        $this->db->createCommand('ALTER TABLE public.customer ALTER COLUMN email DROP NOT NULL;')->execute();
        $this->refreshTableSchema('{{%customer}}');
    }

    public function safeDown()
    {
        $this->db->createCommand('ALTER TABLE public.customer ALTER COLUMN email SET NOT NULL;')->execute();
        $this->refreshTableSchema('{{%customer}}');
    }
}
