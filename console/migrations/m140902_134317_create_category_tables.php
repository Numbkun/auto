<?php

use yii\db\Schema;
use console\components\Migration;

class m140902_134317_create_category_tables extends Migration
{
    public function safeUp()
    {
        $tableOptions = $this->getDefaultTableOptions();

        $this->createTable(
            '{{%category}}',
            [
                'id'                 => Schema::TYPE_PK,
                'shop_id'            => Schema::TYPE_INTEGER  . ' NOT NULL',
                'name'               => Schema::TYPE_STRING   . ' NOT NULL',
                'description'        => Schema::TYPE_TEXT     . ' NOT NULL',
                'parent_category_id' => Schema::TYPE_INTEGER  . ' NULL',
                'created_at'         => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
                'updated_at'         => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
            ],
            $tableOptions
        );
        $this->addForeignKey('fk_category_shop_id', '{{%category}}', 'shop_id', '{{%shop}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_category_parent_category_id', '{{%category}}', 'parent_category_id', '{{%category}}', 'id', 'RESTRICT', 'RESTRICT');

        $this->createTable(
            '{{%category_tree}}',
            [
                'parent_category_id' => Schema::TYPE_INTEGER . ' NOT NULL',
                'child_category_id' => Schema::TYPE_INTEGER . ' NOT NULL',
                'PRIMARY KEY (parent_category_id, child_category_id)'
            ],
            $tableOptions
        );
        $this->addForeignKey('fk_category_tree_parent_category_id', '{{%category_tree}}', 'parent_category_id', '{{%category}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_category_tree_child_category_id', '{{%category_tree}}', 'child_category_id', '{{%category}}', 'id', 'RESTRICT', 'RESTRICT');

        $this->createTable(
            '{{%category_tree_cache}}',
            [
                'parent_category_id' => Schema::TYPE_INTEGER . ' NOT NULL',
                'child_category_id' => Schema::TYPE_INTEGER . ' NOT NULL',
                'PRIMARY KEY (parent_category_id, child_category_id)'
            ],
            $tableOptions
        );
        $this->addForeignKey('fk_category_tree_cache_parent_category_id', '{{%category_tree_cache}}', 'parent_category_id', '{{%category}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_category_tree_cache_child_category_id', '{{%category_tree_cache}}', 'child_category_id', '{{%category}}', 'id', 'RESTRICT', 'RESTRICT');

        $this->refreshTableSchema('{{%category}}');
        $this->refreshTableSchema('{{%category_tree}}');
        $this->refreshTableSchema('{{%category_tree_cache}}');

        $auth = $this->getAuthManager();

        $permision = $auth->createPermission('backend.category.read');
        $permision->description = 'Категория: Просмотр';
        $permision->is_system = false;
        $auth->add($permision);
        $permision = $auth->createPermission('backend.category.edit');
        $permision->description = 'Категория: Изменение';
        $permision->is_system = false;
        $auth->add($permision);
    }

    public function safeDown()
    {
        $auth = $this->getAuthManager();
        $auth->remove($auth->getPermission('backend.category.read'));
        $auth->remove($auth->getPermission('backend.category.edit'));

        $this->dropForeignKey('fk_category_tree_cache_parent_category_id', '{{%category_tree_cache}}');
        $this->dropForeignKey('fk_category_tree_cache_child_category_id', '{{%category_tree_cache}}');
        $this->dropTable('{{%category_tree_cache}}');
        $this->dropForeignKey('fk_category_tree_parent_category_id', '{{%category_tree}}');
        $this->dropForeignKey('fk_category_tree_child_category_id', '{{%category_tree}}');
        $this->dropTable('{{%category_tree}}');
        $this->dropForeignKey('fk_category_parent_category_id', '{{%category}}');
        $this->dropForeignKey('fk_category_shop_id', '{{%category}}');
        $this->dropTable('{{%category}}');
    }
}
