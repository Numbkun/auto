<?php

use yii\db\Schema;
use console\components\Migration;

class m141119_120655_create_file_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = $this->getDefaultTableOptions();

        $this->createTable(
            '{{%file}}',
            [
                'id'            => Schema::TYPE_PK,
                'name'          => Schema::TYPE_STRING . ' NOT NULL',
                'extension'     => Schema::TYPE_STRING . ' NOT NULL',
                'path'          => Schema::TYPE_STRING . ' NOT NULL',
                'created_at'    => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
                'updated_at'    => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
            ],
            $tableOptions
        );
        $this->refreshTableSchema('{{%file}}');

        $this->addColumn('{{%dignity}}', 'image_id', Schema::TYPE_INTEGER . ' NULL');
        $this->addForeignKey('fk_dignity_image_id', '{{%dignity}}', 'image_id', '{{%file}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->refreshTableSchema('{{%dignity}}');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%dignity}}', 'image_id');
        $this->refreshTableSchema('{{%dignity}}');
        $this->dropTable('{{%file}}');
    }
}
