<?php

use yii\db\Schema;
use console\components\Migration;

class m140912_070826_create_product_type_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = $this->getDefaultTableOptions();

        $this->createTable(
            '{{%product_type}}',
            [
                'id'             => Schema::TYPE_PK,
                'name'           => Schema::TYPE_STRING . ' NOT NULL',
                'created_at'     => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
                'updated_at'     => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
            ],
            $tableOptions
        );

        $this->refreshTableSchema('{{%product_type}}');

        $auth = $this->getAuthManager();

        $permision = $auth->createPermission('backend.product-type.read');
        $permision->description = 'Типы товаров: Просмотр';
        $permision->is_system = true;
        $auth->add($permision);
        $permision = $auth->createPermission('backend.product-type.edit');
        $permision->description = 'Типы товаров: Изменение';
        $permision->is_system = true;
        $auth->add($permision);
    }

    public function safeDown()
    {
        $auth = $this->getAuthManager();
        $auth->remove($auth->getPermission('backend.product-type.read'));
        $auth->remove($auth->getPermission('backend.product-type.edit'));

        $this->dropTable('{{%product_type}}');
    }
}
