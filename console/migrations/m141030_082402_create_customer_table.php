<?php

use yii\db\Schema;
use console\components\Migration;

class m141030_082402_create_customer_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = $this->getDefaultTableOptions();

        $this->createTable(
            '{{%customer}}',
            [
                'id'            => Schema::TYPE_PK,
                'shop_id'       => Schema::TYPE_INTEGER . ' NOT NULL',
                'name'          => Schema::TYPE_STRING . ' NOT NULL',
                'first_name'    => Schema::TYPE_STRING . '(85) NOT NULL',
                'last_name'     => Schema::TYPE_STRING . '(85) NOT NULL',
                'patronymic'    => Schema::TYPE_STRING . '(85) NOT NULL',
                'email'         => Schema::TYPE_STRING . ' NOT NULL',
                'mobile'        => Schema::TYPE_STRING . '(15) NOT NULL',
                'is_active'     => Schema::TYPE_BOOLEAN . ' NOT NULL',
                'auth_key'      => Schema::TYPE_STRING . '(32) NOT NULL',
                'password_hash' => Schema::TYPE_STRING . ' NOT NULL',
                'created_at'    => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
                'updated_at'    => Schema::TYPE_DATETIME . ' with time zone NOT NULL',
            ],
            $tableOptions
        );
        $this->addForeignKey('fk_customer_shop_id', '{{%customer}}', 'shop_id', '{{%shop}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->refreshTableSchema('{{%customer}}');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_customer_shop_id', '{{%customer}}');
        $this->dropTable('{{%customer}}');
    }
}
