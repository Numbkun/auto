<?php

use console\components\Migration;
use common\components\DateTime;

class m150105_132942_add_col_phone_setting_table extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%setting}}',
            [
                'shop_id'    => 1,
                'name'       => 'shop_default_phone',
                'value'      => '4957966257',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]
        );
        $this->insert('{{%setting}}',
            [
                'shop_id'    => 1,
                'name'       => 'shop_secondary_phone',
                'value'      => '4957966257',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]
        );
    }

    public function safeDown()
    {
        $this->delete('{{%setting}}', ['shop_id' => 1, 'name' => 'shop_default_phone']);
        $this->delete('{{%setting}}', ['shop_id' => 1, 'name' => 'shop_secondary_phone']);
    }
}
