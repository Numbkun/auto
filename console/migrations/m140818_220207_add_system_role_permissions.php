<?php

use console\components\Migration;

class m140818_220207_add_system_role_permissions extends Migration
{
    public function safeUp()
    {
        $auth = $this->getAuthManager();

        $permision = $auth->createPermission('backend.system-role.read');
        $permision->description = 'Роли (все): Просмотр';
        $permision->is_system = true;
        $auth->add($permision);
        $permision = $auth->createPermission('backend.system-role.edit');
        $permision->description = 'Роли (все): Изменение';
        $permision->is_system = true;
        $auth->add($permision);
    }

    public function safeDown()
    {
        $auth = $this->getAuthManager();
        $auth->remove($auth->getPermission('backend.system-role.edit'));
        $auth->remove($auth->getPermission('backend.system-role.read'));
    }
}
