<?php

use yii\db\Schema;
use console\components\Migration;

class m160303_065936_add_text_field_banner_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%banner}}', 'text', Schema::TYPE_STRING);
        $this->refreshTableSchema('{{%banner}}');

        $this->addColumn('{{%product}}', 'in_stock', Schema::TYPE_BOOLEAN);
        $this->refreshTableSchema('{{%product}}');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%product}}','in_stock');
        $this->dropColumn('{{%banner}}','text');
    }
}
