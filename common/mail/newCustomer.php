<?php

/* @var $customer Customer */
/** @var string $password */

use common\models\Customer;
use yii\helpers\Url;

?>
Здравствуйте, <?= $customer->name ?> ! <br />

Вы успешно зарегестрированны на сайте <?= Url::base(true) ?>. <br />
Хотим сообщить Вам пароль. Вы можете поменять его в личном кабинете <br />
Ваш пароль: <?= $password ?>
<br />
-------------------------------------------------- <br />
<br />
С уважением, «<?= Yii::$app->name ?>» <br />
<?= Url::base(true) ?> <br />
<?= Yii::$app->params['supportEmail'] ?> <br />
8 (495) 796-62-57 <br />
Бесплатно для регионов: 9 899 000-33-33 <br />