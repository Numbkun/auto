<?php

/* @var $customer Customer */

use common\models\Customer;
use yii\helpers\Html;
use yii\helpers\Url;

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['cabinet/reset-password', 'token' => $customer->password_reset_token]);
?>
Здравствуйте, <?= $customer->name ?> ! <br />

Сообщаем Вам пароль для сайта <?= Url::base(true) ?>. <br />
Для смены пароля перейдите по ссылке: <?= Html::a(Html::encode($resetLink), $resetLink) ?> <br />
<br />
-------------------------------------------------- <br />
<br />
С уважением, «<?= Yii::$app->name ?>» <br />
<?= Url::base(true) ?> <br />
<?= Yii::$app->params['supportEmail'] ?> <br />
8 (495) 796-62-57 <br />
Бесплатно для регионов: 9 899 000-33-33 <br />