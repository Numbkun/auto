<?php

/* @var $customer Customer */
/** @var string $password */
/** @var string $orderNumber */

use common\models\Customer;
use yii\helpers\Url;

?>
Здравствуйте, <?= $customer->name ?> ! <br />

Ваш заказ в интернет-магазине <?= Yii::$app->name ?> №<?= $orderNumber ?> принят к обработке. <br />
В ближайшее время наш менеджер свяжется с Вами для уточнения деталей заказа.<br />
<br />
-------------------------------------------------- <br />
<br />
С уважением, «<?= Yii::$app->name ?>» <br />
<?= Url::base(true) ?> <br />
<?= Yii::$app->params['supportEmail'] ?> <br />
8 (495) 796-62-57 <br />
Бесплатно для регионов: 9 899 000-33-33 <br />