<?php

namespace common\models;

use backend\models\AuthItem;
use backend\models\User;
use Yii;
use yii\base\UserException;
use yii\caching\TagDependency;

/**
 * Модель магазина
 *
 * @property boolean    $is_predefined
 * @property integer    $theme_id
 * @property Theme      $theme
 * @property Domain[]   $domains
 * @property Setting[]  $settings
 * @property User[]     $users
 * @property AuthItem[] $authItems
 * @property integer    $currency_id идентификатор дефолтной валюты
 * @property Currency   $currency основная валюта для магазина
 * @property Region[]     $regions
 * @property ShopRegion[] $shopRegions
 */
class Shop extends Reference
{

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [self::SCENARIO_DEFAULT => ['name', 'theme_id', 'currency_id', '!is_predefined']];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['name'], 'string', 'max' => 255],
                [
                    ['name'],
                    'match',
                    'pattern' => '/^[a-zA-Z0-9]+$/',
                    'message' => 'Поле может содержать только латинские символы и цифры'
                ],
                [['is_predefined'], 'boolean'],
                [['currency_id'], 'default', 'value' => 1],
                [['currency_id'], 'integer'],
                [['theme_id'], 'integer'],
                [['theme_id'], 'exist', 'targetClass' => Theme::className(), 'targetAttribute' => 'id'],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'is_predefined' => 'Предопределенный',
                'theme_id'      => 'Идентификатор темы',
                'theme'         => 'Тема',
                'currency_id'   => 'Идентификатор валюты',
                'currency'      => 'Валюта',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        TagDependency::invalidate(Yii::$app->cache, '#table_cache#' . $this->tableName());
        $res = parent::beforeDelete();
        if ($res && $this->is_predefined) {
            throw new UserException('Нельзя удалить предопределенный магазин');
        }

        // Удаляем зависимые релейшины
        $relations = ['domains', 'settings', 'users'];
        foreach ($relations as $relation) {
            foreach ($this->$relation as $object) {
                /** @var \yii\db\ActiveRecord $object */
                $object->delete();
            };
        }

        // Удаляем зависимые роли и операции RBAC
        $authItems = $this->getAuthItems()->asArray()->all();
        $auth = Yii::$app->getAuthManager();
        foreach ($authItems as $authItem) {
            $item = $auth->getItem($authItem['name']);
            if ($item) {
                $auth->remove($item);
            }
        }

        return $res;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert) {
        TagDependency::invalidate(Yii::$app->cache, '#table_cache#' . $this->tableName());
        return parent::beforeSave($insert);
    }

    /**
     * Возвращает условия для выборки темы, которая привязана к магазину
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTheme()
    {
        return $this->hasOne(Theme::className(), ['id' => 'theme_id']);
    }

    /**
     * Возвращает условия для выборки доменов, которые привязана к магазину
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDomains()
    {
        return $this->hasMany(Domain::className(), ['shop_id' => 'id']);
    }

    /**
     * Возвращает условия для выборки настроек, которые привязана к магазину
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSettings()
    {
        return $this->hasMany(Setting::className(), ['shop_id' => 'id']);
    }

    /**
     * Возвращает условия для выборки пользователей, которые привязана к магазину
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['shop_id' => 'id']);
    }

    /**
     * Возвращает условия для выборки операций и ролей RBAC, которые привязана к магазину
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAuthItems()
    {
        return $this->hasMany(AuthItem::className(), ['shop_id' => 'id']);
    }

    /**
     * Возвращает условия для выборки экземпляра модели Валюты
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * Возвращает условия для выборки регионов для магазина
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShopRegions()
    {
        return $this->hasMany(ShopRegion::className(), ['shop_id' => 'id']);
    }

    /**
     * Возвращает условия для выборки регионов
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegions()
    {
        return $this->hasMany(Region::className(), ['id' => 'region_id'])->via('shopRegions');
    }

    /**
     * @inheritdoc
     */
    public static function getSingularNominativeName()
    {
        return 'Магазин';
    }

    /**
     * @inheritdoc
     */
    public static function getPluralNominativeName()
    {
        return 'Магазины';
    }

}
