<?php

namespace common\models;

use Yii;

/**
 * Модель типа товара
 */
class ProductType extends Reference
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['name'], 'string', 'max' => 255]
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public static function getSingularNominativeName()
    {
        return 'Тип товара';
    }

    /**
     * @inheritdoc
     */
    public static function getPluralNominativeName()
    {
        return 'Типы товаров';
    }
}
