<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * Модель элемента перечисления
 *
 * @property integer $id   идентификатор элемента
 * @property string  $name наименование элемента
 */
abstract class Enum extends BaseActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::attributeLabels(),
            [
                'id'   => 'ID',
                'name' => 'Наименование',
            ]
        );
    }

    /**
     * Магическая функция для приведения элемента к строке
     *
     * @return string
     */
    public function __toString()
    {
        return $this->isNewRecord ? '(новый)' : $this->name;
    }

    /**
     * @inheritdoc
     */
    public function searchDefaultOrder() {
        return ['name' => SORT_ASC];
    }

}
