<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * Модель дерева категорий
 *
 * @property integer  $parent_category_id идентификатор родительской категории
 * @property integer  $child_category_id  идентификатор дочерней категории
 * @property Category $parentCategory     экземпляр родительской категории
 * @property Category $childCategory      экземпляр дочерней категории
 */
class CategoryTree extends BaseActiveRecord
{
    /** Сценарий для установки родителя */
    const SCENARIO_PARENT = 'parent';
    /** Сценарий для установки дочерней категории */
    const SCENARIO_CHILD = 'child';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['parent_category_id', 'child_category_id'], 'required'],
                [['parent_category_id', 'child_category_id'], 'integer'],
                [
                    ['parent_category_id', 'child_category_id'],
                    'exist',
                    'skipOnError'     => true,
                    'targetClass'     => Category::className(),
                    'targetAttribute' => 'id'
                ],
                [
                    ['parent_category_id', 'child_category_id'],
                    'unique',
                    'targetAttribute' => ['parent_category_id', 'child_category_id'],
                    'skipOnError'     => true,
                ],
                [['parent_category_id'], 'validateParentCategory'],
            ]
        );
    }

    /**
     * Валидатор родительской категории
     */
    public function validateParentCategory()
    {
        if ($this->parent_category_id && !$this->hasErrors('parent_category_id')) {
            $child = $this->childCategory;
            $children = ArrayHelper::map(
                $child->getChildCategoriesCache()->select(['child_category_id'])->asArray()->all(),
                'child_category_id',
                'child_category_id'
            );
            if (isset($children[$this->parent_category_id])) {
                $this->addError('parent_category_id', 'Циклическая ссылка');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return array_merge(
            parent::scenarios(),
            [
                self::SCENARIO_CHILD  => ['child_category_id', '!parent_category_id'],
                self::SCENARIO_PARENT => ['parent_category_id', '!child_category_id'],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'parent_category_id' => 'Идентификатор родителя',
                'parentCategory'     => 'Родитель',
                'child_category_id'  => 'Идентификатор дочерней категории',
                'childCategory'      => 'Дочерняя категория',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function getFieldsOptions()
    {
        if ($this->_fieldsOptions === []) {
            parent::getFieldsOptions();
            $this->_fieldsOptions['parent_category_id'] = ['type' => 'reference'];
            $this->_fieldsOptions['child_category_id'] = ['type' => 'reference'];
        }
        return $this->_fieldsOptions;
    }

    /**
     * Возвращает условия для выбора родительской категории
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParentCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'parent_category_id']);
    }

    /**
     * Возвращает условия для выбора дочерней категории
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChildCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'child_category_id']);
    }

    /**
     * @inheritdoc
     */
    public static function getSingularNominativeName()
    {
        return 'Связь категорий';
    }

    /**
     * @inheritdoc
     */
    public static function getPluralNominativeName()
    {
        return 'Связи категорий';
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        CategoryTreeCache::updateCache($this->child_category_id);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();
        CategoryTreeCache::updateCache($this->child_category_id);
    }
}