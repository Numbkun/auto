<?php

namespace common\models;

/**
 * Модель связи товаров магазина с категориями
 *
 * @property integer  $shop_product_id идентификатор товара магазина
 * @property integer  $category_id     идентификатор категории
 * @property Category $shopProduct     товар магазина
 * @property Category $category        категория
 * @property Product $product          базовый товар
 */
class ShopProductCategory extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['shop_product_id', 'category_id', 'sort'], 'required'],
                [['shop_product_id', 'category_id', 'sort'], 'integer'],
                [
                    ['shop_product_id'],
                    'exist',
                    'skipOnError'     => true,
                    'targetClass'     => ShopProduct::className(),
                    'targetAttribute' => 'id'
                ],
                [
                    ['category_id'],
                    'exist',
                    'skipOnError'     => true,
                    'targetClass'     => Category::className(),
                    'targetAttribute' => 'id'
                ],
                [
                    ['shop_product_id', 'category_id'],
                    'unique',
                    'targetAttribute' => ['shop_product_id', 'category_id'],
                    'skipOnError'     => true,
                ],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return array_merge(
            parent::scenarios(),
            [
                self::SCENARIO_DEFAULT  => ['category_id', 'sort', '!shop_product_id'],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'shop_product_id' => 'Идентификатор товара',
                'shopProduct'     => 'Товар',
                'category_id'     => 'Идентификатор категории',
                'category'        => 'Категория',
                'sort'            => 'Сортировка',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function getFieldsOptions()
    {
        if ($this->_fieldsOptions === []) {
            parent::getFieldsOptions();
            $this->_fieldsOptions['shop_product_id'] = ['type' => 'hidden'];
        }
        return $this->_fieldsOptions;
    }

    /**
     * Возвращает условия для выбора товара магазина
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShopProduct()
    {
        return $this->hasOne(ShopProduct::className(), ['id' => 'shop_product_id']);
    }

    /**
     * Возвращает условия для выбора базового товара
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id'])->via('shopProduct');
    }

    /**
     * Возвращает условия для выбора категории
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @inheritdoc
     */
    public static function getSingularNominativeName()
    {
        return 'Дополнительная категория';
    }

    /**
     * @inheritdoc
     */
    public static function getPluralNominativeName()
    {
        return 'Дополнительные категории';
    }

}