<?php

namespace common\models;

use Yii;
use yii\caching\TagDependency;
use yii\db\ActiveQuery;

/**
 * Модель Домена
 */
class Domain extends ReferenceByShop
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['name'], 'string', 'max' => 253],
                [['name'], 'unique'],
                [
                    ['name'],
                    'match',
                    'pattern' => '/^([a-z0-9]([a-z0-9\-]{0,61}[a-z0-9])?\.)+[a-z0-9]([a-z0-9\-]{0,61}[a-z0-9])?$/i',
                    'message' => 'Доменное имя некорректно'
                ]
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return Yii::createObject(ActiveQuery::className(), [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public static function getSingularNominativeName()
    {
        return 'Домен';
    }

    /**
     * @inheritdoc
     */
    public static function getPluralNominativeName()
    {
        return 'Домены';
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert) {
        TagDependency::invalidate(Yii::$app->cache, '#table_cache#' . $this->tableName());
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete() {
        TagDependency::invalidate(Yii::$app->cache, '#table_cache#' . $this->tableName());
        return parent::beforeDelete();
    }
}
