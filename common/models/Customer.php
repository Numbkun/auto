<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;

/**
 * Модель покупателя
 *
 * @property string  $first_name    Имя
 * @property string  $last_name     Фамилия
 * @property string  $patronymic    Отчество
 * @property string  $mobile        Мобильный телефон
 * @property string  $email         E-mail
 * @property boolean $is_active     Активен
 * @property string  $shipping_address     Адрес доставки
 * @property string  $auth_key      Ключ аутентификации
 * @property string  $password_hash Хеш пароля
 * @property string  $password      Хеш пароля
 * @property Region  $region        Регион
 * @property Order[] $order         Заказы
 * @property string  $password_reset_token
 */
class Customer extends ReferenceByShop implements IdentityInterface
{

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'name'          => 'Имя Фамилия',
                'first_name'    => 'Имя',
                'last_name'     => 'Фамилия',
                'patronymic'    => 'Отчество',
                'mobile'        => 'Мобильный телефон',
                'email'         => 'E-mail',
                'shipping_address' => 'Адрес доставки',
                'is_active'     => 'Активен',
                'auth_key'      => 'Ключ аутентификации',
                'password_hash' => 'Хеш пароля',
                'password'      => 'Пароль',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['first_name', 'last_name', 'mobile',], 'required'],
                [['name', 'shipping_address'], 'string', 'max' => 255],
                [['first_name', 'last_name', 'patronymic'], 'string', 'max' => 85],
                [['mobile'], 'string', 'max' => 15],
                [['mobile'], 'integer'],
                [['email'], 'email', 'checkDNS' => true, 'enableIDN' => true, 'message' => 'Данный Email не существует'],
                [['is_active'], 'boolean'],
                [['email'], 'unique', 'targetAttribute' => ['email', 'shop_id']],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * Поиск пользователя по E-mail
     *
     * @param  string $email
     *
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'is_active' => true]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Проверка пароля
     *
     * @param  string $password пароль для проверки
     *
     * @return boolean
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
    }

    /**
     * Генерация ключа для функции "Запомнить меня"
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->getSecurity()->generateRandomString();
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->name = $this->first_name . ' ' . $this->last_name;
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public static function getSingularNominativeName()
    {
        return 'Покупатель';
    }

    /**
     * @inheritdoc
     */
    public static function getPluralNominativeName()
    {
        return 'Покупатели';
    }

    /**
     * Возвращает условия для выбора региона
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int)end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        return static::findOne([
            'password_reset_token' => $token,
        ]);
    }

    /**
     * Возвращает условия для выбора заказов
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['customer_id' => 'id']);
    }
}
