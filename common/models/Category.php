<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;

/**
 * Модель категории
 *
 * @property string              $description                описание категории
 * @property string              $text                       текст категории
 * @property string              $translite                  транслит названия категории
 * @property boolean             $is_list                    отображение товаров в категории
 * @property boolean             $in_catalog                 отображение категории в каталоге
 * @property integer             $parent_category_id         идентификатор родительской категории
 * @property Category            $activeCategory             Активная категория
 * @property Category            $parentCategory             Родительская категория
 * @property Category[]          $parentCategories           Все родительские категории (в том числе не прямые родители)
 * @property Category[]          $additionalParentCategories Дополнительные родительские категории
 * @property Category[]          $forwardChildCategories     Прямые дети категории
 * @property Category[]          $childCategories            Дочерних категории (в том числе не прямые)
 * @property Category[]          $products                   Получение товаров выбранной категории
 * @property CategoryTree[]      $childCategoriesTree        Модели дочерних веток дерева категорий
 * @property CategoryTree[]      $parentCategoriesTree       Модели родительских веток дерева категорий
 */
class Category extends ReferenceByShop
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['name', 'translite'],  'string', 'max' => 255],
                [['text'],  'string'],
                [['is_list', 'in_catalog'],  'boolean'],
                [['description'], 'safe'],
                [['parent_category_id'], 'integer'],
                [
                    ['parent_category_id'],
                    'exist',
                    'skipOnError'     => true,
                    'targetClass'     => self::className(),
                    'targetAttribute' => 'id'
                ],
                [['parent_category_id'], 'validateParentCategory'],
            ]
        );
    }

    /**
     * Валидатор родительской категории
     */
    public function validateParentCategory()
    {
        if ($this->parent_category_id && !$this->isNewRecord && !$this->hasErrors('parent_category_id')) {
            $children = ArrayHelper::map(
                $this->getChildCategoriesCache()->select(['child_category_id'])->asArray()->all(),
                'child_category_id',
                'child_category_id'
            );
            if (isset($children[$this->parent_category_id])) {
                $this->addError('parent_category_id', 'Циклическая ссылка');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'translite'          => 'ЧПУ',
                'text'               => 'Текст категории',
                'description'        => 'Описание',
                'parent_category_id' => 'Идентификатор родителя',
                'parentCategory'     => 'Родитель',
                'is_list'            => 'Отображение списком',
                'in_catalog'         => 'Показывать в каталоге',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public static function getSingularNominativeName()
    {
        return 'Категория';
    }

    /**
     * @inheritdoc
     */
    public static function getPluralNominativeName()
    {
        return 'Категории';
    }

    /**
     * Возвращает массив родительских категорий
     */
    public function getDirectParentCategories($category = null)
    {
        if($category === null){
            if(!$category = $this->parentCategory){
                return [];
            }
        }
        if($category->parentCategory === null){
            return [$category];
        }
        return array_merge($this->getDirectParentCategories($category->parentCategory), [$category]);
    }

    /**
     * Возвращает условия для отбора прямой родительской категории
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParentCategory()
    {
        return $this->hasOne(self::className(), ['id' => 'parent_category_id']);
    }

    /**
     * Возвращает условия для отбора прямых детей
     *
     * @return \yii\db\ActiveQuery
     */
    public function getForwardChildCategories()
    {
        return $this->hasMany(self::className(), ['parent_category_id' => 'id']);
    }

    /**
     * Возвращает условия для отбора всех родителей
     *
     * @return \yii\db\ActiveQuery
     */
    public static function getParentAllCategories()
    {
        return self::find(Category::className())->where(['parent_category_id' => null]);
    }

    /**
     * Возращает условия для выборки моделей веток дерева дополнительных родителей
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParentCategoriesTree()
    {
        return $this->hasMany(CategoryTree::className(), ['child_category_id' => 'id']);
    }

    /**
     * Возращает условия для выборки моделей веток дерева дополнительных детей
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChildCategoriesTree()
    {
        return $this->hasMany(CategoryTree::className(), ['parent_category_id' => 'id']);
    }

    /**
     * Возвращает условия для выборки дополнительных родителей (прямых)
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAdditionalParentCategories()
    {
        return $this->hasMany(self::className(), ['id' => 'parent_category_id'])->via('parentCategoriesTree');
    }


    /**
     * Возвращает условия для выборки дополнительных детей (прямых)
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAdditionalChildCategories()
    {
        return $this->hasMany(self::className(), ['id' => 'child_category_id'])->via('childCategoriesTree');
    }

    /**
     * Возвращает условия для выборки родительских моделей кеша дерева категорий
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParentCategoriesCache()
    {
        return $this->hasMany(CategoryTreeCache::className(), ['child_category_id' => 'id']);
    }

    /**
     * Возвращает условия для выборки дочерних моделей кеша дерева категорий
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChildCategoriesCache()
    {
        return $this->hasMany(CategoryTreeCache::className(), ['parent_category_id' => 'id']);
    }


    /**
     * Возвращает условия для выборки всех родительских категорий (в том числе не прямых)
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParentCategories()
    {
        return $this->hasMany(self::className(), ['id' => 'parent_category_id'])->via('parentCategoriesCache');
    }

    /**
     * Возвращает условия для выборки всех дочерних категорий (в том числе не прямых)
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChildCategories()
    {
        return $this->hasMany(self::className(), ['id' => 'child_category_id'])->via('childCategoriesCache');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])
            ->via('shopProducts');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopProducts()
    {
        return $this->hasMany(ShopProduct::className(), ['category_id' => 'id'])
            ->via('childCategories');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    static public function getRootCategories()
    {
        return self::find()->where('parent_category_id IS NULL')->andWhere(['in_catalog' => true]);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert
            || (array_key_exists('parent_category_id', $changedAttributes)
                && $this->parent_category_id != $changedAttributes['parent_category_id'])
        ) {
            CategoryTreeCache::updateCache($this->primaryKey);
        }
    }

    /**
     * @inheritdoc
     * Генерация транслита названия категории
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)){
            if(!$this->translite){
                $this->translite = Inflector::slug($this->name, '_');
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            foreach ($this->childCategoriesTree as $childTree) {
                $childTree->delete();
            }
            foreach ($this->parentCategoriesTree as $parentTree) {
                $parentTree->delete();
            }
            foreach ($this->forwardChildCategories as $child) {
                $child->delete();
            }
            CategoryTreeCache::clearCache($this->primaryKey);
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function getFieldsOptions()
    {
        if ($this->_fieldsOptions === []) {
            parent::getFieldsOptions();
            $this->_fieldsOptions['text'] = ['type' => 'html'];
        }
        return $this->_fieldsOptions;
    }
}
