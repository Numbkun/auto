<?php

namespace common\models;

use yii\base\InvalidParamException;
use yii\db\Query;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Модель кеша дерева категорий
 *
 * @property integer  $parent_category_id идентификатор родительской категории
 * @property integer  $child_category_id  идентификатор дочерней категории
 * @property Category $parentCategory     экземпляр родительской категории
 * @property Category $childCategory      экземпляр дочерней категории
 *
 * TODO покрыть тестами
 */
class CategoryTreeCache extends BaseActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['parent_category_id', 'child_category_id'], 'required'],
                [['parent_category_id', 'child_category_id'], 'integer'],
                [
                    ['parent_category_id', 'child_category_id'],
                    'exist',
                    'skipOnError' => true,
                    'targetClass' => Category::className(),
                    'targetAttribute' => 'id'
                ],
                [
                    ['parent_category_id', 'child_category_id'],
                    'unique',
                    'targetAttribute' => ['parent_category_id', 'child_category_id'],
                    'skipOnError'     => true,
                ],
            ]
        );
    }

    /**
     * Возвращает условия для выбора родительской категории
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParentCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'parent_category_id']);
    }

    /**
     * Возвращает условия для выбора дочерней категории
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChildCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'child_category_id']);
    }

    /**
     * Обновление кеша дерева категорий для указанной категории
     *
     * @param integer $categoryId идентификатор категории
     */
    public static function updateCache($categoryId)
    {
        // Сначала все надо удалить
        self::clearCache($categoryId);
        // Создать все поновой
        self::createCache($categoryId);
    }

    /**
     * Генерация кеша дерева категорий для указанной категории
     *
     * @param integer $categoryId идентификатор категории
     *
     * @throws \yii\base\InvalidParamException
     */
    public static function createCache($categoryId)
    {
        $category = Category::findOne($categoryId);
        if (!$category) {
            throw new InvalidParamException("Категория с идентификатором {$categoryId} не найдена");
        }

        // Создадим ссылку на себя
        $parents[$categoryId] = [$categoryId];

        // Получим родителей по прямому родителю
        if ($category->parent_category_id) {
            $parents += ArrayHelper::map(
                $category->parentCategory->getParentCategoriesCache()->select(['parent_category_id'])->asArray()->all(),
                'parent_category_id',
                'parent_category_id'
            );
        }
        // Получим родителей по дополнительным родителям
        foreach ($category->additionalParentCategories as $parentCategory) {
            $parents += ArrayHelper::map(
                $parentCategory->getParentCategoriesCache()->select(['parent_category_id'])->asArray()->all(),
                'parent_category_id',
                'parent_category_id'
            );
        }

        foreach ($parents as $parentId => $temp) {
            if (!self::find()->where(['parent_category_id' => $parentId, 'child_category_id' => $categoryId])->exists()
            ) {
                $model = new self;
                $model->child_category_id = $categoryId;
                $model->parent_category_id = $parentId;
                $model->save();
            }
        }

        // Запустим генерацию кеша по детям
        $children = ArrayHelper::map(
            $category->getForwardChildCategories()->select('id')->asArray()->all(),
            'id',
            'id'
        );
        $children += ArrayHelper::map(
            $category->getChildCategoriesTree()->select('child_category_id')->asArray()->all(),
            'child_category_id',
            'child_category_id'
        );
        foreach ($children as $childId => $childValue) {
            self::createCache($childId);
        }
    }

    /**
     * Очистка кеша дерева категорий для указанной категории
     *
     * @param integer $categoryId идентификатор категории
     */
    public static function clearCache($categoryId)
    {
        // Удалим ссылки на текущую категорию
        self::deleteAll(['child_category_id' => $categoryId]);
        // Удалим ссылки у детей
        $children = (new Query())
            ->select(['child_category_id'])
            ->from(self::tableName())
            ->where(['parent_category_id' => $categoryId])
            ->column();
        foreach ($children as $child) {
            self::clearCache($child);
        }
    }
}