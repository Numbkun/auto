<?php

namespace common\models;

use Yii;

/**
 * Модель баннера
 *
 * @property integer   $id  ID баннера
 * @property integer   $image_id  id картинки баннера
 * @property File      $image модель файла баннера
 * @property string    $url ссылка баннера
 * @property string    $text текст баннера
 *
 */
class Banner extends ReferenceByShop
{
    /**
     * @var \yii\web\UploadedFile
     */
    public $imageFile;
    private $_oldImageId;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['url'], 'url'],
                [['text'], 'string'],
                [['url'], 'required'],
                [['image_id'], 'integer'],
                [['imageFile'], 'file'],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'url'       => 'Ссылка баннера',
                'imageFile' => 'Картинка',
                'text'      => 'Текст баннера',
                'image_id'  => 'ID Картинки',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public static function getSingularNominativeName()
    {
        return 'Баннер';
    }

    /**
     * @inheritdoc
     */
    public static function getPluralNominativeName()
    {
        return 'Баннеры';
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)){

            if ($this->imageFile && !$this->imageFile->error) {
                if($this->image_id){
                    $this->_oldImageId = $this->image_id;
                }
                $file = new File();
                $file->uploadFile = $this->imageFile;
                $file->path = 'banner';
                $file->save();
                $this->image_id = $file->primaryKey;
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if($this->_oldImageId){
            File::findOne($this->_oldImageId)->delete();
        }
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();
        if($this->image_id){
            File::findOne($this->image_id)->delete();
        }
    }

    /**
     * @inheritdoc
     */
    public function getImage(){
        return $this->hasOne(File::className(), ['id' => 'image_id']);
    }

    /**
     * @inheritdoc
     */
    public function getFieldsOptions()
    {
        if ($this->_fieldsOptions === []) {
            parent::getFieldsOptions();
            $this->_fieldsOptions['imageFile'] = ['type' => 'file'];
            $this->_fieldsOptions['image_id'] = ['type' => 'readonly'];
            $this->_fieldsOptions['url'] = ['type' => 'url'];
        }
        return $this->_fieldsOptions;
    }
}
