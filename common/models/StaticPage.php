<?php

namespace common\models;

use Yii;
use yii\helpers\Inflector;

/**
 * Модель статической страницы магазина
 *
 * @property integer   $id  ID статической страницы
 * @property string    $description описание статической страницы
 * @property string    $text  текст статической страницы
 * @property string    $translite  транслит названия статической страницы
 *
 */
class StaticPage extends ReferenceByShop
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['description', 'text', 'translite'], 'string'],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'translite' => 'ЧПУ',
                'description' => 'Описание статической страницы',
                'text' => 'Текст статической страницы',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public static function getSingularNominativeName()
    {
        return 'Статическая страница';
    }

    /**
     * @inheritdoc
     */
    public static function getPluralNominativeName()
    {
        return 'Статические страницы';
    }

    /**
     * @inheritdoc
     * Генерация транслита названия страницы
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)){
            if(!$this->translite){
                $this->translite = Inflector::slug($this->name, '_');
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function getFieldsOptions()
    {
        if ($this->_fieldsOptions === []) {
            parent::getFieldsOptions();
            $this->_fieldsOptions['text'] = ['type' => 'html'];
            $this->_fieldsOptions['description'] = ['type' => 'html'];
        }
        return $this->_fieldsOptions;
    }
}
