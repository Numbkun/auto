<?php

namespace common\models;

use Yii;


/**
 * Модель карзины покупок
 * @property integer          $customer_id  идентификатор покупателя
 * @property integer          $sku_id       идентификатор модификации товара
 * @property integer          $count        кол-во покупаемого товара
 * @property Sku              $sku          модификация товара
 * @property Customer         $customer     покупатель
 * @property SkuParameterPrice $skuParameterPrice     цена товара
 * @property Product           $product               товар
 * @property Currency          $priceCurrency         валюта товара
 *
 */
class Cart extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['customer_id', 'sku_id',], 'required'],
                [['customer_id', 'sku_id', 'count',], 'integer'],
                [['count'], 'default', 'value' => 1],
                [
                    ['customer_id'],
                    'exist',
                    'skipOnError' => true,
                    'targetClass' => Customer::className(),
                    'targetAttribute' => 'id'
                ],
                [
                    ['sku_id'],
                    'exist',
                    'skipOnError' => true,
                    'targetClass' => Sku::className(),
                    'targetAttribute' => 'id'
                ],
            ]
        );
    }

    /**
     * Возвращает условия для отбора покупателя
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * Возвращает условия для отбора модификации товара
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSku()
    {
        return $this->hasOne(Sku::className(), ['id' => 'sku_id']);
    }

    /**
     * Возвращает условия для отбора цены товара
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSkuParameterPrice()
    {
        return $this->hasOne(SkuParameterPrice::className(), ['sku_id' => 'id'])->via('sku');
    }

    /**
     * Возвращает условия для отбора товара
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id'])->via('sku');
    }

}