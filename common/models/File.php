<?php

namespace common\models;

use common\components\PathFile;
use Yii;
use yii\base\Exception;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Imagine\Image\Color;

/**
 * Модель файла
 *
 * @property string                 $path путь для хранения файла
 * @property string                 $extension расширение файла
 * @property UploadedFile|PathFile  $uploadFile загружаемый файл
 */
class File extends Reference
{
    const NOIMAGE = 1;
    /**
     * @var UploadedFile|PathFile загружаемый файл
     */
    protected $uploadFile = null;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['name'], 'string', 'max' => 255],
                [['extension'], 'required'],
                [['extension'], 'string', 'max' => 10],
                [['uploadFile'], 'file'],
                [
                    ['extension'],
                    'filter',
                    'filter' => function ($ext) {
                        return strtolower(trim($ext));
                    }
                ],
                [['path'], 'default', 'value' => ''],
                [
                    ['uploadFile'],
                    'required',
                    'when' => function () {
                        return $this->isNewRecord;
                    }
                ],
                [['uploadFile'], 'validateUploadFile'],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function getFieldsOptions()
    {
        return array_merge(parent::getFieldsOptions(), [
            'uploadFile' => ['type' => 'file'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function getSingularNominativeName()
    {
        return 'Загрузка файла';
    }

    /**
     * @inheritdoc
     */
    public static function getPluralNominativeName()
    {
        return 'Загрузка файлов';
    }

    /**
     * Валидация поля uploadFile
     */
    public function validateUploadFile()
    {
        $uploadFile = $this->uploadFile;
        if ($uploadFile) {
            if (!($uploadFile instanceof UploadedFile || $uploadFile instanceof PathFile)) {
                $this->addError('uploadFile', 'UploadFile должен быть экземпляром UploadedFile или PathFile');
            }
        }
    }

    /**
     * Установка поля uploadFile
     *
     * @param UploadedFile|PathFile $value
     */
    public function setUploadFile($value)
    {
        if ($this->uploadFile != $value) {
            if ($value instanceof UploadedFile || $value instanceof PathFile) {
                $this->name = $value->name;
                $this->extension = $value->extension;
                $this->uploadFile = $value;
            }
        }
    }

    /**
     * Возвращает значение поля uploadFile
     *
     * @return PathFile|UploadedFile
     */
    public function getUploadFile()
    {
        return $this->uploadFile;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'name' => 'Имя файла',
                'extension' => 'Расширение',
                'uploadFile' => 'Загружаемый файл',
            ]
        );
    }

    /**
     * Возвращает путь до папки с оригинальным изображением
     *
     * @return string
     */
    public function getOriginalDir()
    {
        return Yii::getAlias('@uploads/' . ltrim($this->path . '/', '/'));
    }

    /**
     * Возвращает путь до оригинального изображения
     *
     * @return string
     */
    public function getOriginalPath()
    {
        return $this->getOriginalDir() . "{$this->id}.{$this->extension}";
    }

    /**
     * @param UploadedFile|PathFile $value
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            if ($this->uploadFile) {
                $this->name = $this->uploadFile->name;
                $this->extension = $this->uploadFile->extension;
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($this->uploadFile) {
            FileHelper::createDirectory($this->getOriginalDir());
            $this->uploadFile->saveAs($this->getOriginalPath());
        }
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();
        FileHelper::removeDirectory(
            Yii::getAlias('@backend/web/assets/thumb/' . ltrim($this->path . '/', '/') . $this->primaryKey)
        );
        FileHelper::removeDirectory(
            Yii::getAlias('@frontend/web/assets/thumb/' . ltrim($this->path . '/', '/') . $this->primaryKey)
        );
        if (!unlink($this->getOriginalPath())) {
            throw new Exception('Не удалось удалить файл изображения');
        }
    }

    /**
     * Публикация изображения
     *
     * @param integer $width ширина изображения, если не задано будет использована ширина исходного изображения
     * @param integer $height высота изображения, если не задано будет использована высота исходного изображения
     * @param string $background цвет фона
     * @param integer $alpha прозрачность фона
     *
     * @return string путь к опубликованному изображению
     */
    public function publishImage($width = null, $height = null, $background = 'fff', $alpha = 100)
    {
        if (!$width || !$height) {
            $size = getimagesize($this->getOriginalPath());
            if (!$width) {
                $width = $size[0];
            }
            if (!$height) {
                $height = $size[1];
            }
        }

        $backgroundColor = new Color($background, $alpha);

        $subdir = ltrim($this->path . '/', '/');
        $publishFileName = "{$width}x{$height}_" . substr((string)$backgroundColor, 1) . "_{$alpha}.{$this->extension}";
        $publishDir = Yii::getAlias("@webroot/assets/thumb/{$subdir}{$this->primaryKey}");
        $publishPath = $publishDir . '/' . $publishFileName;
        $publishUrl = Yii::getAlias("@web/assets/thumb/{$subdir}{$this->primaryKey}/{$publishFileName}");

        if (file_exists($publishPath)) {
            return $publishUrl;
        }
        FileHelper::createDirectory($publishDir);
        $box = new Box($width, $height);
        $thumb = Image::getImagine()->create($box, $backgroundColor);
        $img = Image::getImagine()->open($this->getOriginalPath());
        $size = $img->getSize();

        if ($size->getWidth() > $width || $size->getHeight() > $height) {
            $ratio = min([$width / $size->getWidth(), $height / $size->getHeight()]);
            $resizeBox = $size->scale($ratio);
            $img->resize($resizeBox);
            $size = $img->getSize();
        }

        $startX = 0;
        $startY = 0;
        if ($size->getWidth() < $width) {
            $startX = ceil($width - $size->getWidth()) / 2;
        }
        if ($size->getHeight() < $height) {
            $startY = ceil($height - $size->getHeight()) / 2;
        }
        $thumb->paste($img, new Point($startX, $startY));
        $thumb->save($publishPath);
        return $publishUrl;
    }

    /**Перенос картинки с upload в assets на frontend
     *
     * @return bool|string $publishUrl url картинки во frontend
     */
    public function publish()
    {
        $subdir = ltrim($this->path . '/', '/');
        $publishDir = Yii::getAlias("@frontend/web/assets/{$subdir}");
        $publishPath = $publishDir . '/' . $this->primaryKey . '.' . $this->extension;
        $publishUrl = Yii::getAlias("/assets/{$subdir}{$this->primaryKey}.{$this->extension}");
        if (file_exists($publishPath)) {
            return $publishUrl;
        }
        FileHelper::createDirectory($publishDir);
        copy($this->getOriginalPath(),$publishPath);
        return $publishUrl;
    }
}