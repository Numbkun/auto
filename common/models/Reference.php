<?php

namespace common\models;

use common\components\DateTime;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * Модель элемента справочника
 *
 * @property integer  $id         идентификатор объекта
 * @property string   $name       наименование объекта
 * @property DateTime $created_at дата и время создания объекта
 * @property DateTime $updated_at дата и время последнего изменения объекта
 */
abstract class Reference extends BaseActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'nameRequired' => [['name'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'timestamp' => [
                    'class' => 'yii\behaviors\TimestampBehavior',
                    'value' => new Expression('NOW()'),
                ],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function getFieldsOptions()
    {
        if ($this->_fieldsOptions === []) {
            parent::getFieldsOptions();
            $this->_fieldsOptions['created_at'] = ['type' => 'datetime'];
            $this->_fieldsOptions['updated_at'] = ['type' => 'datetime'];
        }
        return $this->_fieldsOptions;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::attributeLabels(),
            [
                'id'         => 'ID',
                'name'       => 'Наименование',
                'created_at' => 'Дата создания',
                'updated_at' => 'Дата изменения',
            ]
        );
    }

    /**
     * Магическая функция для приведения объекта к строке
     * @return string
     */
    public function __toString()
    {
        return $this->isNewRecord ? '(новый)' : $this->name;
    }

    /**
     * @inheritdoc
     */
    public function searchDefaultOrder() {
        return ['name' => SORT_ASC];
    }
}
