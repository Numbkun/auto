<?php

namespace common\models;

use Yii;

/**
 * Модель дополняющего товара магазина
 *
 * @property integer    $product_id  идентификатор базового товара
 * @property Product    $product     базовый товар
 * @property integer    $complementary_product_id Идентификатор дополняющего товара
 */
class ComplementaryProduct extends BaseActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['product_id'], 'required'],
                [['product_id', 'complementary_product_id'], 'integer'],
                [
                    ['product_id'],
                    'exist',
                    'skipOnError'     => true,
                    'targetClass'     => Product::className(),
                    'targetAttribute' => 'id'
                ],
                [
                    ['complementary_product_id'],
                    'exist',
                    'skipOnError'     => true,
                    'targetClass'     => Product::className(),
                    'targetAttribute' => 'id'
                ],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'complementary_product_id' => 'Идентификатор дополняющего товара',
                'complementaryProduct'     => 'Дополняющий товар',
                'product_id'               => 'Идентификатор базового товара',
                'product'                  => 'Базовый товар',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public static function getSingularNominativeName()
    {
        return 'С этим товаром покупают';
    }

    /**
     * @inheritdoc
     */
    public static function getPluralNominativeName()
    {
        return 'С этими товарами покупают';
    }

    /**
     * Возвращает условия для отбора базового товара
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * Возвращает условия для отбора дополняющего товара
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComplementaryProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'complementary_product_id']);
    }

}
