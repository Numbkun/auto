<?php

namespace common\models;

use Yii;

/**
 * Модель производителя
 *
 * @property integer   $id  ID производителя
 * @property integer   $image_id  id картинки производителя
 * @property File      $image модель файла картинки
 * @property string    $description
 */
class Manufacture extends Reference
{

    /**
     * @var \yii\web\UploadedFile
     */
    public $imageFile;
    private $_oldImageId;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['name'], 'string', 'max' => 255],
            [['description'], 'safe'],
            [['imageFile'], 'file'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'description' => 'Описание',
                'imageFile' => 'Картинка',
                'image' => 'Картинка',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public static function getSingularNominativeName() {
        return 'Производитель';
    }

    /**
     * @inheritdoc
     */
    public static function getPluralNominativeName() {
        return 'Производители';
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)){
            if ($this->imageFile && !$this->imageFile->error) {
                if($this->image_id){
                    $this->_oldImageId = $this->image_id;
                }
                $file = new File();
                $file->uploadFile = $this->imageFile;
                $file->path = 'manufacture';
                $file->save();
                $this->image_id = $file->primaryKey;
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if($this->_oldImageId){
            File::findOne($this->_oldImageId)->delete();
        }
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();
        if($this->image_id){
            File::findOne($this->image_id)->delete();
        }
    }

    /**
     * @inheritdoc
     */
    public function getImage(){
        return $this->hasOne(File::className(), ['id' => 'image_id']);
    }

    /**
     * @inheritdoc
     */
    public function getFieldsOptions()
    {
        if ($this->_fieldsOptions === []) {
            parent::getFieldsOptions();
            $this->_fieldsOptions['imageFile'] = ['type' => 'file'];
        }
        return $this->_fieldsOptions;
    }
}
