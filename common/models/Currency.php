<?php

namespace common\models;

use Yii;

/**
 * Модель валюты
 *
 * @property string $short_name сокращениe валюты
 */
class Currency extends Reference
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['name', '$short_name'], 'string', 'max' => 255],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public static function getSingularNominativeName()
    {
        return 'Валюта';
    }

    /**
     * @inheritdoc
     */
    public static function getPluralNominativeName()
    {
        return 'Валюты';
    }

}
