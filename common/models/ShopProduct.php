<?php

namespace common\models;

use Yii;

/**
 * Модель товара магазина
 *
 * @property integer    $product_id  идентификатор базового товара
 * @property Product    $product     базовый товар
 * @property integer    $category_id идентификатор основной категории
 * @property Category   $category    основная категория
 * @property Category[] $additionalCategories дополнительные категории
 * @property integer    $description описание
 */
class ShopProduct extends ReferenceByShop
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['name'], 'string', 'max' => 255],
                [['description'], 'safe'],
                [['product_id', 'category_id'], 'required'],
                [['product_id', 'category_id'], 'integer'],
                [
                    ['product_id'],
                    'exist',
                    'skipOnError'     => true,
                    'targetClass'     => Product::className(),
                    'targetAttribute' => 'id'
                ],
                [
                    ['category_id'],
                    'exist',
                    'skipOnError'     => true,
                    'targetClass'     => Category::className(),
                    'targetAttribute' => 'id'
                ],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'description' => 'Описание',
                'category_id' => 'Идентификатор категории',
                'category'    => 'Категория',
                'product_id'  => 'Идентификатор базового товара',
                'product'     => 'Базовый товар',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public static function getSingularNominativeName()
    {
        return 'Товар магазина';
    }

    /**
     * @inheritdoc
     */
    public static function getPluralNominativeName()
    {
        return 'Товары магазина';
    }

    /**
     * Возвращает условия для отбора основоной категории
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * Возвращает условия для отбора базового товара
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * Возвращает условия для отбора связей товара с дополнительными категориями
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShopProductCategories() {
        return $this->hasMany(ShopProductCategory::className(), ['shop_product_id' => 'id']);
    }

    /**
     * Возвращает условия для отбора дополнительных категорий
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAdditionalCategories() {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])->via('shopProductCategories');
    }

    /**
     * Возвращает условия для выбора цены товара магазина
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShopProductPrices()
    {
        return $this->hasMany(ShopProductPrice::className(), ['shop_product_id' => 'id']);
    }

    /**Очистка всхех ключей для удаления товара
     *
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $relations = ['shopProductPrices',];
            foreach ($relations as $relation) {
                $relation = $this->$relation;
                if (is_array($relation)) {
                    foreach ($relation as $item) {
                        /** @var BaseActiveRecord $item */
                        $item->delete();
                    }
                } elseif($relation) {
                    /** @var BaseActiveRecord $relation */
                    $relation->delete();
                }
            }
            return true;
        }
        return false;
    }

}
