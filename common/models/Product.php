<?php

namespace common\models;

use Yii;

/**
 * Модель базового товара
 *
 * @property integer          $manufacture_id  идентификатор производителя
 * @property Manufacture      $manufacture     производитель
 * @property integer          $product_type_id идентификатор типа товара
 * @property boolean          $new             новый товар
 * @property boolean          $special         акционный товар
 * @property boolean          $in_stock        в наличии
 * @property string           $article         артикул товар
 * @property ProductType      $productType     тип товара
 * @property string           $description     описание товара
 * @property number rating Рейтинг продукта
 * @property integer rating_count Рейтинг который поставил пользователь продукту
 * @property File               $image модель файла картинки
 * @property integer            $product_image_id модель файла картинки
 * @property ShopProduct        $shopProduct        Товар магазина
 * @property string             $price              Цена товара
 * @property ShopProductPrice[] $shopProductPrices  Массив Цен товара
 * @property Product[]          $complementaryProducts  Массив дополняющих товаров
 * @property Product[]          $similarProducts  Массив похлжих товаров
 * @property File               $mainImage  Основная картинка товара
 * @property File[]             $images Массив картинок ??
 * @property ProductImage[]     $productImages Массив какртинок к товару
 * @property ProductImage       $productImage  главная картинка товара
 * @property Category           $category      категория товара
 *
 */
class Product extends Reference
{

    /**
     * $user_rating локальная переменная для записи выбора рейтинга пользователем
     */
    public $user_rating;

    /**
     * $category_id локальная переменная для записи выбранной категории пользователем
     */
    public $category_id;

    /**
     * Сценарий для полей используемых при изменении рейтинга
     */
    const SCENARIO_RATING = 'rating';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['name'], 'string', 'max' => 255],
                [['description'], 'safe'],
                [['new', 'special', 'in_stock'], 'boolean'],
                [['article'], 'string'],
                [['rating'], 'number', 'min' => 0, 'max' => 5],
                [['rating_count', 'price', 'discount'], 'integer'],
                [['user_rating'], 'integer', 'min' => 0, 'max' => 5],
                [['manufacture_id', 'product_type_id', 'category_id'], 'required'],
                [['manufacture_id', 'product_type_id', 'category_id', 'product_image_id'], 'integer'],
                [
                    ['manufacture_id'],
                    'exist',
                    'skipOnError'     => true,
                    'targetClass'     => Manufacture::className(),
                    'targetAttribute' => 'id'
                ],
                [
                    ['product_type_id'],
                    'exist',
                    'skipOnError'     => true,
                    'targetClass'     => ProductType::className(),
                    'targetAttribute' => 'id'
                ],
                [
                    ['product_image_id'],
                    'exist',
                    'skipOnError'     => true,
                    'targetClass'     => ProductImage::className(),
                    'targetAttribute' => 'id'
                ],
                [
                    ['category_id'],
                    'exist',
                    'skipOnError'     => true,
                    'targetClass'     => Category::className(),
                    'targetAttribute' => 'id'
                ],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'description'     => 'Описание',
                'manufacture_id'  => 'Идентификатор производителя',
                'manufacture'     => 'Производитель',
                'product_type_id' => 'Идентификатор типа товара',
                'productType'     => 'Тип товара',
                'article'         => 'Артикул',
                'rating'          => 'Рейтинг',
                'rating_count'    => 'Кол-во проголосовавший',
                'category_id'     => 'Идентификатор категории',
                'category'        => 'Категория',
                'new'             => 'Новый',
                'special'         => 'Акция',
                'price'           => 'Цена',
                'discount'        => 'Скидка',
                'in_stock'        => 'В наличии',
            ]
        );
    }

    /**
     * Сценарий для полей используемых при изменении рейтинга
     */
    public function scenarios()
    {
        return array_merge(
            parent::scenarios(),
            [
                self::SCENARIO_RATING => ['user_rating', '!rating', '!rating_count'],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public static function getSingularNominativeName()
    {
        return 'Базовый товар';
    }

    /**
     * @inheritdoc
     */
    public static function getPluralNominativeName()
    {
        return 'Базовые товары';
    }

    /**
     * Возвращает условия для отбора производителя
     *
     * @return \yii\db\ActiveQuery
     */
    public function getManufacture()
    {
        return $this->hasOne(Manufacture::className(), ['id' => 'manufacture_id']);
    }

    /**
     * Возвращает условия для отбора типа товара
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductType()
    {
        return $this->hasOne(ProductType::className(), ['id' => 'product_type_id']);
    }

    /**
     * Возвращает условия для отбора товара магазина
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShopProduct()
    {
        return $this->hasOne(ShopProduct::className(), ['product_id' => 'id']);
    }

    /**
     * Возвращает условия для отбора дополняющих товаров к базовому товару
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComplementaryProduct()
    {
        return $this->hasMany(ComplementaryProduct::className(), ['product_id' => 'id']);
    }

    /**
     * Возвращает условия для отбора дополняющих товаров к базовому товару
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComplementaryProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'complementary_product_id'])->via('complementaryProduct');
    }

    /**
     * Возвращает условия для отбора дополняющих товаров к родительскому базовому товару
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParentComplementaryProducts()
    {
        return $this->hasMany(ComplementaryProduct::className(), ['complementary_product_id' => 'id']);
    }

    /**
     * Возвращает условия для отбора похожих товаров с базовым товаром
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSimilarProducts()
    {
        $nextProducts = Product::find()
            ->where(['>', 'id' , $this->id])
            ->andWhere(['product_type_id' => $this->product_type_id])
            ->orderBy(['id' => SORT_ASC])
            ->limit(3)
            ->all();
        $prevProducts = Product::find()
            ->where(['<','id', $this->id])
            ->andWhere(['product_type_id' => $this->product_type_id])
            ->orderBy(['id' => SORT_DESC])
            ->limit(3)
            ->all();
        $similarProducts = array_merge($prevProducts, $nextProducts);
        return $similarProducts;
    }

    /**
     * Возвращает условия для отбора основоной категории
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }


    /**
     * Возвращает условия для отбора связей товара с дополнительными категориями
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShopProductCategories()
    {
        return $this->hasMany(ShopProductCategory::className(), ['shop_product_id' => 'id']);
    }

    /**
     * Возвращает условия для отбора дополнительных категорий
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAdditionalCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])->via('shopProductCategories');
    }

//    /**
//     * Возвращает условия для отбора Цены товара
//     *
//     * @return \yii\db\ActiveQuery
//     */
//    public function getPrice()
//    {
//        return $this->hasOne(ShopProductPrice::className(),
//            ['shop_product_id' => 'id'])->via('shopProduct')->where(['region_id' => Yii::$app->user->getRegion()->primaryKey]);
//    }
//
//    /**
//     * Возвращает условия для отбора связей товара с ценами
//     *
//     * @return \yii\db\ActiveQuery
//     */
//    public function getShopProductPrices()
//    {
//        return $this->hasMany(ShopProductPrice::className(), ['shop_product_id' => 'id'])->via('shopProduct');
//    }

    /**
     * Возвращает условия для отбора новых товаров
     *
     * @return \yii\db\ActiveQuery
     */
    static public function getNewProducts()
    {
        $newProducts = Product::find()
            ->Where(['new' => true])
            ->orderBy(['id' => SORT_DESC]);
        return $newProducts;
    }

    /**
     * Возвращает условия для отбора товаров
     *
     * @return \yii\db\ActiveQuery
     */
    static public function getProducts()
    {
        $products = Product::find()
            ->innerJoin(ShopProduct::tableName(), ShopProduct::tableName() . '.product_id = ' . Product::tableName() . '.id')
            ->innerJoin(Category::tableName() . ' c', 'c.id = ' . ShopProduct::tableName() . '.category_id')
            ->andWhere('c.is_list=false');
        return $products;
    }

    /**
     * Возвращает условия для отбора услуг
     *
     * @return \yii\db\ActiveQuery
     */
    static public function getServices()
    {
        $products = Product::find()
            ->innerJoin(ShopProduct::tableName(), ShopProduct::tableName() . '.product_id = ' . Product::tableName() . '.id')
            ->innerJoin(Category::tableName() . ' c', 'c.id = ' . ShopProduct::tableName() . '.category_id')
            ->andWhere('c.is_list=true');
        return $products;
    }

    /**
     * Возвращает условия для отбора акционных товаров
     *
     * @return \yii\db\ActiveQuery
     */
    static public function getSpecialProducts()
    {
        $specialProducts = Product::find()
            ->Where(['special' => true])
            ->orderBy(['id' => SORT_DESC]);
        return $specialProducts;
    }

    /**
     * Если пользователь выставил рейтинг, то пересчитываем на новое значение
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            if ($this->user_rating) {
                $this->rating = ($this->rating * $this->rating_count + $this->user_rating) / ($this->rating_count + 1);
                $this->rating_count = $this->rating_count + 1;
            }
            return true;
        }
        return false;
    }

    /**
     * Скрываем поля, пользователь не может их менять в ручную
     */
    public function getFieldsOptions()
    {
        if ($this->_fieldsOptions === []) {
            parent::getFieldsOptions();
            $this->_fieldsOptions['rating_count'] = ['type' => 'readonly'];
            $this->_fieldsOptions['rating'] = ['type' => 'readonly'];
            $this->_fieldsOptions['user_rating'] = ['type' => 'ignore'];
            $this->_fieldsOptions['product_image_id'] = ['type' => 'ignore'];
        }
        return $this->_fieldsOptions;
    }

    /**
     * Дублирование данных в таблицу shop_product
     *
     * @param bool $insert
     * @param array $changedAttributes
     * @throws \Exception
     * @throws \yii\base\Exception
     * @throws \yii\base\UserException
     * @throws null
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            $model = new ShopProduct;
        } else {
            $model = $this->shopProduct;
        }
        $model->name = $this->name;
        $model->product_id = $this->id;
        $model->category_id = $this->category_id;
        $model->description = $this->description;
        $model->save();
    }

    /**
     * Заполнение локального поля category_id
     *
     */
    public function afterFind()
    {
        parent::afterFind();
        if ($this->shopProduct) {
            $this->category_id = $this->shopProduct->category_id;
        }
    }

    /**Очистка всхех ключей для удаления товара
     *
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $relations = ['shopProduct', 'parameterValues', 'complementaryProduct', 'parentComplementaryProducts'];
            foreach ($relations as $relation) {
                $relation = $this->$relation;
                if (is_array($relation)) {
                    foreach ($relation as $item) {
                        /** @var BaseActiveRecord $item */
                        $item->delete();
                    }
                } elseif($relation) {
                    /** @var BaseActiveRecord $relation */
                    $relation->delete();
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Получаем массив картинок товара
     */
    public function getImages()
    {
        return $this->hasMany(File::className(), ['id' => 'file_id'])
            ->viaTable('product_image', ['product_id' => 'id']);
    }

    /**
     * Получаем связи с картинками из модели товара
     */
    public function getProductImages()
    {
        return $this->hasMany(ProductImage::className(), ['product_id' => 'id']);
    }

    /**
     * Получаем главную картинок из модели товара
     */
    public function getProductImage()
    {
        return $this->hasOne(ProductImage::className(), ['id' => 'product_image_id']);
    }

    /**
     * Получить главную картинку из модели File
     *если картинки нет, то выводится дефолтная картинка
     */
    public function getMainImage()
    {
        if($this->product_image_id){
            $mainImage = $this->hasOne(File::className(), ['id' => 'file_id'])
                ->via('productImage');
        } else {
            $mainImage = File::find()->andWhere(['id' => File::NOIMAGE]);
        }
        return $mainImage;
    }

    /**
     * Получаем главную картинок из модели товара
     */
    public function getNoImageProduct()
    {
        return $this->hasOne(ProductImage::className(), ['id' => 'product_image_id']);
    }
}
