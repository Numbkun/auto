<?php

namespace common\models;

use Exception;
use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use ZipArchive;

/**
 * Модель темы
 *
 * @property Shop[] $shops
 */
class Theme extends Reference
{

    /** @var UploadedFile архив с темой */
    public $archive;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['name'], 'string', 'max' => 64],
            [
                ['name'],
                'match',
                'pattern' => '/^[a-z0-9]+$/',
                'message' => 'Поле может содержать только латинские символы в нижнем регистре и цифры'
            ],
            [['name'], 'unique'],
            [['archive'], 'file', 'extensions' => ['zip', 'gz']]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'archive' => 'Архив',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function getFieldsOptions()
    {
        if ($this->_fieldsOptions === []) {
            parent::getFieldsOptions();
            $this->_fieldsOptions['archive'] = ['type' => 'file'];
        }
        return $this->_fieldsOptions;
    }

    /**
     * Возвращает магазины, к которым привязана тема
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShops()
    {
        return $this->hasMany(Shop::className(), ['theme_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        $res = parent::beforeSave($insert);
        if ($res) {
            if ($this->isAttributeChanged('name') && $this->getOldAttribute('name')) {
                try {
                    $oldThemePath = Yii::getAlias('@themes/' . $this->getOldAttribute('name'));
                    if (is_dir($oldThemePath)) {
                        $newThemePath = $this->getThemePath();
                        rename($oldThemePath, $newThemePath);
                    }
                } catch (Exception $e) {
                    throw new \yii\base\Exception('Не удалось переименовать тему');
                }
            }
            if ($this->archive && !$this->archive->hasError) {
                try {
                    $zip = new ZipArchive;
                    if ($zip->open($this->archive->tempName) === true) {
                        $themePath = $this->getThemePath();
                        if (is_dir($themePath)) {
                            FileHelper::removeDirectory($themePath);
                        }
                        $zip->extractTo($themePath);
                        $zip->close();
                    }
                } catch (Exception $e) {
                    throw new \yii\base\Exception('Не удалось распаковать архив');
                }
            }
        }
        return $res;
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $themePath = $this->getThemePath();
            if (is_dir($themePath)) {
                try {
                    FileHelper::removeDirectory($themePath);
                } catch (Exception $e) {
                    throw new \yii\base\Exception('Не удалось удалить папку с темой');
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Возвращает путь к теме
     *
     * @return string
     */
    public function getThemePath()
    {
        return Yii::getAlias($this->getThemePathAlias());
    }

    /**
     * Возвращает алиас пути к теме
     *
     * @return string
     */
    public function getThemePathAlias()
    {
        if ($this->name) {
            return '@themes/' . $this->name;
        } else {
            return '';
        }
    }

    /**
     * @inheritdoc
     */
    public static function getSingularNominativeName() {
        return 'Тема';
    }

    /**
     * @inheritdoc
     */
    public static function getPluralNominativeName() {
        return 'Темы';
    }
}
