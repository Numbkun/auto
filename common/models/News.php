<?php

namespace common\models;

use Yii;
use yii\helpers\Inflector;

/**
 * Модель новостей магазина
 *
 * @property integer   $id  ID страницы новостей
 * @property string    $description описание страницы новостей
 * @property string    $text  текст страницы новостей
 * @property string    $translite  транслит названия страницы новостей
 * @property string    $page_keywords  keywords страницы новостей
 * @property string    $page_title   заголовок(title) страницы новостей
 * @property string    $page_description   description страницы новостей
 *
 */
class News extends ReferenceByShop
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['description', 'text'], 'required'],
                [['translite', 'page_description', 'page_keywords', 'page_title'], 'string'],

            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'translite' => 'ЧПУ',
                'description' => 'Краткое описание новости',
                'text' => 'Полный текс новости',
                'page_description' => 'Description страницы',
                'page_keywords' => 'Keywords страницы',
                'page_title' => 'Title страницы',
            ]
        );
    }

    /**Имя для отображения в единственном числе
     * @inheritdoc
     */
    public static function getSingularNominativeName()
    {
        return 'Новость';
    }

    /***Имя для отображения во множественном числе
     * @inheritdoc
     */
    public static function getPluralNominativeName()
    {
        return 'Новости';
    }

    /**
     * @inheritdoc
     * Генерация транслита названия страницы
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)){
            if(!$this->translite){
                $this->translite = Inflector::slug($this->name, '_');
            }
            if (empty($this->page_title)) {
                $this->page_title = $this->name;
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function getFieldsOptions()
    {
        if ($this->_fieldsOptions === []) {
            parent::getFieldsOptions();
            $this->_fieldsOptions['page_description'] = ['type' => 'text'];
            $this->_fieldsOptions['page_keywords'] = ['type' => 'text'];
            $this->_fieldsOptions['page_title'] = ['type' => 'text'];
            $this->_fieldsOptions['text'] = ['type' => 'html'];
            $this->_fieldsOptions['description'] = ['type' => 'html'];
        }
        return $this->_fieldsOptions;
    }
}
