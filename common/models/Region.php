<?php

namespace common\models;

use Yii;

/**
 * Модель регионов
 * @property ShopProductPrice[] $shopProductPrices  Массив цен товара к этому региону
 * @property ShopRegion $shopRegion  Магазин связанный с этим регионом
 */
class Region extends Reference
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['name'], 'required'],
                [['name'], 'string', 'max' => 255],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public static function getSingularNominativeName()
    {
        return 'Регион';
    }

    /**
     * @inheritdoc
     */
    public static function getPluralNominativeName()
    {
        return 'Регионы';
    }

    /**
     * Возвращает условия для отбора связей региона с ценами товара
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShopProductPrices()
    {
        return $this->hasMany(ShopProductPrice::className(), ['region_id' => 'id']);
    }

    /**
     * Возвращает условия для отбора связей региона с магазинами в этом регионе
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShopRegion()
    {
        return $this->hasOne(ShopRegion::className(), ['region_id' => 'id']);
    }

    /**Очистка всхех ключей для удаления товара
     *
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $relations = ['shopProductPrices', 'shopRegion',];
            foreach ($relations as $relation) {
                $relation = $this->$relation;
                if (is_array($relation)) {
                    foreach ($relation as $item) {
                        /** @var BaseActiveRecord $item */
                        $item->delete();
                    }
                } elseif($relation) {
                    /** @var BaseActiveRecord $relation */
                    $relation->delete();
                }
            }
            return true;
        }
        return false;
    }

}
