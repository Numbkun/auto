<?php

namespace common\models;

use common\components\BaseModelInterface;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\db\ActiveQueryInterface;
use yii\helpers\Inflector;

/**
 * Базовый класс для всех форм приложения
 *
 * @package common\models
 */
abstract class BaseForm extends Model implements BaseModelInterface {

    /**
     * @var array массив настроек полей модели, для внутреннего использования
     */
    protected $_fieldsOptions = [];

    /**
     * @var array массив атрибутов со связями
     */
    protected $_attributesWithRelations = [];

    /**
     * @inheritdoc
     */
    protected $_related = [];

    /**
     * @inheritdoc
     */
    public function getFieldsOptions()
    {
        // TODO сделать кеширование
        if ($this->_fieldsOptions === []) {
            $relations = $this->getAttributesWithRelation();
            foreach ($this->attributes() as $attribute) {
                if (isset($relations[$attribute])) {
                    $this->_fieldsOptions[$attribute]['type']
                        = is_subclass_of($relations[$attribute]['class'], Enum::className(), true) ?
                        'enum' : 'reference';
                } else {
                    $type = gettype($this->$attribute);
                    if (in_array($type, ['boolean', 'integer'])) {
                        $this->_fieldsOptions[$attribute]['type'] = $type;
                    } elseif($type == 'double') {
                        $this->_fieldsOptions[$attribute]['type'] = 'float';
                    } else {
                        $this->_fieldsOptions[$attribute]['type'] = 'string';
                    }
                }
            }
        }

        return $this->_fieldsOptions;
    }

    /**
     * @inheritdoc
     */
    public function getFieldOptions($attribute)
    {
        $fieldsOptions = $this->getFieldsOptions();
        if (isset($fieldsOptions[$attribute])) {
            return $fieldsOptions[$attribute];
        }
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getAttributesWithRelation()
    {
        // TODO сделать кеширование
        if ($this->_attributesWithRelations === []) {
            foreach ($this->attributes() as $attribute) {
                if (($pos = strpos($attribute, '_id')) !== false && strlen($attribute) == ($pos + 3)) {
                    $relation = Inflector::id2camel(substr($attribute, 0, $pos), '_');
                    $getter = 'get' . $relation;
                    if (method_exists($this, $getter)) {
                        $query = $this->$getter();
                        if ($query instanceof ActiveQuery) {
                            $this->_attributesWithRelations[$attribute] = [
                                'name'  => lcfirst($relation),
                                'class' => $query->modelClass,
                            ];
                        }
                    }
                }
            }
        }
        return $this->_attributesWithRelations;
    }

    /**
     * @inheritdoc
     */
    public function getAttributeRelation($attribute)
    {
        $relations = $this->getAttributesWithRelation();
        if (isset($relations[$attribute])) {
            return $relations[$attribute];
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function __get($name)
    {
        $value = parent::__get($name);
        if ($value instanceof ActiveQueryInterface) {
            return $this->_related[$name] = $value->findFor($name, $this);
        } else {
            return $value;
        }
    }

    /**
     * todo
     * @param $class
     * @param $link
     *
     * @return \yii\db\ActiveQuery
     */
    public function hasOne($class, $link)
    {
        /* @var $class \yii\db\ActiveRecordInterface */
        /* @var $query \yii\db\ActiveQuery */
        $query = $class::find();
        $query->primaryModel = $this;
        $query->link = $link;
        $query->multiple = false;
        return $query;
    }
}