<?php

namespace common\models;

use Yii;

/**
 * Модель изображения товара
 *
 * @property integer $id ID изображения товара
 * @property integer $file_id ID файла изображения товара
 * @property integer $product_id ID товара
 * @property Product $product модель товара
 * @property File $image
 */
class ProductImage extends BaseActiveRecord
{
    /**
     * @var \yii\web\UploadedFile
     */
    public $imageFile;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['imageFile'], 'file'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'imageFile' => 'Картинка',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function getSingularNominativeName()
    {
        return 'Картинка товара';
    }

    /**
     * @inheritdoc
     */
    public static function getPluralNominativeName()
    {
        return 'Картинки товара';
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)){
            if ($this->imageFile && !$this->imageFile->error) {
                $file = new File();
                $file->uploadFile = $this->imageFile;
                $file->path = 'product';
                $file->save();
                $this->file_id = $file->primaryKey;
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $product = Product::findOne($this->product_id);
        if(!$product->product_image_id){
            $product->product_image_id = $this->primaryKey;
            $product->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()){
            if($this->product->product_image_id == $this->primaryKey){
                $nextImage = ProductImage::find()
                    ->where('product_id = :product_id AND id != :id', [':product_id' => $this->product_id, ':id' => $this->primaryKey])
                    ->one();
                $this->product->product_image_id = $nextImage ? $nextImage->primaryKey : null;
                $this->product->save();
            }

            return true;
        }
        return false;
    }

    /**
    * @inheritdoc
    */
    public function getFieldsOptions()
    {
        return array_merge(parent::getFieldsOptions(), [
            'imageFile' => ['type' => 'file'],
        ]);
    }

    /**
     *Получить главную картинку из модели File
     */
    public function getImage()
    {
        return $this->hasOne(File::className(), ['id' => 'file_id']);
    }

    /**
     *Получить модель товара
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}