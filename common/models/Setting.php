<?php

namespace common\models;

use Yii;

/**
 * Модель записи настроек
 *
 * @property string $value значение настройки
 */
class Setting extends ReferenceByShop
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['name'], 'unique'],
                [['name'], 'string', 'max' => 64],
                [['value'], 'required'],
                [['value'], 'string', 'max' => 255],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return array_merge(
            parent::scenarios(),
            [
                self::SCENARIO_DEFAULT => ['value', '!name', '!shop_id'],
                self::SCENARIO_SYSTEM => ['value', '!name', 'shop_id'],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'value' => 'Значение',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public static function getSingularNominativeName() {
        return 'Настройка';
    }

    /**
     * @inheritdoc
     */
    public static function getPluralNominativeName() {
        return 'Настройки';
    }

}
