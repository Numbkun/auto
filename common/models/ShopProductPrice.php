<?php

namespace common\models;

use Yii;

/**
 * Модель связи товаров магазина с ценой
 *
 * @property integer  $shop_product_id идентификатор товара магазина
 * @property integer  $category_id     идентификатор категории
 * @property ShopProduct $shopProduct  товар магазина
 * @property Category $category        категория
 * @property float    $price           цена
 * @property Region   $region          регион
 * @property Currency $currency        валюта
 */
class ShopProductPrice extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['currency_id'], 'default', 'value' => Yii::$app->shop->getCurrencyId()],
                [['currency'], 'default', 'value' => Yii::$app->shop->getCurrency()],
                [['shop_product_id', 'region_id', 'price', 'currency_id'], 'required'],
                [['shop_product_id',  'region_id', 'currency_id'], 'integer'],
                [['price'], 'number', 'min' => 0],
                [
                    ['shop_product_id'],
                    'exist',
                    'skipOnError'     => true,
                    'targetClass'     => ShopProduct::className(),
                    'targetAttribute' => 'id'
                ],
                [
                    ['region_id'],
                    'exist',
                    'skipOnError'     => true,
                    'targetClass'     => Region::className(),
                    'targetAttribute' => 'id'
                ],
                [
                    ['currency_id'],
                    'exist',
                    'skipOnError'     => true,
                    'targetClass'     => Currency::className(),
                    'targetAttribute' => 'id'
                ],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return array_merge(
            parent::scenarios(),
            [
                self::SCENARIO_DEFAULT  => [ 'region_id', 'price', '!currency_id', '!shop_product_id'],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'shop_product_id' => 'Идентификатор товара',
                'shopProduct'     => 'Товар',
                'region_id'       => 'Идентификатор региона',
                'region'          => 'Регион',
                'price'           => 'Цена',
                'currency_id'     => 'Идентификатор валюты',
                'currency'        => 'Валюта',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function getFieldsOptions()
    {
        if ($this->_fieldsOptions === []) {
            parent::getFieldsOptions();
            $this->_fieldsOptions['shop_product_id'] = ['type' => 'hidden'];
        }
        return $this->_fieldsOptions;
    }

    /**
     * Возвращает условия для выбора товара магазина
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShopProduct()
    {
        return $this->hasOne(ShopProduct::className(), ['id' => 'shop_product_id']);
    }

    /**
     * Возвращает условия для выбора валюты
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * Возвращает условия для выбора региона
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * @inheritdoc
     */
    public static function getSingularNominativeName()
    {
        return 'Цена товара';
    }

    /**
     * @inheritdoc
     */
    public static function getPluralNominativeName()
    {
        return 'Цены товаров';
    }

}