<?php

namespace common\models;

use common\components\ReferenceByShopQuery;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Модель элемента справочника с привязкой к магазину
 *
 * @property integer  $shop_id идентификатор магазина
 * @property Shop     $shop    экземпляр магазина
 */
abstract class ReferenceByShop extends Reference
{
    /** Сценарий для установки системных атрибутов */
    const SCENARIO_SYSTEM = 'system';

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $systemAttributes = $defaultAttributes = array_flip($scenarios[self::SCENARIO_DEFAULT]);
        unset($defaultAttributes['shop_id']);
        $defaultAttributes = array_flip($defaultAttributes);
        $defaultAttributes[] = '!shop_id';
        $scenarios[self::SCENARIO_DEFAULT] = $defaultAttributes;
        $scenarios[self::SCENARIO_SYSTEM] = $systemAttributes;
        return $scenarios;
    }

    /**
     * @inheritdoc
     * @return ReferenceByShopQuery
     */
    public static function find()
    {
        return Yii::createObject(ReferenceByShopQuery::className(), [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['shop_id'], 'default', 'value' => Yii::$app->shop->id],
                [['shop_id'], 'integer'],
                [
                    ['shop_id'],
                    'exist',
                    'skipOnError'     => true,
                    'targetClass'     => Shop::className(),
                    'targetAttribute' => 'id'
                ],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::attributeLabels(),
            [
                'shop_id' => 'Идентификатор магазина',
                'shop'    => 'Магазин',
            ]
        );
    }

    /**
     * Возвращает магазин, к которому привязана модель
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShop()
    {
        return $this->hasOne(Shop::className(), ['id' => 'shop_id']);
    }

    /**
     * @inheritdoc
     */
    public function getFieldsOptions()
    {
        if ($this->_fieldsOptions === []) {
            parent::getFieldsOptions();
            if (!$this->isAttributeSafe('shop_id')) {
                $this->_fieldsOptions['shop_id'] = ['type' => 'ignore'];
            }
        }
        return $this->_fieldsOptions;
    }

}
