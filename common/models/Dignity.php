<?php

namespace common\models;

use Yii;
use yii\helpers\Inflector;

/**
 * Модель достоинства
 *
 * @property integer   $id  ID достоинства
 * @property string    $text  текст достоинства
 * @property string    $translite  транслит названия достоинства
 * @property integer   $image_id  id картинки достоинства
 * @property File      $image модель файла картинки
 *
 */
class Dignity extends ReferenceByShop
{
    /**
     * @var \yii\web\UploadedFile
     */
    public $imageFile;
    private $_oldImageId;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['text', 'translite'], 'string'],
                [['image_id'], 'integer'],
                [['imageFile'], 'file'],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'translite' => 'ЧПУ',
                'text' => 'Текст достоинства',
                'imageFile' => 'Картинка',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public static function getSingularNominativeName()
    {
        return 'Достоинство';
    }

    /**
     * @inheritdoc
     */
    public static function getPluralNominativeName()
    {
        return 'Достоинства';
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)){
            if(!$this->translite){
                $this->translite = Inflector::slug($this->name, '_');
            }
            if ($this->imageFile && !$this->imageFile->error) {
                if($this->image_id){
                    $this->_oldImageId = $this->image_id;
                }
                $file = new File();
                $file->uploadFile = $this->imageFile;
                $file->path = 'dignity';
                $file->save();
                $this->image_id = $file->primaryKey;
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if($this->_oldImageId){
            File::findOne($this->_oldImageId)->delete();
        }
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();
        if($this->image_id){
            File::findOne($this->image_id)->delete();
        }
    }

    /**
     * @inheritdoc
     */
    public function getImage(){
        return $this->hasOne(File::className(), ['id' => 'image_id']);
    }

    /**
     * @inheritdoc
     */
    public function getFieldsOptions()
    {
        if ($this->_fieldsOptions === []) {
            parent::getFieldsOptions();
            $this->_fieldsOptions['image_id'] = ['type' => 'readonly'];
            $this->_fieldsOptions['imageFile'] = ['type' => 'file'];
            $this->_fieldsOptions['text'] = ['type' => 'html'];
        }
        return $this->_fieldsOptions;
    }
}
