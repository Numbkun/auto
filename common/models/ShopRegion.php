<?php

namespace common\models;

use Yii;

/**
 * Модель региона магазина
 *
 *@property Region $region  регион
 *@property Shop   $shop    магазин
 */
class ShopRegion extends ReferenceByShop
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['region_id', 'shop_id'], 'required'],
                [['region_id', 'shop_id'], 'integer'],
                [
                    ['region_id'],
                    'exist',
                    'skipOnError'     => true,
                    'targetClass'     => Region::className(),
                    'targetAttribute' => 'id'
                ],
                [
                    ['shop_id'],
                    'exist',
                    'skipOnError'     => true,
                    'targetClass'     => Shop::className(),
                    'targetAttribute' => 'id'
                ],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'region_id' => 'Идентификатор региона',
                'region'    => 'Регион',
                'shop_id'  => 'Идентификатор магазина',
                'shop'     => 'Магазин',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public static function getSingularNominativeName()
    {
        return 'Магазин региона';
    }

    /**
     * @inheritdoc
     */
    public static function getPluralNominativeName()
    {
        return 'Магазины Региона';
    }

    /**
     * Возвращает условия для отбора региона
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * Возвращает условия для отбора магазина
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShop()
    {
        return $this->hasOne(Shop::className(), ['id' => 'shop_id']);
    }

}
