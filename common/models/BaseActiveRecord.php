<?php

namespace common\models;

use common\components\BaseModelInterface;
use common\components\DateTime;
use yii\base\Exception;
use yii\base\Model;
use yii\base\UserException;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Schema;
use yii\helpers\Inflector;
use yii\web\UploadedFile;

/**
 * Родительский класс для всех моделей приложения
 */
abstract class BaseActiveRecord extends ActiveRecord implements BaseModelInterface
{

    /** Сценарий для фильтрации моделей */
    const SCENARIO_SEARCH = 'search';

    /**
     * @inheritdoc
     */
    public function transactions()
    {
        $scenarios = $this->scenarios();
        foreach ($scenarios as $scenario => $value) {
            $scenarios[$scenario] = self::OP_ALL;
        }
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        foreach($this->attributes() as $attribute) {
            $scenarios[self::SCENARIO_SEARCH][] = $attribute;
        }
        return $scenarios;
    }

    /**
     * @var array массив настроек полей модели, для внутреннего использования
     */
    protected $_fieldsOptions = [];

    /**
     * @var array массив атрибутов со связями
     */
    protected $_attributesWithRelations = [];

    /**
     * Магическая функция для получения свойства объекта
     *
     * @param string $name наименование свойства
     *
     * @return mixed
     */
    public function __get($name)
    {
        $columns = $this->getDb()->getTableSchema($this->tableName())->columns;
        if (isset($columns[$name])) {
            $column = $columns[$name];
            if ($column->type == Schema::TYPE_DATETIME || $column->type == Schema::TYPE_DATE) {
                $value = parent::__get($name);
                if (is_null($value)) {
                    return null;
                } elseif (strtotime($value) !== false) {
                    return new DateTime($value, $column->type == Schema::TYPE_DATETIME);
                }
            }
        }
        return parent::__get($name);
    }

    /**
     * Сохранение модели
     *
     * @param bool $runValidation
     * @param null $attributes
     *
     * @return bool
     * @throws Exception
     */
    public function save($runValidation = true, $attributes = null)
    {

        $exception = null;

        try {
            $result = parent::save($runValidation, $attributes);
        } catch (Exception $e) {
            $result = false;
            $exception = $e;
        }

        if (!$result) {
            if ($exception === null) {
                $exceptionMessage = 'Ошибка сохранения';
                if ($this->hasErrors()) {
                    $exceptionMessage = 'Ошибки сохранения:';
                    foreach ($this->getErrors() as $attributeErrors) {
                        foreach ($attributeErrors as $attributeError) {
                            $exceptionMessage .= PHP_EOL . $attributeError;
                        }
                    }
                }
                $exception = new UserException($exceptionMessage);
            } else {
                $errorMessage = ($exception instanceof UserException ? $exception->getMessage() : 'Ошибка сервера');
                if (!in_array($errorMessage, $this->getErrors(''))) {
                    $this->addError('', $errorMessage);
                }
            }
            throw $exception;
        }

        return $result;
    }

    /**
     * Удаление экземпляра модели
     *
     * @throws Exception
     * @return boolean результат операции
     */
    public function delete()
    {

        $exception = null;

        try {
            $result = parent::delete();
        } catch (Exception $e) {
            $result = false;
            $exception = $e;
        }

        if ($result === false) {
            if ($exception === null) {
                $exception = new UserException('Ошибка удаления');
            } else {
                $errorMessage = ($exception instanceof UserException ? $exception->getMessage() : 'Ошибка сервера');
                if (!in_array($errorMessage, $this->getErrors(''))) {
                    $this->addError('', $errorMessage);
                }
            }
            throw $exception;
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), Model::attributes());
    }

    /**
     * @inheritdoc
     */
    public function getFieldsOptions()
    {
        // TODO сделать кеширование
        if ($this->_fieldsOptions === []) {
            $schema = $this->getTableSchema();
            $relations = $this->getAttributesWithRelation();
            foreach ($this->attributes() as $attribute) {
                if (isset($relations[$attribute])) {
                    $this->_fieldsOptions[$attribute]['type']
                        = is_subclass_of($relations[$attribute]['class'], Enum::className(), true) ?
                        'enum' : 'reference';
                } elseif (($column = $schema->getColumn($attribute)) !== null) {
                    if ($column->isPrimaryKey) {
                        $this->_fieldsOptions[$attribute]['type'] = 'hidden';
                    } else {
                        $this->_fieldsOptions[$attribute]['type'] = $column->type;
                    }
                } else {
                    $this->_fieldsOptions[$attribute]['type'] = 'string';
                }
            }
        }

        return $this->_fieldsOptions;
    }

    /**
     * @inheritdoc
     */
    public function getFieldOptions($attribute)
    {
        $fieldsOptions = $this->getFieldsOptions();
        if (isset($fieldsOptions[$attribute])) {
            return $fieldsOptions[$attribute];
        }
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getAttributesWithRelation()
    {
        // TODO сделать кеширование
        if ($this->_attributesWithRelations === []) {
            foreach ($this->attributes() as $attribute) {
                if (($pos = strpos($attribute, '_id')) !== false && strlen($attribute) == ($pos + 3)) {
                    $relation = Inflector::id2camel(substr($attribute, 0, $pos), '_');
                    $getter = 'get' . $relation;
                    if (method_exists($this, $getter)) {
                        $query = $this->$getter();
                        if ($query instanceof ActiveQuery) {
                            $this->_attributesWithRelations[$attribute] = [
                                'name'  => lcfirst($relation),
                                'class' => $query->modelClass,
                            ];
                        }
                    }
                }
            }
        }
        return $this->_attributesWithRelations;
    }

    /**
     * @inheritdoc
     */
    public function getAttributeRelation($attribute)
    {
        $relations = $this->getAttributesWithRelation();
        if (isset($relations[$attribute])) {
            return $relations[$attribute];
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function load($data, $formName = null)
    {
        if (parent::load($data, $formName)) {
            $fieldsOptions = $this->getFieldsOptions();
            foreach ($fieldsOptions as $field => $fieldOptions) {
                if ($fieldOptions['type'] == 'file') {
                    $this->{$field} = UploadedFile::getInstance($this, $field);
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Мультизагрузка моделей
     * @param array $models массив моделей
     * @param array $data данные
     * @param null  $formName
     *
     * @return bool
     */
    public static function loadMultiple($models, $data, $formName = null)
    {
        /* @var $model Model */
        $model = reset($models);
        if ($model === false) {
            return false;
        }
        $success = false;
        $scope = $formName === null ? $model->formName() : $formName;
        foreach ($models as $i => $model) {
            if ($scope == '') {
                if (isset($data[$i]) && $model->load($data[$i], '')) {
                    $success = true;
                }
            } elseif (isset($data[$scope][$i]) && $model->load($data[$scope][$i], '')) {
                $success = true;
            }
        }

        return $success;
    }

    /**
     * Загрузка значения атрибута из массива данных
     *
     * @param string      $name     имя атрибута
     * @param array       $data     массив данных
     * @param null|string $formName название формы
     *
     * @return bool
     */
    public function loadAttribute($name, $data, $formName = null)
    {
        $scope = $formName === null ? $this->formName() : $formName;
        if ($scope == '' && isset($data[$name])) {
            $this->setAttribute($name, $data[$name]);
            return true;
        } elseif (isset($data[$scope][$name])) {
            $this->setAttribute($name, $data[$scope][$name]);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Возвращает наименование сущности модели в единственном числе
     *
     * @return string
     */
    public static function getSingularNominativeName()
    {
        return get_called_class();
    }

    /**
     * Возвращает наименование сущности модели во множественном числе
     *
     * @return string
     */
    public static function getPluralNominativeName()
    {
        return get_called_class();
    }

    /**
     * Возвращает массив сортировок по умолчанию для поиска
     * @return array
     */
    protected function searchDefaultOrder() {
        $defaultOrder = [];
        foreach ($this->primaryKey() as $field) {
            $defaultOrder[$field] = SORT_ASC;
        };
        return $defaultOrder;
    }

    /**
     * Возврщает провайдер для поиска моделей
     * @param array $params параметры для поиска
     * @param ActiveQuery $query экземпляр запроса к бд
     * @return ActiveDataProvider
     */
    public function search($params, $query = null)
    {
        /** @var ActiveQuery $query */
        if (!$query) {
            $query = static::find();
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->setSort(['defaultOrder' => $this->searchDefaultOrder()]);
        $this->load($params);

        $fieldOptions = $this->getFieldsOptions();
        foreach ($this->attributes() as $attribute) {
            if ($this->$attribute && isset($fieldOptions[$attribute])) {
                if (in_array($fieldOptions[$attribute]['type'], ['smallint', 'integer', 'bigint', 'float', 'decimal', 'money', 'boolean', 'date', 'datetime'])) {
                    $value = $this->$attribute;
                    $operand = false;
                    if (strpos($value, '>=') === 0 || strpos($value, '<=') === 0) {
                        $operand = substr($value, 0, 2);
                        $value = substr($value, 2);
                    } elseif (strpos($value, '>') === 0 || strpos($value, '<') === 0) {
                        $operand = substr($value, 0, 1);
                        $value = substr($value, 1);
                    }
                    if (in_array($fieldOptions[$attribute]['type'], ['date', 'datetime'])) {
                        $value = (string)(new DateTime($value));
                    }
                    if ($operand) {
                        $query->andWhere($attribute . $operand . ':' . $attribute, [':' . $attribute => $value]);
                    } else {
                        $query->andWhere([$attribute => $value]);
                    }
                } else {
                    $query->andWhere(['ilike', $attribute, $this->$attribute]);
                }
            }
        }

        return $dataProvider;
    }

}