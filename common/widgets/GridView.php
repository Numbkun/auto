<?php

namespace common\widgets;

use common\components\BaseModelInterface;
use yii\bootstrap\ButtonGroup;
use yii\grid\ActionColumn;
use yii\grid\DataColumn;
use yii\grid\SerialColumn;
use yii\helpers\ArrayHelper;

/**
 * Виджета для вывода списка моделей в табличном виде
 *
 * @package common\widgets
 */
class GridView extends \yii\grid\GridView
{

    /**
     * @inheritdoc
     */
    public $layout = "<div class=\"panel panel-default\"><div class=\"panel-heading\">{toolbar}</div>\n{items}\n{pager}\n</div>\n{summary}";

    /**
     * @var array массив сопоставлений типа поля с видом форматирования значения
     */
    public $typeMap
        = [
            'string'    => 'text',
            'text'      => 'text',
            'boolean'   => 'boolean',
            'smallint'  => 'integer',
            'integer'   => 'integer',
            'bigint'    => 'integer',
            'float'     => 'decimal',
            'decimal'   => 'decimal',
            'datetime'  => 'datetime',
            'timestamp' => 'datetime',
            'time'      => 'time',
            'date'      => 'date',
            'money'     => 'decimal',
            'readonly'  => 'text',
        ];

    /**
     * @var array список типов полей, которые надо скрыть
     */
    public $hideTypes
        = [
            'binary',
            'hidden',
            'ignore',
            'file',
            'password',
            'html',
        ];

    /**
     * @var array настройки для столбца с порядковым номером. Установить false, если необходимо скрыть столбец
     */
    public $serialColumn = [];

    /**
     * @var array настройки для столбца с действиями. Установить false, если необходимо скрыть столбец
     */
    public $actionColumn = [
        'template' => '{view} {update} {delete}',
    ];

    /**
     * @var array массив кнопок для панели инструментов
     */
    public $toolbarButtons = [];

    /** @var \yii\base\Model модель связанная с гридом */
    public $model;

    /**
     * Возвращает модель связанную с гридом
     * @return \yii\base\Model|null
     */
    protected function getModel() {
        if (!$this->model) {
            $models = $this->dataProvider->getModels();
            $this->model = reset($models);
        }
        return $this->model;
    }

    /**
     * Возвращет массив настроек полей модели
     * @return array
     */
    protected function getFieldsOptions()
    {
        $model = $this->getModel();
        $fieldsOptions = [];
        if ($model instanceof BaseModelInterface) {
            $fieldsOptions = $model->getFieldsOptions();
        }
        return $fieldsOptions;
    }

    /**
     * Возвращает атрибут модели
     * @param $column
     *
     * @return string
     */
    protected function getModelField($column)
    {
        $attribute = '';
        if (is_string($column)) {
            if (strpos($column, ':') === false) {
                $attribute = $column;
            }
        } elseif ((!isset($column['class']) || is_a($column['class'], DataColumn::className(), true))
            && !isset($column['format'])
        ) {
            if (isset($column['value'])) {
                if (is_string($column['value'])) {
                    $attribute = $column['value'];
                }
            } elseif (isset($column['attribute'])) {
                $attribute = $column['attribute'];
            }
        }
        return $attribute;
    }

    /**
     * @inheritdoc
     */
    protected function initColumns()
    {
        if (empty($this->columns)) {
            $this->guessColumns();
        }

        $fieldsOptions = $this->getFieldsOptions();
        $isSerial = false;
        $isAction = false;
        foreach ($this->columns as $i => $column) {
            if (is_array($column)) {
                if (isset($column['class']) && is_a($column['class'], SerialColumn::className(), true)) {
                    $isSerial = true;
                }
                if (isset($column['class']) && is_a($column['class'], ActionColumn::className(), true)) {
                    $isAction = true;
                }
            }
            if ($field = $this->getModelField($column)) {
                $type = isset($fieldsOptions[$field]['type']) ? $fieldsOptions[$field]['type'] : 'string';
                if (in_array($type, $this->hideTypes)) {
                    unset($this->columns[$i]);
                    continue;
                }
                if (($model = $this->getModel()) && $model instanceof BaseModelInterface) {
                    if (in_array($type, ['reference','enum']) && ($relation = $model->getAttributeRelation($field))) {
                        $column = ['attribute' => $relation['name']];
                    }
                }
                if (is_string($column)) {
                    $column = ['attribute' => $column];
                }
                $column = array_merge(
                    [
                        'format' => isset($this->typeMap[$type]) ? $this->typeMap[$type] : 'text',
                    ],
                    $column
                );

                $this->columns[$i] = $column;
            }
        }

        if ($this->columns) {
            if ($this->serialColumn !== false && !$isSerial) {
                array_unshift($this->columns, ArrayHelper::merge(['class' => SerialColumn::className()], $this->serialColumn));
            }
            if ($this->actionColumn !== false && !$isAction) {
                $this->columns[] = ArrayHelper::merge(['class' => ActionColumn::className()], $this->actionColumn);
            }
            parent::initColumns();
        }
    }

    /**
     * Генерирует содержимое панели инструментов
     * @return string
     */
    public function renderToolbar()
    {
        return $this->toolbarButtons ? ButtonGroup::widget(['buttons' => $this->toolbarButtons]) : '';
    }

    /**
     * @inheritdoc
     */
    public function renderSection($name)
    {
        switch ($name) {
            case "{toolbar}":
                return $this->renderToolbar();
            default:
                return parent::renderSection($name);
        }
    }

    /**
     * @inheritdoc
     */
    protected function guessColumns()
    {
        $model = $this->getModel();
        if ($model) {
            if (is_array($model) || is_object($model)) {
                foreach ($model as $name => $value) {
                    $this->columns[] = $name;
                }
            }
        }
    }
}