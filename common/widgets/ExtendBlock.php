<?php

namespace common\widgets;

use yii\base\Widget;

/**
 * Виджет для работы с расширяемыми блоками в представлениях
 *
 * @package common\widgets
 */
class ExtendBlock extends Widget
{
    /**
     * @var boolean выводить ли содержимое блока после окончания записи
     */
    public $renderInPlace = false;

    /**
     * @var array массив с содержимый блока
     */
    protected $_content;

    /**
     * Старт записи блока
     */
    public function init()
    {
        ob_start();
        ob_implicit_flush(false);
    }

    /**
     * Устанавливает в содержимом блока метку для вывода родительского блока
     */
    public function parentBlock() {
        $this->_content[] = ob_get_clean();
        $this->_content[] = [];
        ob_start();
        ob_implicit_flush(false);
    }

    /**
     * Конец записи блока. Содержимое блока записывается как именованный блок в компоненте View
     */
    public function run()
    {
        $this->_content[] = ob_get_clean();
        $id = $this->getId();
        if (isset($this->view->blocks[$id])) {
            $this->view->blocks[$id] = $this->setParent($this->view->blocks[$id]);
        } else {
            $this->view->blocks[$id] = $this->_content;
        }
        if ($this->renderInPlace) {
            $this->renderContent($this->view->blocks[$id]);
        }
    }

    /**
     * Рекурсивная функция для замены меток содержимый родительского блока
     *
     * @param array $block ветка содержимого блока
     *
     * @return array
     */
    protected function setParent($block) {
        foreach ($block as $i => $content) {
            if ($content === []) {
                $block[$i] = $this->_content;
            } elseif (is_array($content)) {
                $block[$i] = $this->setParent($content);
            }
        }
        return $block;
    }

    /**
     * Рекрсивная функция для вывода содержимого блока
     *
     * @param array $block ветка содержимого блока
     */
    protected function renderContent($block) {
        foreach ($block as $content) {
            if (is_string($content)) {
                echo $content;
            } elseif (is_array($content) && $content !== []) {
                $this->renderContent($content);
            }
        }
    }
}