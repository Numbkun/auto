<?php

namespace common\widgets;


use common\components\BaseModelInterface;
use common\models\Enum;
use common\models\Reference;
use kartik\widgets\Select2;
use yii\base\InvalidParamException;
use yii\db\ActiveRecordInterface;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use mihaildev\ckeditor\CKEditor;

/**
 * Виджет для вывода полей формы
 *
 * @property \yii\base\Model|BaseModelInterface $model
 * @package common\widgets
 */
class ActiveField extends \yii\widgets\ActiveField
{

    public $renderMap
        = [
            'string'    => 'stringField',
            'text'      => 'textField',
            'boolean'   => 'booleanField',
            'smallint'  => 'stringField',
            'integer'   => 'stringField',
            'bigint'    => 'stringField',
            'float'     => 'stringField',
            'decimal'   => 'stringField',
            'datetime'  => 'stringField',
            'timestamp' => 'stringField',
            'time'      => 'stringField',
            'date'      => 'dateField',
            'binary'    => 'stringField',
            'money'     => 'stringField',
            'hidden'    => 'hiddenField',
            'readonly'  => 'readonlyField',
            'ignore'    => 'ignoreField',
            'file'      => 'fileField',
            'reference' => 'referenceField',
            'enum'      => 'enumField',
            'password'  => 'passwordField',
            'html'  => 'htmlField',
        ];

    /**
     * @inheritdoc
     */
    public function render($content = null)
    {
        if (!preg_match('/(^|.*\])([\w\.]+)(\[.*|$)/', $this->attribute, $matches)) {
            throw new InvalidParamException('Attribute name must contain word characters only.');
        }
        $attribute = $matches[2];
        if ($this->model instanceof BaseModelInterface
            && ($fieldOptions = $this->model->getFieldOptions($attribute)) !== []
        ) {
            if (isset($fieldOptions['inputOptions'])) {
                $this->inputOptions = ArrayHelper::merge($fieldOptions['inputOptions'], $this->inputOptions);
            }
            if (isset($fieldOptions['errorOptions'])) {
                $this->errorOptions = ArrayHelper::merge($fieldOptions['errorOptions'], $this->errorOptions);
            }
            if (isset($fieldOptions['labelOptions'])) {
                $this->labelOptions = ArrayHelper::merge($fieldOptions['labelOptions'], $this->labelOptions);
            }
            if (isset($fieldOptions['hintOptions'])) {
                $this->hintOptions = ArrayHelper::merge($fieldOptions['hintOptions'], $this->hintOptions);
            }
            if (!isset($this->parts['{input}']) && !isset($this->parts['{label}']) && !isset($this->parts['{error}'])
                && !isset($this->parts['{hint}'])
                && $content === null
                && isset($fieldOptions['type'])
                && isset($this->renderMap[$fieldOptions['type']])
            ) {
                $method = $this->renderMap[$fieldOptions['type']];
                if (method_exists($this, $method)) {
                    $this->$method();
                }
            }
        }
        return parent::render($content);
    }

    /**
     * Генерация строкового поля
     *
     * @return $this
     */
    public function stringField()
    {
        $this->textInput();
        return $this;
    }

    /**
     * Генерация поля с датой
     *
     * @return $this
     */
    public function dateField()
    {
        $this->input('date');
        return $this;
    }

    /**
     * Генерация поля для текста
     *
     * @return $this
     */
    public function textField()
    {
        $this->textarea();
        return $this;
    }
    /**
     * Генерация поля редактора CKEditor
     *
     * @return $this
     */
    public function htmlField()
    {
        $this->widget(CKEditor::className(), $this->inputOptions);
        return $this;
    }

    /**
     * Генерация скрытого поля
     *
     * @return $this
     */
    public function hiddenField()
    {
        $this->parts['{label}'] = '';
        $this->parts['{error}'] = '';
        $this->parts['{hint}'] = '';
        $this->hiddenInput();
        return $this;
    }

    /**
     * Генерация поля только для чтения
     *
     * @return $this
     */
    public function readonlyField()
    {
        $this->parts['{error}'] = '';
        $this->parts['{hint}'] = '';
        $this->textInput(['disabled' => 'disabled']);
        return $this;
    }

    /**
     * Генерация для игнорируемого поля
     *
     * @return $this
     */
    public function ignoreField()
    {
        $this->parts['{error}'] = '';
        $this->parts['{hint}'] = '';
        $this->parts['{label}'] = '';
        $this->parts['{input}'] = '';
        return $this;
    }

    /**
     * Генерация поля загрузки файлов
     *
     * @return $this
     */
    public function fileField()
    {
        $this->fileInput();
        return $this;
    }

    /**
     * Генерация поля ссылки
     *
     * @throws \yii\base\InvalidParamException
     * @return $this
     */
    public function referenceField()
    {
        if (!preg_match('/(^|.*\])([\w\.]+)(\[.*|$)/', $this->attribute, $matches)) {
            throw new InvalidParamException('Attribute name must contain word characters only.');
        }
        $attribute = $matches[2];

        if ($relation = $this->model->getAttributeRelation($attribute)) {
            /** @var ActiveRecordInterface $class */
            $class = $relation['class'];
            if (is_subclass_of($class, Enum::className(), true) || is_subclass_of($class, Reference::className(), true)) {
                $data = $class::find()->asArray()->all();
                $data = ArrayHelper::map($data, 'id', 'name');
                $this->parts['{input}'] = Select2::widget(
                    [
                        'data'      => $data,
                        'model'     => $this->model,
                        'attribute' => $attribute,
                        'options'   => [
                            'placeholder' => 'Выберите значение ...',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]
                );
                $this->parts['{label}'] = Html::activeLabel($this->model, $relation['name'], $this->labelOptions);
                return $this;
            }
        }
        return $this->stringField();
    }

    /**
     * Генерация выпадающего поля
     *
     * @return $this
     */
    public function enumField()
    {
        $attribute = Html::getAttributeName($this->attribute);
        if ($relation = $this->model->getAttributeRelation($attribute)) {
            /** @var ActiveRecordInterface $class */
            $class = $relation['class'];
            if (is_subclass_of($class, Enum::className(), true) || is_subclass_of($class, Reference::className(), true)) {
                if (!isset($this->inputOptions['prompt'])) {
                    $this->inputOptions['prompt'] = '(не указано)';
                }
                $this->dropDownList(ArrayHelper::map($class::find()->all(), 'id', 'name'));
                $this->parts['{label}'] = Html::activeLabel($this->model, $relation['name'], $this->labelOptions);
                return $this;
            }
        }
        return $this->stringField();
    }

    /**
     * Генерация логического поля
     *
     * @return $this
     */
    public function booleanField()
    {
        $this->checkbox($this->inputOptions);
        $this->parts['{input}'] = '<div class="checkbox">' . $this->parts['{input}'] .'</div>';
        return $this;
    }

    /**
     * Генерация поля для ввода пароля
     *
     * @return $this
     */
    public function passwordField()
    {
        $this->passwordInput();
        return $this;
    }

}