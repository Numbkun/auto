<?php
/**
 * Created by IntelliJ IDEA.
 * User: aydin
 * Date: 11.08.2014
 * Time: 5:23
 */

namespace common\widgets;

use common\models\BaseActiveRecord;
use yii\bootstrap\ButtonGroup;

/**
 * Виджет для вывода детализации по объекту
 *
 * @package common\widgets
 */
class DetailView extends \yii\widgets\DetailView {

    /**
     * @var string шаблон для вывода содержимого виджета
     */
    public $layout = "<div class=\"panel panel-default\"><div class=\"panel-heading\">{toolbar}</div>\n{detail}\n</div>";

    /**
     * @var array массив сопоставления типа поля с видом форматирования значения
     */
    public $typeMap
        = [
            'string'    => 'text',
            'text'      => 'text',
            'boolean'   => 'boolean',
            'smallint'  => 'integer',
            'integer'   => 'integer',
            'bigint'    => 'integer',
            'float'     => 'decimal',
            'decimal'   => 'decimal',
            'datetime'  => 'datetime',
            'timestamp' => 'datetime',
            'time'      => 'time',
            'date'      => 'date',
            'money'     => 'decimal',
            'readonly'  => 'text',
        ];

    /**
     * @var array массив типов поля, которые необходимо скрывать
     */
    public $hideTypes
        = [
            'binary',
            'hidden',
            'ignore',
            'file',
            'password',
        ];

    /**
     * @var array массив кнопок для вывода в панели инструментов
     */
    public $toolbarButtons = [];

    /**
     * Возвращает название атрибута модели
     * @param $attribute
     *
     * @return string
     */
    protected function getModelField($attribute)
    {
        $field = '';
        if (is_string($attribute)) {
            if (strpos($attribute, ':') === false) {
                $field = $attribute;
            }
        } elseif (isset($attribute['attribute']) && !isset($attribute['format'])) {
            $field = $attribute['attribute'];
        }
        return $field;
    }

    /**
     * @inheritdoc
     */
    protected function normalizeAttributes() {
        if ($this->model instanceof BaseActiveRecord) {
            if ($this->attributes === null) {
                $this->attributes = $this->model->attributes();
            }

            $fieldsOptions = $this->model->getFieldsOptions();
            foreach ($this->attributes as $i => $attribute) {
                if ($field = $this->getModelField($attribute)) {
                    $type = isset($fieldsOptions[$field]['type']) ? $fieldsOptions[$field]['type'] : 'string';
                    if (in_array($type, $this->hideTypes)) {
                        unset($this->attributes[$i]);
                        continue;
                    }
                    if (in_array($type, ['reference','enum']) && ($relation = $this->model->getAttributeRelation($field))) {
                        $attribute = ['attribute' => $relation['name']];
                    }
                    if (is_string($attribute)) {
                        $attribute = ['attribute' => $attribute];
                    }
                    $attribute = array_merge(
                        [
                            'format' => isset($this->typeMap[$type]) ? $this->typeMap[$type] : 'text',
                        ],
                        $attribute
                    );
                }
                $this->attributes[$i] = $attribute;
            }
        }
        parent::normalizeAttributes();
    }

    /**
     * Генерирует содержимое панели иснструментов
     * @return string
     */
    public function renderToolbar()
    {
        return $this->toolbarButtons ? ButtonGroup::widget(['buttons' => $this->toolbarButtons]) : '';
    }

    /**
     * Генерирует содержимое секции
     */
    public function renderSection($name)
    {
        switch ($name) {
            case "{toolbar}":
                return $this->renderToolbar();
            case "{detail}":
                return $this->renderDetail();
            default:
                return false;
        }
    }

    /**
     * Генерирует детальную информацию по объекту
     * @return string
     */
    public function renderDetail() {
        ob_start();
        parent::run();
        return ob_get_clean();
    }

    /**
     * @inheritdoc
     */
    public function run() {
        $content = preg_replace_callback("/{\\w+}/", function ($matches) {
                $content = $this->renderSection($matches[0]);

                return $content === false ? $matches[0] : $content;
            }, $this->layout);
        echo $content;
    }
}