<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */

namespace common\components\rbac;

use common\components\DateTime;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\base\UserException;
use yii\caching\TagDependency;
use yii\db\Connection;
use yii\db\Query;
use yii\db\Expression;
use yii\base\InvalidCallException;
use yii\base\InvalidParamException;
use yii\di\Instance;
use yii\rbac\Assignment;
use yii\rbac\ManagerInterface;

/**
 * DbManager represents an authorization manager that stores authorization information in database.
 *
 * The database connection is specified by [[db]]. The database schema could be initialized by applying migration:
 *
 * ```
 * yii migrate --migrationPath=@yii/rbac/migrations/
 * ```
 *
 * If you don't want to use migration and need SQL instead, files for all databases are in migrations directory.
 *
 * You may change the names of the three tables used to store the authorization data by setting [[itemTable]],
 * [[itemChildTable]] and [[assignmentTable]].
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @author Alexander Kochetov <creocoder@gmail.com>
 * @since  2.0
 */
class DbManager extends Component implements ManagerInterface
{
    /**
     * @var array a list of role names that are assigned to every user automatically without calling [[assign()]].
     */
    public $defaultRoles = [];
    /**
     * @var Connection|string the DB connection object or the application component ID of the DB connection.
     * After the DbManager object is created, if you want to change this property, you should only assign it
     * with a DB connection object.
     */
    public $db = 'db';

    /**
     * @var string the name of the table storing authorization items. Defaults to "auth_item".
     */
    public $itemTable = '{{%auth_item}}';

    /**
     * @var string the name of the table storing authorization item hierarchy. Defaults to "auth_item_child".
     */
    public $itemChildTable = '{{%auth_item_child}}';

    /**
     * @var string the name of the table storing authorization item assignments. Defaults to "auth_assignment".
     */
    public $assignmentTable = '{{%auth_assignment}}';

    /**
     * @var string the name of the table storing rules. Defaults to "auth_rule".
     */
    public $ruleTable = '{{%auth_rule}}';

    /**
     * @var string имя роли с полными правами
     */
    public $superRoleName = 'administrator';

    /**
     * @var string описание роли с полными правами
     */
    public $superRoleDescription = 'Полные права';

    /**
     * Initializes the application component.
     * This method overrides the parent implementation by establishing the database connection.
     */
    public function init()
    {
        $this->db = Instance::ensure($this->db, Connection::className());
    }

    /**
     * @inheritdoc
     */
    public function checkAccess($userId, $permissionName, $params = [])
    {
        if ($permissionName === false) {
            return false;
        }
        $assignments = $this->getAssignments($userId);
        return $this->checkAccessRecursive($userId, $permissionName, $params, $assignments);
    }

    /**
     * Performs access check for the specified user.
     * This method is internally called by [[checkAccess()]].
     *
     * @param string|integer $user        the user ID. This should can be either an integer or a string representing
     *                                    the unique identifier of a user. See [[\yii\web\User::id]].
     * @param string         $itemName    the name of the operation that need access check
     * @param array          $params      name-value pairs that would be passed to rules associated
     *                                    with the tasks and roles assigned to the user. A param with name 'user' is added to this array,
     *                                    which holds the value of `$userId`.
     * @param Assignment[]   $assignments the assignments to the specified user
     *
     * @return boolean whether the operations can be performed by the user.
     */
    protected function checkAccessRecursive($user, $itemName, $params, $assignments)
    {
        if (($item = $this->getItem($itemName)) === null) {
            return false;
        }

        Yii::trace($item instanceof Role ? "Checking role: $itemName" : "Checking permission: $itemName", __METHOD__);

        if (!$this->executeRule($user, $item, $params)) {
            return false;
        }

        if (isset($assignments[$itemName]) || in_array($itemName, $this->defaultRoles)) {
            return true;
        }

        $parents = $this->db->cache(function ($db) use ($itemName) {
            return (new Query)->select(['parent'])
                ->from($this->itemChildTable)
                ->where(['child' => $itemName])
                ->column($db);
        }, 0, new TagDependency(['tags' => '#table_cache#' . $this->itemChildTable]));

        foreach ($parents as $parent) {
            if ($this->checkAccessRecursive($user, $parent, $params, $assignments)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns the named auth item.
     * @param string $name the auth item name.
     * @return Item the auth item corresponding to the specified name. Null is returned if no such item.
     */
    public function getItem($name)
    {

        $row = $this->db->cache(function () use ($name) {
            return (new Query)->from($this->itemTable)
                ->where(['name' => $name])
                ->one($this->db);
        }, 0, new TagDependency(['tags' => '#table_cache#' . $this->itemTable]));

        if ($row === false) {
            return null;
        }

        if (!isset($row['data']) || ($data = @unserialize($row['data'])) === false) {
            $row['data'] = null;
        }

        return $this->populateItem($row);
    }

    /**
     * Returns a value indicating whether the database supports cascading update and delete.
     * The default implementation will return false for SQLite database and true for all other databases.
     *
     * @return boolean whether the database supports cascading update and delete.
     */
    protected function supportsCascadeUpdate()
    {
        return strncmp($this->db->getDriverName(), 'sqlite', 6) !== 0;
    }

    /**
     * Adds an auth item to the RBAC system.
     *
     * @param Item $item
     *
     * @return boolean whether the auth item is successfully added to the system
     * @throws \Exception if data validation or saving fails (such as the name of the role or permission is not unique)
     */
    protected function addItem($item)
    {
        $time = new DateTime();
        if ($item->createdAt === null) {
            $item->createdAt = $time;
        }
        if ($item->updatedAt === null) {
            $item->updatedAt = $time;
        }
        TagDependency::invalidate(Yii::$app->cache, '#table_cache#' . $this->itemTable);
        $this->db->createCommand()
            ->insert(
                $this->itemTable,
                [
                    'name'        => $item->name,
                    'shop_id'     => $item->shop_id,
                    'type'        => $item->type,
                    'description' => $item->description,
                    'rule_name'   => $item->ruleName,
                    'data'        => $item->data === null ? null : serialize($item->data),
                    'is_system'   => $item->is_system,
                    'created_at'  => $item->createdAt,
                    'updated_at'  => $item->updatedAt,
                ]
            )->execute();

        if ($item->type == Item::TYPE_PERMISSION) {
            $superRole = $this->getRole($this->superRoleName);
            $this->addChild($superRole, $item);
        }

        return true;
    }

    /**
     * Removes an auth item from the RBAC system.
     *
     * @param Item $item
     *
     * @return boolean whether the role or permission is successfully removed
     * @throws \Exception if data validation or saving fails (such as the name of the role or permission is not unique)
     */
    protected function removeItem($item)
    {
        if ($item->type == Item::TYPE_ROLE && $item->name == $this->superRoleName) {
            throw new UserException('Нельзя удалить привилегированную роль');
        }
        TagDependency::invalidate(Yii::$app->cache, '#table_cache#' . $this->assignmentTable);
        TagDependency::invalidate(Yii::$app->cache, '#table_cache#' . $this->itemChildTable);
        TagDependency::invalidate(Yii::$app->cache, '#table_cache#' . $this->itemTable);
        if (!$this->supportsCascadeUpdate()) {
            $this->db->createCommand()
                ->delete($this->itemChildTable, ['or', 'parent=:name', 'child=:name'], [':name' => $item->name])
                ->execute();
            $this->db->createCommand()
                ->delete($this->assignmentTable, ['item_name' => $item->name])
                ->execute();
        }

        $this->db->createCommand()
            ->delete($this->itemTable, ['name' => $item->name])
            ->execute();

        return true;
    }

    /**
     * Updates an auth item in the RBAC system.
     *
     * @param string $name the old name of the auth item
     * @param Item   $item
     *
     * @return boolean whether the auth item is successfully updated
     * @throws \Exception if data validation or saving fails (such as the name of the role or permission is not unique)
     */
    protected function updateItem($name, $item)
    {
        TagDependency::invalidate(Yii::$app->cache, '#table_cache#' . $this->assignmentTable);
        TagDependency::invalidate(Yii::$app->cache, '#table_cache#' . $this->itemChildTable);
        TagDependency::invalidate(Yii::$app->cache, '#table_cache#' . $this->itemTable);
        if (!$this->supportsCascadeUpdate() && $item->name !== $name) {
            $this->db->createCommand()
                ->update($this->itemChildTable, ['parent' => $item->name], ['parent' => $name])
                ->execute();
            $this->db->createCommand()
                ->update($this->itemChildTable, ['child' => $item->name], ['child' => $name])
                ->execute();
            $this->db->createCommand()
                ->update($this->assignmentTable, ['item_name' => $item->name], ['item_name' => $name])
                ->execute();
        }

        $item->updatedAt = new DateTime();

        $this->db->createCommand()
            ->update(
                $this->itemTable,
                [
                    'name'        => $item->name,
                    'shop_id'     => $item->shop_id,
                    'description' => $item->description,
                    'rule_name'   => $item->ruleName,
                    'data'        => $item->data === null ? null : serialize($item->data),
                    'updated_at'  => $item->updatedAt,
                    'is_system'   => $item->is_system,
                ], [
                    'name' => $name,
                ]
            )->execute();

        return true;
    }

    /**
     * @inheritdoc
     */
    protected function addRule($rule)
    {
        $time = new DateTime();
        if ($rule->createdAt === null) {
            $rule->createdAt = $time;
        }
        if ($rule->updatedAt === null) {
            $rule->updatedAt = $time;
        }
        $this->db->createCommand()
            ->insert(
                $this->ruleTable,
                [
                    'name'       => $rule->name,
                    'data'       => serialize($rule),
                    'created_at' => $rule->createdAt,
                    'updated_at' => $rule->updatedAt,
                ]
            )->execute();

        return true;
    }

    /**
     * Updates a rule to the RBAC system.
     * @param string $name the old name of the rule
     * @param Rule $rule
     * @return boolean whether the rule is successfully updated
     * @throws \Exception if data validation or saving fails (such as the name of the rule is not unique)
     */
    protected function updateRule($name, $rule)
    {
        TagDependency::invalidate(Yii::$app->cache, '#table_cache#' . $this->itemTable);
        if (!$this->supportsCascadeUpdate() && $rule->name !== $name) {
            $this->db->createCommand()
                ->update($this->itemTable, ['rule_name' => $rule->name], ['rule_name' => $name])
                ->execute();
        }

        $rule->updatedAt = new DateTime();

        $this->db->createCommand()
            ->update(
                $this->ruleTable,
                [
                    'name'       => $rule->name,
                    'data'       => serialize($rule),
                    'updated_at' => $rule->updatedAt,
                ],
                [
                    'name' => $name,
                ]
            )->execute();

        return true;
    }

    /**
     * Removes a rule from the RBAC system.
     * @param Rule $rule
     * @return boolean whether the rule is successfully removed
     * @throws \Exception if data validation or saving fails (such as the name of the rule is not unique)
     */
    protected function removeRule($rule)
    {
        TagDependency::invalidate(Yii::$app->cache, '#table_cache#' . $this->itemTable);
        if (!$this->supportsCascadeUpdate()) {
            $this->db->createCommand()
                ->update($this->itemTable, ['rule_name' => null], ['rule_name' => $rule->name])
                ->execute();
        }

        $this->db->createCommand()
            ->delete($this->ruleTable, ['name' => $rule->name])
            ->execute();

        return true;
    }

    /**
     * Returns the items of the specified type.
     *
     * @param integer $type the auth item type (either [[Item::TYPE_ROLE]] or [[Item::TYPE_PERMISSION]]
     * @param         $withSystem
     *
     * @return Item[] the auth items of the specified type.
     */
    protected function getItems($type, $withSystem)
    {
        $query = (new Query)
            ->from($this->itemTable)
            ->where(['type' => $type]);

        if (!$withSystem) {
            $query->andWhere(['is_system' => false]);
        }

        $items = [];
        foreach ($query->all($this->db) as $row) {
            $items[$row['name']] = $this->populateItem($row);
        }

        return $items;
    }

    /**
     * Populates an auth item with the data fetched from database
     *
     * @param array $row the data from the auth item table
     *
     * @return Item the populated auth item instance (either Role or Permission)
     */
    protected function populateItem($row)
    {
        $class = $row['type'] == Item::TYPE_PERMISSION ? Permission::className() : Role::className();

        if (!isset($row['data']) || ($data = @unserialize($row['data'])) === false) {
            $data = null;
        }

        return new $class(
            [
                'name'        => $row['name'],
                'shop_id'     => $row['shop_id'],
                'type'        => $row['type'],
                'description' => $row['description'],
                'ruleName'    => $row['rule_name'],
                'data'        => $data,
                'is_system'   => $row['is_system'],
                'createdAt'   => $row['created_at'],
                'updatedAt'   => $row['updated_at'],
            ]
        );
    }

    /**
     * Returns the roles that are assigned to the user via [[assign()]].
     * Note that child roles that are not assigned directly to the user will not be returned.
     * @param string|integer $userId the user ID (see [[\yii\web\User::id]])
     * @return Role[] all roles directly or indirectly assigned to the user. The array is indexed by the role names.
     */
    public function getRolesByUser($userId)
    {
        if (empty($userId)) {
            return [];
        }

        $query = (new Query)->select('b.*')
            ->from(['a' => $this->assignmentTable, 'b' => $this->itemTable])
            ->where('a.item_name=b.name')
            ->andWhere(['a.user_id' => (string)$userId]);

        $roles = [];
        foreach ($query->all($this->db) as $row) {
            $roles[$row['name']] = $this->populateItem($row);
        }
        return $roles;
    }

    /**
     * Returns all permissions that the specified role represents.
     * @param string $roleName the role name
     * @return Permission[] all permissions that the role represents. The array is indexed by the permission names.
     */
    public function getPermissionsByRole($roleName)
    {
        $childrenList = $this->getChildrenList();
        $result = [];
        $this->getChildrenRecursive($roleName, $childrenList, $result);
        if (empty($result)) {
            return [];
        }
        $query = (new Query)->from($this->itemTable)->where(
            [
                'type' => Item::TYPE_PERMISSION,
                'name' => array_keys($result),
            ]
        );
        $permissions = [];
        foreach ($query->all($this->db) as $row) {
            $permissions[$row['name']] = $this->populateItem($row);
        }
        return $permissions;
    }

    /**
     * Returns all permissions that the user has.
     * @param string|integer $userId the user ID (see [[\yii\web\User::id]])
     * @return Permission[] all permissions that the user has. The array is indexed by the permission names.
     */
    public function getPermissionsByUser($userId)
    {
        if (empty($userId)) {
            return [];
        }

        $query = (new Query)->select('item_name')
            ->from($this->assignmentTable)
            ->where(['user_id' => (string)$userId]);

        $childrenList = $this->getChildrenList();
        $result = [];
        foreach ($query->column($this->db) as $roleName) {
            $this->getChildrenRecursive($roleName, $childrenList, $result);
        }

        if (empty($result)) {
            return [];
        }

        $query = (new Query)->from($this->itemTable)->where(
            [
                'type' => Item::TYPE_PERMISSION,
                'name' => array_keys($result),
            ]
        );
        $permissions = [];
        foreach ($query->all($this->db) as $row) {
            $permissions[$row['name']] = $this->populateItem($row);
        }
        return $permissions;
    }

    /**
     * Returns the children for every parent.
     *
     * @return array the children list. Each array key is a parent item name,
     * and the corresponding array value is a list of child item names.
     */
    protected function getChildrenList()
    {
        $query = (new Query)->from($this->itemChildTable);
        $parents = [];
        foreach ($query->all($this->db) as $row) {
            $parents[$row['parent']][] = $row['child'];
        }
        return $parents;
    }

    /**
     * Recursively finds all children and grand children of the specified item.
     *
     * @param string $name         the name of the item whose children are to be looked for.
     * @param array  $childrenList the child list built via [[getChildrenList()]]
     * @param array  $result       the children and grand children (in array keys)
     */
    protected function getChildrenRecursive($name, $childrenList, &$result)
    {
        if (isset($childrenList[$name])) {
            foreach ($childrenList[$name] as $child) {
                $result[$child] = true;
                $this->getChildrenRecursive($child, $childrenList, $result);
            }
        }
    }

    /**
     * Returns the rule of the specified name.
     * @param string $name the rule name
     * @return Rule the rule object, or null if the specified name does not correspond to a rule.
     */
    public function getRule($name)
    {
        $row = (new Query)->select(['data'])
            ->from($this->ruleTable)
            ->where(['name' => $name])
            ->one($this->db);
        return $row === false ? null : unserialize($row['data']);
    }

    /**
     * Returns all rules available in the system.
     * @return Rule[] the rules indexed by the rule names
     */
    public function getRules()
    {
        $query = (new Query)->from($this->ruleTable);

        $rules = [];
        foreach ($query->all($this->db) as $row) {
            $rules[$row['name']] = unserialize($row['data']);
        }

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function getAssignment($roleName, $userId)
    {
        if (empty($userId)) {
            return null;
        }

        $row = $this->db->cache(function () use ($userId, $roleName){
            return (new Query)->from($this->assignmentTable)
                ->where(['user_id' => (string)$userId, 'item_name' => $roleName])
                ->one($this->db);
        }, 0, new TagDependency(['tags' => '#table_cache#' . $this->assignmentTable]));

        if ($row === false) {
            return null;
        }

        return new Assignment(
            [
                'userId'    => $row['user_id'],
                'roleName'  => $row['item_name'],
                'createdAt' => $row['created_at'],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function getAssignments($userId)
    {
        if (empty($userId)) {
            return [];
        }

        $result = $this->db->cache(function () use ($userId){
            return (new Query)
                ->from($this->assignmentTable)
                ->where(['user_id' => (string)$userId])
                ->all($this->db);
        }, 0, new TagDependency(['tags' => '#table_cache#' . $this->assignmentTable]));

        $assignments = [];
        foreach ($result as $row) {
            $assignments[$row['item_name']] = new Assignment([
                'userId'    => $row['user_id'],
                'roleName'  => $row['item_name'],
                'createdAt' => $row['created_at'],
            ]);
        }

        return $assignments;
    }

    /**
     * Adds an item as a child of another item.
     *
     * @param Item $parent
     * @param Item $child
     *
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\InvalidCallException
     * @return bool
     */
    public function addChild($parent, $child)
    {
        if ($parent->name === $child->name) {
            throw new InvalidParamException("Cannot add '{$parent->name}' as a child of itself.");
        }

        if ($parent instanceof Permission && $child instanceof Role) {
            throw new InvalidParamException("Cannot add a role as a child of a permission.");
        }

        if ($this->detectLoop($parent, $child)) {
            throw new InvalidCallException(
                "Cannot add '{$child->name}' as a child of '{$parent->name}'. A loop has been detected."
            );
        }

        TagDependency::invalidate(Yii::$app->cache, '#table_cache#' . $this->itemChildTable);
        $this->db->createCommand()
            ->insert($this->itemChildTable, ['parent' => $parent->name, 'child' => $child->name])
            ->execute();

        return true;
    }

    /**
     * Removes a child from its parent.
     * Note, the child item is not deleted. Only the parent-child relationship is removed.
     *
     * @param Item $parent
     * @param Item $child
     *
     * @throws \yii\base\UserException
     * @return boolean whether the removal is successful
     */
    public function removeChild($parent, $child)
    {
        if ($child->type == Item::TYPE_PERMISSION && $parent->name == $this->superRoleName) {
            throw new UserException('Нельзя удалить операцию из привилегированной роли');
        }
        TagDependency::invalidate(Yii::$app->cache, '#table_cache#' . $this->itemChildTable);
        return $this->db->createCommand()
            ->delete($this->itemChildTable, ['parent' => $parent->name, 'child' => $child->name])
            ->execute() > 0;
    }

    /**
     * @inheritdoc
     */
    public function removeChildren($parent)
    {
        if ($parent->name == $this->superRoleName) {
            throw new UserException('Нельзя удалить операцию из привилегированной роли');
        }
        TagDependency::invalidate(Yii::$app->cache, '#table_cache#' . $this->itemChildTable);
        return $this->db->createCommand()
            ->delete($this->itemChildTable, ['parent' => $parent->name])
            ->execute() > 0;
    }

    /**
     * Returns a value indicating whether the child already exists for the parent.
     * @param Item $parent
     * @param Item $child
     * @return boolean whether `$child` is already a child of `$parent`
     */
    public function hasChild($parent, $child)
    {
        return (new Query)
            ->from($this->itemChildTable)
            ->where(['parent' => $parent->name, 'child' => $child->name])
            ->one($this->db) !== false;
    }

    /**
     * Returns the child permissions and/or roles.
     *
     * @param string $name the parent name
     *
     * @return Item[] the child permissions and/or roles
     */
    public function getChildren($name)
    {
        $query = (new Query)
            ->select(
                ['name', 'shop_id', 'type', 'description', 'rule_name', 'data', 'is_system', 'created_at', 'updated_at']
            )
            ->from([$this->itemTable, $this->itemChildTable])
            ->where(['parent' => $name, 'name' => new Expression('child')]);

        $children = [];
        foreach ($query->all($this->db) as $row) {
            $children[$row['name']] = $this->populateItem($row);
        }

        return $children;
    }

    /**
     * Checks whether there is a loop in the authorization item hierarchy.
     *
     * @param Item $parent the parent item
     * @param Item $child  the child item to be added to the hierarchy
     *
     * @return boolean whether a loop exists
     */
    protected function detectLoop($parent, $child)
    {
        if ($child->name === $parent->name) {
            return true;
        }
        foreach ($this->getChildren($child->name) as $grandchild) {
            if ($this->detectLoop($parent, $grandchild)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Assigns a role to a user.
     *
     * @param Role $role
     * @param string|integer $userId the user ID (see [[\yii\web\User::id]])
     * @param Rule $rule the rule to be associated with this assignment. If not null, the rule
     * will be executed when [[allow()]] is called to check the user permission.
     * @param mixed $data additional data associated with this assignment.
     * @return Assignment the role assignment information.
     * @throws \Exception if the role has already been assigned to the user
     */
    public function assign($role, $userId, $rule = null, $data = null)
    {
        $assignment = new Assignment([
                'userId'    => $userId,
                'roleName'  => $role->name,
                'createdAt' => new DateTime(),
        ]);

        TagDependency::invalidate(Yii::$app->cache, '#table_cache#' . $this->assignmentTable);
        $this->db->createCommand()
            ->insert(
                $this->assignmentTable,
                [
                    'user_id'    => $assignment->userId,
                    'item_name'  => $assignment->roleName,
                    'created_at' => $assignment->createdAt,
                ]
            )->execute();
        return $assignment;
    }

    /**
     * Revokes a role from a user.
     *
     * @param Role           $role
     * @param string|integer $userId the user ID (see [[\yii\web\User::id]])
     *
     * @throws \yii\base\UserException
     * @return boolean whether the revoking is successful
     */
    public function revoke($role, $userId)
    {
        if ($role->name == $this->superRoleName && count($this->getUsersByRole($role)) == 1) {
            throw new UserException('Нельзя удалить привилегированную роль у последнего пользователя');
        }
        TagDependency::invalidate(Yii::$app->cache, '#table_cache#' . $this->assignmentTable);
        return $this->db->createCommand()
            ->delete($this->assignmentTable, ['user_id' => (string)$userId, 'item_name' => $role->name])
            ->execute() > 0;
    }

    /**
     * @inheritdoc
     */
    public function revokeAll($userId)
    {
        if ($this->getAssignment($this->superRoleName, $userId)
            && count($this->getUsersByRole($this->getRole($this->superRoleName))) == 1
        ) {
            throw new UserException('Нельзя удалить последнего пользователя с привилегированными правами');
        }
        TagDependency::invalidate(Yii::$app->cache, '#table_cache#' . $this->assignmentTable);
        return $this->db->createCommand()
            ->delete($this->assignmentTable, ['user_id' => (string)$userId])
            ->execute() > 0;
    }

    /**
     * @inheritdoc
     */
    public function removeAll()
    {
        $this->removeAllAssignments();
        TagDependency::invalidate(Yii::$app->cache, '#table_cache#' . $this->itemChildTable);
        TagDependency::invalidate(Yii::$app->cache, '#table_cache#' . $this->itemTable);
        $this->db->createCommand()->delete($this->itemChildTable)->execute();
        $this->db->createCommand()->delete($this->itemTable)->execute();
        $this->db->createCommand()->delete($this->ruleTable)->execute();
    }

    /**
     * @inheritdoc
     */
    public function removeAllPermissions()
    {
        $this->removeAllItems(Item::TYPE_PERMISSION);
    }

    /**
     * @inheritdoc
     */
    public function removeAllRoles()
    {
        $this->removeAllItems(Item::TYPE_ROLE);
    }

    /**
     * Removes all auth items of the specified type.
     *
     * @param integer $type the auth item type (either Item::TYPE_PERMISSION or Item::TYPE_ROLE)
     */
    protected function removeAllItems($type)
    {
        TagDependency::invalidate(Yii::$app->cache, '#table_cache#' . $this->assignmentTable);
        TagDependency::invalidate(Yii::$app->cache, '#table_cache#' . $this->itemChildTable);
        TagDependency::invalidate(Yii::$app->cache, '#table_cache#' . $this->itemTable);
        if (!$this->supportsCascadeUpdate()) {
            $names = (new Query)
                ->select(['name'])
                ->from($this->itemTable)
                ->where(['type' => $type])
                ->column($this->db);
            if (empty($names)) {
                return;
            }
            $key = $type == Item::TYPE_PERMISSION ? 'child' : 'parent';
            $this->db->createCommand()
                ->delete($this->itemChildTable, [$key => $names])
                ->execute();
            $this->db->createCommand()
                ->delete($this->assignmentTable, ['item_name' => $names])
                ->execute();
        }
        $this->db->createCommand()
            ->delete($this->itemTable, ['type' => $type])
            ->execute();
    }

    /**
     * @inheritdoc
     */
    public function removeAllRules()
    {
        TagDependency::invalidate(Yii::$app->cache, '#table_cache#' . $this->itemTable);
        if (!$this->supportsCascadeUpdate()) {
            $this->db->createCommand()
                ->update($this->itemTable, ['ruleName' => null])
                ->execute();
        }

        $this->db->createCommand()->delete($this->ruleTable)->execute();
    }

    /**
     * @inheritdoc
     */
    public function removeAllAssignments()
    {
        TagDependency::invalidate(Yii::$app->cache, '#table_cache#' . $this->assignmentTable);
        $this->db->createCommand()->delete($this->assignmentTable)->execute();
    }

    /**
     * Creates a new Role object.
     * Note that the newly created role is not added to the RBAC system yet.
     * You must fill in the needed data and call [[add()]] to add it to the system.
     *
     * @param string $name the role name
     *
     * @return Role the new Role object
     */
    public function createRole($name)
    {
        $role = new Role;
        $role->name = $name;
        $role->is_system = false;
        return $role;
    }

    /**
     * Creates a new Permission object.
     * Note that the newly created permission is not added to the RBAC system yet.
     * You must fill in the needed data and call [[add()]] to add it to the system.
     *
     * @param string $name the permission name
     *
     * @return Permission the new Permission object
     */
    public function createPermission($name)
    {
        $permission = new Permission();
        $permission->name = $name;
        $permission->is_system = false;
        return $permission;
    }

    /**
     * Adds a role, permission or rule to the RBAC system.
     *
     * @param Role|Permission|Rule $object
     *
     * @return boolean whether the role, permission or rule is successfully added to the system
     * @throws \Exception if data validation or saving fails (such as the name of the role or permission is not unique)
     */
    public function add($object)
    {
        if ($object instanceof Item) {
            return $this->addItem($object);
        } elseif ($object instanceof Rule) {
            return $this->addRule($object);
        } else {
            throw new InvalidParamException("Adding unsupported object type.");
        }
    }

    /**
     * Removes a role, permission or rule from the RBAC system.
     *
     * @param Item|Rule $object
     *
     * @throws \yii\base\InvalidParamException
     * @return boolean whether the role, permission or rule is successfully removed
     */
    public function remove($object)
    {
        if ($object instanceof Item) {
            return $this->removeItem($object);
        } elseif ($object instanceof Rule) {
            return $this->removeRule($object);
        } else {
            throw new InvalidParamException("Removing unsupported object type.");
        }
    }

    /**
     * Updates the specified role, permission or rule in the system.
     *
     * @param string               $name the old name of the role, permission or rule
     * @param Role|Permission|Rule $object
     *
     * @return boolean whether the update is successful
     * @throws \Exception if data validation or saving fails (such as the name of the role or permission is not unique)
     */
    public function update($name, $object)
    {
        if ($object instanceof Item) {
            return $this->updateItem($name, $object);
        } elseif ($object instanceof Rule) {
            return $this->updateRule($name, $object);
        } else {
            throw new InvalidParamException("Updating unsupported object type.");
        }
    }

    /**
     * Returns the named role.
     *
     * @param string $name the role name.
     *
     * @return Role the role corresponding to the specified name. Null is returned if no such role.
     */
    public function getRole($name)
    {
        $item = $this->getItem($name);
        return $item instanceof Item && $item->type == Item::TYPE_ROLE ? $item : null;
    }

    /**
     * Returns the named permission.
     *
     * @param string $name the permission name.
     *
     * @return Permission the permission corresponding to the specified name. Null is returned if no such permission.
     */
    public function getPermission($name)
    {
        $item = $this->getItem($name);
        return $item instanceof Item && $item->type == Item::TYPE_PERMISSION ? $item : null;
    }

    /**
     * Returns all roles in the system.
     *
     * @param bool $withSystem
     *
     * @return Role[] all roles in the system. The array is indexed by the role names.
     */
    public function getRoles($withSystem = false)
    {
        return $this->getItems(Item::TYPE_ROLE, $withSystem);
    }

    /**
     * Returns all permissions in the system.
     *
     * @param bool $withSystem Включать или нет системные операции
     *
     * @return Permission[] all permissions in the system. The array is indexed by the permission names.
     */
    public function getPermissions($withSystem = false)
    {
        return $this->getItems(Item::TYPE_PERMISSION, $withSystem);
    }

    /**
     * Executes the rule associated with the specified auth item.
     *
     * If the item does not specify a rule, this method will return true. Otherwise, it will
     * return the value of [[Rule::execute()]].
     *
     * @param string|integer $user   the user ID. This should be either an integer or a string representing
     *                               the unique identifier of a user. See [[\yii\web\User::id]].
     * @param Item           $item   the auth item that needs to execute its rule
     * @param array          $params parameters passed to [[ManagerInterface::checkAccess()]] and will be passed to the rule
     *
     * @return boolean the return value of [[Rule::execute()]]. If the auth item does not specify a rule, true will be returned.
     * @throws InvalidConfigException if the auth item has an invalid rule.
     */
    protected function executeRule($user, $item, $params)
    {
        if ($item->ruleName === null) {
            return true;
        }
        $rule = $this->getRule($item->ruleName);
        if ($rule instanceof Rule) {
            return $rule->execute($user, $item, $params);
        } else {
            throw new InvalidConfigException("Rule not found: {$item->ruleName}");
        }
    }

    /**
     * Возвращает массив идентификаторов пользователей, которые привязаны к роли
     *
     * @param Role $role
     *
     * @return array
     */
    public function getUsersByRole($role)
    {
        return (new Query)
            ->select('user_id')
            ->from($this->assignmentTable)
            ->where(['item_name' => $role->name])
            ->column();
    }
}
