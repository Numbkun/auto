<?php

namespace common\components;

use common\models\Domain;
use yii\base\Component;
use Yii;
use yii\caching\TagDependency;

class Shop extends Component {
    /** @var null|integer идентификатор магазина */
    public $id = null;
    /** @var string класс модели магазина */
    public $identityClass = 'common\models\Shop';
    /** @var string запущенное приложение (frontend|backend) */
    public $app = 'frontend';
    /** @var null|\common\models\Shop экземпляр магазина */
    protected $_identity = null;

    /**
     * @inheritdoc
     */
    public function init() {
        // Если идентификатор магазина еще не указан, определим его по заголовкам
        if ($this->id === null && isset($_SERVER['HTTP_HOST'])) {
            /** @var Domain $domain */
            $domain = Yii::$app->db->cache(function () {
                return Domain::find()->select('shop_id')->where(['name' => $_SERVER['HTTP_HOST']])->one();
            }, 0, new TagDependency(['tags' => '#table_cache#' . Domain::tableName()]));
            if ($domain) {
                $this->id = $domain->shop_id;
            }
        }
        $identity = $this->getIdentity();
        if ($identity) {
            Yii::$app->id = $this->id;
            if ($identity->theme_id) {
                Yii::$app->view->theme->pathMap = ['@app/views' => $identity->theme->getThemePathAlias() . '/' . $this->app];
            }
        } else {
            throw new ShopNotFoundHttpException('Доменное имя, к которому вы обращаетесь, неккоректно. Проверьте, что написано в адресной строке.');
        }
    }

    /**
     * Возвращает идентификатор основной валюты магазина
     *
     * @return int
     */
    public function getCurrencyId()
    {
        return $this->getIdentity()->currency_id;
    }

    /**
     * Возвращает экземпляр основной валюты магазина
     *
     * @return \common\models\Currency
     */
    public function getCurrency()
    {
        return $this->getIdentity()->currency;
    }

    /**
     * Возвращает модель магазина
     * @return null|\common\models\Shop
     */
    public function getIdentity() {
        if (!$this->_identity && $this->id !== null) {
            /** @var \common\models\Shop $class */
            $class = $this->identityClass;
            $this->_identity = Yii::$app->db->cache(function () use ($class) {
                return $class::findOne($this->id);
            }, 0, new TagDependency(['tags' => '#table_cache#' . $class::tableName()]));

        }
        return $this->_identity;
    }
}