<?php

namespace common\components;

use yii\db\ActiveQuery;
use Yii;

/**
 * Класс для работы с запросами к бд связанных с моделью PriceByShop
 *
 * @package common\components
 */
class PriceByShopQuery extends ActiveQuery
{

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->where(['shop_id' => Yii::$app->shop->id]);
        parent::init();
    }

    /**
     * Сброс условий
     * @return static
     */
    public function reset() {
        $this->where = null;
        return $this;
    }

}