<?php

namespace common\components;

use yii\base\Component;
use Yii;

/**
 * Класс компонента для работы с настройками
 *
 * @property string $shop_default_user_name        имя пользователя для вновь создаваемого магазина
 * @property string $shop_default_user_password    пароль пользователя для вновь создаваемого магазина
 * @property string $shop_default_user_email       e-mail пользователя для вновь создаваемого магазина
 * @property string $shop_default_role_name        идентификатор роли администратора для вновь создаваемого магазина
 * @property string $shop_default_role_description наименование роли администратора для вновь создаваемого магазина
 * @property string $shop_default_phone            главный телефон магазина
 * @property string $shop_secondary_phone          второстепенный телефон магазина
 *
 * @package common\components
 */
class Setting extends Component
{

    /** @var array Массив значений настроек для кеширования */
    protected $_settings = array();

    /**
     * @inheritdoc
     */
    public function __get($name)
    {
        if ($value = $this->get($name)) {
            return $value;
        }
        return parent::__get($name);
    }

    /**
     * Возвращает значение настройки
     * @param string $name    наименование настройки
     * @param mixed  $default значение, которое вернется если значение настройки не найдено
     *
     * @return null
     */
    public function get($name, $default = null)
    {
        if (isset($this->_settings[$name])) {
            return $this->_settings[$name];
        } elseif (($model = \common\models\Setting::findOne(['name' => $name])) !== null) {
            // TODO сделать кеширование модели
            $this->_settings[$name] = $model->value;
            return $this->_settings[$name];
        }
        return $default;
    }
}