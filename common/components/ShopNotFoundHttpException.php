<?php

namespace common\components;

use yii\web\NotFoundHttpException;

/**
 * Класс Исключения "Магазин не найден"
 *
 * @package common\components
 */
class ShopNotFoundHttpException extends NotFoundHttpException {

}