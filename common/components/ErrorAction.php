<?php

namespace common\components;

use Yii;

/**
 * Класс для вывода ошибок работы приложения
 *
 * @package common\components
 */
class ErrorAction extends \yii\web\ErrorAction {

    /**
     * @inheritdoc
     */
    public function run() {
        if (($exception = Yii::$app->getErrorHandler()->exception) !== null) {
            if ($exception instanceof ShopNotFoundHttpException) {
                $this->controller->layout = '@common/views/layouts/system';
            }
        }
        return parent::run();
    }

}