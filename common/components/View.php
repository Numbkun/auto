<?php
/**
 * Created by IntelliJ IDEA.
 * User: aydin
 * Date: 05.08.2014
 * Time: 19:45
 */

namespace common\components;

use common\widgets\ExtendBlock;
use Yii;

/**
 * Компонент View в MVC паттерне
 *
 * @package backend\components
 */
class View extends \yii\web\View
{

    /** @var string папка с шаблонами по умолчанию */
    public $defaultDir = 'default';
    public $description = '';
    public $keywords = '';

    /**
     * @inheritdoc
     */
    public function findViewFile($view, $context = null)
    {
        $path = parent::findViewFile($view, $context);
        if ($this->theme !== null) {
            $path = $this->theme->applyTo($path);
        }
        if (!is_file($path) && Yii::$app->controller !== null) {
            $file = Yii::$app->controller->module->getViewPath() . '/' . $this->defaultDir . '/' . ltrim($view, '/');

            if (pathinfo($file, PATHINFO_EXTENSION) !== '') {
                return $file;
            }

            $path = $file . '.' . $this->defaultExtension;
            if ($this->defaultExtension !== 'php' && !is_file($path)) {
                $path = $file . '.php';
            }
        }
        return $path;
    }

    /**
     * Открывает блок, который можно расширить в наследуемых шаблонах
     *
     * @param integer $id            идентификатор блока
     * @param bool    $renderInPlace вывести блок на месте
     *
     * @return ExtendBlock
     */
    public function beginExtBlock($id, $renderInPlace = false)
    {
        return ExtendBlock::begin(
            [
                'id'            => $id,
                'renderInPlace' => $renderInPlace,
                'view'          => $this,
            ]
        );
    }

    /**
     * Закрывает расширяемый блок
     */
    public function endExtBlock()
    {
        ExtendBlock::end();
    }

}