<?php

namespace common\components;

use InvalidArgumentException;
use yii\base\Object;
use Yii;

/**
 * Компонент для загрузки файла с помощью пути
 *
 * @package common\components
 */
class PathFile extends Object
{

    /**
     * @var string путь к файлу
     */
    public $path;

    /**
     * @var string имя файла
     */
    public $name;

    /**
     * @var string расширение файла
     */
    public $extension;

    /**
     * Получение экземпляра текущего компонента по пути к файлу
     *
     * @param string $path путь к файлу
     *
     * @throws \InvalidArgumentException
     * @return static
     */
    public static function getInstance($path)
    {
        if (!file_exists($path)) {
            throw new InvalidArgumentException('$path должен указывать на реальный файл');
        }
        $pathInfo = pathinfo($path);
        $object = new static([
                'path'      => $path,
                'name'      => $pathInfo['filename'],
                'extension' => $pathInfo['extension'],
            ]);
        return $object;
    }

    /**
     * Сохраняет загружаемый файл в требуемое место
     *
     * @param string $file           путь по которому надо сохранить загружаемый файл
     * @param bool   $deleteTempFile удалить или нет исходный файл
     *
     * @return bool результат сохранения
     */
    public function saveAs($file, $deleteTempFile = false)
    {
        if ($deleteTempFile) {
            return rename($this->path, $file);
        } else {
            return copy($this->path, $file);
        }
    }
}