<?php
return [
    'name' => 'Кооожа',
    'language' => 'ru',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class'        => '\yii\caching\MemCache',
            'useMemcached' => true,
            'servers'      => [
                [
                    'host' => '127.0.0.1',
                ],
            ],
        ],
        'authManager' => [
            'class' => 'common\components\rbac\DbManager',
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'dateFormat' => 'dd.MM.yyyy',
            'datetimeFormat' => 'dd.MM.yyyy HH:mm:ss',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules'          => [
                '/'                                         => 'site/index',
                '<controller>' => '<controller>/index',
                '<controller>/<action>/<id:\d+>' => '<controller>/<action>/view',
                '<controller>/<action>' => '<controller>/<action>',
            ]
        ],
        'settings' => [
            'class' => 'common\components\Setting'
        ],
        'view' => [
            'class' => 'common\components\View',
            'theme' => [
                'basePath' => '@app/views'
            ],
        ],
        'db' => [
                 'enableSchemaCache'   => true,
                 'schemaCacheDuration' => 300,
             ],
    ],
];
