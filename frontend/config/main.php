<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'shop'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'user' => [
            'class'         => 'frontend\components\User',
            'identityClass' => 'common\models\Customer',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'shop' => [
            'class' => 'common\components\Shop',
            'app'   => 'frontend',
        ],
        'assetManager' => [
                'bundles' => [
                    'yii\bootstrap\BootstrapAsset' => [
                        'css' => [],
                    ],
                ],
        ],
    ],
    'params' => $params,
];
