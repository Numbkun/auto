<?php
/**
 * Вывод полной новости
 *
 * @var  News $model
 * @var \common\components\View $this
 */

use common\models\News;
use yii\helpers\Html;

$this->title = $model->name;
$this->description = $model->page_description;
$this->keywords = $model->page_keywords;
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['/news/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $model->text ?>

