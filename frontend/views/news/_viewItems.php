<?php
/**
 * Вывод новостей
 *
 * @var  News $model
 *
 */

use common\models\News;
use yii\bootstrap\Collapse;
use yii\helpers\Html;
?>

<?= Collapse::widget([
    'items' => [
        [
            'label' => '<span style="color: #a8a8a8">' . Yii::$app->formatter->format($model->created_at, 'date')  . '</span> ' . $model->name.'  ' . Html::a('Страница новости', ['/news/view', 'translite' => $model->translite],['style' => 'display:none; float:right']),
            'content' => $model->description,
        ],
    ],
    'encodeLabels' => false
]);

$this->registerJs( '$(".collapse-toggle" ).click(function(){$(this).next().toggle();});');

