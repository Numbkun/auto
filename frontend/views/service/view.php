<?php
/**
 * Вывод карточки товара
 *
 * @var  common\models\Category $model
 * @var  common\models\Category $parentCategory
 * @var  common\models\Product[] $products
 * @var  common\models\Category[] $childCategories
 * @var  ActiveDataProvider $dataProvider
 */

use common\models\Product;
use common\widgets\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;


if($model->parent_category_id){
    foreach($model->getDirectParentCategories() as $parent){
        $this->params['breadcrumbs'][] = ['label' => $parent, 'url' => ['/service/view', 'translite' => $parent->translite]];
    }
}
$this->params['breadcrumbs'][] = $model->name;
?>
<h1><?= $model->name; ?></h1>
<div><?= $model->text; ?></div>
<div class="row">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'actionColumn' => [
            'template' => '{view}',
            'buttons' => [
                'view' => function ($url, $model) {
                    /** @var Product $model */
                    unset($url);
                    return Html::a(
                        'Подробнее',
                        [
                            '/product/view',
                            'id' => $model->primaryKey,
                        ]
                    );
                },
            ]
        ],
        'columns' => [
            'name',
        ],
    ]); ?>
</div>