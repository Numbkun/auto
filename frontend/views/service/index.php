<?php
/**
 * @var yii\web\View                    $this
 * @var yii\data\DataProviderInterface  $dataProvider
 */
use common\models\Product;
use common\widgets\GridView;
use yii\helpers\Html;

$this->title = 'Каталог услуг';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'actionColumn' => [
        'template' => '{view}',
        'buttons' => [
            'view' => function ($url, $model) {
                /** @var Product $model */
                unset($url);
                return Html::a(
                    'Подробнее',
                    [
                        '/product/view',
                        'id' => $model->primaryKey,
                    ]
                );
            },
        ]
    ],
    'columns' => [
        'name',
    ],
]); ?>