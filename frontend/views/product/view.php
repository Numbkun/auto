<?php
/**
 * Вывод карточки товара
 *
 * @var  Product $model
 * @var \common\components\View $this
 */

use common\models\Product;
use frontend\assets\PrettyPhotoAsset;
use yii\bootstrap\Carousel;
use yii\helpers\Html;

PrettyPhotoAsset::register($this);

$this->title = $model->name;
if($model->category_id){
    foreach($model->category->getDirectParentCategories() as $parent){
        $this->params['breadcrumbs'][] = ['label' => $parent, 'url' => ['/catalog/view', 'translite' => $parent->translite]];
    }
    $this->params['breadcrumbs'][] = ['label' => $model->category->name, 'url' => ['/catalog/view', 'translite' => $model->category->translite]];
}
$this->params['breadcrumbs'][] = $model->name;
?>
    <!--ГЛАВНЫЙ ТОВАР-->
    <div class="border-top m-top-55 m-bottom-10">
        <div class="border-top-shadow"></div>
        <div class="rubric-name"><h1><?= $model->name ?></h1></div>
    </div>
    <div class="row block-product carousel-main-images" style="padding: 15px">
        <!--  Картинки товара  -->
        <div class="col-md-6">
            <?php if (!$model->productImages) {
                echo Html::img($model->mainImage->publishImage(350, 350), ['alt' => $model->name]);
            } else { ?>
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <?php
                        $i = 0;
                        foreach ($model->productImages as $image) {
                            if ($i === 0) {
                                ?>
                                <li data-target="#carousel-example-generic" data-slide-to="<?= $i ?>" class="active"
                                    style="background: url('<?= $image->image->publishImage(100,
                                        70) ?>') no-repeat; width: 100px; height: 70px;"></li>
                                <?php
                                $i = $i + 1;
                            } else {
                                ?>
                                <li data-target="#carousel-example-generic" data-slide-to="<?= $i ?>"
                                    style="background: url('<?= $image->image->publishImage(100,
                                        70) ?>') no-repeat; width: 100px; height: 70px;"></li>
                                <?php
                                $i = $i + 1;
                            }
                        }
                        ?>
                    </ol>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <?php
                        $i = 0;
                        foreach ($model->productImages as $image) {
                            if ($i === 0) {
                                $class = ' active';
                                $i = $i + 1;
                            }
                            ?>
                            <div class="item <?= $class ? $class : '' ?>">
                                <?= Html::a(Html::img($image->image->publishImage(350, 350), ['alt' => $model->name]),
                                    $image->image->publishImage(1024, 768), ['rel' => 'prettyPhoto[gallery1]',]); ?>
                            </div>
                            <?php
                            $class = '';
                        }
                        ?>
                    </div>
                </div>
            <?php } ?>
        </div>
        <!--  Центральный блок (ЦЕНА, РЕЙТИНГ, КНОПКА, ОПИСАНИЕ)  -->
        <div class="col-md-4 col-xs-10 pad-top-15">
            <?php if ($model->in_stock) { ?>
                <?php if ($model->price) { ?>
                    <span class="m-bottom-5" style="display: block; font-size: 2.5em"><?= $model->price . '&#8381;' ?></span>
                <?php } ?>
            <?php } else { ?>
                <span class="m-bottom-5" style="display: block; font-size: 2.5em">Нет в наличии</span>
            <?php } ?>
            <?php if ($model->description) { ?>
                <span class="text-product-bold" style="display: block">Описание</span>
                <p><?= $model->description ?></p>
            <?php } ?>
        </div>
        <!--  Блок вывода производителя  -->
        <div class="col-md-2 col-xs-2 pad-top-15">
            <?php if ($image = $model->manufacture->image) { ?>
                <img src="<?= $image->publishImage(130, 70) ?>" align="right">
            <?php } ?>
        </div>
    </div>

<?php if (!empty($model->complementaryProducts)) { ?>
    <!--С ЭТИМ ТОВАРОМ ПОКУПАЮТ-->
    <div class="border-top m-top-55 m-bottom-10" style="">
        <div class="border-top-shadow"></div>
        <div class="rubric-name"><h1>С этим товаром покупают</h1></div>
    </div>
    <?php
    $complementaryProductsChunks = array_chunk($model->complementaryProducts, 4);
    $items = [];
    foreach ($complementaryProductsChunks as $products) {
        $items[] = $this->render('_viewProduct', ['products' => $products]);
    }
    echo Carousel::widget(
        [
            'items' => $items,
            'showIndicators' => false,
            'controls' => ['<span class="arrow-left"></span>', '<span class="arrow-right"></span>']
        ]
    );
}

$this->registerJs('$("a[rel^=\'prettyPhoto\']").prettyPhoto({deeplinking: false});');
