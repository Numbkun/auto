<?php
/**
 * Вывод вспомогательной вьюшки для предстовления товара в рубриках
 *
 * @var  Product[] $products
 * @var \common\components\View $this
 */
use common\models\Product;
use yii\helpers\Html;

?>
<div class="row block-product">
    <?php foreach ($products as $product) { ?>
        <div class="col-xs-3 pad-bottom-20 pad-top-20" style="min-height: 315px">
            <?php
            if ($image = $product->productImage) {
                echo Html::a('<img src="' . $image->image->publishImage(260, 190) . '" class="img-product">',
                    ['product/view', 'id' => $product->id]);
            } else {
                echo Html::a(Html::img($product->mainImage->publishImage(260, 190), ['alt' => $product->name]),  ['product/view', 'id' => $product->id]);
            }
            echo Html::a('<span class="name-product m-top-10 m-bottom-10">' . $product->name . '</span>',
                ['product/view', 'id' => $product->id]);
            ?>
            <?php if ($product->in_stock) { ?>
                <?php if ($product->price) { ?>
                    <span class="price-product"><?= $product->price . '&#8381;' ?></span>
                <?php } ?>
            <?php } else { ?>
                <span class="price-product">Нет в наличии</span>
            <?php } ?>
        </div>
    <?php } ?>
</div>
