<?php
/**
 * Вывод карточки товара
 *
 * @var  common\models\Category $model
 * @var  common\models\Category $parentCategory
 * @var  common\models\Product[] $products
 * @var  common\models\Category[] $childCategories
 * @var  ActiveDataProvider $dataProvider
 */

use yii\data\ActiveDataProvider;
use yii\widgets\ListView;


$this->params['breadcrumbs'][] = 'Поиск товаров по запросу: ' . $term;
?>
<h1>Результат поиска товаров по запросу: "<?= $term; ?>"</h1>
<div class="row">
    <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_viewItems',
            'layout' => "{items}\n{pager}"
        ]); ?>
</div>