<?php
/**
 * Вывод карточки товара
 *
 * @var  common\models\Category $model
 * @var  common\models\Category $parentCategory
 * @var  common\models\Product[] $products
 * @var  common\models\Category[] $childCategories
 * @var  ActiveDataProvider $dataProvider
 */

use yii\data\ActiveDataProvider;
use yii\widgets\ListView;


if($model->parent_category_id){
    foreach($model->getDirectParentCategories() as $parent){
        $this->params['breadcrumbs'][] = ['label' => $parent, 'url' => ['/catalog/view', 'translite' => $parent->translite]];
    }
}
$this->params['breadcrumbs'][] = $model->name;
?>
<h1><?= $model->name; ?></h1>
<div><?= $model->text; ?></div>
<div class="row">
    <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_viewItems',
            'layout' => "{items}\n{pager}"
        ]); ?>
</div>