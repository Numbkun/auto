<?php
/**
 * Вывод товара
 *
 * @var  Product $model
 */

use common\models\Product;
use frontend\widgets\BuyButton;
use yii\helpers\Html;

?>
<div class="product col-md-4">
    <div>
        <?php
//        if ($model->mainImage) {
//            echo Html::a(Html::img($model->mainImage->publishImage(120, 160), ['width' => 120, 'height' => 160, 'alt' => '']), ['product/view', 'id' => $model->id]);
//        } else {
//            echo Html::img($model->mainImage->publishImage(120, 160), ['width' => 120, 'height' => 160, 'alt' => '']);
//        }
        ?>
        <div class="product-name">
            <?= Html::a($model->name, ['/product/view','id' => $model->primaryKey,]) ?>
        </div>
        <?php if ($model->in_stock) { ?>
            <?php if ($model->price) { ?>
                <div class="product-price"><?= $model->price . '&#8381;' ?></div>
            <?php } ?>
        <?php } else { ?>
            <div class="product-price">Нет в наличии</div>
        <?php } ?>
    </div>
</div>