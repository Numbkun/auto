<?php
/**
 * @var yii\web\View                    $this
 * @var yii\data\DataProviderInterface  $dataProvider
 */
use yii\helpers\Html;
use yii\widgets\ListView;

$this->title = 'Каталог';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>
<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_viewItems',
    'layout' => "{items}\n{pager}"
]); ?>