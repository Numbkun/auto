<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
if ($exception instanceof \yii\web\HttpException) {
    $code = $exception->statusCode;
} else {
    $code = $exception->getCode();
}

$this->params['breadcrumbs'][] = Html::encode($code) . '. ' . nl2br(Html::encode($message));

?>
<div class="not-found center-block">
    <div class="not-found-code"><?= Html::encode($code) ?></div>
    <div class="not-found-title"><?= nl2br(Html::encode($message)) ?></div>
    <div class="not-found-text">Пожалуйста, перейдите на <span><?= Html::a('главную страницу', Url::home()) ?></span><br />что бы продолжить покупки</div>
</div>