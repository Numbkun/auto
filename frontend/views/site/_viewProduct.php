<?php
/**
 * Вывод вспомогательной вьюшки для предстовления товара в рубриках
 *
 * @var  Product[] $products
 * @var \common\components\View $this
 */
use common\models\Product;
use yii\helpers\Html;

$horizontalLine = true;
?>
<div class="row">
    <?php foreach ($products as $product) { ?>
        <div class="col-md-6">
            <div class="block-product" style="padding: 30px">
                <div class="row">
                    <div class="col-xs-7 col-md-6 col-lg-7">
                        <?= Html::a('<span class="name-product-main m-bottom-20">' . $product->name . '</span>',
                            ['product/view', 'id' => $product->id]) ?>
                        <?php if ($product->manufacture->image) { ?>
                            <img src="<?= $product->manufacture->image->publishImage(130, 70) ?>" style="display: block"
                                 class="m-bottom-20">
                        <?php } ?>
                        <p class="text-product-main m-bottom-20"><?= $product->productType ? $product->productType : '' ?>
                            <br/><?= $product->article ? 'Артикул: ' . $product->article : '' ?></p>
                        <?php if ($product->in_stock) { ?>
                            <?php if ($product->price) { ?>
                                <span class="price-product-main m-right-10"><?= $product->price . '&#8381;' ?></span>
                            <?php } ?>
                        <?php } else { ?>
                            <span class="price-product-main m-right-10">Нет в наличии</span>
                        <?php } ?>
                    </div>
                    <div class="pull-right">
                        <?php if ($product->productImage) { ?>
                            <?= Html::a(Html::img($product->productImage->image->publishImage(210, 310),
                                ['class' => 'img-product']), ['product/view', 'id' => $product->id]) ?>
                        <?php } else {
                            echo Html::a(Html::img($product->mainImage->publishImage(210, 310),
                                ['width' => 210, 'height' => 310, 'alt' => '', 'class' => 'img-product']),
                                ['product/view', 'id' => $product->id]);
                        } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($horizontalLine) { ?>
            <div class="visible-sm-block visible-xs-block" style="height: 20px"></div>
        <?php }
        $horizontalLine = false;
    } ?>
</div>
