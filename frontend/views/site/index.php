<?php
/* @var $this yii\web\View
 * @var $banners \common\models\Banner[]
 * @var $products \common\models\Product[]
 * @var $news \common\models\News[]
 */
use frontend\widgets\Banner\Banner;
use yii\helpers\Html;

$this->title = 'Коооожа';
?>
<div style="padding: 35px 0">
    <?= Banner::widget(['banners' => $banners])?>

    <div class="row">
        <div class="col-xs-6">
            <?php if ($news) {?>
                <h3>Новости</h3>
                <?php foreach ($news as $item) { ?>
                    <div>
                        <div><?=$item->page_title ?></div>
                        <div><?=$item->description ?></div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>

        <div class="col-xs-6">
            <?php if ($products) { ?>
                <h3>Зап. части</h3>
                <?php foreach ($products as $product) { ?>
                    <div class="clearfix m-top-15">
                        <div class="pull-left"><?=Html::img($product->mainImage->publishImage(100, 100), ['alt' => $product->name]) ?></div>
                        <div class="pull-left m-left-15">
                            <div><?=$product->name ?></div>
                            <div><?=$product->description ?></div>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>

</div>
