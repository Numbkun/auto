<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;

/* @var \frontend\controllers\CatalogController $controller */
/* @var \common\components\View $this */
/* @var $content string */

AppAsset::register($this);
if ($this->description) {
    $this->registerMetaTag(['content' => $this->description, 'name' => 'description'], 'description');
}
if ($this->keywords) {
    $this->registerMetaTag(['content' => $this->keywords, 'name' => 'keywords'], 'keywords');
}
if (Yii::$app->settings->shop_default_phone) {
    $defaultPhone1 = substr(Yii::$app->settings->shop_default_phone, 0, 3);
    $defaultPhone2 = substr(Yii::$app->settings->shop_default_phone, 3, 3);
    $defaultPhone3 = substr(Yii::$app->settings->shop_default_phone, 6, 2);
    $defaultPhone4 = substr(Yii::$app->settings->shop_default_phone, 8, 2);
}
if (Yii::$app->settings->shop_secondary_phone) {
    $secondaryPhone1 = substr(Yii::$app->settings->shop_secondary_phone, 0, 3);
    $secondaryPhone2 = substr(Yii::$app->settings->shop_secondary_phone, 3, 3);
    $secondaryPhone3 = substr(Yii::$app->settings->shop_secondary_phone, 6, 2);
    $secondaryPhone4 = substr(Yii::$app->settings->shop_secondary_phone, 8, 2);
}
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <div class="container m-top-20">
            <div class="row">
                <div class="col-xs-2">
                    <?= Html::a('', Url::home(), ['class' => 'logo']) ?>
                </div>
                <div class="col-xs-10">
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Главная</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                    <li><?=Html::a('Кондиционеры', '/static-page/view?translite=kondicionery') ?></li>
                                    <li><?=Html::a('Автономка', '/static-page/view?translite=avtonomka') ?></li>
                                    <li><a href="<?=Url::to('/service') ?>">Доп. услуги</a></li>
                                    <li><a href="<?=Url::to('/catalog') ?>">Каталог</a></li>
                                    <li><?=Html::a('Контакты', '/static-page/view?translite=kontakty') ?></li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="border-top">
            <div class="border-top-shadow"></div>
        </div>
        <div class="col-xs-3 small pad-left-0">
            &copy; <?= date('Y') ?> "Автосервис 5+"<br>
        </div>
        <div class="col-xs-2">
            <ul class="list-unstyled smaller">
                <li><a href="<?=Url::to('/service') ?>">Доп. услуги</a></li>
                <li><a href="<?=Url::to('/catalog') ?>">Каталог</a></li>
            </ul>
        </div>
        <div class="col-xs-2">
            <ul class="list-unstyled smaller">
                <li><?=Html::a('Контакты', '/static-page/view?translite=kontakty') ?></li>
            </ul>
        </div>
        <div class="col-xs-3">
            <?php if (isset($defaultPhone1) && isset($defaultPhone2) && isset($defaultPhone3) && isset($defaultPhone4)) { ?>
                <div class="phone">
                    <small style="padding-bottom: 5px"><?= $defaultPhone1 ?></small>
                    <span><?= $defaultPhone2 . '-' . $defaultPhone3 . '-' . $defaultPhone4 ?></span>
                </div>
            <?php } ?>
            <?php if (isset($secondaryPhone1) && isset($secondaryPhone2) && isset($secondaryPhone3) && isset($secondaryPhone4)) { ?>
                <div class="phone">
                    <small><?= $secondaryPhone1 ?></small>
                    <span><?= $secondaryPhone2 . '-' . $secondaryPhone3 . '-' . $secondaryPhone4 ?></span>
                </div>
            <?php } ?>
        </div>
        <div class="col-xs-2 pad-right-0">
            <ul class="list-inline pull-right">
                <li><a href="#">
                        <div class="fb"></div>
                    </a></li>
                <li><a href="#">
                        <div class="tw"></div>
                    </a></li>
                <li><a href="#">
                        <div class="vk"></div>
                    </a></li>
            </ul>
        </div>
    </footer>

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>
<?php

