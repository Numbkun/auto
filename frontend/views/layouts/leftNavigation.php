<?php

/**
 * @var \yii\web\View $this
 * @var string        $content
 * @var \frontend\controllers\CatalogController        $controller
 * @var  common\models\Category $model
 */

use frontend\assets\AppAsset;

AppAsset::register($this);
$controller = $this->context;
?>
<?php $this->beginContent('@app/views/layouts/main.php'); ?>
<div style="display: table; width: 100%;">
        <div id="left-menu">
            <?=
            \yii\widgets\Menu::widget(
                [
                    'activateParents' => true,
                    'items' => $controller->getSiteMenu(),
                    'options' => ['class' => 'catalog-menu'],
                ]
            )
            ?>
        </div>
        <div>
            <?= $content ?>
        </div>
</div>
<?php $this->endContent();
