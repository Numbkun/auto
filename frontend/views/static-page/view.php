<?php
/**
 * Вывод полной новости
 *
 * @var  StaticPage $model
 * @var \common\components\View $this
 */

use common\models\StaticPage;
use yii\helpers\Html;

$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>
<?= $model->text ?>


