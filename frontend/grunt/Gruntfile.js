module.exports = function(grunt) {
    'use strict';

    // Force use of Unix newlines
    grunt.util.linefeed = '\n';

    var bumped = false;

    grunt.registerTask('increase-version', 'Increase Version of Package', function() {
        if (!bumped) {
            grunt.task.run(['bump-only', 'clean:version', 'version-file']);
            bumped = true;
        }
    });

    grunt.registerMultiTask('version-file', 'Creating Version Files', function() {
        var pkg = grunt.file.readJSON('package.json');
        this.files.forEach(function(file) {
            file.dest.forEach(function (dest) {
                grunt.file.write(dest + 'version-' + pkg.version + '.txt', '');
            })
        });
    });

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        assets: '../web',
        bootstrap: '../../vendor/bower/bootstrap',
        less: {
            // Компиляция less файлов терминала в css
            leather: {
                options: {
                    strictMath: true,
                    sourceMap: true,
                    outputSourceFiles: true,
                    sourceMapURL: '<%= pkg.name %>.css.map',
                    sourceMapFilename: '<%= assets %>/css/<%= pkg.name %>.css.map'
                },
                files: {
                    '<%= assets %>/css/<%= pkg.name %>.css': 'less/leather.less'
                }
            },
            // Минификация css файлов терминала
            leatherMinify: {
                options: {
                    cleancss: true,
                    report: 'min'
                },
                files: {
                    '<%= assets %>/css/<%= pkg.name %>.min.css': '<%= assets %>/css/<%= pkg.name %>.css'
                }
            }
        },
        concat: {

            // Объединение js файлов Bootstrap
            bootstrap: {
                src: [
                    //'<%= bootstrap %>/js/transition.js',
                    '<%= bootstrap %>/js/carousel.js',
                    //'<%= bootstrap %>/js/collapse.js',
                    //'<%= bootstrap %>/js/modal.js',
                    //'<%= bootstrap %>/js/tooltip.js',
                    //'<%= bootstrap %>/js/popover.js'
                ],
                dest: '<%= assets %>/js/bootstrap.js'
            }
        },
        uglify: {
            // Минификация js файлов Bootstrap
            bootstrap: {
                src: '<%= concat.bootstrap.dest %>',
                dest: '<%= assets %>/js/bootstrap.min.js'
            }
        },
        copy: {
            // Копирование шрифтов из bootstrap
            bootstrapFonts: {
                expand: true,
                cwd: '<%= bootstrap %>/',
                src: 'fonts/*',
                dest: '<%= assets %>/'
            }
        },
        clean: {
            options: {
                force: true
            },
            // Очистка папки css в assets
            css: '<%= assets %>/css',
            // Очистка папки fonts в assets
            fonts: '<%= assets %>/fonts',
            // Очистка папки js в assets
            js: '<%= assets %>/js',
            // Удаление файла с версией
            version: [
                '<%= assets %>/version-*.txt',
                '<%= assets %>/*/version-*.txt'
            ]
        },
        "version-file":{
            // Папки, в которых создавать файл с версией
            main:{
                dest: [
                    '<%= assets %>/',
                    '<%= assets %>/css/',
                    '<%= assets %>/js/',
                    '<%= assets %>/fonts/'
                ]
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-bump');

    // Общие задачи
    grunt.registerTask('base', []);

    // Сборка js bootstrap
    grunt.registerTask('bootstrap-js', ['concat:bootstrap', 'uglify:bootstrap', 'base']);

    // Сборка css для модуля "leather"
    grunt.registerTask('css', ['less:leather', 'less:leatherMinify', 'base']);
    // Сборка js для модуля "leather". Пока ничего не делает забил на всякий случай
    grunt.registerTask('js', ['base']);

    // Сборка всего и вся
    grunt.registerTask('default', ['css', 'js', 'bootstrap-js', 'base']);

};