<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class PrettyPhotoAsset extends AssetBundle
{
    public $sourcePath = '@bower/jquery-prettyPhoto';
    public $css = [
        'css/prettyPhoto.css',
    ];
    public $js = [
        'js/jquery.prettyPhoto.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
