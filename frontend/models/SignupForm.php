<?php
namespace frontend\models;

use common\models\Customer;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $email;
    public $password;
    public $first_name;
    public $last_name;
    public $mobile;
    public $patronymic;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\Customer', 'targetAttribute' => ['email'], 'message' => 'Этот E-mail адрес уже зарегестрирован.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            [['first_name', 'last_name', 'mobile'], 'required'],
            [['first_name', 'last_name', 'patronymic'], 'string', 'max' => 85],
            [['mobile'], 'integer',],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'first_name'    => 'Имя',
                'last_name'     => 'Фамилия',
                'patronymic'    => 'Отчество',
                'mobile'        => 'Мобильный телефон',
                'email'         => 'E-mail',
                'auth_key'      => 'Ключ аутентификации',
                'password'      => 'Пароль',
            ]
        );
    }

    /**
     * Signs user up.
     *
     * @return Customer|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $customer = new Customer();
            $customer->first_name = $this->first_name;
            $customer->last_name = $this->last_name;
            $customer->patronymic = $this->patronymic;
            $customer->email = $this->email;
            $customer->mobile = $this->mobile;
            $customer->is_active = true;
            $customer->password_hash = Yii::$app->getSecurity()->generatePasswordHash($this->password);
            $customer->generateAuthKey();
            $customer->save();
            return $customer;
        }

        return null;
    }
}
