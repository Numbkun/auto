<?php
namespace frontend\models;

use common\models\BaseForm;
use common\models\Customer;
use common\models\Order;
use common\models\OrderProduct;
use common\models\Sku;
use Yii;
use yii\base\Exception;
use yii\db\Query;

/**
 * Форма для создания заказа
 * @property string  $first_name    Имя
 * @property string  $last_name     Фамилия
 * @property string  $patronymic    Отчество
 * @property string  $mobile        Мобильный телефон
 * @property string  $email         E-mail
 * @property string  $comment       Комментарий к заказу
 */
class CartOrderForm extends BaseForm
{
    public $last_name = '';
    public $first_name = '';
    public $mobile = '';
    public $email = '';
    public $shipping_address = '';
    public $comment = '';

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [

                'first_name' => 'Имя',
                'last_name' => 'Фамилия',
                'mobile' => 'Мобильный телефон',
                'email' => 'E-mail',
                'shipping_address' => 'Адрес доставки',
                'comment' => 'Комментарий',

            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['first_name', 'last_name', 'mobile', 'email', 'shipping_address'], 'required'],
                [['shipping_address', 'comment'], 'string', 'max' => 255],
                [['first_name', 'last_name',], 'string', 'max' => 85],
                [['mobile'], 'string', 'max' => 15],
                [['mobile'], 'integer'],
                [['email'], 'email'],
                [
                    ['email'],
                    'unique',
                    'targetClass' => Customer::className(),
                    'filter' => function ($query) {
                        if (!Yii::$app->user->isGuest) {
                            /** @var Query $query */
                            $query->andWhere(['!=', 'id', Yii::$app->user->id]);
                        }
                    }
                ],
            ]
        );
    }

    /**Создание нового заказа
     * @return int|null
     * @throws Exception
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public function createOrder()
    {
        if (!Yii::$app->session['productCart']) {
            throw new Exception('Ошибка при создании заказа');
        }
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $modelOrder = new Order();

            if (Yii::$app->user->isGuest) {
                $customer = $this->createNewCustomer();
            } else {
                $customer = $this->updateOldCustomer();
            }

            $modelOrder->customer_id = $customer->id;
            $modelOrder->shop_id = $customer->shop_id;
            $modelOrder->comment = $this->comment;
            $newPrice = 0;
            foreach (Yii::$app->user->getProductsCart() as $skuId => $count) {
                $skus = Sku::findOne($skuId);
                $newPrice += $skus->price->value;
            }
            $modelOrder->price = $newPrice;
            $modelOrder->save();

            foreach (Yii::$app->user->getProductsCart() as $skuId => $count) {
                $skus = Sku::findOne($skuId);
                if (!$skus) {
                    throw new Exception('Модель Sku не найдена ' . $skuId);
                }
                $modelOrderProduct = new OrderProduct();
                $modelOrderProduct->order_id = $modelOrder->id;
                $modelOrderProduct->sku_id = $skuId;
                $modelOrderProduct->amount = $count;
                $modelOrderProduct->price = $skus->price->value;
                $modelOrderProduct->save();
            }

            Yii::$app->user->clearCart();

            Yii::$app->mailer->compose('successOrder',
                ['customer' => $customer, 'orderNumber' => $modelOrder->name])
                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                ->setTo($this->email)
                ->setSubject('Ваш заказ в интернет-магазине ' . Yii::$app->name)
                ->send();

            $transaction->commit();
            return $modelOrder->id;
        } catch (Exception $e) {
            $transaction->rollBack();
            if (defined('YII_DEBUG') && YII_DEBUG) {
                throw $e;
            }
        }
        return null;
    }

    /**Создание нового покупателя
     */
    public function createNewCustomer()
    {
        $customer = new Customer();
        $customer->first_name = $this->first_name;
        $customer->last_name = $this->last_name;
        $customer->email = $this->email;
        $customer->mobile = $this->mobile;
        $customer->shipping_address = $this->shipping_address;
        $customer->patronymic = '';
        $customer->is_active = true;
        $password = Yii::$app->getSecurity()->generateRandomString(8);
        $customer->password_hash = Yii::$app->getSecurity()->generatePasswordHash($password);
        $customer->auth_key = Yii::$app->getSecurity()->generateRandomString();
        $customer->save();

        Yii::$app->mailer->compose('newCustomer', ['customer' => $customer, 'password' => $password])
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo($this->email)
            ->setSubject('Регистрация нового пользователя ' . Yii::$app->name)
            ->send();
        return $customer;
    }

    /**Изменение данных зарегестрированного покупателя
     */
    public function updateOldCustomer()
    {
        $customer = Customer::findOne(Yii::$app->user->id);
        $customer->first_name = $this->first_name;
        $customer->last_name = $this->last_name;
        $customer->mobile = $this->mobile;
        $customer->shipping_address = $this->shipping_address;
        $customer->save();
        return $customer;
    }

    /**Загрузка данных уже зарегестрированного покупателя
     * @param Customer $customer
     */
    public function loadFromModel($customer)
    {
        if ($customer) {
            $this->first_name = $customer->first_name;
            $this->last_name = $customer->last_name;
            $this->email = $customer->email;
            $this->mobile = $customer->mobile;
            $this->shipping_address = $customer->shipping_address;
        }
    }
}
