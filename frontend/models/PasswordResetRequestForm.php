<?php
namespace frontend\models;

use common\models\Customer;
use Yii;
use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => 'common\models\Customer',
                'message'     => 'Не существует клиента с таким E-mail'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $customer Customer */
        $customer = Customer::findOne([
            'email' => $this->email,

        ]);
        if ($customer) {
            if (!Customer::isPasswordResetTokenValid($customer->password_reset_token)) {
                $customer->generatePasswordResetToken();
            }
            if ($customer->save()) {
                return Yii::$app->mailer->compose('passwordResetToken', ['customer' => $customer])
                    ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                    ->setTo($this->email)
                    ->setSubject('Восстановление пароля для ' . Yii::$app->name)
                    ->send();
            }
        }

        return false;
    }
}
