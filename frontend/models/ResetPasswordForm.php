<?php
namespace frontend\models;

use common\models\Customer;
use yii\base\InvalidParamException;
use yii\base\Model;
use Yii;

/**
 * Password reset form
 */
class ResetPasswordForm extends Model
{
    public $password;

    /**
     * @var \common\models\Customer
     */
    private $_customer;


    /**
     * Creates a form model given a token.
     *
     * @param  string                          $token
     * @param  array                           $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Password reset token cannot be blank.');
        }
        $this->_customer = Customer::findByPasswordResetToken($token);
        if (!$this->_customer) {
            throw new InvalidParamException('Wrong password reset token.');
        }
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'password'      => 'Новый пароль',
            ]
        );
    }

    /**
     * Поменять пароль
     *
     * @return boolean if password was reset.
     */
    public function resetPassword()
    {
        $customer = $this->_customer;
        $customer->password_hash = Yii::$app->getSecurity()->generatePasswordHash($this->password);
        $customer->removePasswordResetToken();

        return $customer->save();
    }
}
