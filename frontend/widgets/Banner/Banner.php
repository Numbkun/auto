<?php
namespace frontend\widgets\Banner;

use yii\base\Widget;

class Banner extends Widget
{
    /**
     * @var array Массив баннеров
     */
    public $banners = [];
    /**
     * @inheritdoc
     */
    public function run()
    {
        if ($this->banners) {
            return $this->render('@frontend/widgets/Banner/views/banner', ['banners' => $this->banners]);
        } else {
            return null;
        }
    }
}
