<?php

namespace frontend\widgets\Banner;

use kartik\widgets\AssetBundle;

class BannerAsset extends AssetBundle
{
    public $sourcePath = '@frontend/widgets/Banner/assets';

    public $css = [
        'css/banner.css'
    ];
    public $js = [
        'js/modernizr.custom.js',
        'js/banner.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\jui\JuiAsset',
    ];
}
