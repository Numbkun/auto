<?php
/**
 * @var Banner[] $banners
 */
use common\models\Banner;
use frontend\widgets\Banner\BannerAsset;

BannerAsset::register($this);
$this->registerJs("
                $('#da-slider').cslider({
                    autoplay	: true,
					bgincrement	: 500
                });
            ");
?>
<div id="da-slider" class="da-slider">
    <?php foreach ($banners as $banner) { ?>
        <div class="da-slide">
            <h2><?=$banner->name?></h2>
            <?php if ($banner->text) { ?>
                <p><?=$banner->text?></p>
            <?php } ?>
            <?php if ($banner->url) { ?>
                <a href="<?=$banner->url ?>" class="da-link">Подробнее...</a>
            <?php } ?>
            <div class="da-img"><img src="<?=$banner->image->publishImage(256, 256)?>" alt="" /></div>
        </div>
    <?php } ?>
    <nav class="da-arrows">
        <span class="da-arrows-prev"></span>
        <span class="da-arrows-next"></span>
    </nav>
</div>
