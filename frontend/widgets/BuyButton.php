<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\widgets;

use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * Виджет вывода кнопки добавления в корзину
 */
class BuyButton extends Widget
{
    /**
     * @var integer Обязательный атрибут! Идентификатор модификации товара
     */
    public $skuId;

    /**
     * @var string Контент кнопки
     */
    public $content = '<span class="glyphicon glyphicon-shopping-cart pad-right-5"></span>В КОРЗИНУ';

    /**
     * @var array Массив настроек кнопки
     */
    public $options = ['class' => 'btn btn-default'];

    /**
     * @var string Размер кнопки (btn-lg, btn-long)
     */
    public $sizeClass = '';

    public function init()
    {
        parent::init();
        if (!$this->skuId) {
            throw new InvalidConfigException('Атрибут skuId обязательный!');
        }
        Html::addCssClass($this->options, 'addToCart');
        if ($this->sizeClass) {
            Html::addCssClass($this->options, $this->sizeClass);
        }
        if (!isset($this->options['data-sku-id'])) {
            $this->options['data-sku-id'] = $this->skuId;
        }
    }

    /**
     * Создаем кнопку и регистрируем JS к ней
     */
    public function run()
    {
        parent::run();

        echo Html::button($this->content, $this->options);

        $this->getView()->registerJs
        ('
            $(".addToCart").click(function(e){
            e.preventDefault();
            $.ajax({
                type: "GET",
                dataType : "json",
                url: "' . Url::to(['cart/add-product',]) . '",
                data: {"skuId": $(this).data("skuId")},
                success: function(data) {
                if(data["totalCount"]){
                    $("#shopping-cart").html("Товаров в корзине: " + data["totalCount"]);
                }else{
                    $("#shopping-cart").html("Корзина");
                }
                }
                });
            });
        ', View::POS_READY, 'addToCart');
    }
}
