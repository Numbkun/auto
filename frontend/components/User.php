<?php
namespace frontend\components;

use common\models\Cart;
use common\models\Customer;
use common\models\Region;
use Yii;
use yii\web\Cookie;

/**
 * @inheritdoc
 * @property Region   $region
 * @property Customer $identity
 * @package frontend\components
 */
class User extends \yii\web\User
{
    /**
     * @return Region
     */
    public function getRegion()
    {
        if (!$this->isGuest) {
            return $this->identity->region;
        }

        if (isset(Yii::$app->request->cookies['region_id'])) {
            return Region::findOne(Yii::$app->request->cookies['region_id']);
        }
        $regions = Yii::$app->shop->getIdentity()->regions;
        if ($regions) {
            return $regions[0];
        }
        $regions = Region::find()->limit(1)->all();
        if ($regions) {
            return $regions[0];
        }
        return null;
    }

    /**
     * Метод для запоминания посещенных товаров
     * @param $id
     */
    public function setProductHistory($id)
    {
        $data = Yii::$app->request->cookies->getValue('productHistory', []);
        if (!in_array($id, $data)) {
            array_push($data, $id);
        }
        if (count($data) > 10) {
            $data = array_slice($data, -10, 10);
        }
        $cookie = new Cookie([
            'name' => 'productHistory',
            'value' => $data,
            'expire' => time() + 86400 * 7,
        ]);
        \Yii::$app->response->cookies->add($cookie);
    }

    /**
     * Метод для вывода посещенных товаров
     *
     * @return array возвращает массив id-шников
     */
    public function getProductHistory()
    {
        return Yii::$app->request->cookies->getValue('productHistory', []);
    }

    /**
     * Метод для вывода товаров в корзине
     *
     * @return array
     */
    public function getProductsCart()
    {
        $session = Yii::$app->session['productCart'];
        if (!$this->isGuest && !$session) {
            /** @var Cart[] $models */
            $models = Cart::find()->where(['customer_id' => $this->id])->all();
            foreach ($models as $model) {
                $session[$model->sku_id] = $model->count;
            }
            Yii::$app->session['productCart'] = $session;
        }

        return $session;
    }

    /**
     * Метод для добавления товара в корзину
     *
     * @return bool
     * @param integer $skuId
     */
    public function addProductCart($skuId)
    {
        $session = Yii::$app->session['productCart'];
        if (!$session) {
            $session = [];
        }
        if (!array_key_exists($skuId, $session)) {
            $session[$skuId] = 1;
            Yii::$app->session['productCart'] = $session;
        }
        if (!Yii::$app->user->isGuest) {
            $checkModel = Cart::find()->where(['sku_id' => $skuId, 'customer_id' => Yii::$app->user->id])->one();
            if (!$checkModel) {
                $model = new Cart();
                $model->customer_id = Yii::$app->user->id;
                $model->sku_id = $skuId;
                $model->save();
            }
        }
        return true;
    }

    /**
     * Метод для удаления товара из корзины
     *
     * @param integer $skuId
     * @return bool
     */
    public function delProductCart($skuId)
    {
        if (!$this->isGuest) {
            $model = Cart::find()->where(['sku_id' => $skuId, 'customer_id' => Yii::$app->user->id])->one();
            $model->delete();
        }
            $session = Yii::$app->session['productCart'];
        if ($session) {
            unset ($session[$skuId]);
            Yii::$app->session['productCart'] = $session;
        }
        return true;
    }

    /**
     * Метод для изменения кол-ва товаров в корзине
     *
     * @return array возвращает массив выбранных товаров
     * @param integer $count
     * @param integer $skuId
     */
    public function changeProductCart($skuId, $count)
    {
        $session = Yii::$app->session['productCart'];
        if (Yii::$app->session['productCart'][$skuId]) {
            $count = $count< 1 ? 1 : $count;
            $session[$skuId] = $count;
            Yii::$app->session['productCart'] = $session;
        }
        if (!Yii::$app->user->isGuest) {
            /** @var Cart $model */
            $model = Cart::find()->where(['sku_id' => $skuId, 'customer_id' => Yii::$app->user->id])->one();
            $model->count = $count;
            $model->save();
        }
        return true;
    }

    /**
     * Полная Очистка корзины
     */
    public function clearCart()
    {
        Yii::$app->session['productCart'] = [];

        $models = Cart::find()->where(['customer_id' => Yii::$app->user->id])->all();
        foreach ($models as $model) {
        $model->delete();
        }
    }
}