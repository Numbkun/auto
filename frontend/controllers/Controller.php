<?php

namespace frontend\controllers;

use common\models\BaseActiveRecord;
use common\models\Category;
use yii\web\NotFoundHttpException;
use Yii;

/**
 * Базовый контроллер
 *
 * @package backend\controllers
 */
abstract class Controller extends \yii\web\Controller
{
    /**
     * @var string название класса базовой модели контроллера, для стандартных действий (CRUD)
     */
    public $modelClass = 'Model';

    /**
     * @var array массив меню с подкаталогом.
     */
    protected $siteMenu;

    /**
     * @var string активная категория.
     */
    protected $activeCategory;

    /**
     * @var array массив меню с подкаталогом.
     */
    protected $menu = [];

    /**
     * Получаем массив категорий для бокового меню
     */
    public function getSiteMenu()
    {}

    /**
     * Пытаемся получить активную категорию
     * @return string
     */
    public function getActiveCategory(){
        if(!$this->activeCategory){
            if($translite = Yii::$app->request->get('translite')){
                $this->activeCategory = Category::find()->where(['translite' => $translite])->one();
                return $this->activeCategory;
            }
            return null;
        }
        return $this->activeCategory;
    }


    /**
     * Поиск и получение экземпляра модели по идентификатору
     *
     * @param mixed $id идентификатор
     *
     * @param null  $modelClass название класса, модель которого надо найти.
     *                                Если не указано, то будет использовано свойство контроллера "modelClass"
     *
     * @throws \yii\web\NotFoundHttpException если экземпляр не найден
     * @return BaseActiveRecord
     */
    public function findModel($id, $modelClass = null)
    {
        /** @var BaseActiveRecord $className */
        $className = $modelClass ? : $this->modelClass;
        if (($model = $className::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException("Экземпляр не найден");
        }
    }
}