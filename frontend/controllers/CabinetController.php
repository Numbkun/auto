<?php
namespace frontend\controllers;

use common\models\Order;
use common\models\Product;
use kartik\widgets\ActiveForm;
use Yii;
use frontend\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Response;

/**
 * Cabinet controller
 */
class CabinetController extends Controller
{
    public $layout = 'leftNavigation';
    /**
     *Личный кабинет
     */
    public function actionIndex()
    {
        $model = Yii::$app->user->getIdentity();

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                if (!($errors = ActiveForm::validate($model))) {
                    $model->save();
                }
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $errors;
            }
        } elseif (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->save();
            }
        }
        $modelOrder = Order::findAll(['customer_id' => Yii::$app->user->id]);

        return $this->render('index', ['model' => $model, 'modelOrder' => $modelOrder]);
    }

    /**
     * Вход в учетную запись
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            return $this->goBack();
        } else {
            if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Выход из учетной записи
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }


    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($model);
                }
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Восстановление пароля через письмо на емаил
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Проверьте свой E-mail для дальнейшего восстановления.');

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error',
                    'Извините, мы не можем отправит сообщение на этот E-mail');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Изменение пароля
     * @param $token ResetPasswordForm уникальный токен для каждого клиента
     * @throws BadRequestHttpException
     * @return string|\yii\web\Response
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'Новый пароль был сохранен успешно.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}