<?php

namespace frontend\controllers;

use common\models\File;
use Yii;


/**
 * Контроллер публикации изображений
 *
 * @package frontend\controllers
 */
class PublisherController extends Controller
{

    /** @inheritdoc */
    public $modelClass = 'common\models\File';

    /**
     * Перенос файлов с папки uploads в assets
     *
     * @param integer $id
     * @return string
     */
    public function actionPublish($id)
    {
        /** @var File $model */
        $model = $this->findModel(['id' => $id, 'path' => 'static']);
        $model->publish();
        return Yii::$app->response->sendFile($model->getOriginalPath(), null, ['inline' => true]);
    }
}