<?php
namespace frontend\controllers;

use common\models\Product;
use Exception;
use Yii;
use yii\web\Response;

/**
 * Контроллер товара
 *
 * @package frontend\controllers
 */
class ProductController extends Controller
{
    public $modelClass = 'common\models\Product';

    /**
     * Вывод карточки товара
     *
     * @param $id Product id товара который нужно вывести
     * @return string
     */
    public function actionView($id)
    {
        Yii::$app->user->setProductHistory($id);

        $model = $this->findModel($id);

        return $this->render('view', ['model' => $model]);
    }

    /**
     * Изменение рейтинга товара
     *
     * @package frontend\controllers
     * @param $productId
     * @throws Exception
     * @throws \yii\web\NotFoundHttpException
     * @return array
     */
    public function actionRating($productId)
    {
        $result = 'fail';
        $model = $this->findModel($productId);
        $model->scenario = Product::SCENARIO_RATING;
        if ($model->load(Yii::$app->request->get()) && $model->validate()) {
            try {
                $model->save();
                $result = 'success';
            } catch (Exception $e) {
                if (defined('YII_DEBUG') && YII_DEBUG) {
                    throw $e;
                }
            }
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            "result" => $result,
            'errors' => $model->getErrors()
        ];
    }

}