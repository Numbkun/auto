<?php

namespace frontend\controllers;

use common\models\News;
use yii\data\ActiveDataProvider;

/**
 * Контроллер новостей
 *
 * @package frontend\controllers
 */
class NewsController extends Controller
{

    /** @inheritdoc */
    public $modelClass = 'common\models\News';

    /**
     * Вывод списка экземпляров модели
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => News::find(),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                    'id' => SORT_ASC,
                ]
            ]
        ]);
        return $this->render(
            'index',
            [
                'dataProvider' => $dataProvider
            ]
        );
    }

    public function actionView($translite)
    {
        $model = $this->findModel(['translite' => $translite]);
        return $this->render(
            'view',
            [
                'model' => $model
            ]
        );
    }
}