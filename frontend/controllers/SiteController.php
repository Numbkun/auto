<?php
namespace frontend\controllers;

use common\models\Banner;
use common\models\News;
use common\models\Product;
use Yii;
use common\models\Dignity;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $banners = Banner::find()->orderBy(['id' => SORT_DESC])->all();

        /** @var Product $product */
        $products = [] ;
        foreach (Product::getProducts()->limit(6)->all() as $product) {
            if ($product) {
                $products[] = $product;
            }
        }

        $news = [] ;
        foreach (News::find()->limit(6)->orderBy(['created_at' => SORT_DESC, 'id' => SORT_ASC])->all() as $new) {
            $news[] = $new;
        }

        return $this->render('index',[
            'banners'  => $banners,
            'products' => $products,
            'news'     => $news,
        ]);
    }

    public function getAllDignity()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Dignity::find(),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                    'id' => SORT_ASC,
                ]
            ]
        ]);
        return $dataProvider;

    }
}
