<?php

namespace frontend\controllers;

/**
 * Контроллер статической страницы
 *
 * @package frontend\controllers
 */
class StaticPageController extends Controller
{

    /** @inheritdoc */
    public $modelClass = 'common\models\StaticPage';

    /** @inheritdoc */
    public function actionView($translite)
    {
        $model = $this->findModel(['translite' => $translite]);
        return $this->render(
            'view',
            [
                'model' => $model
            ]
        );
    }
}