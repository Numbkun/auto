<?php
namespace frontend\controllers;

use frontend\models\CartOrderForm;
use common\models\Order;
use common\models\Product;
use common\models\Sku;
use Exception;
use kartik\widgets\ActiveForm;
use Yii;
use yii\data\ArrayDataProvider;
use yii\web\Response;

/**
 * Контроллер корзины заказов
 *
 * @package frontend\controllers
 */
class CartController extends Controller
{
    /**
     * Вывод корзины
     * @var integer $sumCount колличество товаров в корзине
     * @var integer $sumPrice общая цена товаров в корзине
     * @var array $data массив товаров, где идентификатор это sku_id, а значение - кол-во товара
     * @return array
     */
    public function actionView()
    {
        $data = Yii::$app->user->getProductsCart();
        $dataProvider = [];
        $totalCount = '';
        $totalPrice = '';
        if ($data) {
            $totalCount = array_sum($data);
            $skus = Sku::find()->where(['id' => array_keys($data)])->indexBy('id')->all();
            $dataForProvider = [];
            foreach ($data as $skuId => $count) {
                $dataForProvider[] = [
                    'model' => $skus[$skuId],
                    'count' => $count,
                ];
            }
            $prices = [];
            foreach ($dataForProvider as $product) {
                /** @var Sku $product['model'] */
                $prices[] = $product['model']->price->value * $product['count'];
            }
            $totalPrice = array_sum($prices);
            $dataProvider = new ArrayDataProvider([
                'allModels' => $dataForProvider,
                'sort' => false,
            ]);
        }
        $history = [];
        if ($productId = Yii::$app->user->getProductHistory()) {
            $history = Product::find()->where(['id' => $productId])->orderBy(['id' => SORT_ASC])->all();
        }

        $cartOrderForm = new CartOrderForm();
        $cartOrderForm->loadFromModel(Yii::$app->user->getIdentity());

        return $this->render('view', [
            'dataProvider' => $dataProvider,
            'totalCount' => $totalCount,
            'totalPrice' => $totalPrice,
            'history' => $history,
            'cartOrderForm' => $cartOrderForm,
        ]);
    }

    /**
     * Добавление товара в корзину
     * @param  integer $skuId
     */
    public function actionAddProduct($skuId)
    {
        Yii::$app->user->addProductCart($skuId);
        $data = Yii::$app->user->getProductsCart();
        $totalCount = '';
        if ($data) {
            $totalCount = array_sum($data);
        }
        echo json_encode(['totalCount' => $totalCount]);
    }

    /**
     * Удаление товара в корзину
     * @param  integer $skuId
     * @return \yii\web\Response
     */
    public function actionDelProduct($skuId)
    {
        Yii::$app->user->delProductCart($skuId);

        return $this->redirect(['view',]);
    }

    /**
     * Контроллер товара
     * @param int $skuId идентификатор товара корзины
     * @param int $count идентификатор кол-ва товаров корзины
     * @return array
     */
    public function actionUpdateProduct($skuId, $count)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $count = (integer)$count;
            $skuId = (integer)$skuId;
            if (Yii::$app->user->changeProductCart($skuId, $count)) {
                $data = Yii::$app->user->getProductsCart();
                $totalPrice = 0;
                $totalCount = 0;
                foreach ($data as $skuId => $count) {
                    /** @var Sku $sku */
                    $sku = Sku::findOne($skuId);
                    if ($sku) {
                        $totalPrice += $sku->price->value * $count;
                        $totalCount += $count;
                    }
                }
                return ['totalPrice' => Yii::$app->formatter->asCurrency($totalPrice, 'RUB'), 'totalCount' => $totalCount];
            }
        }
        return [];
    }

    /**
     * Контроллер нового заказа
     * @return array
     * @throws Exception
     */
    public function actionNewOrder()
    {
        $model = new CartOrderForm();
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            if ($model->validate() && $orderId = $model->createOrder()) {
                return $this->redirect(['cart/success-order', 'orderId' => $orderId] );
            }
        }
        throw new Exception('Новый заказ не создан');
    }

    /**
     * Создание заказа прошло успешно
     * @param integer $orderId
     * @return array
     */
    public function actionSuccessOrder($orderId)
    {
        $order = Order::findOne($orderId);
        return $this->render('successOrder', ['order' => $order]);
    }
}