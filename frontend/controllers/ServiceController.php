<?php

namespace frontend\controllers;

use common\models\Category;
use common\models\Product;
use common\models\ShopProduct;
use yii\data\ActiveDataProvider;


/**
 * Контроллер каталога услуг
 *
 * @package frontend\controllers *
 */
class ServiceController extends Controller
{
    /**
     * @inheritdoc
     */
    public $layout = 'leftNavigation';
    public $leftMenu = array();
    public $curentCategory;

    /**
     * Вывод списка экземпляров модели
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Product::find()
                ->innerJoin(ShopProduct::tableName(), ShopProduct::tableName() . '.product_id = ' . Product::tableName() . '.id')
                ->innerJoin(Category::tableName() . ' c', 'c.id = ' . ShopProduct::tableName() . '.category_id')
                ->andWhere('c.is_list=true'),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                    'id' => SORT_ASC,
                ]
            ]
        ]);
        return $this->render(
            'index',
            [
                'dataProvider' => $dataProvider
            ]
        );
    }

    /** @inheritdoc */
    public function actionView($translite)
    {
        /** @var Category $model */
        $model = $this->findModel(['translite' => $translite], Category::className());
        $this->curentCategory = $model;
        $this->leftMenu = $model->childCategories;
        $dataProvider = new ActiveDataProvider([
            'query' => $model->getProducts(),
                'pagination' => [
                'pageSize' => 15,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                    'id' => SORT_ASC,
                ]
            ]
        ]);

        return $this->render('view',['model' => $model, 'dataProvider' => $dataProvider]);
    }

    /**
     * Экшн поиска товаров
     */
    public function actionSearchProducts($term)
    {
        $this->layout = 'leftNavigation';
        $dataProvider = new ActiveDataProvider([
            'query' => Product::find()->where(['ilike','name',$term]),
            'pagination' => [
                'pageSize' => 15,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                    'id' => SORT_ASC,
                ]
            ]
        ]);

        return $this->render('/catalog/searchView',['dataProvider' => $dataProvider, 'term' => $term]);
    }

    /**
     * inhered
     */
    public function getSiteMenu()
    {
        if(!$this->siteMenu){
            $rootCategories = Category::getRootCategories()->andWhere(['is_list' => true])->all();
            $activeCategory = $this->getActiveCategory();
            /** @var Category $rootCategory */
            foreach($rootCategories as $rootCategory){
                $this->menu[$rootCategory['id']] = ['label' => $rootCategory->name, 'url' => ['service/view', 'translite' => $rootCategory->translite]];
                $menuItems = Category::find()->where(['parent_category_id' => $rootCategory['id']])->all();
                foreach($menuItems as $item){
                    $active = false;
                    $item = Category::find()->where(['id' => $item['id']])->one();
                    if($activeCategory['id'] == $item['id']){
                        $active = true;
                    }
                    $this->menu[$rootCategory['id']]['items'][$item['id']] = ['label' => $item['name'], 'url' => ['service/view', 'translite' => $item['translite']], 'active' => $active];
                }
            }
            $this->siteMenu = $this->menu;
        }

        return $this->siteMenu;
    }
}