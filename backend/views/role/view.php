<?php

use common\widgets\GridView;
use yii\data\ActiveDataProvider;

/**
 * @var \common\components\View $this
 * @var \backend\models\AuthRole $model
 */

require($this->findViewFile('/default/view', $this->context));

echo "<h2>Операции</h2>",
GridView::widget(['dataProvider' => new ActiveDataProvider(['query' => $model->getPermissions()]), 'actionColumn' => false]);
