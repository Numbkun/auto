<?php

use yii\helpers\Html;

/**
 * @var \common\components\View $this
 * @var \backend\models\AuthRole $model
 */

// Добавим полей в базовую форму
$block = $this->beginExtBlock('fields');
$block->parentBlock();
$permissions = $model->getAvailablePermissions();
if ($permissions) {
    echo '<h2>Разрешения для роли</h2>';
    echo Html::activeCheckboxList($model, 'permissions', $permissions, [
            'item' => function ($index, $label, $name, $checked, $value) {
                    return '<div class="checkbox">' . Html::checkbox(
                        $name,
                        $checked,
                        ['label' => $label, 'value' => $value]
                    ) . '</div>';
                }
    ]);
}
$this->endExtBlock();

// Подключим базовую форму
require($this->findViewFile('/default/_form', $this->context));