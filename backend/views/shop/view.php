<?php

/**
 * @var \common\components\View $this
 * @var \common\models\Shop      $model
 */

// Подключим базовый шаблон
require($this->findViewFile('/default/view', $this->context));
// Список доменов
require($this->findViewFile('_listDomain', $this->context));
