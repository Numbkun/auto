<?php

/**
 * @var yii\web\View          $this
 * @var \common\models\Domain $model
 */

use common\models\Shop;

$this->title = $model . ' (редактирование)';
$this->params['breadcrumbs'][] = ['label' => Shop::getPluralNominativeName(), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->shop, 'url' => ['view', 'id' => $model->shop_id]];
$this->params['breadcrumbs'][] = $this->title;
echo $this->render('_formDomain', ['model' => $model], $this->context);

