<?php
/**
 * @var \common\components\View $this
 * @var \common\models\Reference $model
 */


use yii\helpers\Html;

if ($model->isNewRecord):

    $block = $this->beginExtBlock('fields');
    $block->parentBlock();
    ?>
    <div class="form-group">
        <div class="checkbox">
            <?= Html::label(Html::checkbox('createUser', true) . ' Создать пользователя') ?>
        </div>
    </div>
    <?php
    $this->endExtBlock();

endif;
// Подключим базовый шаблон
require($this->findViewFile('/default/create', $this->context));
