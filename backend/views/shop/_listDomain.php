<?php

use common\widgets\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

/**
 * @var \common\components\View $this
 * @var \common\models\Shop      $model
 */

/** @var \backend\controllers\Controller $controller */
$controller = $this->context;

$actionsTemplate = [];
if (Yii::$app->user->can($controller->viewPermission)) {
    $actionsTemplate[] = '{view}';
}
if (Yii::$app->user->can($controller->updatePermission)) {
    $actionsTemplate[] = '{update}';
    $actionsTemplate[] = '{delete}';
}

$actionsTemplate = implode(' ', $actionsTemplate);

echo "<h2>Домены</h2>",
GridView::widget(
    [
        'dataProvider'   => new ActiveDataProvider(['query' => $model->getDomains()]),
        'toolbarButtons' => Yii::$app->user->can($controller->updatePermission) ? [
                Html::a(
                    '<span class="glyphicon glyphicon-plus"></span>',
                    ['create-domain', 'shopId' => $model->primaryKey],
                    ['class' => 'btn btn-success']
                )
            ] : [],
        'actionColumn'   => [
            'template' => $actionsTemplate,
            'buttons'  => [
                'view'   => function ($url, $model) {
                        unset($url);
                        return Html::a(
                            '<span class="glyphicon glyphicon-eye-open"></span>',
                            ['view-domain', 'id' => $model->primaryKey],
                            [
                                'title'     => Yii::t('yii', 'View'),
                                'data-pjax' => '0',
                            ]
                        );
                    },
                'update' => function ($url, $model) {
                        unset($url);
                        return Html::a(
                            '<span class="glyphicon glyphicon-pencil"></span>',
                            ['update-domain', 'id' => $model->primaryKey],
                            [
                                'title'     => Yii::t('yii', 'Update'),
                                'data-pjax' => '0',
                            ]
                        );
                    },
                'delete' => function ($url, $model) {
                        unset($url);
                        return Html::a(
                            '<span class="glyphicon glyphicon-trash"></span>',
                            ['delete-domain', 'id' => $model->primaryKey],
                            [
                                'title'        => Yii::t('yii', 'Delete'),
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                'data-method'  => 'post',
                                'data-pjax'    => '0',
                            ]
                        );
                    },
            ]
        ],
    ]
);