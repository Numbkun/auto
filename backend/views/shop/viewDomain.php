<?php

use common\models\Shop;
use common\widgets\DetailView;
use yii\helpers\Html;

/**
 * @var yii\web\View          $this
 * @var \common\models\Domain $model
 */

$this->title = $model;
$this->params['breadcrumbs'][] = ['label' => Shop::getPluralNominativeName(), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->shop, 'url' => ['view', 'id' => $model->shop_id]];
$this->params['breadcrumbs'][] = $this->title;

/** @var \backend\controllers\Controller $controller */
$controller = $this->context;

$actions = [];
if (Yii::$app->user->can($controller->updatePermission)) {
    $actions[] = Html::a(
        '<span class="glyphicon glyphicon-pencil"></span>',
        ['update-domain', 'id' => $model->primaryKey],
        ['class' => 'btn btn-primary']
    );
    $actions[] = Html::a(
        '<span class="glyphicon glyphicon-trash"></span>',
        ['delete-domain', 'id' => $model->primaryKey],
        [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => 'Вы действительно хотите удалить данную модель?',
                'method'  => 'post',
            ],
        ]
    );
}

echo DetailView::widget(['model' => $model, 'toolbarButtons' => $actions]);
