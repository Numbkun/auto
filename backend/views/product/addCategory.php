<?php

/**
 * @var yii\web\View                $this
 * @var \common\models\ShopProductCategory $model
 */

use common\models\Product;


$this->title = $model->getSingularNominativeName() . ' (новый элемент)';
$this->params['breadcrumbs'][] = ['label' => Product::getPluralNominativeName(), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->product->name, 'url' => ['view', 'id' => $model->shop_product_id]];
$this->params['breadcrumbs'][] = $this->title;

require($this->findViewFile('/default/_form', $this->context));
