<?php

/**
 * @var \common\components\View $this
 */

$this->beginContent($this->findViewFile('_tabs'), ['model' => $model]);
// Подключим базовый шаблон
require($this->findViewFile('/default/update', $this->context));
$this->endContent();