<?php

/**
 * @var yii\web\View                $this
 * @var \common\models\ComplementaryProduct $model
 */

use common\models\Product;

$this->title = $model->product . ' - ' . $model->getSingularNominativeName() . ' (редактирование)';
$this->params['breadcrumbs'][] = ['label' => Product::getPluralNominativeName(), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->product, 'url' => ['view', 'id' => $model->product_id]];
$this->params['breadcrumbs'][] = $this->title;

// Подключим базовый шаблон
require($this->findViewFile('_formComplementaryProduct', $this->context));
