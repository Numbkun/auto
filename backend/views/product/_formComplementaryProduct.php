<?php

/**
 * @var \common\components\View $this
 * @var \common\models\Reference $model
 */

use kartik\widgets\ActiveForm;
use yii\helpers\Html;

?>
<div class="Model-form">
    <?php
    $form = ActiveForm::begin(['method' => 'post', 'options' => ['enctype' => 'multipart/form-data']]);
    echo $form->errorSummary($model);
    echo $form->field($model, 'complementary_product_id');
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => 'btn btn-primary'])?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
