<?php

/**
 * @var yii\web\View                $this
 * @var \common\models\ShopProductCategory $model
 */

use common\models\ShopProduct;

$this->title = $model->getSingularNominativeName() . ' (редактирование)';
$this->params['breadcrumbs'][] = ['label' => ShopProduct::getPluralNominativeName(), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->product->name, 'url' => ['view', 'id' => $model->shop_product_id]];
$this->params['breadcrumbs'][] = $this->title;

require($this->findViewFile('/default/_form', $this->context));
