<?php
/**
 * @var string $content
 * @var \common\components\View $this
 * @var \backend\controllers\Controller $controller
 */

use yii\bootstrap\Nav;

$controller = $this->context;
$menuItems = [];
$menuItems[] = [
    'label'   => 'Основные',
    'url'     => [$controller->action->id == 'view' ? 'product/view' : 'product/update', 'id' => $model->primaryKey],
];
$menuItems[] = [
    'label'   => 'С этим товаром покупают',
    'url'     => ['product/complementary-product', 'id' => $model->primaryKey],
];
$menuItems[] = [
    'label'   => 'Картинки',
    'url'     => ['product/list-image', 'id' => $model->primaryKey],
    'active'  => in_array($controller->action->id, ['list-image', 'add-image'])
];

echo Nav::widget(['options' => ['class' => 'nav-tabs'], 'items' => $menuItems,]);
echo $content;