<?php

use common\models\Product;
use common\widgets\GridView;
use yii\data\ActiveDataProvider;
use yii\grid\SerialColumn;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


/**
 * @var \common\components\View $this
 * @var \common\models\Product  $model
 */

/** @var \backend\controllers\Controller $controller */
$controller = $this->context;

$actionsTemplate = [];
if (Yii::$app->user->can($controller->updatePermission)) {
    $actionsTemplate[] = '{delete}';
}
$this->title = $model . ' - привязанные изображения';
$this->params['breadcrumbs'][] = ['label' => Product::getPluralNominativeName(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->beginContent($this->findViewFile('_tabs'), ['model' => $model]);
$form = ActiveForm::begin(
    [
        'method'      => 'post',
        'fieldConfig' => [
            'template' => "{input}\n{hint}\n{error}",
        ]
    ]
);
$actionsTemplate = implode(' ', $actionsTemplate);
echo "<h2>Картинки товра</h2>";
if(!$model->isNewRecord)
{
    $productId = $model->primaryKey;
    echo GridView::widget(
        [
            'dataProvider'   => new ActiveDataProvider(['query' => $model->getProductImages()]),
            'toolbarButtons' => [
                Html::a(
                    '<span class="glyphicon glyphicon-plus"></span> Добавить',
                    ['add-image', 'productId' => $model->primaryKey],
                    ['class' => 'btn btn-success']
                )
                ],
            'columns' => [
                    ['class' => SerialColumn::className()],
                    [
                        'attribute' => 'name',
                        'format' => 'html',
                        'label' => 'Изображение товара',
                        'value' => function($productImage)
                            {
                                /** @var \common\models\ProductImage $productImage */
                                return Html::img($productImage->image->publishImage(50, 50), ['width' => 50, 'height' => 50, 'alt' => '']);
                            }
                    ],
                    [
                        'headerOptions' => ['width' => '50px'],
                        'label' => 'Главная',
                        'format' => 'raw',
                        'value' => function($productImage) use ($model)
                            {
                                /** @var \common\models\ProductImage $productImage */
                                return Html::radio(Html::getInputName($model, 'product_image_id'), $productImage->id == $model->product_image_id, ['value' => $productImage->id] );
                            }
                    ],
            ],
            'actionColumn'   => [
                'headerOptions' => ['width' => '50px'],
                'template' => $actionsTemplate,
                'buttons'  => [
                    'delete' => function($url, $productImage)
                        {
                            unset($url);
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>',
                                /** @var \common\models\ProductImage $productImage */
                                ['delete-image', 'imageId' => $productImage->id],
                                [
                                    'title'        => Yii::t('yii', 'Delete'),
                                    'data-confirm' => Yii::t('yii', 'Вы точно хотите удалить это изображение?'),
                                    'data-method'  => 'post',
                                    'data-pjax'    => '0',
                                ]
                            );
                        },
                ]
            ],
        ]
    );
}
?>
<div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
</div>
<?php
ActiveForm::end();
$this->endContent();