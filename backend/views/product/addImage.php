<?php

/**
 * @var \common\components\View $this
 * @var \common\models\ProductImage $model
 */

use common\models\Product;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Добавить изображение';
$this->params['breadcrumbs'][] = ['label' => Product::getPluralNominativeName(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->beginContent($this->findViewFile('_tabs'), ['model' => $model->product]);
?>
<div class="Model-form">
    <?php
    $form = ActiveForm::begin(['method' => 'post', 'options' => ['enctype' => 'multipart/form-data']]);
    echo $form->errorSummary($model);

    // Блок полей формы
    $this->beginExtBlock('fields', true);
    echo $form->field($model, 'imageFile');
    $this->endExtBlock();

    ?>
    <div class="form-group">
        <?php
        // Блок кнопок формы
        $this->beginExtBlock('buttons', true);
        echo Html::submitButton('Добавить', ['class' => 'btn btn-primary']);
        $this->endExtBlock();
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php $this->endContent(); ?>