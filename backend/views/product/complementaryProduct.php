<?php

/**
 * @var yii\web\View                    $this
 * @var yii\data\DataProviderInterface  $dataProvider
 * @var \common\models\BaseActiveRecord $searchModel
 * @var Product $model
 */

use common\models\Product;
use common\widgets\GridView;
use yii\helpers\Html;

$this->title = $model . ' - с этим товаром покупают';
$this->params['breadcrumbs'][] = ['label' => Product::getPluralNominativeName(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
/** @var \backend\controllers\Controller $controller */
$controller = $this->context;
$this->beginContent($this->findViewFile('_tabs'), ['model' => $model]);

$actionsTemplate = [];
if (Yii::$app->user->can($controller->updatePermission)) {
    $actionsTemplate[] = '{update}';
    $actionsTemplate[] = '{delete}';
}
$actionsTemplate = implode(' ', $actionsTemplate);

echo GridView::widget(
    [
        'dataProvider'         => $dataProvider,
        'filterModel'          => $searchModel,
        'actionColumn'         => [
            'template' => $actionsTemplate,
            'buttons'  => [
                'update' => function ($url, $model) {
                        unset($url);
                        return Html::a(
                            '<span class="glyphicon glyphicon-pencil"></span>',
                            ['update-complementary-product', 'id' => $model->product_id, 'complementaryId' => $model->complementary_product_id ]
                        );
                    },
                'delete' => function ($url, $model) {
                        unset($url);
                        return Html::a(
                            '<span class="glyphicon glyphicon-trash"></span>',
                            ['delete-complementary-product', 'id' => $model->product_id, 'complementaryId' => $model->complementary_product_id],
                            [
                                'title'        => Yii::t('yii', 'Delete'),
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                'data-method'  => 'post',
                                'data-pjax'    => '0',
                            ]
                        );
                    },
            ]
        ],
        'model'                => $searchModel,
        'toolbarButtons'       => Yii::$app->user->can($controller->createPermission) ? [
             Html::a('<span class="glyphicon glyphicon-plus"></span>', ['add-complementary-product', 'id' => $model->primaryKey], ['class' => 'btn btn-success'])
        ] : [],
    ]
);
$this->endContent();