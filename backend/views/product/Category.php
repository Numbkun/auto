<?php

use common\models\Product;
use common\widgets\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

/**
 * @var \common\components\View $this
 * @var \common\models\ShopProduct  $model
 * * @var \common\models\BaseActiveRecord $searchModel
 */

/** @var \backend\controllers\Controller $controller */
$controller = $this->context;

$this->beginContent($this->findViewFile('_tabs'), ['model' => $model]);

if (empty($this->title)) {
    $this->title = $model;
}

if (empty($this->params['breadcrumbs'])) {
    $this->title = $model . ' - дополнительные категории';
    $this->params['breadcrumbs'][] = ['label' => Product::getPluralNominativeName(), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
}

$actionsTemplate = [];
if (Yii::$app->user->can($controller->updatePermission)) {
    $actionsTemplate[] = '{update}';
    $actionsTemplate[] = '{delete}';
}

$actionsTemplate = implode(' ', $actionsTemplate);

echo GridView::widget(
    [
        'dataProvider'   => new ActiveDataProvider(['query' => $model->getShopProductCategories()]),
        'actionColumn'         => [
            'template' => $actionsTemplate,
            'buttons'  => [
                'update' => function ($url, $model) {
                    unset($url);
                    return Html::a(
                        '<span class="glyphicon glyphicon-pencil"></span>',
                        ['update-category', 'productId' => $model->shop_product_id, 'categoryId' => $model->category_id]
                    );
                },
                'delete' => function ($url, $model) {
                    unset($url);
                    return Html::a(
                        '<span class="glyphicon glyphicon-trash"></span>',
                        ['delete-category', 'productId' => $model->shop_product_id, 'categoryId' => $model->category_id],
                        [
                            'title'        => Yii::t('yii', 'Delete'),
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method'  => 'post',
                            'data-pjax'    => '0',
                        ]
                    );
                },
            ]
        ],
        'model'                => $searchModel,
        'toolbarButtons'       => Yii::$app->user->can($controller->createPermission) ? [
            Html::a(
                '<span class="glyphicon glyphicon-plus"></span>',
                ['add-category', 'productId' => $model->primaryKey],
                ['class' => 'btn btn-success']
            )
        ] : [],
    ]
);
$this->endContent();