<?php

use common\widgets\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

/**
 * @var \common\components\View $this
 * @var \common\models\SkuParameterPrice $model
 */

/** @var \backend\controllers\Controller $controller */
$controller = $this->context;

$actionsTemplate = [];
if (Yii::$app->user->can($controller->updatePermission)) {
    $actionsTemplate[] = '{delete}';
}

$actionsTemplate = implode(' ', $actionsTemplate);
echo "<h2>Цены</h2>",
GridView::widget(
    [
        'dataProvider' => new ActiveDataProvider(['query' => $model->getPrice()]),
        'toolbarButtons' => Yii::$app->user->can($controller->updatePermission) ? [
            Html::a(
                '<span class="glyphicon glyphicon-plus"></span>',
                ['add-price', 'skuId' => $model->id],
                ['class' => 'btn btn-success']
            )
        ] : [],
        'actionColumn' => [
            'template' => $actionsTemplate,
            'buttons' => [
                'delete' => function ($url, $model) {
                    unset($url);
                    return Html::a(
                        '<span class="glyphicon glyphicon-trash"></span>',
                        ['delete-price', 'id' => $model->primaryKey, 'skuId' => $model->sku->id],
                        [
                            'title' => Yii::t('yii', 'Delete'),
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ]
                    );
                },
            ]
        ],
    ]
);