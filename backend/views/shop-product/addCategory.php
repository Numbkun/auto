<?php

/**
 * @var yii\web\View                $this
 * @var \common\models\ShopProductCategory $model
 */

use common\models\ShopProduct;

$this->title = $model->getSingularNominativeName() . ' (новый элемент)';
$this->params['breadcrumbs'][] = ['label' => ShopProduct::getPluralNominativeName(), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->shopProduct, 'url' => ['view', 'id' => $model->shop_product_id]];
$this->params['breadcrumbs'][] = $this->title;
echo $this->render('_formCategory', ['model' => $model], $this->context);
