<?php

use common\widgets\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

/**
 * @var \common\components\View $this
 * @var \common\models\Category  $model
 */

/** @var \backend\controllers\Controller $controller */
$controller = $this->context;

$actionsTemplate = [];
if (Yii::$app->user->can($controller->updatePermission)) {
    $actionsTemplate[] = '{create}';
}

$actionsTemplate = implode(' ', $actionsTemplate);
$childCategory = $model;
echo "<h2>Родительские категории</h2>",
GridView::widget(
    [
        'dataProvider'   => new ActiveDataProvider(['query' => $model->getAdditionalParentCategories()]),
        'toolbarButtons' => Yii::$app->user->can($controller->updatePermission) ? [
                Html::a(
                    '<span class="glyphicon glyphicon-plus"></span>',
                    ['add-parent', 'childCategory' => $model->primaryKey],
                    ['class' => 'btn btn-success']
                )
            ] : [],
        'actionColumn'   => [
            'template' => $actionsTemplate,
            'buttons'  => [
                'delete' => function ($url, $model) use ($childCategory) {
                        unset($url);
                        return Html::a(
                            '<span class="glyphicon glyphicon-trash"></span>',
                            ['delete-link', 'childCategory' => $childCategory->primaryKey, 'parentCategory' => $model->primaryKey],
                            [
                                'title'        => Yii::t('yii', 'Delete'),
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                'data-method'  => 'post',
                                'data-pjax'    => '0',
                            ]
                        );
                    },
            ]
        ],
    ]
);