<?php

/**
 * @var \common\components\View $this
 * @var \common\models\Category  $model
 */
// Подключим базовый шаблон
require($this->findViewFile('/default/update', $this->context));
// Список дополнительных родителей
require($this->findViewFile('_listCategory', $this->context));
