<?php

/**
 * @var \common\components\View $this
 */

// Подключим базовый шаблон
require($this->findViewFile('/default/view', $this->context));
// Список дополнительных родителей
require($this->findViewFile('_listCategory', $this->context));
