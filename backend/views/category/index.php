<?php

/**
 * @var \yii\web\View $this
 * @var \common\models\Category  $model
 * @var \backend\controllers\CategoryController $controller
 */

use common\widgets\GridView;
use common\models\Category;
use wbraganca\fancytree\FancytreeAsset;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;


$controller = $this->context;?>
    <h1>Категории</h1>
    <div class="page-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="row">
        <div class="col-sm-4 col-md-3">
            <?= \wbraganca\fancytree\FancytreeWidget::widget([
                    'id' => 'category_tree',
                    'options' =>[
                        'source' => $controller->getCategoryTree(),
                        'activeVisible' => true,
                    ]
                ]);
            ?>

        </div>
        <div class="col-sm-8 col-md-9">
<?php
if($model) {
    $this->params['breadcrumbs'][] = ['label' => Category::getPluralNominativeName(), 'url' => ['index']];
} else {
    $this->params['breadcrumbs'][] = ['label' => Category::getPluralNominativeName()];
}
if($model){
    $this->params['breadcrumbs'][] = ['label' => $model->name];
}

echo GridView::widget(
    [
        'dataProvider'   => new ActiveDataProvider(['query' => $model ? $model->getForwardChildCategories() : Category::getParentAllCategories()]),
        'toolbarButtons' => Yii::$app->user->can($controller->updatePermission) ? [
                Html::a(
                    '<span class="glyphicon glyphicon-plus"></span>',
                    ['create', 'parentCategory' => $model ? $model->primaryKey : null],
                    ['class' => 'btn btn-success']
                )
            ] : [],
        'actionColumn'   => [
            'buttons'  => [
                'delete' => function ($url, $model) {
                        unset($url);
                        return Html::a(
                            '<span class="glyphicon glyphicon-trash"></span>',
                            ['delete-category','id' => $model->primaryKey],
                            [
                                'title'        => Yii::t('yii', 'Delete'),
                                'data-confirm' => Yii::t('yii', 'Вы точно хотите удалить эту категорию?'),
                                'data-method'  => 'post',
                                'data-pjax'    => '0',
                            ]
                        );
                    },
            ]
        ],

    ]
);?>
    </div>
    </div>
<?php
FancytreeAsset::register($this);
$this->registerJs('$("#fancyree_category_tree").fancytree("getTree").activateKey("' . Yii::$app->request->get('id') . '");');
