<?php

/**
 * @var yii\web\View                $this
 * @var \common\models\CategoryTree $model
 */

use common\models\Category;

$this->title = $model->getSingularNominativeName() . ' (новый элемент)';
$this->params['breadcrumbs'][] = ['label' => Category::getPluralNominativeName(), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->childCategory, 'url' => ['view', 'id' => $model->child_category_id]];
$this->params['breadcrumbs'][] = $this->title;
echo $this->render('_formParent', ['model' => $model], $this->context);
