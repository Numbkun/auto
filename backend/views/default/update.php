<?php

/**
 * @var yii\web\View             $this
 * @var \common\models\Reference $model
 */

if (empty($this->title)) {
    $this->title = $model->name . ' (редактирование)';
}

if (empty($this->params['breadcrumbs'])) {
    $this->params['breadcrumbs'][] = ['label' => $model->getPluralNominativeName(), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
}
?>
    <h2>Основные параметры</h2>
<?php
echo $this->render('_form', ['model' => $model], $this->context);
