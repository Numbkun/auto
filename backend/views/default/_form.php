<?php

/**
 * @var \common\components\View $this
 * @var \common\models\Reference $model
 */

use kartik\widgets\ActiveForm;
use yii\helpers\Html;

?>
<div class="Model-form">
    <?php
    $form = ActiveForm::begin(['method' => 'post', 'options' => ['enctype' => 'multipart/form-data']]);
    echo $form->errorSummary($model);

    // Блок полей формы
    $this->beginExtBlock('fields', true);
    $fieldsOptions = $model->getFieldsOptions();
    foreach ($model->safeAttributes() as $attribute) {
        if ($fieldsOptions[$attribute]['type'] != 'readonly') {
            echo $form->field($model, $attribute);
        }
    }
    $this->endExtBlock();

    ?>
    <div class="form-group">
        <?php

        // Блок кнопок формы
        $this->beginExtBlock('buttons', true);
            echo Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => 'btn btn-primary']);
        $this->endExtBlock();
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
