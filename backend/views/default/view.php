<?php

use common\widgets\DetailView;
use yii\helpers\Html;

/**
 * @var \common\components\View $this
 * @var \common\models\Reference $model
 */

/** @var \backend\controllers\Controller $controller */
$controller = $this->context;

if (empty($this->title)) {
    $this->title = $model;
}

if (empty($this->params['breadcrumbs'])) {
    $this->params['breadcrumbs'][] = ['label' => $model->getPluralNominativeName(), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
}

$actions = [];
if (Yii::$app->user->can($controller->updatePermission)) {
    $actions[] = Html::a(
        '<span class="glyphicon glyphicon-pencil"></span>',
        ['update', 'id' => $model->primaryKey],
        ['class' => 'btn btn-primary']
    );
}
if (Yii::$app->user->can($controller->deletePermission)) {
    $actions[] = Html::a(
        '<span class="glyphicon glyphicon-trash"></span>',
        ['delete', 'id' => $model->primaryKey],
        [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => 'Вы действительно хотите удалить данную модель?',
                'method'  => 'post',
            ],
        ]
    );
}
?>
    <h2>Основные параметры</h2>
<?php
$this->beginExtBlock('detail', true);
echo DetailView::widget(['model' => $model, 'toolbarButtons' => $actions]);
$this->endExtBlock();