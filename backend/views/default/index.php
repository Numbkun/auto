<?php

/**
 * @var yii\web\View                    $this
 * @var yii\data\DataProviderInterface  $dataProvider
 * @var \common\models\BaseActiveRecord $searchModel
 */

use common\widgets\GridView;
use yii\helpers\Html;

/** @var \backend\controllers\Controller $controller */
$controller = $this->context;
/** @var \common\models\Reference $modelClass */
$modelClass = $controller->modelClass;

if (empty($this->title)) {
    $this->title = $modelClass::getPluralNominativeName();
}

if (empty($this->params['breadcrumbs'])) {
    $this->params['breadcrumbs'][] = $this->title;
}

$actionsTemplate = [];
if (Yii::$app->user->can($controller->viewPermission)) {
    $actionsTemplate[] = '{view}';
}
if (Yii::$app->user->can($controller->updatePermission)) {
    $actionsTemplate[] = '{update}';
}
if (Yii::$app->user->can($controller->deletePermission)) {
    $actionsTemplate[] = '{delete}';
}
$actionsTemplate = implode(' ', $actionsTemplate);

echo GridView::widget(
    [
        'dataProvider'         => $dataProvider,
        'filterModel'          => $searchModel,
        'actionColumn'         => ['template' => $actionsTemplate],
        'model'                => $controller->createModel(),
        'toolbarButtons'       => Yii::$app->user->can($controller->createPermission) ? [
             Html::a('<span class="glyphicon glyphicon-plus"></span>', ['create'], ['class' => 'btn btn-success'])
        ] : [],
    ]
);