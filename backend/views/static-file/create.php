<?php

/**
 * @var \common\components\View $this
 * @var \common\models\File $model
 */

use common\models\File;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Добавить Файл';
$this->params['breadcrumbs'][] = ['label' => File::getPluralNominativeName(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="Model-form">
    <?php
    $form = ActiveForm::begin(['method' => 'post', 'options' => ['enctype' => 'multipart/form-data']]);
    echo $form->errorSummary($model);

    // Блок полей формы
    $this->beginExtBlock('fields', true);
    echo $form->field($model, 'uploadFile');
    $this->endExtBlock();

    ?>
    <div class="form-group">
        <?php
        // Блок кнопок формы
        $this->beginExtBlock('buttons', true);
        echo Html::submitButton('Добавить', ['class' => 'btn btn-primary']);
        $this->endExtBlock();
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>