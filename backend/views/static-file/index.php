<?php

use common\models\File;
use common\widgets\GridView;
use frontend\assets\PrettyPhotoAsset;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


/**
 * @var \common\components\View $this
 * @var File $model
 */

/** @var \backend\controllers\Controller $controller */
$controller = $this->context;

$actionsTemplate = [];

if (Yii::$app->user->can($controller->deletePermission)) {
    $actionsTemplate[] = '{delete}';
}


$this->title = 'Загрузка файлов';
$this->params['breadcrumbs'][] = ['label' => File::getPluralNominativeName(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$form = ActiveForm::begin(
    [
        'method' => 'post',
        'fieldConfig' => [
            'template' => "{input}\n{hint}\n{error}",
        ]
    ]
);
$actionsTemplate = implode(' ', $actionsTemplate);
echo GridView::widget(
    [
        'dataProvider' => new ActiveDataProvider(['query' => File::find()->where(['path' => 'static'])]),
        'toolbarButtons' => [
            Html::a(
                '<span class="glyphicon glyphicon-plus"></span> Добавить',
                ['create'],
                ['class' => 'btn btn-success']
            )
        ],
        'columns' => [
            [
                'attribute' => 'name',
                'format' => 'raw',
                'label' => 'Изображение товара',
                'value' => function ($model) {
                    /** @var File $model */
                    return Html::a(Html::img($model->publishImage(50, 50)),
                        $model->publishImage(1024, 768), ['rel' => 'prettyPhoto[gallery1]']);
                }
            ],
            [
                'format' => 'html',
                'label' => 'URL картинки',
                'value' => function ($model) {
                    /** @var File $model */
                    return  Html::a($model->publish(),$model->publish());
                }
            ],
        ],
        'actionColumn' => [
            'headerOptions' => ['width' => '50px'],
            'template' => $actionsTemplate,
            'buttons' => [
                'delete' => function ($url, $model) {
                    unset($url);
                    return Html::a(
                        '<span class="glyphicon glyphicon-trash"></span>',
                        ['delete', 'id' => $model->id],
                        [
                            'title' => Yii::t('yii', 'Delete'),
                            'data-confirm' => Yii::t('yii', 'Вы точно хотите удалить это изображение?'),
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ]
                    );
                },
            ]
        ],
    ]
);

?>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
<?php
ActiveForm::end();
PrettyPhotoAsset::register($this);
$this->registerJs('$("a[rel^=\'prettyPhoto\']").prettyPhoto({deeplinking: false});');