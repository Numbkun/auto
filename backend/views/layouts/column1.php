<?php

/**
 * @var \yii\web\View $this
 * @var string        $content
 */

use yii\helpers\Html;

?>

<?php $this->beginContent('@app/views/layouts/main.php'); ?>
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $content ?>
<?php $this->endContent();