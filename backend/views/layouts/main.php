<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/**
 * @var \yii\web\View $this
 * @var string $content
 */
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel'            => Yii::$app->shop->getIdentity(),
                'brandUrl'              => Yii::$app->homeUrl,
                'innerContainerOptions' => ['class' => 'container-fluid'],
                'options'               => ['class' => 'navbar-inverse navbar-fixed-top'],
            ]);
        $menuItems = [];
        $menuItems[] = [
            'label'   => 'Каталог',
            'visible' => Yii::$app->user->can('backend.category.read') ||
                Yii::$app->user->can('backend.product.read') ||
                Yii::$app->user->can('backend.manufacture.read') ||
                Yii::$app->user->can('backend.product-type.read'),
            'items' => [
                [
                    'label'   => 'Базовые товары',
                    'url'     => ['product/index'],
                    'visible' => Yii::$app->user->can('backend.product.read')
                ],
                [
                    'label'   => 'Производители',
                    'url'     => ['manufacture/index'],
                    'visible' => Yii::$app->user->can('backend.manufacture.read')
                ],
                [
                    'label'   => 'Типы товаров',
                    'url'     => ['product-type/index'],
                    'visible' => Yii::$app->user->can('backend.product-type.read')
                ],
                [
                    'label'   => 'Категории',
                    'url'     => ['category/index'],
                    'visible' => Yii::$app->user->can('backend.category.read')
                ],
            ],
        ];
        $menuItems[] = [
            'label'   => 'Администрирование',
            'visible' => Yii::$app->user->can('backend.user.read') ||
                Yii::$app->user->can('backend.system-user.read') ||
                Yii::$app->user->can('backend.role.read') ||
                Yii::$app->user->can('backend.system-role.read') ||
                Yii::$app->user->can('backend.region.read') ||
                Yii::$app->user->can('backend.setting.read'),
            'items' => [
                [
                    'label'   => 'Пользователи',
                    'url'     => ['user/index'],
                    'visible' => Yii::$app->user->can('backend.user.read')
                ],
                [
                    'label'   => 'Пользователи (все)',
                    'url'     => ['system-user/index'],
                    'visible' => Yii::$app->user->can('backend.system-user.read')
                ],
                [
                    'label'   => 'Роли',
                    'url'     => ['role/index'],
                    'visible' => Yii::$app->user->can('backend.role.read')
                ],
                [
                    'label'   => 'Роли (все)',
                    'url'     => ['system-role/index'],
                    'visible' => Yii::$app->user->can('backend.system-role.read')
                ],
                [
                    'label'   => 'Настройки',
                    'url'     => ['setting/index'],
                    'visible' => Yii::$app->user->can('backend.setting.read')
                ],
                [
                    'label'   => 'Регионы',
                    'url'     => ['region/index'],
                    'visible' => Yii::$app->user->can('backend.region.read')
                ],
            ],
        ];

        $menuItems[] = [
            'label'   => 'Контент',
            'visible' => Yii::$app->user->can('backend.static-page.read') ||
                Yii::$app->user->can('backend.news.read') ||
                Yii::$app->user->can('backend.dignity.read'),
            'items' => [
                [
                    'label'   => 'Достоинства',
                    'url'     => ['dignity/index'],
                    'visible' => Yii::$app->user->can('backend.dignity.read')
                ],
                [
                    'label'   => 'Статические страницы',
                    'url'     => ['static-page/index'],
                    'visible' => Yii::$app->user->can('backend.static-page.read')
                ],
                [
                    'label'   => 'Новости',
                    'url'     => ['news/index'],
                    'visible' => Yii::$app->user->can('backend.news.read')
                ],
                [
                    'label'   => 'Баннеры',
                    'url'     => ['banner/index'],
                    'visible' => Yii::$app->user->can('backend.banner.read')
                ],
                [
                    'label'   => 'Загрузка файлов',
                    'url'     => ['static-file/index'],
                ],
            ],
        ];
            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => 'Login', 'url' => ['site/login']];
            } else {
                $menuItems[] = [
                    'label' => 'Выход (' . Yii::$app->user->identity . ')',
                    'url' => ['site/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ];
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);
            NavBar::end();
        ?>
        <div class="container-fluid">
            <?= Breadcrumbs::widget(['options' => ['class' => 'pull-right breadcrumb'], 'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []]) ?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container-fluid">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage();
