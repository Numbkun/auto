<?php

/**
 * @var yii\web\View         $this
 * @var \backend\models\User $model
 */

use kartik\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = $model . ' (установка пароля)';
$this->params['breadcrumbs'][] = ['label' => $model->getPluralNominativeName(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="Model-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary($model) ?>
    <?= $form->field($model, 'password') ?>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>


