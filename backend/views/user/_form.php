<?php

use yii\helpers\Html;

/**
 * @var \common\components\View $this
 * @var \backend\models\User     $model
 */

// Добавим полей в базовую форму
$block = $this->beginExtBlock('fields');
$block->parentBlock();
$roles = $model->getAvailableRoles();
if ($roles) {
    echo "<h2>Роли</h2>";
    echo Html::activeCheckboxList($model, 'roles', $roles, [
            'item' => function ($index, $label, $name, $checked, $value) {
                    return '<div class="checkbox">' . Html::checkbox(
                        $name,
                        $checked,
                        ['label' => $label, 'value' => $value]
                    ) . '</div>';
                }
    ]);
}
$this->endExtBlock();
/** @var \backend\controllers\UserController $controller */
$controller = $this->context;
if (!$model->isNewRecord && Yii::$app->user->can($controller->setPasswordPermission)) {
    // Добавим кнопок в базовую форму
    $block = $this->beginExtBlock('buttons');
    $block->parentBlock();
    echo ' ',
    Html::a('Установить пароль', ['set-password', 'id' => $model->primaryKey], ['class' => 'btn btn-success']);
    $this->endExtBlock();
}

// Подключим базовую форму
require($this->findViewFile('/default/_form', $this->context));
