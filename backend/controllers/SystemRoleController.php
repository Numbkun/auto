<?php

namespace backend\controllers;

use backend\models\AuthRole;
use common\models\BaseActiveRecord;
use common\models\ReferenceByShop;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Контроллер для RBAC-ролей
 */
class SystemRoleController extends Controller
{

    /** @inheritdoc */
    public $modelClass = 'backend\models\AuthRole';
    /** @inheritdoc */
    public $viewPermission = 'backend.system-role.read';
    /** @inheritdoc */
    public $updatePermission = 'backend.system-role.edit';
    /** @inheritdoc */
    public $createPermission = 'backend.system-role.edit';
    /** @inheritdoc */
    public $deletePermission = 'backend.system-role.edit';

    /**
     * @inheritdoc
     */
    public function getViewPath()
    {
        return $this->module->getViewPath() . DIRECTORY_SEPARATOR . 'role';
    }

    /**
     * @inheritdoc
     */
    public function findModel($id, $modelClass = null)
    {
        $model = AuthRole::find()->reset()->role()->excludeDefaultRoles()->andWhere(['name' => $id])->one();
        if ($model !== null) {
            $model->scenario = ReferenceByShop::SCENARIO_SYSTEM;
            return $model;
        } else {
            throw new NotFoundHttpException('Роль не найдена');
        }
    }

    /**
     * @inheritdoc
     */
    public function createModel($modelClass = null)
    {
        $model = parent::createModel($modelClass);
        $model->scenario = ReferenceByShop::SCENARIO_SYSTEM;
        return $model;
    }

    /**
     * @inheritdoc
     */
    public function actionIndex()
    {
        $searchModel = $this->createModel();
        $searchModel->scenario = BaseActiveRecord::SCENARIO_SEARCH;
        $dataProvider = $searchModel->search(Yii::$app->request->get(), $searchModel->find()->reset()->role()->excludeDefaultRoles());
        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider
            ]
        );
    }
}
