<?php

namespace backend\controllers;

use Yii;

/**
 * Контроллер типов товаров
 *
 * @package backend\controllers
 */
class ProductTypeController extends Controller
{
    /** @inheritdoc */
    public $modelClass = 'common\models\ProductType';
    /** @inheritdoc */
    public $viewPermission = 'backend.product-type.read';
    /** @inheritdoc */
    public $updatePermission = 'backend.product-type.edit';
    /** @inheritdoc */
    public $createPermission = 'backend.product-type.edit';
    /** @inheritdoc */
    public $deletePermission = 'backend.product-type.edit';

}