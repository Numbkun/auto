<?php

namespace backend\controllers;

use common\models\BaseActiveRecord;
use common\models\ParameterValue;
use common\models\Product;
use common\models\ComplementaryProduct;
use common\models\ProductImage;
use common\models\ShopProductCategory;
use common\models\ShopProductPrice;
use common\models\Sku;
use common\models\SkuParameterPrice;
use Exception;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;

/**
 * Контроллер базовых товаров
 *
 * @package backend\controllers
 */
class ProductController extends Controller
{
    /** @inheritdoc */
    public $modelClass = 'common\models\Product';
    /** @inheritdoc */
    public $viewPermission = 'backend.product.read';
    /** @inheritdoc */
    public $updatePermission = 'backend.product.edit';
    /** @inheritdoc */
    public $createPermission = 'backend.product.edit';
    /** @inheritdoc */
    public $deletePermission = 'backend.product.edit';


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'access' => [
                    'rules' => [
                        [
                            'allow' => true,
                            'actions' => ['parameter', 'complementary-product', 'category', 'sku'],
                            'roles' => [$this->viewPermission],
                        ],
                        [
                            'allow' => true,
                            'actions' => [
                                'create',
                                'parameter',
                                'add-parameter',
                                'delete-parameter',
                                'complementary-product',
                                'add-complementary-product',
                                'delete-complementary-product',
                                'update-complementary-product',
                                'add-category',
                                'update-category',
                                'delete-category',
                                'add-price',
                                'delete-price',
                                'list-image',
                                'add-image',
                                'delete-image',
                                'add-sku',
                                'update-sku',
                                'delete-sku',
                                'select-parameters-groups',
                                'select-parameters',
                                'select-parameters-values',
                            ],
                            'roles' => [$this->updatePermission],
                        ],
                    ],
                ],
                'verbs' => [
                    'actions' => [
                        'delete-link' => ['post'],
                        'delete-parameter' => ['post'],
                        'delete-category' => ['post'],
                    ],
                ],
            ]
        );
    }

    /**
     * Создание нового товара
     *
     * @return string|\yii\web\Response
     * @throws \Exception
     */
    public function actionCreate()
    {
        $model = $this->createModel();

        if (Yii::$app->request->isPost) {
            /** @var Product $model */
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                try {
                    $model->save();
                    return $this->redirect(['update', 'id' => $model->primaryKey]);
                } catch (Exception $e) {
                    if (defined('YII_DEBUG') && YII_DEBUG) {
                        throw $e;
                    }
                }
            }
        }

        return $this->render('create', ['model' => $model]);
    }

    /**
     * Действие для вывода списка привязанных дополняющих товаров
     *
     * @param integer $id идентификатор категории
     * @return string
     * @throws \Exception
     */
    public function actionComplementaryProduct($id)
    {
        /** @var Product $model */
        $model = $this->findModel($id);
        $searchModel = $this->createModel(ComplementaryProduct::className());
        $searchModel->scenario = BaseActiveRecord::SCENARIO_SEARCH;
        return $this->render(
            'complementaryProduct',
            [
                'searchModel' => $searchModel,
                'dataProvider' => $searchModel->search(Yii::$app->request->get(), $model->getComplementaryProduct()),
                'model' => $model,
            ]
        );
    }

    /**
     * Действие для удаления дополняющих товаров
     * @param integer $id идентификатор значения
     * @param $complementaryId
     * @throws Exception
     * @throws \yii\base\Exception
     * @throws \yii\base\UserException
     * @throws \yii\web\NotFoundHttpException
     * @throws null
     * @return \yii\web\Response
     */
    public function actionDeleteComplementaryProduct($id, $complementaryId)
    {
        /** @var ParameterValue $model */
        $model = $this->findModel(['product_id' => $id, 'complementary_product_id' => $complementaryId],
            ComplementaryProduct::className());
        $productId = $model->product_id;
        $model->delete();
        return $this->redirect(['complementary-product', 'id' => $productId]);
    }

    /**
     * Действие для добавления дополняющих товаров
     * @param integer $id идентификатор товара
     * @return string|\yii\web\Response
     * @throws \Exception
     */
    public function actionAddComplementaryProduct($id)
    {
        /** @var ComplementaryProduct $model */
        $model = $this->createModel(ComplementaryProduct::className());
        $model->product_id = $id;
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                try {
                    $model->save();
                    return $this->redirect(['complementary-product', 'id' => $id]);
                } catch (Exception $e) {
                    throw new BadRequestHttpException ('При сохранении произошла ошибка');
                }
            }
        }

        return $this->render('addComplementaryProduct', ['model' => $model]);
    }

    /**
     * Редактирование экземпляра модели дополняющих товаров
     * @param integer $id идентификатор
     * @param $complementaryId
     * @throws Exception
     * @throws \yii\web\NotFoundHttpException
     * @return string|\yii\web\Response
     */
    public function actionUpdateComplementaryProduct($id, $complementaryId)
    {
        $model = $this->findModel(['product_id' => $id, 'complementary_product_id' => $complementaryId],
            ComplementaryProduct::className());
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                try {
                    $model->save();
                    return $this->redirect(['complementary-product', 'id' => $id]);
                } catch (Exception $e) {
                    if (defined('YII_DEBUG') && YII_DEBUG) {
                        throw $e;
                    }
                }
            }
        }
        return $this->render('updateComplementaryProduct', ['model' => $model]);
    }

    /**
     * Отображение всех изображений товара
     *
     * @param integer $id товара
     *
     * @throws \Exception
     * @return string
     */
    public function actionListImage($id)
    {
        /** @var Product $model */
        $model = $this->findModel($id);
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                try {
                    $model->save();
                    return $this->redirect('');
                } catch (Exception $e) {
                    if (defined('YII_DEBUG') && YII_DEBUG) {
                        throw $e;
                    }
                }
            }
        }
        return $this->render('listImage', ['model' => $model]);
    }

    /**
     * Действие для добавления изображениr к товару
     *
     * @param integer $productId ID товара
     *
     * @throws \Exception
     * @return string
     */
    public function actionAddImage($productId)
    {
        /** @var ProductImage $model */
        $model = $this->createModel(ProductImage::className());
        $model->product_id = $productId;
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                try {
                    $model->save();
                    return $this->redirect(['update', 'id' => $productId]);
                } catch (Exception $e) {
                    if (defined('YII_DEBUG') && YII_DEBUG) {
                        throw $e;
                    }
                }
            }
        }

        return $this->render('addImage', ['model' => $model]);
    }

    /**
     * Действие для удаления изображения к товару
     *
     * @param integer $imageId ID картинки
     *
     * @throws \Exception
     * @return string
     */
    public function actionDeleteImage($imageId)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            /** @var ProductImage $productImage */
            $productImage = $this->findModel($imageId, ProductImage::className());
            $productImage->delete();
            $productImage->image->delete();
            $transaction->commit();
            return $this->redirect(['update', 'id' => $productImage->product_id]);
        } catch (Exception $e) {
            $transaction->rollBack();
            if (defined('YII_DEBUG') && YII_DEBUG) {
                throw $e;
            }
            return $this->redirect('');
        }
    }

    /**
     * Действие для вывода списка привязанных категорий
     *
     * @param integer $id идентификатор категории
     * @return string
     * @throws \Exception
     */
    public function actionCategory($id)
    {
        /** @var Product $model */
        $model = $this->findModel($id);
        $searchModel = $this->createModel(ShopProductCategory::className());
        return $this->render(
            'Category',
            [
                'searchModel' => $searchModel,
                'dataProvider' => $searchModel->search(Yii::$app->request->get(), $model->getCategory()),
                'model' => $model,
            ]
        );
    }

    /**
     * Действие для добавления дополнительной категории
     *
     * @param integer $productId идентификатор товара
     * @throws \Exception
     * @return string|\yii\web\Response
     */
    public function actionAddCategory($productId)
    {
        /** @var ShopProductCategory $model */
        $model = $this->createModel(ShopProductCategory::className());
        $model->shop_product_id = $productId;
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                try {
                    $model->save();
                    return $this->redirect(['updateCategory', 'id' => $productId]);
                } catch (Exception $e) {
                    if (defined('YII_DEBUG') && YII_DEBUG) {
                        throw $e;
                    }
                }
            }
        }

        return $this->render('addCategory', ['model' => $model]);
    }

    /**
     * Действие для редактирования связи с дополнительной категорией
     *
     * @param integer $productId идентификатор товара
     * @param integer $categoryId идентификатор категории
     * @throws \Exception
     * @return string|\yii\web\Response
     */
    public function actionUpdateCategory($productId, $categoryId)
    {
        /** @var ShopProductCategory $model */
        $model = $this->findModel(['shop_product_id' => $productId, 'category_id' => $categoryId],
            ShopProductCategory::className());
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                try {
                    $model->save();
                    return $this->redirect(['updateCategory', 'id' => $productId]);
                } catch (Exception $e) {
                    if (defined('YII_DEBUG') && YII_DEBUG) {
                        throw $e;
                    }
                }
            }
        }

        return $this->render('updateCategory', ['model' => $model]);
    }

    /**
     * Действие для удаления связи с дополнительной категорией
     *
     * @param integer $productId идентификатор товара
     * @param integer $categoryId идентификатор категории
     * @return string|\yii\web\Response
     */
    public function actionDeleteCategory($productId, $categoryId)
    {
        /** @var ShopProductCategory $model */
        $model = $this->findModel(['shop_product_id' => $productId, 'category_id' => $categoryId],
            ShopProductCategory::className());
        $model->delete();
        return $this->redirect(['view', 'id' => $productId]);
    }
}