<?php

namespace backend\controllers;

use common\models\Category;
use common\models\CategoryTree;
use Exception;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Контроллер категорий товаров
 *
 * @package backend\controllers
 */
class CategoryController extends Controller
{
    /** @inheritdoc */
    public $modelClass = 'common\models\Category';
    /** @inheritdoc */
    public $viewPermission = 'backend.category.read';
    /** @inheritdoc */
    public $updatePermission = 'backend.category.edit';
    /** @inheritdoc */
    public $createPermission = 'backend.category.edit';
    /** @inheritdoc */
    public $deletePermission = 'backend.category.edit';

    /** @var array массив дерева категорий */
    protected $categoryTree;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'access' => [
                    'rules' => [
                        [
                            'allow'   => true,
                            'actions' => ['parameter'],
                            'roles'   => [$this->viewPermission],
                        ],
                        [
                            'allow'   => true,
                            'actions' => [
                                'add-parent',
                                'delete-link',
                                'parameter',
                                'add-parameter',
                                'delete-parameter',
                                'update-parameter',
                                'delete-category'
                            ],
                            'roles'   => [$this->updatePermission],
                        ],
                    ],
                ],
                'verbs'  => [
                    'actions' => [
                        'delete-link'      => ['post'],
                        'delete-parameter' => ['post'],
                    ],
                ],
            ]
        );
    }

    /**
     * Действие для добавления родителя категории
     *
     * @param integer $childCategory идентификатор дочерней категории
     *
     * @throws \Exception
     * @return string|\yii\web\Response
     */
    public function actionAddParent($childCategory = null)
    {
        /** @var CategoryTree $model */
        $model = $this->createModel(CategoryTree::className());
        $model->scenario = CategoryTree::SCENARIO_PARENT;
        $model->child_category_id = $childCategory;
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                try {
                    $model->save();
                    return $this->redirect(['view', 'id' => $childCategory]);
                } catch (Exception $e) {
                    if (defined('YII_DEBUG') && YII_DEBUG) {
                        throw $e;
                    }
                }
            }
        }

        return $this->render('addParent', ['model' => $model]);
    }

    /**
     * Удаление связки (родитель-ребенок) категорий
     *
     * @param integer $childCategory  дочерняя категория
     * @param integer $parentCategory родительская категория
     *
     * @return \yii\web\Response
     */
    public function actionDeleteLink($childCategory, $parentCategory)
    {
        $model = $this->findModel(
            ['child_category_id' => $childCategory, 'parent_category_id' => $parentCategory],
            CategoryTree::className()
        );
        $model->delete();
        return $this->redirect(['view', 'id' => $childCategory]);
    }

    /**
     * Получаем массив категорий для бокового меню
     */
    public function getCategoryTree()
    {
        function categoryTree($parent_id = null){
            $categories = Category::findAll(['parent_category_id' => $parent_id]);
            $items = [];
            foreach($categories as $category){
                $items[] = ['title' => Html::a($category['name'] ,['category/index', 'id' => $category['id']]), 'key' => $category['id'], 'children' => categoryTree($category['id'])];
            }
            return $items;
        }
        if(!$this->categoryTree){
            $this->categoryTree = [['title' => Html::a('Категории', ['category/index']), 'key' => 0, 'children' => categoryTree()]];
        }
        return $this->categoryTree;
    }

    /**
     * Действие для вывода списка категорий c деревом категорий
     * @param integer $id идентификатор категории
     *
     * @return string
     * @throws \Exception
     */
    public function actionIndex($id = null) {
        $model = $id ? $this->findModel($id) : null;
        return $this->render('index', ['model' => $model]);
    }

    /**
     * Действие для добавления категории с установленной дефолтной категорией
     * @param integer $parentCategory идентификатор родительской категории
     *
     * @return string|\yii\web\Response
     * @throws \Exception
     */
    public function actionCreate($parentCategory = null) {
        /** @var Category $model */

        $model = $this->createModel(Category::className());
        $model->parent_category_id = $parentCategory;
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                try {
                    $model->save();
                    return $this->redirect(['index', 'id' => $parentCategory]);
                } catch (Exception $e) {
                    if (defined('YII_DEBUG') && YII_DEBUG) {
                        throw $e;
                    }
                }
            }
        }

        return $this->render('create', ['model' => $model]);
    }

    /**
     * Удаление связки (родитель-ребенок) категорий
     *
     * @param integer $id  выбранная категория
     *
     * @return \yii\web\Response
     */
    public function actionDeleteCategory($id)
    {
        $model = $this->findModel(['id' => $id],Category::className());
        /** @var Category $model */
        $model->delete();
        return $this->redirect(['category', 'id' => $model->parent_category_id]);
    }
}