<?php

namespace backend\controllers;

use common\models\File;
use Yii;

/**
 * Контроллер загрузки файлов
 *
 * @package backend\controllers
 */
class StaticFileController extends Controller
{
    /** @inheritdoc */
    public $modelClass = 'common\models\File';
    /** @inheritdoc */
    public $viewPermission = 'backend.file.read';
    /** @inheritdoc */
    public $updatePermission = 'backend.file.edit';
    /** @inheritdoc */
    public $createPermission = 'backend.file.edit';
    /** @inheritdoc */
    public $deletePermission = 'backend.file.edit';

    /**
     * Отображение всех статических изображений
     *
     * @param null $modelClass
     * @return string
     */
    public function createModel($modelClass = null)
    {
        /** @var File $model */
        $model = parent::createModel($modelClass);
        $model->path = 'static';
        return $model;
    }
}