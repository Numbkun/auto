<?php

namespace backend\controllers;

use common\models\BaseActiveRecord;
use common\models\Enum;
use common\models\Reference;
use Exception;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use Yii;
use yii\web\Response;

/**
 * Базовый контроллер
 *
 * @package backend\controllers
 */
abstract class Controller extends \yii\web\Controller
{

    /**
     * @inheritdoc
     */
    public $layout = 'column1';

    /**
     * @var string название класса базовой модели контроллера, для стандартных действий (CRUD)
     */
    public $modelClass = 'Model';

    /** @var string название операции доступа к действию для просмотра экземпляра модели */
    public $viewPermission = false;

    /** @var string название операции доступа к действию для редактирования и создания экземпляра модели */
    public $updatePermission = false;

    /** @var string название операции доступа к действию для редактирования и создания экземпляра модели */
    public $createPermission = false;

    /** @var string название операции доступа к действию для удаления экземпляра модели */
    public $deletePermission = false;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    'view'   => [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => [$this->viewPermission],
                    ],
                    'create' => [
                        'allow'   => true,
                        'actions' => ['create'],
                        'roles'   => [$this->createPermission],
                    ],
                    'update' => [
                        'allow'   => true,
                        'actions' => ['update'],
                        'roles'   => [$this->updatePermission],
                    ],
                    'delete' => [
                        'allow'   => true,
                        'actions' => ['delete'],
                        'roles'   => [$this->deletePermission],
                    ],
                    'select' => [
                        'allow'   => true,
                        'actions' => ['select'],
                        'roles'   => [$this->updatePermission, $this->createPermission],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Поиск и получение экземпляра модели по идентификатору
     *
     * @param mixed $id идентификатор
     *
     * @param null  $modelClass название класса, модель которого надо найти.
     *                                Если не указано, то будет использовано свойство контроллера "modelClass"
     *
     * @throws \yii\web\NotFoundHttpException если экземпляр не найден
     * @return BaseActiveRecord
     */
    public function findModel($id, $modelClass = null)
    {
        /** @var BaseActiveRecord $className */
        $className = $modelClass ? : $this->modelClass;
        if (($model = $className::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException("Экземпляр не найден");
        }
    }

    /**
     * Создание новой экземпляра модели
     *
     * @param string|null $modelClass название класса, модель которого надо создать.
     *                                Если не указано, то будет использовано свойство контроллера "modelClass"
     *
     * @return BaseActiveRecord
     */
    public function createModel($modelClass = null)
    {
        /** @var BaseActiveRecord $className */
        $className = $modelClass ? : $this->modelClass;
        /** @var BaseActiveRecord $model */
        $model = new $className;
        return $model;
    }

    /**
     * Вывод списка экземпляров модели
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = $this->createModel();
        $searchModel->scenario = BaseActiveRecord::SCENARIO_SEARCH;
        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $searchModel->search(Yii::$app->request->get())
            ]
        );
    }

    /**
     * Просмотр экземпляра модели
     *
     * @param mixed $id идентификатор
     *
     * @return string
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', ['model' => $model]);
    }

    /**
     * Редактирование экземпляра модели
     *
     * @param mixed $id идентификатор
     *
     * @return string|\yii\web\Response
     * @throws \Exception
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                try {
                    $model->save();
                    return $this->redirect(['view', 'id' => $model->primaryKey]);
                } catch (Exception $e) {
                    if (defined('YII_DEBUG') && YII_DEBUG) {
                        throw $e;
                    }
                }
            }
        }

        return $this->render('update', ['model' => $model]);
    }

    /**
     * Создание нового экземпляра модели
     *
     * @return string|\yii\web\Response
     * @throws \Exception
     */
    public function actionCreate()
    {
        $model = $this->createModel();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                try {
                    $model->save();
                    return $this->redirect(['view', 'id' => $model->primaryKey]);
                } catch (Exception $e) {
                    if (defined('YII_DEBUG') && YII_DEBUG) {
                        throw $e;
                    }
                }
            }
        }

        return $this->render('create', ['model' => $model]);
    }

    /**
     * Удаление экземплра модели
     *
     * @param mixed $id идентификатор
     *
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Действие для автокомплита
     *
     * @param string $model название модели по которой делаем автокомплит
     * @param string $term поисковая фраза
     *
     * @return array массив результата поиска
     * @throws \yii\web\HttpException
     */
    public function actionSelect($model, $term) {
        if (mb_strlen($term) < 3) {
            throw new HttpException(400, 'Поисковая фраза должна быть длиной 3 и более символов');
        }
        if (!is_subclass_of($model, Reference::className(), true) && !is_subclass_of($model, Enum::className(), true)) {
            throw new HttpException(400, 'Данная модель не поддерживается');
        }

        /** @var Reference|Enum $model */
        $models = $model::find()->where(['ilike', 'name', $term])->select(['id', 'name'])->limit(10)->asArray()->all();
        $response = [];
        foreach ($models as $model) {
            $response[] = ['value' => $model['id'], 'label' => $model['name']];
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;
    }


}