<?php

namespace backend\controllers;

use backend\models\AuthRole;
use common\models\BaseActiveRecord;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Контроллер для RBAC-ролей
 */
class RoleController extends Controller
{

    /** @inheritdoc */
    public $modelClass = 'backend\models\AuthRole';
    /** @inheritdoc */
    public $viewPermission = 'backend.role.read';
    /** @inheritdoc */
    public $updatePermission = 'backend.role.edit';
    /** @inheritdoc */
    public $createPermission = 'backend.role.edit';
    /** @inheritdoc */
    public $deletePermission = 'backend.role.edit';


    /**
     * @inheritdoc
     */
    public function findModel($id, $modelClass = null)
    {
        $model = AuthRole::find()->excludeDefaultRoles()->notSystem()->andWhere(['name' => $id])->one();
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Роль не найдена');
        }
    }

    /**
     * @inheritdoc
     */
    public function actionIndex()
    {
        $searchModel = $this->createModel();
        $searchModel->scenario = BaseActiveRecord::SCENARIO_SEARCH;
        $dataProvider = $searchModel->search(Yii::$app->request->get(), $searchModel->find()->excludeDefaultRoles()->notSystem());
        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider
            ]
        );
    }
}
