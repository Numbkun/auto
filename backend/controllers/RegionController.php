<?php

namespace backend\controllers;

/**
 * Контроллер регионов
 *
 * @package backend\controllers
 */
class RegionController extends Controller
{

    /** @inheritdoc */
    public $modelClass = 'common\models\Region';
    /** @inheritdoc */
    public $viewPermission = 'backend.region.read';
    /** @inheritdoc */
    public $updatePermission = 'backend.region.edit';
    /** @inheritdoc */
    public $createPermission = 'backend.region.edit';
    /** @inheritdoc */
    public $deletePermission = 'backend.region.edit';

}