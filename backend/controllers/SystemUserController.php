<?php

namespace backend\controllers;

use backend\models\User;
use common\models\BaseActiveRecord;
use common\models\ReferenceByShop;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Контроллер пользователей
 *
 * @package backend\controllers
 */
class SystemUserController extends UserController
{

    /** @inheritdoc */
    public $modelClass = 'backend\models\User';
    /** @inheritdoc */
    public $viewPermission = 'backend.system-user.read';
    /** @inheritdoc */
    public $updatePermission = 'backend.system-user.edit';
    /** @inheritdoc */
    public $createPermission = 'backend.system-user.edit';
    /** @inheritdoc */
    public $deletePermission = 'backend.system-user.edit';
    /** @inheritdoc */
    public $setPasswordPermission = 'backend.system-user.set-password';

    /**
     * @inheritdoc
     */
    public function getViewPath()
    {
        return $this->module->getViewPath() . DIRECTORY_SEPARATOR . 'user';
    }

    /**
     * @inheritdoc
     */
    public function createModel($modelClass = null)
    {
        $model = parent::createModel($modelClass);
        $model->scenario = User::SCENARIO_PASSWORD_SYSTEM;
        return $model;
    }

    /**
     * @inheritdoc
     */
    public function findModel($id, $modelClass = null)
    {
        /** @var \backend\models\User $className */
        $className = $modelClass ? : $this->modelClass;
        if (($model = $className::find()->reset()->where(['id' => $id])->one()) !== null) {
            /** @var \backend\models\User $model */
            $model->scenario = ReferenceByShop::SCENARIO_SYSTEM;
            return $model;
        } else {
            throw new NotFoundHttpException('Экземпляр не найден');
        }
    }

    /**
     * @inheritdoc
     */
    protected function getPasswordModel($id)
    {
        $model = parent::getPasswordModel($id);
        $model->scenario = User::SCENARIO_PASSWORD_SYSTEM;
        return $model;
    }

    /**
     * @inheritdoc
     */
    public function actionIndex()
    {
        $searchModel = $this->createModel();
        $searchModel->scenario = BaseActiveRecord::SCENARIO_SEARCH;
        $dataProvider = $searchModel->search(Yii::$app->request->get(), $searchModel->find()->reset());
        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider
            ]
        );
    }
}