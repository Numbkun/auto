<?php

namespace backend\controllers;

use backend\models\User;
use Exception;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Контроллер пользователей
 *
 * @package backend\controllers
 */
class UserController extends Controller
{

    /** @inheritdoc */
    public $modelClass = 'backend\models\User';
    /** @inheritdoc */
    public $viewPermission = 'backend.user.read';
    /** @inheritdoc */
    public $updatePermission = 'backend.user.edit';
    /** @inheritdoc */
    public $createPermission = 'backend.user.edit';
    /** @inheritdoc */
    public $deletePermission = 'backend.user.edit';
    /** @var string название операции доступа к действию для установки пароля */
    public $setPasswordPermission = 'backend.user.set-password';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'access' => [
                    'rules' => [
                        [
                            'allow'   => true,
                            'actions' => ['set-password'],
                            'roles'   => [$this->setPasswordPermission],
                        ],
                    ],
                ],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function createModel($modelClass = null)
    {
        $model = parent::createModel($modelClass);
        $model->scenario = User::SCENARIO_PASSWORD;
        return $model;
    }

    /**
     * Установка нового пароля для пользователя
     *
     * @param integer $id
     *
     * @throws \Exception
     * @return mixed
     */
    public function actionSetPassword($id)
    {
        $model = $this->getPasswordModel($id);
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                try {
                    $model->save();
                    return $this->redirect(['view', 'id' => $model->primaryKey]);
                } catch (Exception $e) {
                    if (defined('YII_DEBUG') && YII_DEBUG) {
                        throw $e;
                    }
                }
            }
        }

        return $this->render('setPassword', ['model' => $model]);
    }

    /**
     * Возвращает модель для установки пароля
     *
     * @param $id
     *
     * @return \common\models\BaseActiveRecord
     */
    protected function getPasswordModel($id)
    {
        $model = $this->findModel($id);
        $model->scenario = User::SCENARIO_PASSWORD;
        return $model;
    }
}