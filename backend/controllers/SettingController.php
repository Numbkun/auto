<?php

namespace backend\controllers;

use Yii;

/**
 * Контроллер Настроек
 *
 * @package backend\controllers
 */
class SettingController extends Controller
{
    /** @inheritdoc */
    public $modelClass = 'common\models\Setting';

    /** @inheritdoc */
    public $viewPermission = 'backend.setting.read';
    /** @inheritdoc */
    public $updatePermission = 'backend.setting.edit';
}