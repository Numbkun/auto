<?php

namespace backend\controllers;

use backend\models\User;
use common\models\Domain;
use Exception;
use yii\helpers\ArrayHelper;
use Yii;
use yii\web\Response;

/**
 * Контроллер Магазинов
 *
 * @package backend\controllers
 */
class ShopController extends Controller
{
    /** @inheritdoc */
    public $modelClass = 'common\models\Shop';
    /** @inheritdoc */
    public $viewPermission = 'backend.shop.read';
    /** @inheritdoc */
    public $updatePermission = 'backend.shop.edit';
    /** @inheritdoc */
    public $createPermission = 'backend.shop.edit';
    /** @inheritdoc */
    public $deletePermission = 'backend.shop.edit';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'access' => [
                    'rules' => [
                        [
                            'allow'   => true,
                            'actions' => ['view-domain'],
                            'roles'   => [$this->viewPermission],
                        ],
                        [
                            'allow'   => true,
                            'actions' => ['create-domain', 'update-domain', 'delete-domain'],
                            'roles'   => [$this->updatePermission],
                        ],
                    ],
                ],
                'verbs'  => [
                    'actions' => [
                        'delete-domain' => ['post'],
                    ],
                ],
            ]
        );
    }

    /**
     * Создание нового магазина
     *
     * @throws \Exception
     * @return mixed
     */
    public function actionCreate()
    {
        $model = $this->createModel();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $model->save();
                    if (Yii::$app->request->post('createUser', false)) {
                        $settings = Yii::$app->settings;
                        $params = Yii::$app->params;

                        $user = $this->createModel(User::className());
                        $user->name = $settings->get(
                            'shop_default_user_name',
                            $params['shop']['default']['user']['name']
                        );
                        $user->password = $settings->get(
                            'shop_default_user_password',
                            $params['shop']['default']['user']['password']
                        );
                        $user->email = $settings->get(
                            'shop_default_user_email',
                            $params['shop']['default']['user']['email']
                        );
                        $user->is_active = true;
                        $user->shop_id = $model->primaryKey;
                        $user->save();

                        $auth = Yii::$app->getAuthManager();
                        $role = $auth->createRole(
                            strtr(
                                $settings->get('shop_default_role_name', $params['shop']['default']['role']['name']),
                                ['{shop}' => $model]
                            )
                        );
                        $role->description = strtr(
                            $settings->get(
                                'shop_default_role_description',
                                $params['shop']['default']['role']['description']
                            ),
                            ['{shop}' => $model]
                        );
                        $role->is_system = false;
                        $role->shop_id = $model->primaryKey;
                        $auth->add($role);
                        foreach ($auth->getPermissions() as $permission) {
                            $auth->addChild($role, $permission);
                        }
                        $auth->assign($role, $user->primaryKey);

                    }
                    $transaction->commit();
                    return $this->redirect(['view', 'id' => $model->primaryKey]);
                } catch (Exception $e) {
                    $transaction->rollBack();
                    if (defined('YII_DEBUG') && YII_DEBUG) {
                        throw $e;
                    }
                }
            }
        }


        return $this->render('create', ['model' => $model]);
    }

    /**
     * Создание нового домена для магазина
     *
     * @param integer $shopId
     *
     * @throws \Exception
     * @return mixed
     */
    public function actionCreateDomain($shopId)
    {
        /** @var Domain $model */
        $model = $this->createModel(Domain::className());
        $model->shop_id = $shopId;
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                try {
                    $model->save();
                    return $this->redirect(['view-domain', 'id' => $model->primaryKey]);
                } catch (Exception $e) {
                    if (defined('YII_DEBUG') && YII_DEBUG) {
                        throw $e;
                    }
                }
            }
        }

        return $this->render('createDomain', ['model' => $model]);
    }

    /**
     * Редактирование существующего домена
     *
     * @param integer $id
     *
     * @throws \Exception
     * @return mixed
     */
    public function actionUpdateDomain($id)
    {
        $model = $this->findModel($id, Domain::className());
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                try {
                    $model->save();
                    return $this->redirect(['view-domain', 'id' => $model->primaryKey]);
                } catch (Exception $e) {
                    if (defined('YII_DEBUG') && YII_DEBUG) {
                        throw $e;
                    }
                }
            }
        }

        return $this->render('updateDomain', ['model' => $model]);
    }

    /**
     * Просмотр существующего домена
     *
     * @param string $id
     *
     * @return string
     */
    public function actionViewDomain($id)
    {
        $model = $this->findModel($id, Domain::className());
        return $this->render('viewDomain', ['model' => $model]);
    }

    /**
     * Удаление существующего домена
     *
     * @param integer $id
     *
     * @return Response
     */
    public function actionDeleteDomain($id)
    {
        $model = $this->findModel($id, Domain::className());
        $shopId = $model->shop_id;
        $model->delete();
        return $this->redirect(['view', 'id' => $shopId]);
    }

}