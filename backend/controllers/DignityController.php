<?php

namespace backend\controllers;

use common\models\Dignity;
use Yii;
use Exception;

/**
 * Контроллер статической страницы
 *
 * @package backend\controllers
 */
class DignityController extends Controller
{

    /** @inheritdoc */
    public $modelClass = 'common\models\Dignity';
    /** @inheritdoc */
    public $viewPermission = 'backend.dignity.read';
    /** @inheritdoc */
    public $updatePermission = 'backend.dignity.edit';
    /** @inheritdoc */
    public $createPermission = 'backend.dignity.edit';
    /** @inheritdoc */
    public $deletePermission = 'backend.dignity.edit';

    /**
     * @inheritdoc
     */
    public function actionCreate()
    {
        /** @var Dignity $model
         *  @var string $fileDirectory
         */
        $model = $this->createModel();
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                try {
                    $model->save();
                    return $this->redirect(['view', 'id' => $model->primaryKey]);
                } catch (Exception $e) {
                    if (defined('YII_DEBUG') && YII_DEBUG) {
                        throw $e;
                    }
                }
            }
        }

        return $this->render('create', ['model' => $model]);
    }

    /**
     * @inheritdoc
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                try {
                    $model->save();
                    return $this->redirect(['view', 'id' => $model->primaryKey]);
                } catch (Exception $e) {
                    if (defined('YII_DEBUG') && YII_DEBUG) {
                        throw $e;
                    }
                }
            }
        }

        return $this->render('update', ['model' => $model]);
    }
}