<?php

namespace backend\controllers;

use common\models\Banner;
use Yii;
use Exception;

/**
 * Контроллер статической страницы
 *
 * @package backend\controllers
 */
class BannerController extends Controller
{

    /** @inheritdoc */
    public $modelClass = 'common\models\Banner';
    /** @inheritdoc */
    public $viewPermission = 'backend.banner.read';
    /** @inheritdoc */
    public $updatePermission = 'backend.banner.edit';
    /** @inheritdoc */
    public $createPermission = 'backend.banner.edit';
    /** @inheritdoc */
    public $deletePermission = 'backend.banner.edit';

    /**
     * @inheritdoc
     */
    public function actionCreate()
    {
        /** @var Banner $model
         *  @var string $fileDirectory
         */
        $model = $this->createModel();
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                try {
                    $model->save();
                    return $this->redirect(['view', 'id' => $model->primaryKey]);
                } catch (Exception $e) {
                    if (defined('YII_DEBUG') && YII_DEBUG) {
                        throw $e;
                    }
                }
            }
        }

        return $this->render('create', ['model' => $model]);
    }

    /**
     * @inheritdoc
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                try {
                    $model->save();
                    return $this->redirect(['view', 'id' => $model->primaryKey]);
                } catch (Exception $e) {
                    if (defined('YII_DEBUG') && YII_DEBUG) {
                        throw $e;
                    }
                }
            }
        }

        return $this->render('update', ['model' => $model]);
    }
}