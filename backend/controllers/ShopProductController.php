<?php

namespace backend\controllers;

use common\models\ShopProductCategory;
use Yii;
use \Exception;
use yii\helpers\ArrayHelper;

/**
 * Контроллер товаров магазина
 *
 * @package backend\controllers
 */
class ShopProductController extends Controller
{
    /** @inheritdoc */
    public $modelClass = 'common\models\ShopProduct';
    /** @inheritdoc */
    public $viewPermission = 'backend.shop-product.read';
    /** @inheritdoc */
    public $updatePermission = 'backend.shop-product.edit';
    /** @inheritdoc */
    public $createPermission = 'backend.shop-product.manage';
    /** @inheritdoc */
    public $deletePermission = 'backend.shop-product.manage';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'access' => [
                    'rules' => [
                        [
                            'allow'   => true,
                            'actions' => ['add-category', 'update-category', 'delete-category'],
                            'roles'   => [$this->updatePermission],
                        ],
                    ],
                ],
                'verbs'  => [
                    'actions' => [
                        'delete-category' => ['post'],
                    ],
                ],
            ]
        );
    }

    /**
     * Действие для добавления дополнительной категории
     *
     * @param integer $productId идентификатор товара
     *
     * @throws \Exception
     * @return string|\yii\web\Response
     */
    public function actionAddCategory($productId)
    {
        /** @var ShopProductCategory $model */
        $model = $this->createModel(ShopProductCategory::className());
        $model->shop_product_id = $productId;
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                try {
                    $model->save();
                    return $this->redirect(['view', 'id' => $productId]);
                } catch (Exception $e) {
                    if (defined('YII_DEBUG') && YII_DEBUG) {
                        throw $e;
                    }
                }
            }
        }

        return $this->render('addCategory', ['model' => $model]);
    }

    /**
     * Действие для редактирования связи с дополнительной категорией
     *
     * @param integer $productId  идентификатор товара
     * @param integer $categoryId идентификатор категории
     *
     * @throws \Exception
     * @return string|\yii\web\Response
     */
    public function actionUpdateCategory($productId, $categoryId)
    {
        /** @var ShopProductCategory $model */
        $model = $this->findModel(['shop_product_id' => $productId, 'category_id' => $categoryId], ShopProductCategory::className());
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                try {
                    $model->save();
                    return $this->redirect(['view', 'id' => $productId]);
                } catch (Exception $e) {
                    if (defined('YII_DEBUG') && YII_DEBUG) {
                        throw $e;
                    }
                }
            }
        }

        return $this->render('updateCategory', ['model' => $model]);
    }

    /**
     * Действие для удаления связи с дополнительной категорией
     *
     * @param integer $productId идентификатор товара
     * @param integer $categoryId идентификатор категории
     *
     * @return string|\yii\web\Response
     */
    public function actionDeleteCategory($productId, $categoryId)
    {
        /** @var ShopProductCategory $model */
        $model = $this->findModel(['shop_product_id' => $productId, 'category_id' => $categoryId], ShopProductCategory::className());
        $model->delete();
        return $this->redirect(['view', 'id' => $productId]);
    }
}