<?php

namespace backend\controllers;

/**
 * Контроллер Тем
 *
 * @package backend\controllers
 */
class ThemeController extends Controller
{
    /** @inheritdoc */
    public $modelClass = 'common\models\Theme';
    /** @inheritdoc */
    public $viewPermission = 'backend.theme.read';
    /** @inheritdoc */
    public $updatePermission = 'backend.theme.edit';
    /** @inheritdoc */
    public $createPermission = 'backend.theme.edit';
    /** @inheritdoc */
    public $deletePermission = 'backend.theme.edit';
}