<?php

namespace backend\controllers;

/**
 * Контроллер новостей
 *
 * @package backend\controllers
 */
class NewsController extends Controller
{

    /** @inheritdoc */
    public $modelClass = 'common\models\News';
    /** @inheritdoc */
    public $viewPermission = 'backend.news.read';
    /** @inheritdoc */
    public $updatePermission = 'backend.news.edit';
    /** @inheritdoc */
    public $createPermission = 'backend.news.edit';
    /** @inheritdoc */
    public $deletePermission = 'backend.news.edit';

}