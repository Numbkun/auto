<?php

namespace backend\controllers;

/**
 * Контроллер статической страницы
 *
 * @package backend\controllers
 */
class StaticPageController extends Controller
{

    /** @inheritdoc */
    public $modelClass = 'common\models\StaticPage';
    /** @inheritdoc */
    public $viewPermission = 'backend.static-page.read';
    /** @inheritdoc */
    public $updatePermission = 'backend.static-page.edit';
    /** @inheritdoc */
    public $createPermission = 'backend.static-page.edit';
    /** @inheritdoc */
    public $deletePermission = 'backend.static-page.edit';

}