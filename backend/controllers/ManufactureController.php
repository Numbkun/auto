<?php

namespace backend\controllers;

use common\models\Manufacture;
use Yii;
use Exception;
/**
 * Контроллер Производителей товаров
 *
 * @package backend\controllers
 */
class ManufactureController extends Controller
{
    /** @inheritdoc */
    public $modelClass = 'common\models\Manufacture';
    /** @inheritdoc */
    public $viewPermission = 'backend.manufacture.read';
    /** @inheritdoc */
    public $updatePermission = 'backend.manufacture.edit';
    /** @inheritdoc */
    public $createPermission = 'backend.manufacture.edit';
    /** @inheritdoc */
    public $deletePermission = 'backend.manufacture.edit';

    /**
     * @inheritdoc
     */
    public function actionCreate()
    {
        /** @var Manufacture $model
         *  @var string $fileDirectory
         */
        $model = $this->createModel();
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                try {
                    $model->save();
                    return $this->redirect(['view', 'id' => $model->primaryKey]);
                } catch (Exception $e) {
                    if (defined('YII_DEBUG') && YII_DEBUG) {
                        throw $e;
                    }
                }
            }
        }

        return $this->render('create', ['model' => $model]);
    }

    /**
     * @inheritdoc
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                try {
                    $model->save();
                    return $this->redirect(['view', 'id' => $model->primaryKey]);
                } catch (Exception $e) {
                    if (defined('YII_DEBUG') && YII_DEBUG) {
                        throw $e;
                    }
                }
            }
        }

        return $this->render('update', ['model' => $model]);
    }
}