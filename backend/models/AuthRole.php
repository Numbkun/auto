<?php

namespace backend\models;

use backend\components\AuthItemQuery;
use Yii;

/**
 * Класс AuthRole
 *
 * @property AuthItem  $permissions
 */
class AuthRole extends AuthItem
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return AuthItem::tableName();
    }

    /**
     * Возвращает условия запроса для выбора привязанных разрешений
     *
     * @return AuthItemQuery
     */
    public function getPermissions()
    {
        $query = AuthItem::find()
            ->reset()
            ->where(['in', 'name', array_keys(Yii::$app->getAuthManager()->getPermissionsByRole($this->name))]);
        $query->multiple = true;
        return $query;
    }

    /**
     * @inheritdoc
     */
    public static function getSingularNominativeName()
    {
        return 'Роль';
    }

    /**
     * @inheritdoc
     */
    public static function getPluralNominativeName()
    {
        return 'Роли';
    }

    /**
     * Возвращает массив доступных операций
     *
     * @return AuthItem[]
     */
    public function getAvailablePermissions()
    {
        $query = AuthItem::find()->reset();
        if ($this->scenario != self::SCENARIO_SYSTEM) {
            $query->notSystem();
        }
        return $query->permission()->indexBy('name')->all();
    }

    /**
     * @inheritdoc
     */
    public function load($data, $formName = null)
    {
        if (parent::load($data, $formName)) {
            $scope = $formName === null ? $this->formName() : $formName;
            if ($scope && isset($data[$scope])) {
                $data = $data[$scope];
            }
            if (array_key_exists('permissions', $data)) {
                $permissions = [];
                if (is_array($data['permissions'])) {
                    $permissions = AuthItem::find()->reset()->where(['name' => $data['permissions']])->all();
                }
                $this->populateRelation('permissions', $permissions);
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    protected function insertInternal($attributes = null)
    {
        $auth = Yii::$app->getAuthManager();
        $role = $auth->createRole($this->name);
        $role->description = $this->description;
        $role->shop_id = $this->shop_id;
        $role->is_system = $this->is_system;
        if ($auth->add($role)) {
            $this->savePermissions(true);
            $this->setOldAttributes($this->attributes);
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    protected function updateInternal($attributes = null)
    {
        $auth = Yii::$app->getAuthManager();
        $role = $auth->getRole($this->getOldAttribute('name'));
        $role->name = $this->name;
        $role->description = $this->description;
        $role->shop_id = $this->shop_id;
        $role->is_system = $this->is_system;
        if ($auth->update($this->getOldAttribute('name'), $role)) {
            $this->savePermissions(false);
            $this->setOldAttributes($this->attributes);
            return true;
        }
        return false;
    }

    /**
     * Сохраняет ссылки роли на операции
     */
    protected function savePermissions($insert)
    {
        $auth = Yii::$app->getAuthManager();
        $role = $auth->getRole($this->name);
        $currentPermissions = $insert ? [] : $auth->getPermissionsByRole($this->name);
        $allowedPermissions = $this->getAvailablePermissions();
        foreach ($this->permissions as $permission) {
            if (isset($allowedPermissions[$permission->name])) {
                if (!isset($currentPermissions[$permission->name])) {
                    $auth->addChild($role, $auth->getPermission($permission->name));
                } else {
                    unset($currentPermissions[$permission->name]);
                }
            }
        }
        foreach ($currentPermissions as $permission) {
            $auth->removeChild($role, $auth->getPermission($permission->name));
        }
    }

    /**
     * @inheritdoc
     */
    protected function deleteInternal()
    {
        $auth = Yii::$app->getAuthManager();
        if ($auth->remove($auth->getRole($this->name))) {
            $this->setOldAttributes(null);
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     * @return AuthItemQuery
     */
    public static function find()
    {
        return parent::find()->role();
    }
}
