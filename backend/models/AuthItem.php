<?php

namespace backend\models;

use backend\components\AuthItemQuery;
use common\components\rbac\Item;
use common\models\ReferenceByShop;
use Yii;
use yii\base\NotSupportedException;

/**
 * Класс AuthItem
 *
 * @property integer   $type
 * @property string    $description
 * @property string    $rule_name
 * @property string    $data
 * @property boolean   $is_system
 */
class AuthItem extends ReferenceByShop
{
    /**
     * @inheritdoc
     */
    public static function primaryKey()
    {
        return ['name'];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return array_merge(
            parent::scenarios(),
            [
                self::SCENARIO_DEFAULT => ['name', 'description', '!type', '!is_system', '!shop_id'],
                self::SCENARIO_SYSTEM  => ['name', 'description', '!type', 'is_system', 'shop_id'],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['description'], 'required'],
                [['name', 'description'], 'trim'],
                [['name'], 'string', 'max' => 64],
                [['name'], 'unique'],
                [['is_system'], 'default', 'value' => false],
                [['is_system'], 'boolean'],
                [['type'], 'in', 'range' => [Item::TYPE_PERMISSION, Item::TYPE_ROLE], 'strict' => true]
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'name' => 'ID',
                'description' => 'Наименование',
                'is_system' => 'Системная роль'
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function getFieldsOptions()
    {
        if ($this->_fieldsOptions === []) {
            parent::getFieldsOptions();
            if (!$this->isAttributeSafe('is_system')) {
                $this->_fieldsOptions['is_system'] = ['type' => 'ignore'];
            }
            $this->_fieldsOptions['rule_name'] = ['type' => 'ignore'];
            $this->_fieldsOptions['data'] = ['type' => 'ignore'];
            $this->_fieldsOptions['type'] = ['type' => 'ignore'];
        }
        return $this->_fieldsOptions;
    }

    /**
     * @inheritdoc
     */
    public function __toString()
    {
        return $this->isNewRecord ? parent::__toString() : $this->description;
    }

    /**
     * @inheritdoc
     * @return AuthItemQuery
     */
    public static function find()
    {
        return Yii::createObject(AuthItemQuery::className(), [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function insert($runValidation = true, $attributes = null)
    {
        if (get_called_class() == AuthItem::className()) {
            throw new NotSupportedException('Модель нельзя использовать для добавления элементов в RBAC');
        }
        return parent::insert($runValidation, $attributes);
    }

    /**
     * @inheritdoc
     */
    public function update($runValidation = true, $attributes = null)
    {
        if (get_called_class() == AuthItem::className()) {
            throw new NotSupportedException('Модель нельзя использовать для изменения элементов в RBAC');
        }
        return parent::update($runValidation, $attributes);
    }

    /**
     * @inheritdoc
     */
    public function delete()
    {
        if (get_called_class() == AuthItem::className()) {
            throw new NotSupportedException('Модель нельзя использовать для удаления элементов из RBAC');
        }
        return parent::delete();
    }
}
