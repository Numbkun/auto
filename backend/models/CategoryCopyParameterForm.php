<?php
namespace backend\models;

use common\models\BaseForm;
use common\models\Category;
use common\models\CategoryParameter;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Модель формы для копирования параметров из одной категории в другую
 *
 * @property Category $sourceCategory исходная категория, из которой копируются характеристики
 * @property Category $destCategory   категория назнаяения, в которую копируются характеристики
 */
class CategoryCopyParameterForm extends BaseForm
{
    /**
     * @var integer идентификатор исходной категории
     */
    public $source_category_id;
    /**
     * @var integer идентификатор целевой категории
     */
    public $dest_category_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['source_category_id', 'dest_category_id'], 'required'],
            [['source_category_id', 'dest_category_id'], 'integer'],
            [
                ['source_category_id'],
                'compare',
                'skipOnError'      => true,
                'compareAttribute' => 'dest_category_id',
                'operator'         => '!='
            ],
            [
                ['source_category_id', 'dest_category_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Category::className(),
                'targetAttribute' => 'id'
            ],
        ];
    }

    /**
     * Возвращает условия для отбора категории исходной категории
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSourceCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'source_category_id']);
    }

    /**
     * Возвращает условия для отбора целевой категории
     * @return \yii\db\ActiveQuery
     */
    public function getDestCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'dest_category_id']);
    }

    /**
     * Копирование характеристик из исходной категории в целевую
     * @throws \Exception
     */
    public function copy()
    {
        $exists = ArrayHelper::map($this->destCategory->getCategoryParameters()->asArray()->all(), 'parameter_id', 'parameter_id');
        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach ($this->sourceCategory->getCategoryParameters()->asArray()->all() as $parameter) {
                if (!isset($exists[$parameter['parameter_id']])) {
                    $categoryParameter = new CategoryParameter();
                    $categoryParameter->category_id = $this->dest_category_id;
                    $categoryParameter->parameter_id = $parameter['parameter_id'];
                    $categoryParameter->save();
                }
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

}
