<?php
namespace backend\models;


use common\models\ReferenceByShop;
use Yii;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;

/**
 * Модель пользователя
 *
 * @property string     $auth_key      ключ для аутентификации
 * @property string     $password_hash хеш пароля
 * @property string     $email         E-mail пользователя
 * @property boolean    $is_active     активен пользователь или нет
 * @property AuthItem[] $roles         роли, привязанные к пользователю
 */
class User extends ReferenceByShop implements IdentityInterface
{
    /** Сценарий для установки пароля */
    const SCENARIO_PASSWORD = 'password';
    /** Сценарий для установки пароля и системных атрибутов */
    const SCENARIO_PASSWORD_SYSTEM = 'passwordSystem';

    /**
     * @var string пароль
     */
    public $password;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'auth_key'      => 'Ключ аутентификации',
                'password_hash' => 'Хеш пароля',
                'password'      => 'Пароль',
                'email'         => 'E-mail',
                'is_active'     => 'Активен',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return array_merge(
            parent::scenarios(),
            [
                self::SCENARIO_PASSWORD        => ['name', 'email', 'password', 'is_active', '!shop_id'],
                self::SCENARIO_PASSWORD_SYSTEM => ['name', 'email', 'password', 'is_active', 'shop_id'],
                self::SCENARIO_DEFAULT         => ['name', 'email', 'is_active', '!shop_id'],
                self::SCENARIO_SYSTEM          => ['name', 'email', 'is_active', 'shop_id'],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['email', 'password'], 'required'],
                [['is_active'], 'boolean'],
                [['email'], 'email'],
                [['name'], 'unique', 'targetAttribute' => ['name', 'shop_id']],
                [['email'], 'unique', 'targetAttribute' => ['email', 'shop_id']],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function getFieldsOptions()
    {
        if ($this->_fieldsOptions === []) {
            parent::getFieldsOptions();
            $this->_fieldsOptions['auth_key'] = ['type' => 'ignore'];
            $this->_fieldsOptions['password_hash'] = ['type' => 'ignore'];
            $this->_fieldsOptions['password'] = ['type' => 'password'];
        }
        return $this->_fieldsOptions;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Поиск пользователя по имени
     *
     * @param  string $name
     *
     * @return static|null
     */
    public static function findByName($name)
    {
        return static::findOne(['name' => $name, 'is_active' => true]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Проверка пароля
     *
     * @param  string $password пароль для проверки
     *
     * @return boolean
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
    }

    /**
     * Генерация ключа для функции "Запомнить меня"
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->getSecurity()->generateRandomString();
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            Yii::$app->authManager->revokeAll($this->primaryKey);
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->generateAuthKey();
            }
            if ($this->password) {
                $this->password_hash = Yii::$app->getSecurity()->generatePasswordHash($this->password);
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public static function getSingularNominativeName()
    {
        return 'Пользователь';
    }

    /**
     * @inheritdoc
     */
    public static function getPluralNominativeName()
    {
        return 'Пользователи';
    }

    /**
     * Возвращает условия запроса для выбора привязанных ролей
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRoles()
    {
        $query = AuthRole::find()->reset()->where(
            ['in', 'name', array_keys(Yii::$app->getAuthManager()->getRolesByUser($this->primaryKey))]
        );
        $query->multiple = true;
        return $query;
    }

    /**
     * @inheritdoc
     */
    public function load($data, $formName = null)
    {
        if (parent::load($data, $formName)) {
            $scope = $formName === null ? $this->formName() : $formName;
            if ($scope && isset($data[$scope])) {
                $data = $data[$scope];
            }
            if (array_key_exists('roles', $data)) {
                $roles = [];
                if (is_array($data['roles'])) {
                    foreach ($data['roles'] as $roleName) {
                        $roles[] = AuthItem::find()->reset()->where(['name' => $roleName])->one();
                    }
                }
                $this->populateRelation('roles', $roles);
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $auth = Yii::$app->getAuthManager();
        $currentRoles = $this->isNewRecord ? [] : $auth->getRolesByUser($this->primaryKey);
        $allowedRoles = $this->getAvailableRoles();
        foreach ($this->roles as $role) {
            if (isset($allowedRoles[$role->name])) {
                if (!isset($currentRoles[$role->name])) {
                    $auth->assign($auth->getRole($role->name), $this->primaryKey);
                } else {
                    unset($currentRoles[$role->name]);
                }
            }
        }
        foreach ($currentRoles as $role) {
            $auth->revoke($role, $this->primaryKey);
        }
    }

    /**
     * Возвращает массив доступных ролей
     *
     * @return AuthItem[]
     */
    public function getAvailableRoles()
    {
        $query = AuthItem::find();
        if ($this->scenario == self::SCENARIO_SYSTEM || $this->scenario == self::SCENARIO_PASSWORD_SYSTEM) {
            $query->reset();
        } else {
            $query->notSystem();
        }
        return $query->role()->indexBy('name')->all();
    }
}
