<?php
namespace backend\models;

use common\models\BaseForm;
use common\models\Manufacture;
use common\models\Order;
use common\models\OrderStatus;
use common\models\Product;
use Yii;
use yii\db\ActiveQuery;

/**
 * Форма для фильтра заказов
 * @property string  $paginate              кол-во записей на странице
 * @property string  $delivery_date_to      дата доставки ДО
 * @property string  $delivery_date_from    дата доставки ОТ
 * @property string  $order_date_from       дата заказа ОТ
 * @property string  $order_date_to         дата заказа ДО
 * @property integer $manufacture_id        идентификатор производителя
 * @property integer $status_id             идентификатор статуса
 * @property integer $order_id              идентификатор заказа
 * @property Order   $order                 модель заказа
 * @property OrderStatus   $status          модель статуса
 */
class FilterForm extends BaseForm
{
    public $manufacture_id = '';
    public $status_id = '';
    public $order_id = '';
    public $order_date_from = '';
    public $order_date_to = '';
    public $delivery_date_from = '';
    public $delivery_date_to = '';
    public $paginate = 50;


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'manufacture' => 'Производитель',
                'status' => 'Статус',
                'order' => 'Заказ',
                'order_date_from' => 'от',
                'order_date_to' => 'до',
                'delivery_date_from' => 'от',
                'delivery_date_to' => 'до',
                'paginate' => '',


            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['manufacture_id', 'status_id', 'order_id', 'paginate'], 'integer'],
                [['order_date_from', 'order_date_to', 'delivery_date_from', 'delivery_date_to'], 'date', 'format' => 'YYYY-MM-DD'],
                [
                    ['manufacture_id'],
                    'exist',
                    'skipOnError' => true,
                    'targetClass' => Manufacture::className(),
                    'targetAttribute' => 'id'
                ],
                [
                    ['status_id'],
                    'exist',
                    'skipOnError' => true,
                    'targetClass' => OrderStatus::className(),
                    'targetAttribute' => 'id'
                ],
                [
                    ['order_id'],
                    'exist',
                    'skipOnError' => true,
                    'targetClass' => Order::className(),
                    'targetAttribute' => 'id'
                ],
            ]
        );
    }


    /**
     * @inheritdoc
     */
    public static function getSingularNominativeName()
    {
        return 'Фильтер';
    }

    /**
     * @inheritdoc
     */
    public static function getPluralNominativeName()
    {
        return 'Фильтры';
    }

    /**
     * Возвращает статус заказа
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['id' => 'status_id']);
    }

    /**
     * Возвращает заказ
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * Возвращает производителя
     *
     * @return \yii\db\ActiveQuery
     */
    public function getManufacture()
    {
        return $this->hasOne(Manufacture::className(), ['id' => 'manufacture_id']);
    }

    public function getFieldsOptions()
    {
        if ($this->_fieldsOptions === []) {
            parent::getFieldsOptions();
            $this->_fieldsOptions['order_date_from'] = ['type' => 'date'];
            $this->_fieldsOptions['order_date_to'] = ['type' => 'date'];
            $this->_fieldsOptions['delivery_date_from'] = ['type' => 'date'];
            $this->_fieldsOptions['delivery_date_to'] = ['type' => 'date'];
            $this->_fieldsOptions['status_id'] = ['type' => 'reference'];
        }
        return $this->_fieldsOptions;
    }

    /**
     * @param  ActiveQuery $query
     * @return Order
     */
    public function applyFilter($query)
    {
        if ($this->manufacture_id){
            $query = $query->joinWith(['orderProducts.sku.product'])->andWhere([Product::tableName().'.manufacture_id' => $this->manufacture_id]);
        }
        if ($this->order_id){
            $query = $query->andWhere(['name' => $this->order->name]);
        }
        if ($this->status_id ){
            $query = $query->andWhere(['status_id' => $this->status_id]);
        }
        if ($this->delivery_date_from){
            $query = $query->andWhere(['>=','delivery_date', $this->delivery_date_from]);
        }
        if ($this->delivery_date_to){
            $query = $query->andWhere(['<=','delivery_date', $this->delivery_date_to]);
        }
        if ($this->order_date_from){
            $query = $query->andWhere(['>=','created_at', $this->order_date_from]);
        }
        if ($this->order_date_to){
            $query = $query->andWhere(['<=','created_at', $this->order_date_to]);
        }
        return $query;
    }
}
