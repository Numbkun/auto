<?php
/**
 * Created by IntelliJ IDEA.
 * User: aydin
 * Date: 11.08.2014
 * Time: 5:23
 */

namespace backend\widgets;

use common\models\ParameterAvailableValue;
use common\models\ParameterType;
use common\models\ParameterValue;
use kartik\field\FieldRange;
use kartik\widgets\Select2;
use yii\base\InvalidConfigException;
use yii\base\NotSupportedException;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/**
 * Виджет для вывода полей ввода для характеристик в зависимости от их типа
 *
 * @package backend\widgets
 */
class ParameterInput extends Widget
{

    /** @var ParameterValue модель значения характеристики */
    public $model;

    /** @var ActiveForm объект формы */
    public $form;

    /** @var null|integer индекс полей ввода для множественной загрузки моделей */
    public $tabularIndex = null;

    /** @var array массив соответствий типов характеристик способу ввода */
    protected $renderMap
        = [
            ParameterType::INTEGER => 'default',
            ParameterType::STRING  => 'default',
            ParameterType::BOOL    => 'boolean',
            ParameterType::RANGE   => 'range',
            ParameterType::SINGLE  => 'default',
            ParameterType::MULTIPE => 'multipe',
        ];

    /** @var array массив соответствий типов характеристик полям модели */
    public $fieldMap
        = [
            ParameterType::INTEGER => 'value_int',
            ParameterType::STRING  => 'value_string',
            ParameterType::BOOL    => 'value_bool',
            ParameterType::RANGE   => ['begin_range', 'end_range'],
            ParameterType::SINGLE  => 'list_value_id',
            ParameterType::MULTIPE => 'listValues',
        ];

    /** @var string шаблон вывода */
    public $template = "{input}";

    /** @var array массив частей для подстановки в шаблон */
    public $parts = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        if (!$this->model) {
            throw new InvalidConfigException('Пожалуйста укажите модель значения характеристики');
        }
        if (!$this->form) {
            throw new InvalidConfigException('Пожалуйста укажите объект формы');
        }
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $paramaterType = $this->model->parameter->parameter_type_id;
        if (!isset($this->renderMap[$paramaterType]) || !isset($this->fieldMap[$paramaterType])) {
            throw new NotSupportedException(
                'Тип переменной не поддерживается: ' . ParameterType::findOne($paramaterType)
            );
        }
        $render = 'render' . $this->renderMap[$paramaterType];
        $attributes = $this->fieldMap[$paramaterType];
        $this->parts['{input}'] = $this->$render($attributes);
        return strtr($this->template, $this->parts);
    }

    /**
     * Отображение по умолчанию полей ввода значений характеристик
     *
     * @param string $attributes
     *
     * @return \yii\widgets\ActiveField
     * @throws \yii\base\NotSupportedException
     */
    protected function renderDefault($attributes)
    {
        if (!is_string($attributes)) {
            throw new NotSupportedException('Формат атрибутов не верный');
        }
        $attributes = ($this->tabularIndex !== null ? "[{$this->tabularIndex}]" : '') . $attributes;
        return $this->form->field($this->model, $attributes);
    }


    /**
     * Вывод для ввода значения характеристики типа "Логическое"
     *
     * @param string $attributes
     *
     * @throws \yii\base\NotSupportedException
     * @return string
     */
    protected function renderBoolean($attributes)
    {
        if (!is_string($attributes)) {
            throw new NotSupportedException('Формат атрибутов не верный');
        }
        $attribute = ($this->tabularIndex !== null ? "[{$this->tabularIndex}]" : '') . $attributes;
        $field = $this->form->field(
            $this->model,
            $attribute,
            ['inputOptions' => ['label' => null, 'style' => 'margin-left:0']]
        );
        return $field;
    }

    /**
     * Вывод для ввода значения характеристики типа "Диапазон"
     *
     * @param array $attributes
     *
     * @throws \yii\base\NotSupportedException
     * @return string
     */
    protected function renderRange($attributes)
    {
        if (!is_array($attributes) || count($attributes) != 2) {
            throw new NotSupportedException('Формат атрибутов не верный');
        }
        return FieldRange::widget(
            [
                'form'       => $this->form,
                'model'      => $this->model,
                'attribute1' => ($this->tabularIndex !== null ? "[{$this->tabularIndex}]" : '') . array_shift(
                        $attributes
                    ),
                'attribute2' => ($this->tabularIndex !== null ? "[{$this->tabularIndex}]" : '') . array_shift(
                        $attributes
                    ),
                'useAddons' => false,
                'template' => '{widget}{error}'
            ]
        );
    }

    /**
     * Вывод для ввода значения характеристики типа "Перечисление с множественным выбором"
     *
     * @param $attributes
     *
     * @throws \yii\base\NotSupportedException
     * @return string
     */
    protected function renderMultipe($attributes)
    {
        if (!is_string($attributes)) {
            throw new NotSupportedException('Формат атрибутов не верный');
        }
        $attribute = ($this->tabularIndex !== null ? "[{$this->tabularIndex}]" : '') . 'listValues';
        $field = $this->form->field($this->model, $attribute);
        $data = ParameterAvailableValue::find()
            ->where(['parameter_id' => $this->model->parameter_id])->asArray()->all();
        $data = ArrayHelper::map($data, 'id', 'name');
        $field->widget(
            Select2::className(),
            [
                'data'      => $data,
                'model'     => $this->model,
                'attribute' => $attribute,
                'options'   => [
                    'placeholder' => 'Выберите значение ...',
                    'multiple'    => true
                ],
            ]
        );
        return $field;
    }
}