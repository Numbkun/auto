<?php
/**
 * Файл установки глобальных настроек Dependency Injection
 */

use common\widgets\ActiveField;

Yii::$container->set(\yii\widgets\ActiveForm::className(), ['fieldClass' => ActiveField::className()]);
Yii::$container->set(\kartik\widgets\ActiveForm::className(), ['fieldConfig' => ['class' => ActiveField::className()]]);