<?php
return [
    'adminEmail' => 'admin@example.com',
    'shop'       => [
        'default' => [
            'user' => [
                'name'     => 'admin',
                'password' => 'admin',
                'email'    => 'admin@example.com',
            ],
            'role' => [
                'name'        => '{shop}.administrator',
                'description' => '{shop}: Администратор',
            ]
        ]
    ],
];
