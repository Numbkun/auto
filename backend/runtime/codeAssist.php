<?php

use common\components\rbac\DbManager;
use common\components\Setting;
use yii\base\Application;
use yii\BaseYii;

/**
 * Класс приложения для работы подсказок IDE
 *
 * @property DbManager $authManager The authorization manager component
 * @property User|frontend\components\User $user Компонент пользователя. Свойство только для чтения
 * @property Setting $settings Компонент настроек. Свойство только для чтения
 * @property \common\components\Shop $shop Компонент магазина
 *
 * @method DbManager getAuthManager() getAuthManager() The authorization manager component
 * @method User|frontend\components\User getUser() getUser() Возвращает компонент пользователя
 */
abstract class CodeAssistApplication extends Application
{
}

/**
 * Класс компонента пользователя
 *
 * @property backend\models\User $identity Модель пользователя
 * @method backend\models\User getIdentity() getIdentity() Возвращает модель пользователя
 */
abstract class User extends yii\web\User
{

}

/**
 * Вспомогательный класс для работы подсказок IDE
 *
 */
class Yii extends BaseYii
{
    /**
     * @var \yii\console\Application|\yii\web\Application|CodeAssistApplication the application instance
     */
    public static $app;
}
