<?php

namespace backend\components;

use common\components\rbac\Item;
use common\components\ReferenceByShopQuery;
use Yii;

/**
 * Класс для работы с запросами к бд связанных с моделью AuthItem
 *
 * @package backend\components
 */
class AuthItemQuery extends ReferenceByShopQuery
{

    /**
     * Добавляет условия для отбора только ролей
     * @return static
     */
    public function role() {
        return $this->andWhere(['type' => Item::TYPE_ROLE]);
    }

    /**
     * Добавляет условия для отбора только операций
     * @return static
     */
    public function permission() {
        return $this->andWhere(['type' => Item::TYPE_PERMISSION]);
    }

    /**
     * Добавляет условия для отбора только не системных элементов
     * @return static
     */
    public function notSystem() {
        return $this->andWhere(['is_system' => false]);
    }

    public function excludeDefaultRoles() {
        return $this->andWhere(['not in', 'name', Yii::$app->getAuthManager()->defaultRoles]);
    }

}