<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '',
        ],
    ],
];

if (!YII_ENV_TEST) {

    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class'      => 'yii\debug\Module',
        // Раскомментируйте для установки IP
        // 'allowedIPs' => ['127.0.0.1']
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class'      => 'yii\gii\Module',
        // Раскомментируйте для установки IP
        // 'allowedIPs' => ['127.0.0.1']
    ];

    $config['components']['shop']['id'] = 1;

    // Раскомментируйте для настройки кеширования
    // $config['components']['db'] = [
    //     'enableSchemaCache'   => true,
    //     'schemaCacheDuration' => 300,
    // ];
    // $config['components']['cache'] = [
    //     'class'        => '\yii\caching\MemCache',
    //     'useMemcached' => true,
    //     'servers'      => [
    //         [
    //             'host' => '127.0.0.1',
    //         ],
    //     ],
    // ];
}

return $config;
