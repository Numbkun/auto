<?php

namespace tests\codeception\common\fixtures;

use yii\test\ActiveFixture;

/**
 * User fixture
 */
class ParameterGroupFixture extends ActiveFixture
{
    public $modelClass = 'common\models\ParameterGroup';
}
