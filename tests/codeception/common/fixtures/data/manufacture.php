<?php

use common\components\DateTime;
use common\components\PathFile;
use common\models\File;

$file = new File();
$file->uploadFile = PathFile::getInstance(__DIR__ . '/file/manufacture/1.png');
$file->path = 'manufacture';
$file->save();
$file1 = new File();
$file1->uploadFile = PathFile::getInstance(__DIR__ . '/file/manufacture/2.png');
$file1->path = 'manufacture';
$file1->save();
$file2 = new File();
$file2->uploadFile = PathFile::getInstance(__DIR__ . '/file/manufacture/3.png');
$file2->path = 'manufacture';
$file2->save();

return [
    [
        'name'        => 'Hugo Boss',
        'image_id'    =>  $file->primaryKey,
        'description' => 'Hugo Boss AG — немецкая компания-производитель модной одежды. Штаб-квартира — в городе Метцинген',
        'created_at'  => new DateTime(),
        'updated_at'  => new DateTime(),
    ],
    [
        'name'        => 'Glycine',
        'image_id'    =>  $file1->primaryKey,
        'description' => 'Glycine — марка швейцарских часов. Основана в 1914 году Юджином Мейланом с производством в городе Бьен (Швейцария).'
        ,'created_at'  => new DateTime(),
        'updated_at'  => new DateTime(),
    ],
    [
        'name'        => 'Armani Exchange',
        'image_id'    =>  $file2->primaryKey,
        'description' => 'Armani Exchange – дочерний проект итальянского Модного дома "Giorgio Armani S.P.A.". Занимается преимущественно производством продукции для молодежи.',
        'created_at'  => new DateTime(),
        'updated_at'  => new DateTime(),
    ],
];
