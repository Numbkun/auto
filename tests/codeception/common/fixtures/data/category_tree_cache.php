<?php

use common\components\DateTime;
use yii\helpers\Inflector;

return [
    1 => [
        'parent_category_id'       => 1,
        'child_category_id'        => 1,
    ],
    2 => [
        'parent_category_id'       => 1,
        'child_category_id'        => 2,
    ],
    3 => [
        'parent_category_id'       => 3,
        'child_category_id'        => 3,
    ],
    4 => [
        'parent_category_id'       => 3,
        'child_category_id'        => 4,
    ],
    5 => [
        'parent_category_id'       => 3,
        'child_category_id'        => 5,
    ],
    6 => [
        'parent_category_id'       => 2,
        'child_category_id'        => 2,
    ],
    7 => [
        'parent_category_id'       => 4,
        'child_category_id'        => 4,
    ],
    8 => [
        'parent_category_id'       => 5,
        'child_category_id'        => 5,
    ],
];
