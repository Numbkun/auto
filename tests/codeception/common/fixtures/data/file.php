<?php
use common\components\DateTime;

return [
    [
        'name' => 'no_image.png',
        'extension' => 'png',
        'path' => 'product',
        'created_at' => new DateTime(),
        'updated_at' => new DateTime(),
    ],
];