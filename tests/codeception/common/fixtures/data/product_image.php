<?php

use common\components\PathFile;
use common\models\File;

$file = new File();
$file->uploadFile = PathFile::getInstance(__DIR__ . '/file/product_image/2.jpg');
$file->path = 'product';
$file->save();
$file1 = new File();
$file1->uploadFile = PathFile::getInstance(__DIR__ . '/file/product_image/3.jpg');
$file1->path = 'product';
$file1->save();
$file2 = new File();
$file2->uploadFile = PathFile::getInstance(__DIR__ . '/file/product_image/4.jpg');
$file2->path = 'product';
$file2->save();
$file3 = new File();
$file3->uploadFile = PathFile::getInstance(__DIR__ . '/file/product_image/5.jpg');
$file3->path = 'product';
$file3->save();
$file4 = new File();
$file4->uploadFile = PathFile::getInstance(__DIR__ . '/file/product_image/6.jpg');
$file4->path = 'product';
$file4->save();
$file5 = new File();
$file5->uploadFile = PathFile::getInstance(__DIR__ . '/file/product_image/7.jpg');
$file5->path = 'product';
$file5->save();
$file6 = new File();
$file6->uploadFile = PathFile::getInstance(__DIR__ . '/file/product_image/8.jpg');
$file6->path = 'product';
$file6->save();
$file7 = new File();
$file7->uploadFile = PathFile::getInstance(__DIR__ . '/file/product_image/9.jpg');
$file7->path = 'product';
$file7->save();
$file8 = new File();
$file8->uploadFile = PathFile::getInstance(__DIR__ . '/file/product_image/10.jpg');
$file8->path = 'product';
$file8->save();
$file9 = new File();
$file9->uploadFile = PathFile::getInstance(__DIR__ . '/file/product_image/11.jpg');
$file9->path = 'product';
$file9->save();
$file10 = new File();
$file10->uploadFile = PathFile::getInstance(__DIR__ . '/file/product_image/12.jpg');
$file10->path = 'product';
$file10->save();
$file11 = new File();
$file11->uploadFile = PathFile::getInstance(__DIR__ . '/file/product_image/13.jpg');
$file11->path = 'product';
$file11->save();
$file12 = new File();
$file12->uploadFile = PathFile::getInstance(__DIR__ . '/file/product_image/14.jpg');
$file12->path = 'product';
$file12->save();
$file13 = new File();
$file13->uploadFile = PathFile::getInstance(__DIR__ . '/file/product_image/15.jpg');
$file13->path = 'product';
$file13->save();
$file14 = new File();
$file14->uploadFile = PathFile::getInstance(__DIR__ . '/file/product_image/16.jpg');
$file14->path = 'product';
$file14->save();
$file15 = new File();
$file15->uploadFile = PathFile::getInstance(__DIR__ . '/file/product_image/17.jpg');
$file15->path = 'product';
$file15->save();
$file16 = new File();
$file16->uploadFile = PathFile::getInstance(__DIR__ . '/file/product_image/18.jpg');
$file16->path = 'product';
$file16->save();

return [
    [
        'product_id' => 1,
        'file_id' => $file->primaryKey,
    ],
    [
        'product_id' => 2,
        'file_id' => $file1->primaryKey,
    ],
    [
        'product_id' => 4,
        'file_id' => $file2->primaryKey,
    ],
    [
        'product_id' => 5,
        'file_id' => $file3->primaryKey,
    ],
    [
        'product_id' => 6,
        'file_id' => $file4->primaryKey,
    ],
    [
        'product_id' => 7,
        'file_id' => $file5->primaryKey,
    ],
    [
        'product_id' => 8,
        'file_id' => $file6->primaryKey,
    ],
    [
        'product_id' => 9,
        'file_id' => $file7->primaryKey,
    ],
    [
        'product_id' => 10,
        'file_id' => $file8->primaryKey,
    ],
    [
        'product_id' => 11,
        'file_id' => $file9->primaryKey,
    ],
    [
        'product_id' => 12,
        'file_id' => $file10->primaryKey,
    ],
    [
        'product_id' => 13,
        'file_id' => $file11->primaryKey,
    ],
    [
        'product_id' => 14,
        'file_id' => $file12->primaryKey,
    ],
    [
        'product_id' => 15,
        'file_id' => $file13->primaryKey,
    ],
    [
        'product_id' => 16,
        'file_id' => $file14->primaryKey,
    ],
    [
        'product_id' => 17,
        'file_id' => $file15->primaryKey,
    ],
    [
        'product_id' => 18,
        'file_id' => $file16->primaryKey,
    ],
    [
        'product_id' => 1,
        'file_id' => $file2->primaryKey,
    ],
];