<?php

use common\components\DateTime;
use yii\helpers\Inflector;
use common\components\PathFile;
use common\models\File;

$file = new File();
$file->uploadFile = PathFile::getInstance(__DIR__ . '/file/dignity/1.png');
$file->path = 'dignity';
$file->save();

return [
    [
        'name'        => 'Удобно',
        'text'        => '<p>Описание достоинства.</p><p>несколько строк,</p><p>не больше 2х - 3х.</p>',
        'translite'   => Inflector::slug('Удобно', '_'),
        'shop_id'     => 1,
        'image_id'    => $file->primaryKey,
        'created_at'  => new DateTime(),
        'updated_at'  => new DateTime(),
    ],
    [
        'name'        => 'Стильно',
        'text'        => '<p>Описание достоинства.</p><p>несколько строк,</p><p>не больше 2х - 3х.</p>',
        'translite'   => Inflector::slug('Стильно', '_'),
        'shop_id'     => 1,
        'image_id'    => $file->primaryKey,
        'created_at'  => new DateTime(),
        'updated_at'  => new DateTime(),
    ],
    [
        'name'        => 'Модно',
        'text'        => '<p>Описание достоинства.</p><p>несколько строк,</p><p>не больше 2х - 3х.</p>',
        'translite'   => Inflector::slug('Модно', '_'),
        'shop_id'     => 1,
        'image_id'    => $file->primaryKey,
        'created_at'  => new DateTime(),
        'updated_at'  => new DateTime(),
    ],
    [
        'name'        => 'Молодежно',
        'text'        => '<p>Описание достоинства.</p><p>несколько строк,</p><p>не больше 2х - 3х.</p>',
        'translite'   => Inflector::slug('Молодежно', '_'),
        'shop_id'     => 1,
        'image_id'    => $file->primaryKey,
        'created_at'  => new DateTime(),
        'updated_at'  => new DateTime(),
    ],
];
