<?php

use common\components\DateTime;

return [
    [
        'name'       => 'Сумки',
        'created_at' => new DateTime(),
        'updated_at' => new DateTime(),
    ],
    [
        'name'       => 'Женские часы',
        'created_at' => new DateTime(),
        'updated_at' => new DateTime(),
    ],
    [
        'name'       => 'Мужские часы',
        'created_at' => new DateTime(),
        'updated_at' => new DateTime(),
    ],
];
