<?php

use common\components\PathFile;
use common\models\File;
use common\components\DateTime;
use yii\helpers\Url;

$file = new File();
$file->uploadFile = PathFile::getInstance(__DIR__ . '/file/banner/1.jpg');
$file->path = 'banner';
$file->save();
return [
    [
        'name' => 'Banner1',
        'url' => '/news',
        'shop_id' => 1,
        'image_id' => $file->primaryKey,
        'created_at' => new DateTime(),
        'updated_at' => new DateTime(),
    ],
];