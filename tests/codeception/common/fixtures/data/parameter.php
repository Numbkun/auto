<?php

use common\components\DateTime;
use common\models\ParameterType;

return [
    1 => [
        'name'               => 'Материал корпуса',
        'description'        => '',
        'parameter_type_id'  => ParameterType::STRING,
        'parameter_group_id' => 1,
        'created_at'         => new DateTime(),
        'updated_at'         => new DateTime(),
    ],
    2 => [
        'name'               => 'Цвет корпуса',
        'description'        => '',
        'parameter_type_id'  => ParameterType::STRING,
        'parameter_group_id' => 1,
        'created_at'         => new DateTime(),
        'updated_at'         => new DateTime(),
    ],
    3 => [
        'name'               => 'Механизм',
        'description'        => '',
        'parameter_type_id'  => ParameterType::STRING,
        'parameter_group_id' => 1,
        'created_at'         => new DateTime(),
        'updated_at'         => new DateTime(),
    ],
    4 => [
        'name'               => 'Материал браслета',
        'description'        => '',
        'parameter_type_id'  => ParameterType::STRING,
        'parameter_group_id' => 1,
        'created_at'         => new DateTime(),
        'updated_at'         => new DateTime(),
    ],
    5 => [
        'name'               => 'Цвет браслета',
        'description'        => '',
        'parameter_type_id'  => ParameterType::STRING,
        'parameter_group_id' => 2,
        'created_at'         => new DateTime(),
        'updated_at'         => new DateTime(),
    ],
    6 => [
        'name'               => 'Стекло',
        'description'        => '',
        'parameter_type_id'  => ParameterType::STRING,
        'parameter_group_id' => 2,
        'created_at'         => new DateTime(),
        'updated_at'         => new DateTime(),
    ],
    7 => [
        'name'               => 'Цвет циферблата',
        'description'        => '',
        'parameter_type_id'  => ParameterType::STRING,
        'parameter_group_id' => 2,
        'created_at'         => new DateTime(),
        'updated_at'         => new DateTime(),
    ],
    8 => [
        'name'               => 'Индикация',
        'description'        => '',
        'parameter_type_id'  => ParameterType::STRING,
        'parameter_group_id' => 2,
        'created_at'         => new DateTime(),
        'updated_at'         => new DateTime(),
    ],
    9 => [
        'name'               => 'Водозащита',
        'description'        => '',
        'parameter_type_id'  => ParameterType::STRING,
        'parameter_group_id' => 2,
        'created_at'         => new DateTime(),
        'updated_at'         => new DateTime(),
    ],
    10 => [
        'name'               => 'Форма корпуса',
        'description'        => '',
        'parameter_type_id'  => ParameterType::STRING,
        'parameter_group_id' => 2,
        'created_at'         => new DateTime(),
        'updated_at'         => new DateTime(),
    ],
];
