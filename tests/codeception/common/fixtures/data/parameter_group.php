<?php

use common\components\DateTime;

return [
    [
        'name'            => 'Размеры',
        'created_at'      => new DateTime(),
        'updated_at'      => new DateTime(),
    ],
    [
        'name'            => 'Размеры спального места',
        'created_at'      => new DateTime(),
        'updated_at'      => new DateTime(),
    ],
];
