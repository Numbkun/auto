<?php

use common\components\DateTime;
use yii\helpers\Inflector;

return [
    1 => [
        'name'        => 'Часы с кожанным ремешком',
        'shop_id'     => 1,
        'description' => '',
        'created_at'  => new DateTime(),
        'updated_at'  => new DateTime(),
        'translite'   => Inflector::slug('Часы с кожанным ремешком', '_'),
    ],
    2 => [
        'name'        => 'Ремешки кожанные для часов',
        'shop_id'     => 1,
        'description' => '',
        'created_at'  => new DateTime(),
        'updated_at'  => new DateTime(),
        'translite'   => Inflector::slug('Ремешки кожанные для часов', '_'),
    ],
    3 => [
        'name'        => 'Сумки кожаные',
        'shop_id'     => 1,
        'description' => '',
        'created_at'  => new DateTime(),
        'updated_at'  => new DateTime(),
        'translite'   => Inflector::slug('Сумки кожаные', '_'),
    ],
    4 => [
        'name'        => 'Галантерейные изделия',
        'shop_id'     => 1,
        'description' => '',
        'created_at'  => new DateTime(),
        'updated_at'  => new DateTime(),
        'translite'   => Inflector::slug('Галантерейные изделия', '_'),
    ],
    5 => [
        'name'        => 'Чехлы',
        'shop_id'     => 1,
        'description' => '',
        'created_at'  => new DateTime(),
        'updated_at'  => new DateTime(),
        'translite'   => Inflector::slug('Чехлы', '_'),
    ],
    6 => [
        'name'        => 'Акксессуары',
        'shop_id'     => 1,
        'description' => '',
        'created_at'  => new DateTime(),
        'updated_at'  => new DateTime(),
        'translite'   => Inflector::slug('Акксессуары', '_'),
    ],
    7 => [
        'name'        => 'Коллекции',
        'shop_id'     => 1,
        'description' => '',
        'created_at'  => new DateTime(),
        'updated_at'  => new DateTime(),
        'translite'   => Inflector::slug('Коллекции', '_'),
    ],
    8 => [
        'name'        => 'Спецпредложения и акции',
        'shop_id'     => 1,
        'description' => '',
        'created_at'  => new DateTime(),
        'updated_at'  => new DateTime(),
        'translite'   => Inflector::slug('Спецпредложения и акции', '_'),
    ],
];
