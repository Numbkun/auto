<?php

use common\components\DateTime;
use common\components\PathFile;
use common\models\File;

$file = new File();
$file->uploadFile = PathFile::getInstance(__DIR__ . '/file/static_page/5.jpg');
$file->path = 'static';
$file->save();
$file1 = new File();
$file1->uploadFile = PathFile::getInstance(__DIR__ . '/file/static_page/6.jpg');
$file1->path = 'static';
$file1->save();

return [
    [
        'name' => 'О компании',
        'description' => 'О компании',
        'text' => 'Агентство по страхованию вкладов (АСВ) предоставит за счет кредита ЦБ финансовую помощь "Трасту" в виде займа в размере до 99 млрд руб. сроком на 10 лет "на покрытие дисбаланса между справедливой стоимостью активов и балансовой стоимостью обязательств банка".',
        'translite' => 'about',
        'shop_id' => 1,
        'created_at' => new DateTime(),
        'updated_at' => new DateTime(),
    ],
    [
        'name' => 'Контакты',
        'description' => 'Контакты',
        'text' => 'Для частных пользователей повышение цен на программное обеспечение составляет от 15 процентов, в то время как рост цен для коммерческих заказчиков может достигать 30 процентов.',
        'translite' => 'kontakty',
        'shop_id' => 1,
        'created_at' => new DateTime(),
        'updated_at' => new DateTime(),
    ],
    [
        'name' => 'Доставка и оплата',
        'description' => 'Уважаемые покупатели! В этом разделе Вы найдете подробную информацию о доставке заказанного Вами товара.',
        'text' => '<h1><strong>Доставка по Москве и области</strong></h1>

<p>Мы предлагаем множество изделий разных производителей. У изделий различных производителей цены на доставку могут различаться.</p>

<p>Ознакомиться с ними более подробно можно в карточке понравившегося Вам товара (<em>см. изображение</em>).</p>

<p><img alt="" src="/assets/static/'. $file->primaryKey .'.jpg" style="float:left; height:387px; width:500px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Для этого необходимо навести стрелочку мышки на верхний правый угол, точнее &ndash; на ссылку &laquo;Подробнее&raquo;. Вам откроется небольшой список, в котором указана конкретная стоимость услуги. Мы можем доставить:</p>

<ul>
	<li><strong>Матрасы &ndash; стоимость доставки от бесплатной до 600 руб в зависимости от производителя</strong>. При этом подъем будет абсолютно бесплатным!</li>
	<li><strong>Аксессуары для сна &ndash; стоимость доставки от бесплатной до 600 руб в зависимости от &nbsp;производителя.</strong> В эту категорию входят наматрасники, топперы и чехлы</li>
	<li><strong>Мягкую и корпусную мебель &ndash; стоимость доставки до 1000 руб.</strong> В большинстве случаев выбранные изделия доставляются в разобранном виде. Мы готовы собрать предметы обстановки в кратчайшие сроки после доставки!</li>
</ul>

<h2>Доставка в регионы</h2>

<p>Если Вы проживаете за пределами Москвы и Московской области, наш специалист поможет рассчитать конечную стоимость доставки самостоятельно &ndash; для этого необходимо лишь назвать свой город. Также мы занимаемся подбором транспортных компаний для быстрой отправки заказа в регионы. В этом случае стоимость доставки заказа через транспортную компанию будет оплачиваться Вами отдельно при получении груза в Вашем городе. Если Вы хотите забрать товар самостоятельно, пожалуйста, уточните такую возможность у наших консультантов.</p>

<h2>Предоставляемые услуги</h2>

<p>На некоторые мебельные изделия из нашего ассортимента осуществляется платный подъем. Для этого можно заказать у консультанта услуги по подъему и сборке (при необходимости). Стоимость наших работ есть в карточке товара (<em>см. изображение</em>).&nbsp;</p>

<p>&nbsp;<img alt="" src="/assets/static/'. $file1->primaryKey .'.jpg" style="float:left; height:255px; width:500px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><span style="color:#e27c7c">Заказывайте товары для приятного и здорового сна в интернет-магазине ВсеДляСна.ру, а мы позаботимся о том, чтобы доставка приятно порадовала Вас!</span></p>
',
        'translite' => 'dostavka_i_oplata',
        'shop_id' => 1,
        'created_at' => new DateTime(),
        'updated_at' => new DateTime(),
    ],
];
