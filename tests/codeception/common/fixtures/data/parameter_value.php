<?php

return [
    [
        'product_id'   => '1',
        'parameter_id' => '1',
        'value_string'  => 'Серебро',
    ],
    [
        'product_id'   => '1',
        'parameter_id' => '2',
        'value_string'  => 'Серебристый',
    ],
    [
        'product_id'   => '1',
        'parameter_id' => '3',
        'value_string'  => 'Механический',
    ],
    [
        'product_id'   => '1',
        'parameter_id' => '4',
        'value_string'  => 'Сталь',
    ],
    [
        'product_id'   => '1',
        'parameter_id' => '5',
        'value_string'  => 'Белый',
    ],
    [
        'product_id'   => '1',
        'parameter_id' => '6',
        'value_string'  => 'Минеральное',
    ],
    [
        'product_id'   => '1',
        'parameter_id' => '7',
        'value_string'  => 'Белый',
    ],
    [
        'product_id'   => '1',
        'parameter_id' => '8',
        'value_string'  => 'Римские цифры',
    ],
    [
        'product_id'   => '1',
        'parameter_id' => '9',
        'value_string'  => 'Плавание',
    ],
    [
        'product_id'   => '1',
        'parameter_id' => '10',
        'value_string'  => 'Прямоугольный',
    ],
];
