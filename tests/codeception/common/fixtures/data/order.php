<?php

use common\components\DateTime;

return [
    [
        'name'            => '1-1',
        'shop_id'         => 1,
        'customer_id'     => rand(1, 10),
        'currency_id'     => 1,
        'price'           => 15000,
        'created_at'      => new DateTime(),
        'updated_at'      => new DateTime(),
    ],
    [
        'name'            => '1-2',
        'shop_id'         => 1,
        'customer_id'     => rand(1, 10),
        'currency_id'     => 1,
        'price'           => 25000,
        'created_at'      => new DateTime(),
        'updated_at'      => new DateTime(),
    ],
];
