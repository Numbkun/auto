<?php

use common\components\DateTime;
use Faker\Factory;


$faker = Factory::create('ru_RU');
$passwordHash = Yii::$app->getSecurity()->generatePasswordHash('1111');

$result = [];
for ($i = 0; $i < 10; $i++) {
    $firstName = $faker->firstName('male');
    $lastName = $faker->lastName;
    $patronymic = '';
    $result[] = [
        'shop_id'       => 1,
        'name'          => $lastName . ' ' . $firstName . ' ' . $patronymic,
        'first_name'    => $firstName,
        'last_name'     => $lastName,
        'patronymic'    => $patronymic,
        'email'         => $faker->email,
        'mobile'        => $faker->numerify('920#######'),
        'auth_key'      => Yii::$app->getSecurity()->generateRandomString(),
        'password_hash' => $passwordHash,
        'is_active'     => true,
        'created_at'    => new DateTime(),
        'updated_at'    => new DateTime(),
    ];
}
return $result;
