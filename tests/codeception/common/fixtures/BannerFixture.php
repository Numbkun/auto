<?php

namespace tests\codeception\common\fixtures;

use yii\test\ActiveFixture;

/**
 * User fixture
 */
class BannerFixture extends ActiveFixture
{
    public $modelClass = 'common\models\Banner';
    public $depends = [
        'tests\codeception\common\fixtures\FileFixture',
    ];
}
