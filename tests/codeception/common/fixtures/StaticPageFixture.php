<?php

namespace tests\codeception\common\fixtures;

use yii\test\ActiveFixture;

/**
 * User fixture
 */
class StaticPageFixture extends ActiveFixture
{
    public $modelClass = 'common\models\StaticPage';
    public $depends = [
        'tests\codeception\common\fixtures\FileFixture',
    ];
}
