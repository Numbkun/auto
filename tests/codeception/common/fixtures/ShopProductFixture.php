<?php

namespace tests\codeception\common\fixtures;

use yii\test\ActiveFixture;

/**
 * User fixture
 */
class ShopProductFixture extends ActiveFixture
{
    public $modelClass = 'common\models\ShopProduct';

}
