<?php

namespace tests\codeception\common\fixtures;

use yii\test\ActiveFixture;

/**
 * User fixture
 */
class SkuFixture extends ActiveFixture
{
    public $modelClass = 'common\models\Sku';
    public $depends = [
        'tests\codeception\common\fixtures\ProductFixture',
    ];
}
