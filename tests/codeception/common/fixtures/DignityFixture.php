<?php

namespace tests\codeception\common\fixtures;

use yii\test\ActiveFixture;

/**
 * User fixture
 */
class DignityFixture extends ActiveFixture
{
    public $modelClass = 'common\models\Dignity';
    public $depends = [
        'tests\codeception\common\fixtures\FileFixture',
    ];
}
