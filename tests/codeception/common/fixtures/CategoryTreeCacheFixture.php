<?php

namespace tests\codeception\common\fixtures;

use yii\test\ActiveFixture;

/**
 * User fixture
 */
class CategoryTreeCacheFixture extends ActiveFixture
{
    public $modelClass = 'common\models\CategoryTreeCache';
    public $depends = [
        'tests\codeception\common\fixtures\CategoryFixture',
    ];
}
