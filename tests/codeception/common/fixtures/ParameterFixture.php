<?php

namespace tests\codeception\common\fixtures;

use yii\test\ActiveFixture;

/**
 * User fixture
 */
class ParameterFixture extends ActiveFixture
{
    public $modelClass = 'common\models\Parameter';
    public $depends = [
        'tests\codeception\common\fixtures\ParameterGroupFixture',
    ];
}
