<?php

namespace tests\codeception\common\fixtures;

use yii\test\ActiveFixture;

/**
 * User fixture
 */
class ParameterValueFixture extends ActiveFixture
{
    public $modelClass = 'common\models\ParameterValue';
    public $depends = [
        'tests\codeception\common\fixtures\ParameterFixture',
        'tests\codeception\common\fixtures\ProductFixture',
    ];
}
