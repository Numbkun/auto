<?php

namespace tests\codeception\common\fixtures;

use yii\test\ActiveFixture;

/**
 * User fixture
 */
class FileFixture extends ActiveFixture
{
    public $modelClass = 'common\models\File';
}
