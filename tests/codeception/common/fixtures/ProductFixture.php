<?php

namespace tests\codeception\common\fixtures;

use yii\test\ActiveFixture;

/**
 * User fixture
 */
class ProductFixture extends ActiveFixture
{
    public $modelClass = 'common\models\Product';
    public $depends = [
        'tests\codeception\common\fixtures\ManufactureFixture',
        'tests\codeception\common\fixtures\ProductTypeFixture',
        'tests\codeception\common\fixtures\FileFixture',
    ];
}
