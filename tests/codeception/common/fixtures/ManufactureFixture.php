<?php

namespace tests\codeception\common\fixtures;

use yii\test\ActiveFixture;

/**
 * User fixture
 */
class ManufactureFixture extends ActiveFixture
{
    public $modelClass = 'common\models\Manufacture';
    public $depends = [
        'tests\codeception\common\fixtures\FileFixture',
    ];
}
