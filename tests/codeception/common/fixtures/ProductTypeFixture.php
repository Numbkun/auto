<?php

namespace tests\codeception\common\fixtures;

use yii\test\ActiveFixture;

/**
 * User fixture
 */
class ProductTypeFixture extends ActiveFixture
{
    public $modelClass = 'common\models\ProductType';
}
