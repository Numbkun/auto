<?php

namespace tests\codeception\common\fixtures;

use yii\test\ActiveFixture;

/**
 * User fixture
 */
class ComplementaryProductFixture extends ActiveFixture
{
    public $modelClass = 'common\models\ComplementaryProduct';
    public $depends = [
        'tests\codeception\common\fixtures\ProductFixture',
    ];
}
