<?php

namespace tests\codeception\common\fixtures;

use yii\test\ActiveFixture;

/**
 * User fixture
 */
class ProductImageFixture extends ActiveFixture
{
    public $modelClass = 'common\models\ProductImage';
    public $depends = [
        'tests\codeception\common\fixtures\FileFixture',
    ];
}
