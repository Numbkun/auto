<?php

namespace tests\codeception\common\fixtures;

use yii\test\ActiveFixture;

/**
 * Фикстура файлов
 */
class NewsFixture extends ActiveFixture
{
    public $modelClass = 'common\models\News';
}
