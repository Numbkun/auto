<?php

namespace tests\codeception\common\fixtures;

use yii\test\ActiveFixture;

/**
 * User fixture
 */
class SkuParameterPriceFixture extends ActiveFixture
{
    public $modelClass = 'common\models\SkuParameterPrice';
    public $depends = [
        'tests\codeception\common\fixtures\SkuFixture',
    ];
}
