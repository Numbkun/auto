<?php

namespace tests\codeception\common\fixtures;

use yii\test\ActiveFixture;

/**
 * User fixture
 */
class CustomerFixture extends ActiveFixture
{
    public $modelClass = 'common\models\Customer';
}
